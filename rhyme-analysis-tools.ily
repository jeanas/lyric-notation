%{ \version "2.22.0" %}

%%% RHYME ANALYSIS FUNCTIONS
%% HorizontalBracket SETTINGS
bracketSettings = {
  % Bracket padding
  %{ \override Score.HorizontalBracket.padding = #0.5 %}
  %{ \override Score.HorizontalBracket.staff-padding = #0.5 %}
  %{ \override Score.HorizontalBracket.outside-staff-padding = #0.5 %}

  % Bracket Engraving
  \override Score.HorizontalBracket.direction = #DOWN
  \override Score.HorizontalBracket.layer = #4
  %{ \override Score.HorizontalBracket.outside-staff-priority = #1150 %}
  %{ \override Score.HorizontalBracket.avoid-scripts = ##t %}

  % Bracket Shape
  \override Score.HorizontalBracket.shorten-pair = #'(-0.5 . -0.5)
  \override Score.HorizontalBracket.bracket-flare = #'(0.5 . 0.5)
  %{ \override Score.HorizontalBracket.connect-to-neighbor = #'(#t . #t) %}
  \override Score.HorizontalBracket.edge-height = #'(0.5 . 0.5)

  %%% BRACKET TEXT
  \override Score.HorizontalBracketText.self-alignment-X = #left

  %% Look
  \override Score.HorizontalBracketText.font-size = #-3
  \override Score.HorizontalBracketText.font-series = #'bold
  \override Score.HorizontalBracketText.font-shape = #'upright
  %{ \override Score.HorizontalBracketText.font-family = #'serif %}
  \override Score.HorizontalBracketText.whiteout-style = #'box
  \override Score.HorizontalBracketText.whiteout = ##t

  %% Location
  %{ \override Score.HorizontalBracketText.direction = #-1 %}
  %{ \override Score.HorizontalBracketText.padding = #0.2 %}
  %{ \override Score.HorizontalBracketText.staff-padding = #0.2 %}
  %{ \override Score.HorizontalBracketText.outside-staff-padding = #0.2 %}
 }

oneLineBracketSettings = {
  % Bracket padding
  \override Score.HorizontalBracket.padding = #0.1
  %{ \override Score.HorizontalBracket.staff-padding = #0.5 %}
  %{ \override Score.HorizontalBracket.outside-staff-padding = #0.5 %}

  % Bracket Engraving
  \override Score.HorizontalBracket.direction = #DOWN
  \override Score.HorizontalBracket.layer = #5
  %{ \override Score.HorizontalBracket.outside-staff-priority = #1150 %}
  \override Score.HorizontalBracket.avoid-scripts = ##t
  \override Score.HorizontalBracket.thickness = #0.5

  % Bracket Shape
  \override Score.HorizontalBracket.shorten-pair = #'(-0.5 . -0.5)
  \override Score.HorizontalBracket.bracket-flare = #'(0.25 . 0.25)
  %{ \override Score.HorizontalBracket.connect-to-neighbor = #'(#t . #t) %}
  \override Score.HorizontalBracket.edge-height = #'(0.25 . 0.25)

  %%% BRACKET TEXT
  %% Look
  \override Score.HorizontalBracketText.font-size = #-4.25
  %{ \override Score.HorizontalBracketText.font-series = #'bold %}
  \override Score.HorizontalBracketText.font-shape = #'upright
  %{ \override Score.HorizontalBracketText.font-family = #'serif %}
  \override Score.HorizontalBracketText.whiteout-style = #'outline
  \override Score.HorizontalBracketText.whiteout = ##t

  %% Location
  %{ \override Score.HorizontalBracketText.direction = #-1 %}
  %{ \override Score.HorizontalBracketText.padding = #0.2 %}
  %{ \override Score.HorizontalBracketText.staff-padding = #0.2 %}
  %{ \override Score.HorizontalBracketText.outside-staff-padding = #0.2 %}
 }

% ONCE Places bracket labels before line breaks
onceBracketTextBeforeBreak = {
    \once \override Score.HorizontalBracketText.after-line-breaking =
    #(lambda (grob)
       (if (not-first-broken-spanner? grob)
           (ly:grob-set-property! grob 'stencil '())))
         }
% ONCE Places bracket labels after line breaks
onceBracketTextAfterBreak = {
    \once \override Score.HorizontalBracketText.after-line-breaking =
    #(lambda (grob)
       (if (first-broken-spanner? grob)
           (ly:grob-set-property! grob 'stencil '())))
         }

% Places bracket labels before line breaks
bracketTextBeforeBreak = {
    \override Score.HorizontalBracketText.after-line-breaking =
    #(lambda (grob)
       (if (not-first-broken-spanner? grob)
           (ly:grob-set-property! grob 'stencil '())))
         }
% Places bracket labels after line breaks
bracketTextAfterBreak = {
    \override Score.HorizontalBracketText.after-line-breaking =
    #(lambda (grob)
       (if (first-broken-spanner? grob)
           (ly:grob-set-property! grob 'stencil '())))
         }
%%% Remove parens from courtesy bracket LABELS

%{ \layout {
  \context {
    \Voice
     \consists Horizontal_bracket_engraver
     \override HorizontalBracketText.stencil =
       #(lambda (grob)
          (let ((text (ly:grob-property grob 'text)))
            (if (markup? text)
                (grob-interpret-markup grob text)
                (ly:grob-suicide! grob))))}} %}
% Basic, unlabeled Bracket
bB =
  #(define-music-function
   (note)
   (ly:music?)
  #{
    #note
    -\tweak HorizontalBracket.shorten-pair #'(-1.5 . -1.5)
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.direction #DOWN %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    -\tweak HorizontalBracket.staff-padding #1.5
    %{ -\tweak HorizontalBracket.outside-staff-padding #0 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ -\tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.5 . 0.5) %}
    \startGroup
    #})
% overlapping brackets
oB =
  #(define-music-function
   (note)
   (ly:music?)
  #{
    #note
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.direction #DOWN %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #'() %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #0 %}
    -\tweak HorizontalBracket.shorten-pair #'(-8.5 . -1.5)
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ \tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.5 . 0.5) %}
    \startGroup
    #})
% Asymmetrical overlapping bracket pair
% vennBracketB reaches backe to includee
% the last note of vennBracketA
vennBracketA =
  #(define-music-function
   (note)
   (ly:music?)
  #{
    #note
    -\tweak HorizontalBracket.shorten-pair #'(-1.5 . -3.75)
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.direction #DOWN %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #2 %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #0 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ \tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.5 . 0.5) %}
    \startGroup
    #})
vennBracketB =
  #(define-music-function
   (note)
   (ly:music?)
  #{
    #note
    -\tweak HorizontalBracket.shorten-pair #'(-0.5 . -1)
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.direction #DOWN %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #2 %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #0 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ \tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.5 . 0.5) %}
    \startGroup
    #})

% end bracket
eB =
  #(define-music-function
   (note)
   (ly:music?)
  #{
    #note
    \stopGroup
    #})
eb =
  #(define-music-function
   (note)
   (ly:music?)
  #{
    #note
    \stopGroup
    #})
bigTup =
    \tweak TupletNumber.font-size #-3
    \etc
smallTup =
    \tweak TupletNumber.font-size #-4.25
    \etc
boldTup =
    \tweak TupletNumber.font-series #'bold
    \tweak TupletNumber.font-shape #'upright
    \etc
whiteboxTup =
    \tweak TupletNumber.whiteout-style #'rounded-box
    \tweak TupletNumber.whiteout ##t
    \etc
whiteoutTup =
    \tweak TupletNumber.whiteout-style #'outline
    \tweak TupletNumber.whiteout ##t
    \etc
lowerTup =
    \tweak TupletNumber.extra-offset #'(-1.5 . -3)
    \etc%%% DOMAIN LABEL TUPLET CREATION FUNCTIONS
  % N.B it makes sense to put these with the lyrics
  % but sometimes the individual domains need to go above
downTup =
  \tweak TupletBracket.direction #DOWN
  \tuplet 1/1 \etc
upTup =
  \tweak TupletBracket.direction #UP
  \tuplet 1/1 \etc
domTup =
  %{ \tweak TupletBracket.stencil ##f %}
  %{ \tweak TupletNumber.stencil ##t %}
  \tuplet 1/1 \etc
%%% DOMAIN LABEL TUPLET FORMAT FUNCTIONS
 % comes before \domTup
domTupLabel =
  #(define-music-function
   (string tuplet)
   (string? ly:music?)
   #{
    %{ #note %}
    \tweak TupletBracket.self-alignment-X #CENTER
    \tweak TupletNumber.self-alignment-X #CENTER
    %{ \tweak TupletBracket.shorten-pair #'(-0.5 . -0.5) %}
    %{ \tweak TupletNumber.font-size #-4.5 %}
    %{ \tweak TupletNumber.font-series #'bold %}
    %{ \tweak TupletNumber.font-shape #'upright %}
    \tweak TupletNumber.layer #3
    %{ \tweak TupletNumber.whiteout-style #'outline %}
    %{ \tweak TupletNumber.whiteout ##t %}
    %{ \tweak TupletNumber.side-axis #0 %}
    %{ \tweak TupletNumber.direction #0 %}
    %{ \tweak TupletNumber.padding #2 %}
    %{ \tweak TupletNumber.X-offset #0.-25 %}
    %{ \tweak TupletNumber.staff-padding #-4 %}
    %{ \tweak TupletNumber.knee-to-beam ##f %}
    %{ \tweak TupletNumber.outside-staff-padding #2 %}
    %{ \tweak TupletNumber.springs-and-rods ##f %}
    %{ \tweak TupletBracket.direction #DOWN %}
    \tweak TupletNumber.padding #0.1
    \tweak TupletNumber.staff-padding #0.1
    \tweak TupletNumber.outside-staff-padding #0.1
    \tweak TupletNumber.avoid-scripts ##t
    \tweak TupletBracket.avoid-scripts ##t
    %{ \tweak TupletBracket.stencil ##f %}
    \tweak TupletBracket.transparent ##t
    %{ \tweak TupletBracket.outside-staff-priority #1100 %}
    %{ \tweak TupletBracket.staff-padding #'() %}
    %{ \tweak TupletBracket.outside-staff-padding #5 %}
    %{ \tweak TupletBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ \tweak TupletBracket.connect-to-neighbor #'(#f . #f) %}
    %{ \tweak TupletBracket.edge-height #'(0.6 . 0.6) %}
    %{ \tweak TupletBracket.padding #3 %}
    \tweak TupletNumber.extra-offset #'(-1.5 . -2)
    \tweak TupletNumber.text #string
    #tuplet
    #})
domTupLabelOL =
      #(define-music-function
       (string tuplet)
       (string? ly:music?)
   #{
    %{ \tweak TupletNumber.springs-and-rods ##f %}
    %{ \once \override TupletBracket.self-alignment-X = #CENTER %}
    %{ \once \override TupletNumber.self-alignment-X = #CENTER %}
    %{ \tweak TupletBracket.shorten-pair #'(-0.5 . -0.5) %}
    %{ \tweak TupletNumber.font-size #-4.5 %}
    %{ \tweak TupletNumber.font-series #'bold %}
    %{ \tweak TupletNumber.font-shape #'upright %}
    %{ \tweak TupletNumber.layer #4 %}
    %{ \tweak TupletNumber.whiteout-style #'outline %}
    %{ \tweak TupletNumber.whiteout ##t %}
    %{ \tweak TupletNumber.side-axis #0 %}
    %{ \tweak TupletNumber.direction #0 %}
    %{ \tweak TupletNumber.X-offset #0.-25 %}
    \tweak TupletNumber.padding #0.1
    \tweak TupletNumber.staff-padding #0.1
    \tweak TupletNumber.outside-staff-padding #0.1
    \tweak TupletNumber.avoid-scripts = ##t
    %{ \tweak TupletBracket.direction #DOWN %}
    \tweak TupletBracket.stencil ##f
    %{ \tweak TupletBracket.tuplet-slur ##f %}
    %{ \tweak TupletBracket.outside-staff-priority #1100 %}
    \tweak TupletBracket.padding #0
    \tweak TupletBracket.staff-padding #0
    \tweak TupletBracket.outside-staff-padding #0
    %{ \tweak TupletBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ \tweak TupletBracket.connect-to-neighbor #'(#f . #f) %}
    %{ \tweak TupletBracket.edge-height #'(0.6 . 0.6) %}
    \tweak TupletNumber.text #string
    #tuplet
    #})
domTupLabelAlt =
  #(define-music-function
   (string tuplet)
   (string? ly:music?)
  #{
    %{ #note %}
    %{ -\tweak HorizontalBracket.shorten-pair #'(-1 . -1) %}
    %{ -\tweak HorizontalBracketText.font-size #-3.75 %}
    %{ -\tweak HorizontalBracketText.font-series #'bold %}
    %{ -\tweak HorizontalBracketText.layer #4 %}
    %{ -\tweak HorizontalBracketText.whiteout-style #'outline %}
    %{ -\tweak HorizontalBracketText.whiteout ##t %}
    %{ -\tweak HorizontalBracketText.direction #-1 %}
    %{ -\tweak HorizontalBracketText.padding #0.2 %}
    %{ -\tweak HorizontalBracketText.padding #'() %}
    %{ -\tweak HorizontalBracketText.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.staff-padding #'() %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #'() %}
    %{ -\tweak HorizontalBracket.direction #DOWN %}
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #2 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ -\tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.6 . 0.6) %}
    \tweak TupletNumber.text #string
    %{ \tb %}
    #tuplet
    #})

%%% CHAIN/MULTI HorizontalBracket WITH LABELS
%%% -\tweak HorizontalBracketText.text #string \startGroup
%%% For nesting, use in reverse order:
%%% \innerNestedBracket "label" \outerNestedBracket "label" #note
labeledBracket =
    #(define-music-function
     (string note)
     (string? ly:music?)
    #{
      #note
    -\tweak HorizontalBracketText.Y-offset #-0.75
    -\tweak HorizontalBracketText.direction #0
    -\tweak HorizontalBracketText.font-size #-3.75
    -\tweak HorizontalBracketText.font-series #'bold
    -\tweak HorizontalBracketText.layer #5
    -\tweak HorizontalBracketText.whiteout-style #'rounded-box
    -\tweak HorizontalBracketText.whiteout ##t
    %{ -\tweak HorizontalBracketText.direction #-1 %}
    %{ -\tweak HorizontalBracketText.padding #'() %}
    %{ -\tweak HorizontalBracketText.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.staff-padding #'() %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #'() %}

    -\tweak HorizontalBracket.shorten-pair #'(-2 . -0.5)
    -\tweak HorizontalBracket.direction #DOWN
    -\tweak HorizontalBracket.layer #4
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #2 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ -\tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.6 . 0.6) %}
      -\tweak HorizontalBracketText.text #string
      \startGroup
      #})
 % lB =
innerNestedBracket =
  #(define-music-function
   (string note)
   (string? ly:music?)
  #{
    #note
    -\tweak HorizontalBracketText.Y-offset #-0.75
    -\tweak HorizontalBracketText.direction #0
    -\tweak HorizontalBracketText.font-size #-3.75
    -\tweak HorizontalBracketText.font-series #'bold
    -\tweak HorizontalBracketText.layer #5
    -\tweak HorizontalBracketText.whiteout-style #'rounded-box
    -\tweak HorizontalBracketText.whiteout ##t
    %{ -\tweak HorizontalBracketText.direction #-1 %}
    %{ -\tweak HorizontalBracketText.padding #'() %}
    %{ -\tweak HorizontalBracketText.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.staff-padding #'() %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #'() %}

    -\tweak HorizontalBracket.shorten-pair #'(-2 . -0.5)
    -\tweak HorizontalBracket.direction #DOWN
    -\tweak HorizontalBracket.layer #4
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #2 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ -\tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.6 . 0.6) %}

    -\tweak HorizontalBracketText.text #string
    \startGroup
    #})
 % lBB =
outerNestedBracket =
  #(define-music-function
   (string note)
   (string? ly:music?)
  #{
    %{ \once \override Score.HorizontalBracket.Y-offset #'() %} %}
    #note
-\tweak HorizontalBracketText.Y-offset #-0.75
    -\tweak HorizontalBracketText.direction #0
    -\tweak HorizontalBracketText.font-size #-3.75
    -\tweak HorizontalBracketText.font-series #'bold
    -\tweak HorizontalBracketText.layer #5
    -\tweak HorizontalBracketText.whiteout-style #'rounded-box
    -\tweak HorizontalBracketText.whiteout ##t
    %{ -\tweak HorizontalBracketText.direction #-1 %}
    %{ -\tweak HorizontalBracketText.padding #'() %}
    %{ -\tweak HorizontalBracketText.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.staff-padding #'() %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #'() %}

    -\tweak HorizontalBracket.shorten-pair #'(-2 . -0.5)
    -\tweak HorizontalBracket.direction #DOWN
    -\tweak HorizontalBracket.layer #4
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #2 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ -\tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.6 . 0.6) %}

    -\tweak HorizontalBracketText.text #string
    \startGroup
    #})
%%% \once \override options for HorizontalBracket
multiSet =
  {
    %{ \once \override Score.HorizontalBracket.direction = #UP %}
    \once \override Score.HorizontalBracket.outside-staff-priority = #1150
    %{ \once \override Score.HorizontalBracket.padding = #0.2 %}
    \once \override Score.HorizontalBracket.padding = #0.5
    %{ \once \override Score.HorizontalBracket.staff-padding = #0.2 %}
    \once \override Score.HorizontalBracket.staff-padding = #15
    \once \override Score.HorizontalBracket.outside-staff-padding = #15
    %{ \once \override Score.HorizontalBracket.outside-staff-padding = #'() %}

    %{ \once \override Score.HorizontalBracket.shorten-pair = #'(0 . 0) %}
    \once \override Score.HorizontalBracket.bracket-flare = #'(0.5 . 0.5)
    %{ \once \override Score.HorizontalBracket.connect-to-neighbor = #'(#f . #f) %}
    \once \override Score.HorizontalBracket.edge-height = #'(0.6 . 0.6)
    \once \override Score.HorizontalBracket.layer = #5

    \once \override Score.HorizontalBracketText.font-size = #-3
    \once \override Score.HorizontalBracketText.font-series = #'bold
    \once \override Score.HorizontalBracketText.layer = #6
    \once \override Score.HorizontalBracketText.whiteout-style = #'box
    \once \override Score.HorizontalBracketText.whiteout = ##t
    \once \override Score.HorizontalBracketText.direction = #0

    %{ \once \override Score.HorizontalBracketText.padding = #0.2 %}
    \once \override Score.HorizontalBracketText.padding = #'()
    %{ \once \override Score.HorizontalBracketText.staff-padding = #0.2 %}
    \once \override Score.HorizontalBracketText.staff-padding = #'()
    %{ \once \override Score.HorizontalBracketText.outside-staff-padding = #0.2 %}
    \once \override Score.HorizontalBracketText.outside-staff-padding = #'()
 }


% NOT WORKING Experimental tuplet bracket function
  % Was \tB
domainTupletMusicFunction =
  #(define-music-function
       (note)
       (ly:music?)
     #{
         %{ \tweak TupletNumber.font-size #-3.75 %}

         %{ \tweak TupletNumber.font-series #'bold %}

         %{ \tweak TupletNumber.layer #4 %}
         %{ \tweak TupletNumber.whiteout-style #'outline %}
         %{ \tweak TupletNumber.whiteout ##t %}
         %{ \tweak TupletNumber.direction #1 %}
         %{ \tweak TupletBracket.staff-padding #3 %}
         %{ \tweak TupletBracket.layer #3 %}
         %{ \tweak TupletBracket.shorten-pair #'(0 . 0) %}
         %{ \tweak TupletBracket.outside-staff-priority #1101 %}
         %{ \tweak TupletBracket.edge-height #'(0.5 . 0.5) %}
         %{ \tweak TupletBracket.bracket-visibility ##t %}
         %{ \tweak TupletBracket.direction #UP %}
         %{ \tweak TupletNumber.stencil ##f %}
         %{ \tweak TupletBracket.stencil ##f %}
         \tuplet 1/1
          #note
     #})


%{ fix-ah = \lyricmode { {\markup { \teeny \smaller \smaller \smaller  "ə"}}}
fix-i = \lyricmode {{\markup { \teeny \smaller \smaller \smaller  "i"}}}
fix-ii = \lyricmode {{\markup { \teeny \smaller \smaller \smaller  "ii"}}}
fix-iii = \lyricmode {{\markup { \teeny \smaller \smaller \smaller  "iii"}}}
fix-iv = \lyricmode {{\markup { \teeny \smaller \smaller \smaller  "iv"}}}
fix-v = \lyricmode {{\markup { \teeny \smaller \smaller \smaller  "v"}}}
fix-vi = \lyricmode {{\markup { \teeny \smaller \smaller \smaller  "vi"}}} %}

\version "2.22.0"

#(set-object-property! 'offset-from-staff 'backend-type? number?)
#(set-object-property! 'offset-one-line 'backend-type? number?)


#(define (calc-rest-staff-position grob)
   (let* ((staff-symbol (ly:grob-object grob 'staff-symbol))
          (line-count (ly:grob-property staff-symbol 'line-count))
          (offset (ly:grob-property grob 'offset-from-staff))
          (stencil (ly:grob-property grob 'stencil))
          (extent (ly:stencil-extent stencil Y)))
     (+ line-count
        (- (* 2 (car extent)))
        offset)))

#(define (shift-dots grob)
   (let* ((layout (ly:grob-layout grob))
          (one-line (ly:output-def-lookup layout 'one-line-staff)))
     ; This is not very pretty, but the dot position is rounded
     ; automatically.
     (ly:grob-set-property!
       grob
       'extra-offset
       (cons
         0
         (ly:grob-property grob
                           (if one-line
                               'offset-one-line
                               'offset-from-staff))))))


\layout {
  \context {
    \Score
    \override Rest.staff-position = #calc-rest-staff-position
    \override Rest.offset-from-staff = 0.0
    \override Dots.after-line-breaking = #shift-dots
    \override Dots.offset-from-staff = 0.25
    \override Dots.offset-one-line = 0.00
  }
}
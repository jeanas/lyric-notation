\version "2.22.0"


\include "highlight.ily"
%{ \include "highlight-defs.ily" %}
\include "highlight-colors.ily"
\include "groove-analysis-tools.ily"
\include "rhyme-analysis-tools.ily"
\include "bracket-lyric-support.ily"
\include "pitch-registers.ily"
%{ \include "/Users/blankens10/GitHub/lyric-notation-master/XXXX.ily" %}

%{ \include "/Users/blankens10/GitHub/lyric-notation-master/highlight.ily"
\include "/Users/blankens10/GitHub/lyric-notation-master/highlight-defs.ily"
\include "/Users/blankens10/GitHub/lyric-notation-master/highlight-colors.ily"
\include "/Users/blankens10/GitHub/lyric-notation-master/groove-analysis-tools.ily"
\include "/Users/blankens10/GitHub/lyric-notation-master/rhyme-analysis-tools.ily"
\include "/Users/blankens10/GitHub/lyric-notation-master/bracket-lyric-support.ily"
\include "/Users/blankens10/GitHub/lyric-notation-master/pitch-registers.ily" %}



%%% Make Slurs break horizontal
% Usage:
%   \override Slur.after-line-breaking = #horizontalise-broken-slurs
#(define (horizontalise-broken-slurs grob)
    (let*
     ((orig (ly:grob-original grob))
      (siblings (if (ly:grob? orig)
                    (ly:spanner-broken-into orig)
                    '()))
      (control-points (ly:grob-property grob 'control-points)))

     (if (>= (length siblings) 2)
         (let ((new-control-points
                (cond
                 ((eq? (first siblings) grob)
                  (list (first control-points)
                        (second control-points)
                        (third control-points)
                        (third control-points)))
                 ((eq? (last siblings) grob)
                  (list (second control-points)
                        (second control-points)
                        (third control-points)
                        (fourth control-points)))
                 (else control-points))))
           (ly:grob-set-property! grob 'control-points
           new-control-points)))))



% From General Layout
%{ \layout { %}

  %{ \context { %}

    %{ \Score %}

    %{ \remove System_start_delimiter_engraver %}
    %{ \override Slur.avoid-slur = #'inside %}
    %{ \override Slur.outside-staff-priority = #1100 %}
    %{ \override PhrasingSlur.outside-staff-priority = #1200 %}
    %{ \override PhrasingSlur.avoid-slur = #'inside %}

    %{ \override TextScript.self-alignment-X = #LEFT %}
    %{ \override TextScript.X-offset = -0.8 %}
    %{ \override TextScript.avoid-slur = #'inside %}
    %{ \override TextScript.staff-padding = #'() %}
    %{ \override TextScript.outside-staff-padding = #'() %}
    %{ \override TextScript.padding = 0.2 %}
    %{ \override TextScript.outside-staff-priority = ##f %}

    %{ \override TupletBracket.outside-staff-priority = #1150 %}
    %{ \override TupletBracket.avoid-scripts = ##t %}
    %{ \override TupletNumber.avoid-scripts = ##t %}
    %{ \override TupletBracket.stencil = ##f %}
    %{ \override TupletNumber.stencil = ##t %}
    %{ \override TupletNumber.font-size = #-3.5 %}
    %{ \override TupletNumber.font-style = #'upright %}
    %{ \override TupletNumber.whiteout = ##t %}
    %{ \override TupletNumber.whiteout-style = #'outline %}
    %{ \override TupletNumber.layer = #7 %}
    %{ \override TupletNumber.staff-padding = #'() %}
    %{ \override TupletBracket.layer = #1 %}
    %{ \override TupletBracket.direction = #up %}
    %{ \override TupletBracket.edge-height = #'(0.5 . 0.5) %}
    %{ \override TupletBracket.thickness = #1.1 %}
    %{ \override TupletBracket.padding = #0.2 %}
    %{ \override TupletBracket.staff-padding = #'() %}
    %{ \override TupletBracket.outside-staff-padding = #0.2 %}
  %{ } %}

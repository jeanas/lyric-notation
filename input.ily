\version "2.22.0"

\include "headphones.ily"
\include "define-stress-accent.ily"
\include "prepend.ily"
\include "music-filter-map.ily"

#(use-modules (srfi srfi-26)
              (ice-9 regex))


% 5 diphthongs, including EY and OW
#(define VOWELS
    ; Shape of an element is
    ;   (ARPABET-representation pitch [pitch2])
    ; (this actually makes an associative list).
    ; Note: lowercase is supported, too.
    ; Vowels are listed according to their vertical
    ; position on the staff from top to bottom
    ; Diphthongs are listed according to initial position
    ; blank lines separate the vowels of each staff space
    ; ER and AXR are rhotic variants of AH and AX.
    ; They are printed on the same line and have
    ; a rhotic glyph printed on the notehead
    ; EL and AXL are velarized variants of AH and AX.
    ; They are printed on the same line and have
    ; a velar glyph printed on the notehead
    ; Malformed diphthongs (ones that are contradicted
    ; by phonological data) are automatically corrected
    ; to the closest phonologically correct diphthong
    '(
      ; Space 1
      ("IY" (2 2))
      ("IYR" (2 0) (0 6))
      ("IYN" (2 0) (0 6))
      ("IYM" (2 0) (0 6))
      ("IYG" (2 0) (0 6))
      ("IYL" (2 2) (-1 5))

      ; Space 2
      ("IH" (2 0))
      ("IHR" (2 0) (0 6))
      ("IHN" (2 0) (0 6))
      ("IHM" (2 0) (0 6))
      ("IHG" (2 0) (0 6))
      ("IHL" (2 0) (-1 5))

      ; Space 3
      ("EY" (1 5) (2 0))
      ("EYR" (1 3) (0 6))
      ("EYN" (1 3) (0 6))
      ("EYM" (1 3) (0 6))
      ("EYG" (1 3) (0 6))
      ("EYL" (1 5) (-1 5))

      ; Space 4
      ("EH" (1 3))
      ("EHR" (1 3) (0 6))
      ("EHN" (1 3) (0 6))
      ("EHM" (1 3) (0 6))
      ("EHG" (1 3) (0 6))
      ("EHL" (1 3) (-1 5))

      ; Space 5
      ("AE" (1 1))
      ("AER" (1 3) (0 6))
      ("AEN" (1 3) (0 6))
      ("AEM" (1 3) (0 6))
      ("AEG" (1 3) (0 6))
      ("AEL" (1 1) (-1 5))

      ; Space 6
      ("AH" (0 6))
      ("AHN" (0 6))
      ("AHM" (0 6))
      ("AHG" (0 6))
      ("AHR" (0 6))
      ("AHL" (-1 5))
      ("AX" (0 6))
      ("AXN" (0 6))
      ("AXM" (0 6))
      ("AXG" (0 6))
      ("AXR" (0 6))
      ("AXL" (-1 5))
      ("ER" (0 6))
      ("EM" (0 6))
      ("EN" (0 6))
      ("EG" (0 6))

      ; New version
      ("AY" (0 6) (2 0))
      ("AYL" (0 6) (-1 5))
      ("AYR" (0 6) (0 6))
      ("AYN" (0 6))
      ("AYM" (0 6))
      ("AYG" (0 6))
      ("AWN" (0 6))
      ("AWM" (0 6))
      ("AWG" (0 6))
      ("AW" (0 6) (-1 5))
      ("AWL" (0 6) (-1 5))
      ("AWR" (0 6) (0 6))

      ; Space 7
      ("AA" (0 4))
      ("AAR" (0 4) (0 6))
      ("AAN" (0 4) (0 6))
      ("AAM" (0 4) (0 6))
      ("AAG" (0 4) (0 6))
      ("AAL" (0 4) (-1 5))

      ; Space 8
      ("AO" (0 2))
      ("AOR" (0 2) (0 6))
      ("AON" (0 2) (0 6))
      ("AOM" (0 2) (0 6))
      ("AOG" (0 2) (0 6))
      ("AOL" (0 2) (-1 5))
      ("OY" (0 2) (2 0))
      ("OYR" (0 2) (0 6))
      ("OYN" (0 2) (0 6))
      ("OYM" (0 2) (0 6))
      ("OYG" (0 2) (0 6))
      ("OYL" (0 2) (-1 5))

      ; Space 9
      ("OWR" (0 2) (0 6))
      ("OWL" (0 0) (-1 5))
      ("OW" (0 0) (-1 5))

      ; Space 10
      ("UH" (-1 5))
      ("UHR" (-1 5) (0 6))
      ("UHN" (-1 5) (0 6))
      ("UHM" (-1 5) (0 6))
      ("UHG" (-1 5) (0 6))
      ("UHL" (-1 5))
      ("EL" (-1 5))

      ; Space 11
      ("UW" (-1 3))
      ("UWR" (-1 5) (0 6))
      ("UWN" (-1 5) (0 6))
      ("UWM" (-1 5) (0 6))
      ("UWG" (-1 5) (0 6))
      ("UWL" (-1 3) (-1 5))
      ))

#(define CONSONANTS
      ; Maps lists of single-letter ARPABET codes to symbols
      ; representing their groups: '(category voiced?).
   '(("Q" glottal-stop ())
     ("p" plosive #f)
     ("t" plosive #f)
     ("k" plosive #f)
     ("b" plosive #t)
     ("d" plosive #t)
     ("g" plosive #t)
     ("C" affricate #f)
     ("J" affricate #t)
     ("f" fricative #f)
     ("T" fricative #f)
     ("s" fricative #f)
     ("S" fricative #f)
     ("h" fricative #f)
     ("x" fricative #f)
     ("v" fricative #t)
     ("D" fricative #t)
     ("z" fricative #t)
     ("Z" fricative #t)
     ("m" nasal ())
     ("n" nasal ())
     ("M" nasal ())
     ("N" nasal ())
     ("G" nasal ())
     ("q" nasal ())
     ("l" liquid ())
     ("L" liquid ())
     ("r" liquid ())
     ("y" glide ())
     ("w" glide ())))


#(define lyric-regexp (make-regexp "((<>|`|@)*)([a-zA-Z]+)-([a-zA-Z]+)(-([a-zA-Z]+))?"))

#(define (get-vowel-info vowel source)
   (or (assoc-ref VOWELS vowel)
       (begin
         (ly:music-warning source
                          "Cannot find vowel '~a'"
                         vowel)
         '((0 0)))))

#(define (get-consonant-classes str source)

   "Return the consonant classes of all characters in `str`, as list.

The second parameter is a music event to warn about if a consonant
is not found."
   (map
     (lambda (consonant-char)
       (let ((consonant (string consonant-char)))
         (or
           (assoc-ref CONSONANTS consonant)
           (begin
             (ly:music-warning source
                               "Cannot find consonant info for '~a'"
                               consonant)
             '(glottal-stop #f)))))
   (string->list str)))

#(define (chord-from-lyric-text one-line lyric-event)
   "Return music for the text of the given lyric event,
for example: <>f-UW .

The previous event is passed (#f if we are processing
the first event). It is mutated in case we need to tie
the preceding note to this one.

In this helper routine, we don't cater for _, r and R."
   (let* ((text (ly:music-property lyric-event 'text))
          (match (regexp-exec lyric-regexp text)))
     (if (not match)
         (begin
           (ly:music-error lyric-event
                           "Invalid input: ~s"
                           text)
           (make-music 'SequentialMusic))
         (let*
           ((modifiers (match:substring match 1))
            ; Support X to denote a slide. It is ignored.
            (onset (string-delete
                     (match:substring match 3)
                     #\X))
            (vowel (match:substring match 4))
            ; Support lowercase input. Keys in the alist
            ; above are uppercase.
            (vowel (string-upcase! vowel))
            (coda (match:substring match 6))
            ; BEGIN velarized version
            (rhotacized-through-vowel (equal? vowel "ER"))
            (velarized-through-vowel (equal? vowel "EL"))
            (nasalized-through-vowel (equal? vowel "EN"))
            (masalized-through-vowel (equal? vowel "EM"))
            (gasalized-through-vowel (equal? vowel "EG"))
            (rhotacized-through-coda (and coda
                                          (equal? (substring coda 0 1)
                                                  "r")))
            (velarized-through-coda (and coda
                                          (equal? (substring coda 0 1)
                                                  "L")))
            (nasalized-through-coda (and coda
                                          (equal? (substring coda 0 1)
                                                  "N")))
            (masalized-through-coda (and coda
                                          (equal? (substring coda 0 1)
                                                  "M")))
            (gasalized-through-coda (and coda
                                          (equal? (substring coda 0 1)
                                                  "q")))
            (is-rhotacized (or rhotacized-through-vowel
                               rhotacized-through-coda))
            (is-velarized (or velarized-through-vowel
                               velarized-through-coda))
            (is-nasalized (or nasalized-through-vowel
                               nasalized-through-coda))
            (is-masalized (or masalized-through-vowel
                               masalized-through-coda))
            (is-gasalized (or gasalized-through-vowel
                               gasalized-through-coda))
            (vowel (if rhotacized-through-coda
                       (string-append vowel "R")
                       vowel))
            (vowel (if velarized-through-coda
                        (string-append vowel "L")
                        vowel))
            (vowel (if nasalized-through-coda
                        (string-append vowel "N")
                        vowel))
            (vowel (if masalized-through-coda
                        (string-append vowel "M")
                        vowel))
            ; match:substring returns #f in case there is
            ; no onset, but we want the empty string
            ; (although #f is convenient for is-rhotacized
            ; above).
            (coda (or coda ""))
            (coda (if rhotacized-through-vowel
                      (string-append "r" coda)
                      coda))
            (coda (if velarized-through-vowel
                      (string-append "L" coda)
                      coda))
            (coda (if nasalized-through-vowel
                      (string-append "N" coda)
                      coda))
            (coda (if masalized-through-vowel
                      (string-append "M" coda)
                      coda))
            (coda (if gasalized-through-vowel
                      (string-append "G" coda)
                      coda))
            ; END velarized version
            (is-stressed (string-contains modifiers "`"))
            (is-accented (string-contains modifiers "@"))
            (is-alt (string-contains modifiers "<>"))
            (pitches (get-vowel-info vowel lyric-event))
            (pitches (map (cute apply ly:make-pitch <>) pitches))
            (onset-classes (get-consonant-classes onset lyric-event))
            (coda-classes (get-consonant-classes coda lyric-event))
            (duration (ly:music-property lyric-event 'duration))
            (articulations '())
            (articulations (if is-stressed
                               (cons
                                 (make-music 'StressEvent
                                             lyric-event)
                                 articulations)
                               articulations))
            (articulations (if is-accented
                               (cons
                                 (make-music 'AccentEvent
                                             lyric-event)
                                 articulations)
                               articulations))
            (phantom-note (make-music 'NoteEvent
                                       'duration duration
                                       ; Dummy pitch; the staff position is set
                                       ; later on based on is-phantom.
                                       'pitch (ly:make-pitch 0 0)
                                       'is-centered #f
                                       'is-phantom #t
                                       'is-suppressed #t
                                       'index -1))
            (chord
              (case (length pitches)
                ((1)
                 (make-music 'EventChord
                             'elements
                             (list
                               (make-music 'NoteEvent
                                           'pitch (first pitches)
                                           'duration duration
                                           'is-alt is-alt
                                           'is-rhotacized is-rhotacized
                                           'is-velarized is-velarized
                                           'is-nasalized is-nasalized
                                           'is-masalized is-masalized
                                           'is-gasalized is-gasalized
                                           'onset-classes onset-classes
                                           'coda-classes coda-classes
                                           'is-centered #f
                                           'is-phantom #f
                                           'is-suppressed #f
                                           'index 0))
                             'articulations articulations))
                ((2)
                 (make-music 'EventChord
                             'elements
                             (list
                               (make-music 'NoteEvent
                                           'pitch (first pitches)
                                           'duration duration
                                           'is-alt is-alt
                                           'is-rhotacized #f
                                           'is-velarized #f
                                           'is-nasalized #f
                                           'is-masalized #f
                                           'is-gasalized #f
                                           'onset-classes onset-classes
                                           'coda-classes '()
                                           'is-centered #f
                                           'is-phantom #f
                                           'is-suppressed #f
                                           'index 1)
                               (make-music 'NoteEvent
                                           'pitch (second pitches)
                                           'duration duration
                                           'is-alt #f ; TODO: check
                                           'is-rhotacized is-rhotacized
                                           'is-velarized is-velarized
                                           'is-nasalized is-nasalized
                                           'is-masalized is-masalized
                                           'is-gasalized is-gasalized
                                           'onset-classes '()
                                           'coda-classes coda-classes
                                           'is-centered #t
                                           'is-phantom #f
                                           'is-suppressed #f
                                           'index 2))
                             'articulations articulations)))))
       (if (not one-line)
           (prepend! phantom-note
                     (ly:music-property chord 'elements)))
       chord))))

#(define (recursively-set-property! chord property value)
   ; Change property to value where it is defined.
   (for-some-music
     (lambda (m)
       (if (not (null? (ly:music-property m property)))
           (ly:music-set-property! m property value))
       #f)
     chord))

#(define (tie-notes-having-index! chord indices)
   (for-some-music
     (lambda (m)
       (if (member (ly:music-property m 'index)
                   indices)
           (ly:music-set-property! m
                                   'articulations
                                   (list (make-music 'TieEvent))))
       #f)
     chord))


#(define (tied-chord! previous-chord duration)
   (let ((chord (ly:music-deep-copy previous-chord)))
     (ly:music-set-property! chord 'articulations '())
     (recursively-set-property! chord 'duration duration)
     (recursively-set-property! chord 'is-suppressed #t)
     (tie-notes-having-index! previous-chord '(-1 0 2))
    chord))

#(define (set-origin-from! container-event original-event)
   (let ((original-location (ly:music-property original-event 'origin)))
     (map-some-music
       (lambda (music)
         (ly:music-set-property! music 'origin original-location)
         #f)
       container-event)))

#(define (reduce-text! lyric-event char)
   (ly:music-set-property!
     lyric-event
     'text
     (string-delete
       (ly:music-property lyric-event 'text)
       char)))

#(define (add-slur! chord span-direction)
   (prepend!
     (make-music 'SlurEvent
                 'span-direction span-direction)
     (ly:music-property chord 'articulations)))

#(define (copy-articulations-on! source destination)
   (prepend-elements! (ly:music-property source 'articulations)
                      (ly:music-property destination 'articulations)))

#(define (detect-slur! lyric-event previous-chord)
   ; If we have a standalone slur, prepare the previous
   ; chord and return #t. If we don't have a slur, return
   ; #f. If we do have a slur but we should also create
   ; a new chord, return the span direction that a slur
   ; should be added with.
   (let ((text (ly:music-property lyric-event 'text)))
     (cond
       ((equal? text "(")
        (add-slur! previous-chord LEFT)
        (copy-articulations-on! lyric-event previous-chord)
        #t)
       ((equal? text ")")
        (copy-articulations-on! lyric-event previous-chord)
        (add-slur! previous-chord RIGHT)
        #t)
       ((string-contains text "(")
        (reduce-text! lyric-event #\()
        LEFT)
       ((string-contains text ")")
        (reduce-text! lyric-event #\))
        RIGHT)
       (else
        #f))))

#(define (detect-angle-bracket! lyric-event)
   (let ((text (ly:music-property lyric-event 'text)))
     (if (string-contains text "]")
         (begin
           (reduce-text! lyric-event #\])
           #t)
         #f)))

#(define (add-angle-bracket! chord)
   (prepend!
     (make-music 'AngleBracketEvent)
     (ly:music-property chord 'articulations)))

#(define (music-from-lyric-event one-line lyric-event previous-chord)
   (let ((detected-slur (detect-slur! lyric-event previous-chord))
         (detected-angle-bracket (detect-angle-bracket! lyric-event)))
     (if (eq? detected-slur #t)
         ; Previous chord was added a slur. Return false
         ; to let music-filter-map discard this one.
         #f
         (let* ( ; A slur parenthese was possibly removed by
                 ; detect-slur! in 'text here.
                (text (ly:music-property lyric-event 'text))
                (duration (ly:music-property lyric-event 'duration))
                (new-chord
                  (cond
                     ; '_' in the input
                    ((equal? text " ")
                     (if previous-chord
                         (tied-chord! previous-chord duration)
                         (begin
                           (ly:music-warning lyric-event
                                             "No previous note to tie with")
                           (make-music 'RestEvent
                                       'duration duration))))
                    ((equal? text "r")
                     (make-music 'RestEvent
                                 'duration duration))
                    ((equal? text "R")
                     (make-music 'MultiMeasureRestMusic
                                 'duration duration))
                    ((equal? text "'")
                     (make-music 'BreathingEvent))
                    (else
                      (chord-from-lyric-text one-line lyric-event)))))
           (prepend-elements! (ly:music-property lyric-event 'articulations)
                              (ly:music-property new-chord 'articulations))
           (if detected-slur
               ; Had a parenthese at the end of the text.
               ; Add the slur to the newly created chord.
               (add-slur! new-chord detected-slur))
           (if detected-angle-bracket
               (add-angle-bracket! new-chord))
           (set-origin-from! new-chord lyric-event)
           new-chord))))



#(define (lyric-notation-input one-line input)
   (let ((last-chord #f))
     (music-filter-map
       (lambda (music)
         (if (music-is-of-type? music 'lyric-event)
             (let ((new-chord (music-from-lyric-event
                                one-line
                                music
                                last-chord)))
               (if new-chord
                   (set! last-chord new-chord))
               new-chord)
             music))
       input)))

\version "2.22.0"

% Set higher resolution in PNG backend. This has
% no effect in PDF mode.

#(ly:set-option 'resolution 1000)
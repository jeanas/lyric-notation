\version "2.22.0"

%%% GROOVE ANALYSIS FUNCTIONS
%%% Groove Settings for inside lyricmode expression
genGrooveTupSettings = {
 \set Score.tupletFullLength = ##t
  \override Score.TupletNumber.font-series = #'bold
  \override Score.TupletNumber.font-shape = #'upright
  \override Score.TupletNumber.font-size = #-4
  \override Score.TupletNumber.whiteout = ##t
  \override Score.TupletNumber.whiteout-style = #'outline
  %{ \override Score.TupletNumber.side-axis = #1 %}
  %{ \override Score.TupletNumber.direction = #center %}

  %{ \override Score.TupletNumber.layer = #7 %}

  %{ \override Score.TupletBracket.layer = #7 %}
  %{ \override Score.TupletBracket.outside-staff-priority = #1100 %}
  \override Score.TupletBracket.direction = #up
  \override Score.TupletBracket.edge-height = #'(0.5 . 0.5)
  \override Score.TupletBracket.thickness = #1
  \override Score.TupletBracket.padding = #0.15
  \override Score.TupletBracket.staff-padding = #'()
  \override Score.TupletBracket.outside-staff-padding = #0.15
  %{ \override Score.TupletBracket.avoid-scripts = ##t %}
}
% alternante settings
oneLineGrooveSettings = {
 \genGrooveTupSettings
  %{ \override Score.TupletNumber.font-series = #'bold %}
  %{ \override Score.TupletNumber.font-shape = #'upright %}
  %{ \override Score.TupletNumber.font-size = #-4 %}
  %{ \override Score.TupletNumber.whiteout = ##t %}
  %{ \override Score.TupletNumber.whiteout-style = #'outline %}
  %{ \override Score.TupletNumber.self-alignment-X = #CENTER %}
  %{ \override Score.TupletNumber.direction = #center %}
  %{ \override Score.TupletBracket.layer = #7 %}
  %{ \override Score.TupletBracket.outside-staff-priority = #1100 %}
  %{ \override Score.TupletBracket.direction = #up %}
  %{ \override Score.TupletBracket.edge-height = #'(0.5 . 0.5) %}
  %{ \override Score.TupletBracket.thickness = #1 %}
  %{ \override Score.TupletBracket.padding = #0.15 %}
  %{ \override Score.TupletBracket.staff-padding = #'() %}
  %{ \override Score.TupletBracket.outside-staff-padding = #0.15 %}
  %{ \override Score.TupletBracket.avoid-scripts = ##t %}
}
grooveTupletTextBeforeBreak = {
 \override TupletNumber.after-line-breaking =
 #(lambda (grob)
    (if (not-first-broken-spanner? grob)
        (ly:grob-set-property! grob 'stencil '())))
  }
grooveTupletTextAfterBreak = {
 \override TupletNumber.after-line-breaking =
 #(lambda (grob)
    (if (first-broken-spanner? grob)
        (ly:grob-set-property! grob 'stencil '())))
  }

% Groove Tuplets Settings
grooveUnitSettings =
  \tweak TupletBracket.shorten-pair #'(-1 . 0)
  \tweak TupletNumber.font-shape #'upright
  \tweak TupletBracket.direction #UP
  %{ \tweak TupletBracket.outside-staff-priority #1100 %}
  %{ \tweak TupletNumber.font-series #'bold %}
  %{ \tweak TupletNumber.font-size #-4 %}
  %{ \tweak TupletNumber.self-alignment-X #CENTER %}
  %{ \tweak TupletNumber.whiteout ##f %}
  %{ \tweak TupletNumber.whiteout-style #'outline %}
  \tweak TupletNumber.layer #7
  %{ \tweak TupletBracket.layer #1 %}
  %{ \tweak TupletNumber.direction #DOWN %}
  %{ \tweak TupletBracket.edge-height #'(0.4 . 0.4) %}
  %{ \tweak TupletBracket.thickness #1 %}
  %{ \tweak TupletBracket.padding #0.15 %}
  \tweak TupletBracket.staff-padding #3.75
  %{ \tweak TupletBracket.outside-staff-padding #0.15 %}
  %{ \tweak TupletBracket.avoid-scripts ##f %}
  %{ \tweak TupletBracket.full-length-to-extent ##t %}
  \etc
% Groove brackets under the staff
grooveUnitSettingsDown =
  \tweak TupletBracket.shorten-pair #'(0 . 1)
  \tweak TupletNumber.font-shape #'upright
  \tweak TupletBracket.direction #DOWN
  %{ \tweak TupletBracket.outside-staff-priority #1100 %}
  %{ \tweak TupletNumber.font-series #'bold %}
  %{ \tweak TupletNumber.font-size #-4 %}
  %{ \tweak TupletNumber.whiteout ##f %}
  %{ \tweak TupletNumber.whiteout-style #'outline %}
  %{ \tweak TupletNumber.layer #1 %}
  %{ \tweak TupletBracket.layer #1 %}
  %{ \tweak TupletBracket.edge-height #'(0.4 . 0.4) %}
  %{ \tweak TupletBracket.thickness #1 %}
  %{ \tweak TupletBracket.padding #0.15 %}
  %{ \tweak TupletBracket.staff-padding #3.5 %}
  %{ \tweak TupletBracket.outside-staff-padding #0.15 %}
  %{ \tweak TupletBracket.avoid-scripts ##t %}
  %{ \tweak TupletBracket.full-length-to-extent ##t %}
  \etc
%%% Groove tuplet Functions
%%% places tuplet brackets with text 2 or 3
gTwo =

  \grooveUnitSettings
  \tweak TupletNumber.text #(tuplet-number::non-default-tuplet-denominator-text 2)
  \tuplet 1/1 \etc
gTre =
  \grooveUnitSettings
  \tweak TupletNumber.text #(tuplet-number::non-default-tuplet-denominator-text 3)
  \tuplet 1/1 \etc
% Groove Class label, outside units
gClass =
  #(define-music-function
   (string note)
   (string? ly:music?)
  #{
      \once \override TupletNumber.extra-offset = #'(-6.25 . 0.2)
      %{ \tweak TupletNumber.font-series #'bold %}
      %{ \tweak TupletNumber.font-shape #'upright %}
      %{ \tweak TupletNumber.stencil ##t %}
      %{ \tweak TupletNumber.whiteout  ##t %}
      %{ \tweak TupletNumber.whiteout-style  #'outline %}
      %{ \tweak TupletBracket.outside-staff-priority #1200 %}
      %{ \tweak TupletBracket.direction #up %}
      %{ \tweak TupletBracket.edge-height #'(0.5 . 0.5) %}
      %{ \tweak TupletBracket.to-barline ##t %}
      %{ \tweak TupletBracket.shorten-pair #'(0.0 . 1) %}
      \tweak TupletBracket.padding #0.6
      %{ \tweak TupletBracket.staff-padding #3.5 %}
      %{ \tweak TupletBracket.outside-staff-padding #5 %}
      \tweak TupletNumber.font-size  #-3.5
      %{ \tweak TupletNumber.direction #CENTER %}
      \tweak TupletBracket.layer #8
      \tweak TupletBracket.thickness #2
    \tweak TupletNumber.text #string
    \tuplet 1/1
    #note
    #})
%% Groove unit tuplets for under Staff
% N.B. It makes better sense to have them with the rhythm
gTwoD =
  %{ \tweak TupletBracket.outside-staff-priority #1100
  \tweak TupletNumber.font-series #'bold
  \tweak TupletNumber.font-shape #'upright
  \tweak TupletNumber.font-size #-4
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.layer #1
  \tweak TupletBracket.layer #1
  \tweak TupletBracket.direction #up
  \tweak TupletBracket.edge-height #'(0.4 . 0.4)
  \tweak TupletBracket.thickness #1
  \tweak TupletBracket.padding #0
  \tweak TupletBracket.staff-padding #1
  \tweak TupletBracket.shorten-pair #'(0 . 1) %}
  \grooveUnitSettingsDown
  \tweak TupletNumber.text #(tuplet-number::non-default-tuplet-denominator-text 2)
  \tuplet 1/1 \etc
gTreD =
  %{ \tweak TupletBracket.outside-staff-priority #1100
  \tweak TupletNumber.font-series #'BOLD
  \tweak TupletNumber.font-shape #'upright
  \tweak TupletNumber.font-size #-4
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.layer #1
  \tweak TupletBracket.layer #1
  \tweak TupletBracket.direction #up
  \tweak TupletBracket.edge-height #'(0.4 . 0.4)
  \tweak TupletBracket.thickness #1
  \tweak TupletBracket.padding #0.6
  \tweak TupletBracket.shorten-pair #'(0 . 1) %}
  \grooveUnitSettingsDown
  \tweak TupletNumber.text #(tuplet-number::non-default-tuplet-denominator-text 3)
  \tuplet 1/1 \etc

gClassD =
  #(define-music-function
   (string note)
   (string? ly:music?)
  #{
      %{ \tweak TupletNumber.font-series #'bold %}
      %{ \tweak TupletNumber.font-shape #'upright %}
      %{ \tweak TupletNumber.stencil ##t %}
      %{ \tweak TupletNumber.whiteout  ##t %}
      %{ \tweak TupletNumber.whiteout-style  #'outline %}
      %{ \tweak TupletBracket.outside-staff-priority #1200 %}
      %{ \tweak TupletBracket.direction #up %}
      %{ \tweak TupletBracket.edge-height #'(0.5 . 0.5) %}
      %{ \tweak TupletBracket.to-barline ##t %}
      %{ \tweak TupletBracket.shorten-pair #'(0.0 . 1) %}
      %{ \tweak TupletBracket.padding #0.15 %}
      %{ \tweak TupletBracket.staff-padding #3.5 %}
      %{ \tweak TupletBracket.outside-staff-padding #0.2 %}
      \tweak TupletNumber.font-size  #-3.5
      \tweak TupletNumber.direction #center
      \tweak TupletNumber.self-alignment-X #left
      \tweak TupletBracket.layer #8
      \tweak TupletBracket.thickness #2
    \tweak TupletNumber.text #string
    \tuplet 1/1
    #note
    #})

% Smaller 32nd note units, instead of 16ths
microGrooveUnitSettings =
  \tweak TupletBracket.outside-staff-priority #1050
  %{ \tweak TupletNumber.font-series #'bold %}
  %{ \tweak TupletNumber.font-shape #'upright %}
  \tweak TupletNumber.font-size #-5
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.layer #1
  \tweak TupletBracket.layer #1
  \tweak TupletBracket.direction #up
  \tweak TupletBracket.edge-height #'(0.3 . 0.3)
  \tweak TupletBracket.thickness #.75
  \tweak TupletBracket.padding #0.1
  \tweak TupletBracket.staff-padding #3
  \tweak TupletBracket.outside-staff-padding #0.1
  \tweak TupletBracket.shorten-pair #'(0 . 1)
  \tweak TupletBracket.avoid-scripts ##t
  \etc
microGrooveUnitSettingsDown =
  \tweak TupletBracket.outside-staff-priority #1050
  %{ \tweak TupletNumber.font-series #'bold %}
  %{ \tweak TupletNumber.font-shape #'upright %}
  \tweak TupletNumber.font-size #-5
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.layer #1
  \tweak TupletBracket.layer #1
  \tweak TupletBracket.direction #down
  \tweak TupletBracket.edge-height #'(0.3 . 0.3)
  \tweak TupletBracket.thickness #.75
  \tweak TupletBracket.padding #0.1
  \tweak TupletBracket.staff-padding #3
  \tweak TupletBracket.outside-staff-padding #0.1
  \tweak TupletBracket.shorten-pair #'(0 . 1)
  \tweak TupletBracket.avoid-scripts ##t
  \etc

mGTwo =
  %{ \tweak TupletBracket.outside-staff-priority #1100
  \tweak TupletNumber.font-series #'bold
  \tweak TupletNumber.font-shape #'upright
  \tweak TupletNumber.font-size #-4
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.layer #1
  \tweak TupletBracket.layer #1
  \tweak TupletBracket.direction #up
  \tweak TupletBracket.edge-height #'(0.4 . 0.4)
  \tweak TupletBracket.thickness #1
  \tweak TupletBracket.padding #0
  \tweak TupletBracket.staff-padding #1
  \tweak TupletBracket.shorten-pair #'(0 . 1) %}
  \grooveUnitSettings
  \tweak TupletNumber.text #(tuplet-number::non-default-tuplet-denominator-text 2)
  \tuplet 1/1 \etc
mGTre =
  %{ \tweak TupletBracket.outside-staff-priority #1100
  \tweak TupletNumber.font-series #'BOLD
  \tweak TupletNumber.font-shape #'upright
  \tweak TupletNumber.font-size #-4
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.layer #1
  \tweak TupletBracket.layer #1
  \tweak TupletBracket.direction #up
  \tweak TupletBracket.edge-height #'(0.4 . 0.4)
  \tweak TupletBracket.thickness #1
  \tweak TupletBracket.padding #0.6
  \tweak TupletBracket.shorten-pair #'(0 . 1) %}
  \grooveUnitSettings
  \tweak TupletNumber.text #(tuplet-number::non-default-tuplet-denominator-text 3)
  \tuplet 1/1 \etc
mGTwoD =
  %{ \tweak TupletBracket.outside-staff-priority #1100
  \tweak TupletNumber.font-series #'bold
  \tweak TupletNumber.font-shape #'upright
  \tweak TupletNumber.font-size #-4
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.layer #1
  \tweak TupletBracket.layer #1
  \tweak TupletBracket.direction #up
  \tweak TupletBracket.edge-height #'(0.4 . 0.4)
  \tweak TupletBracket.thickness #1
  \tweak TupletBracket.padding #0
  \tweak TupletBracket.staff-padding #1
  \tweak TupletBracket.shorten-pair #'(0 . 1) %}
  \grooveUnitSettingsDown
  \tweak TupletNumber.text #(tuplet-number::non-default-tuplet-denominator-text 2)
  \tuplet 1/1 \etc
mGTreD =
  %{ \tweak TupletBracket.outside-staff-priority #1100
  \tweak TupletNumber.font-series #'BOLD
  \tweak TupletNumber.font-shape #'upright
  \tweak TupletNumber.font-size #-4
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.layer #1
  \tweak TupletBracket.layer #1
  \tweak TupletBracket.direction #up
  \tweak TupletBracket.edge-height #'(0.4 . 0.4)
  \tweak TupletBracket.thickness #1
  \tweak TupletBracket.padding #0.6
  \tweak TupletBracket.shorten-pair #'(0 . 1) %}
  \grooveUnitSettingsDown
  \tweak TupletNumber.text #(tuplet-number::non-default-tuplet-denominator-text 3)
  \tuplet 1/1 \etc

%%% Remove parens from courtesy bracket LABELS
\layout {
  \context {
    \Voice
     \consists Tuplet_engraver
     \override TupletNumber.stencil =
       #(lambda (grob)
          (let ((text (ly:grob-property grob 'text)))
            (if (markup? text)
                (grob-interpret-markup grob text)
                (ly:grob-suicide! grob))))}}

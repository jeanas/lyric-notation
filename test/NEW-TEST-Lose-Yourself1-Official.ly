\version "2.22.0"
\include "../lyric-notation.ily"

songLyricsDomains =
  \lyricmode {
    \rhColor a #(x11-color 'LightSeaGreen)
    \rhColor A #(x11-color 'LightSeaGreen)
    \rhColor b #(x11-color 'LightGreen)
    \rhColor B #(x11-color 'LightGreen)
    \rhColor c #(x11-color 'LawnGreen)
    \rhColor C #(x11-color 'LawnGreen)
    \rhColor i #(x11-color 'GreenYellow)
    \rhColor iii #(x11-color 'MediumSeaGreen)
    \rhColor d #(x11-color 'Gold)
    %{ \rhColor ax #(x11-color 'Khaki) %}
    \rhColor ii #(x11-color 'DarkKhaki)
    \rhColor f #(x11-color 'Tomato)
    \rhColor g #(x11-color 'IndianRed)
    \rhColor h #(x11-color 'Magenta)
    \rhColor H #(x11-color 'Orchid)
    \rhColor HH #(x11-color 'Orchid)
    \rhColor ah #(x11-color 'Khaki)
    \rhColor ax #(x11-color 'PaleGoldenrod)
    \rhColor er #(x11-color 'BurlyWood2)
    \rhColor ER #(x11-color 'BurlyWood1)
    \rhColor eh #(x11-color 'Green)
    \rhColor ehr #(x11-color 'Green3)



  % Pickup
  His\rh i0 palms\rh a0
  % Bar 1
  are\rh b1 swea\rh b2 -- ty,\rh b3
  knees\rh c0 weak,\rh C0
  arms\rh a0 are\rh b0 hea\rh b0 --
  vy\rh b0 There's\rh ehr0 vo\rh a0 -- mit\rh i0
  % Bar 2
  on\rh a0 his\rh i0 swea\rh B2 -- ter\rh B1
  al\rh b0 -- rea\rh b0 -- dy,\rh b0
  mom's\rh a0 spa\rh b0 -- ghe\rh b0 --
  tti\rh b0 He's\rh i0 ner\rh er0 -- vous\rh ax0
  % Bar 3
  but\rh ax0 on\rh a0 the\rh ax0
  sur\rh er0 -- face\rh ax0 he\rh c0 looks\rh iii0
  calm\rh a0 and\rh b0 rea\rh b0 --
  dy\rh b0 To\rh iii0
  drop\rh a0
  % Bar 4
  bombs\rh a0
  but\rh b1 he\rh b3 keeps\rh b3
  on\rh a0 for\rh b0 -- ge\rh b0 --
  tting\rh b0 What\rh ah0 he\rh i0 wrote\rh f0
  % Bar 5
  down,\rh g0 the\rh ax0
  whole\rh f0 crowd\rh g0
  goes\rh f0 so\rh f0 loud\rh g0
  He\rh i0 o\rh f0 --
  % Bar 6
  pens\rh i0 his\rh i0 mouth,\rh g0
  but\rh ah0 the\rh ax0 words\rh er0
  won't\rh f0 come\rh ax0 out\rh g0
  He's\rh i0 cho\rh f0 --
  % Bar 7
  king,\rh i0 how?\rh g0
  Eve\rh eh0 -- ry\rh i0 -- bo\rh ah0 -- dy's\rh i0
  jo\rh f0 -- king\rh i0 now\rh g0
  The\rh ax0 clock's\rh f0
  % Bar 8
  run\rh ax0 out,\rh g0
  time's\rh i0 up,\rh ah0
  o\rh f0 -- ver,\rh er0 blaow!\rh g0
  Snap\rh h0
  % Bar 9
  back\rh H0 to\rh H0 re\rh H0 --
  a\rh h0 -- li\rh h0 -- ty,\rh h0 oh\rh f0
  there\rh ehr0 goes\rh f0
  gra\rh h0 -- vi\rh h0 -- ty,\rh h0 oh\rh f0
  % Bar 10
  There\rh ehr0 goes\rh f0
  Ra\rh h0 -- bbit,\rh h0 he\rh h0 choked,\rh f0
  he's\rh i0 so\rh f0
  mad\rh H0 but\rh H0 he\rh H0 won't\rh f0
  % Bar 11
  Give\rh i0 up\rh f0
  that\rh h0 ea\rh h0 -- sy,\rh h0 no,\rh f0
  he\rh i0 won't\rh f0
  have\rh h0 it,\rh h0 he\rh h0 knows\rh f0
  % Bar 12
  His\rh i0 whole\rh f0
  back's\rh H0 to\rh H0 these\rh H0 ropes,\rh f0
  it\rh i0 don't\rh f0
  ma\rh H0 -- tter,\rh H0 he's\rh H0 dope\rh f0
  % Bar 13
  He\rh i0 knows\rh f0
  that\rh H0 but\rh H0 he's\rh H0 broke,\rh f0
  he's\rh i0 so\rh f0
  stag\rh h0 -- nant,\rh h0 he\rh h0 knows\rh f0
  % Bar 14
  When\rh eh0 he\rh i0 goes\rh f0
  back\rh H0 to\rh H0 this\rh H0 mo\rh f0 --
  bile\rh iii0 home,\rh f0
  that's\rh h0 when\rh h0 it's\rh h0
  % Bar 15
  Back\rh H0 to\rh H0 the\rh H0
  lab\rh HH0 a\rh HH0 -- gain\rh HH0 yo,\rh f0
  this\rh i0 whole\rh f0
  rhap\rh h0 -- so\rh h0 -- dy,\rh h0 Bet\rh eh0 --
  % Bar 16
  ter\rh ER0 go\rh f0 cap\rh H0 -- ture\rh H0
  this\rh H0 mo\rh f0 -- ment\rh eh0 and\rh ax0
  hope\rh f0 it\rh c0 don't\rh f0
  pass\rh h1 him\rh h3
  }

songSylsNew =
  \lyricmode {
    \partial 4

    \bracketSettings
    %{ \bSet %}
    \bracketTextBeforeBreak
    %{ \override Score.AnalysisNumber.outside-staff-priority = ##f %}
    %{ \override Score.AnalysisNumber.direction = #DOWN %}
  % Pickup
    \phrasingSlurDotted \slurDashed
                r16
                \domTupLabel "[a1]" \domTupUP {
                %\multiSet
                \innerNestedBracket "[[ab1]]"
% his palms
h-ih-z16\finger \markup { \teeny \smaller \smaller \smaller "i"}
@p-aa-lmz8 } |
  % Bar 1
                \domTupLabel "[b1]" \domTupUP {
% are
%{ \once \override Fingering.direction = #down %}
Q-aa-r16
%{ \finger \markup { \teeny \smaller \smaller \smaller  "1"} %}
% swea-ty
`sw-eh -- \eB t-iy8 }
                \domTupLabel "[c1]" \domTupUP {
% knees
@n-iy-z }
                \domTupLabel "[c2]" \domTupUP {
% weak
`w-iy-k }
                \domTupLabel "[A2]" \domTupUP {
                %\multiSet
                \innerNestedBracket "[[AB2]]"
% arms
@Q-aa-rm }
                \domTupLabel "[b2]" \domTupUP {
% are
z-aa-r16
% hea-
`h-eh --
                \eB
% -vy
v-iy  }
                \domTupLabel "[a⟺3]" \domTupUP {
                %\multiSet
                \onceBracketTextAfterBreak
                %{ \once \override Score.HorizontalBracketText.direction = #1 %}
                \outerNestedBracket "[[aabb3]]"
% theres
D-eh-rz\finger \markup { \teeny \smaller \smaller \smaller  "ii"}

                \innerNestedBracket "[[aa1]]"
% vo-mit
`v-aa -- m-ih-t\finger \markup { \teeny \smaller \smaller \smaller "i"}
 } |

% Bar 2
                \domTupLabel "[a⟺4]" \domTupUP {
% on his
`Q-aa-n \eb h-ih-z\finger \markup { \teeny \smaller \smaller \smaller "i"}
 }
                \domTupLabel "[b⟺3]"
                \domTupUP {
% swea-ter
`sw-eh -- t-er }
                \domTupLabel "[b4]" \domTupUP {
% all
Q-aa-l --
% rea-dy
`r-eh -- \eB d-iy8 }
                \domTupLabel "[A5]" \domTupUP {
                %\multiSet
                \innerNestedBracket "[[AB4]]"
% mom's spa-
@m-aa-mz }
                \domTupLabel "[b5]" \domTupUP {
% spa-
sp-ah16 --
% -ghe
`g-eh --
                \eB
% -ti
t-iy16 }
                \domTupLabel "[d1]" \domTupUP {
% he's
h-iy-z\finger \markup { \teeny \smaller \smaller \smaller "i"}

% ner-vous
`n-er -- v-ah-s } |

% Bar 3
                    r16.
                \domTupLabel "[a6]" \domTupUP {
% but
b-ah-t32\finger \markup { \teeny \smaller \smaller \smaller  "ə"}
% on
`Q-aa-n16 }
                \domTupLabel "[d2]" \domTupUP
% the
{ D-ah\finger \markup { \teeny \smaller \smaller \smaller  "ə"}
% sur-face
`s-er -- f-ah-s }
                %{ \domTupLabel "c3" \domTupUP { %}
                \domTupLabel "[A7]" \domTupUP {
% he
h-iy\finger \markup { \teeny \smaller \smaller \smaller "i"}

                %{ \domTupLabel "A7" \domTupUP { %}
% looks
l-uh-ks
                %\multiSet
                \innerNestedBracket "[[AB5]]"
% calm and
@k-aa-lm8 }
                \domTupLabel "[B6]" \domTupUP {
% and
Q-ah-nd16
% rea-
`r-eh --
                \eB
% -dy
d-iy }
% to
                \domTupLabel "[a8]" \domTupUP {
                %\multiSet
                \onceBracketTextAfterBreak
                \innerNestedBracket "[[ab6]]"
t-uw
                \once \override Score.HorizontalBracket.shorten-pair = #'(-15 . -1.5)
                \innerNestedBracket "[[aa2]]"
% drop
`dr-aa-p8 } |
% Bar 4
                \domTupLabel "[a9]" \domTupUP {
                    \eB
% bombs
@b-aa-mz8 } r8
% but he
                \domTupLabel "[b7]" \domTupUP {
`b-ah-t16 h-iy
                    \eB
% keeps
`k-iy-ps8 }
                \domTupLabel "[A10]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[AB7]]"
% on for-
@Q-aa-n }
                \domTupLabel "[B8]" \domTupUP {
%  for-
f-er16 --
% -get
`g-eh --
                     \eB
% -ting}
t-ih-G16 }
                \domTupLabel "[E1]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EF1]]"
% what he
`w-ah-t\finger \markup { \teeny \smaller \smaller \smaller  "ə"}
h-iy\finger \markup { \teeny \smaller \smaller \smaller "i"}

% wrote
`r-ow-t } |

% Bar 5
                    _16
                  \domTupLabel "[F1]" \domTupUP {
                  \eB
% down
@d-aw-n8 }
                  \domTupLabel "[E2]" \domTupUP {
                  %\multiSet
                \innerNestedBracket "[[EF*2]]"
% the
D-ah16\finger \markup { \teeny \smaller \smaller \smaller  "ə"}

% whole
`h-ow-l8 }
                \domTupLabel "[F2]" \domTupUP {
                    %{ \eB %}
% crowd
@kr-aw-d }
                \domTupLabel "[E3]" \domTupUP {
                    %\multiSet
                %{ \innerNestedBracket "[[EF3]]" %}
% goes so
`g-ow-z }
% goes so
                \domTupLabel "[F3]" \domTupUP {
s-ow16
                    \eB
% loud
`l-aw-d }
                    _16
                \domTupLabel "[E4]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EF3]]"
% he
h-iy\finger \markup { \teeny \smaller \smaller \smaller "i"}

% o-
`y-ow8 -- } |

% Bar 6
                \domTupLabel "[F4]" \domTupUP {
% -pens his
p-ih-nz16\finger \markup { \teeny \smaller \smaller \smaller "i"}
 h-ih-z\finger \markup { \teeny \smaller \smaller \smaller "i"}

                    \eB
% mouth
`m-aw-T8 }

                \domTupLabel "[XX]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EF*4]]"
% but the
`b-ah-t16
D-ah\finger \markup { \teeny \smaller \smaller \smaller  "ə"}
                %{ } \domTupLabel "XX" \domTupUP { %}
% words
`w-er-dz8%\finger \markup { \teeny \smaller \smaller \smaller "ɝ" }
                } \domTupLabel "[E5]" \domTupUP {
% wont
`w-ow-nt }
                \domTupLabel "[F5]" \domTupUP {
%  come
k-ah16\finger \markup { \teeny \smaller \smaller \smaller  "ə"}
                    \eB
% out
`m-aw-t }
                    r16
                \domTupLabel "[E6]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EF5]]"
% he's
h-iy-z\finger \markup { \teeny \smaller \smaller \smaller "i"}

% cho-
`C-ow8  -- } |

% Bar 7
                \domTupLabel "[F6]" \domTupUP {
% -king
k-ih-G16\finger \markup { \teeny \smaller \smaller \smaller "i"}

                    \eB
% how
@h-aw8 } r16

                \domTupLabel "[XX]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EF*6]]"
% eve-ry
`Q-eh-v16\finger \markup { \teeny \smaller \smaller \smaller  "ii"} --
r-iy\finger \markup { \teeny \smaller \smaller \smaller "i"} --
                } \domTupLabel "[XX]" \domTupUP {
% bo-dys
`b-ah --
d-iy-z\finger \markup { \teeny \smaller \smaller \smaller "i"}
                } \domTupLabel "[E7]" \domTupUP {
% jo-
`J-ow8 --
                } \domTupLabel "[F7]" \domTupUP {
% -king
k-ih-G16\finger \markup { \teeny \smaller \smaller \smaller "i"}
                    \eB
% now
@n-aw }
                    r16
                \domTupLabel "[E8]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EF7]]"
% the
D-ah\finger \markup { \teeny \smaller \smaller \smaller  "ə"}
% clocks
`kl-aa-ks8 } |
% Bar 8
                \domTupLabel "[F8]" \domTupUP {
% run
r-ah-n16\finger \markup { \teeny \smaller \smaller \smaller  "ə"}
                    \eB
% out
@Q-aw-t8 } r16

                \domTupLabel "[E9]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EF*8]]"
% times up
`t-ay-mz8
                %{ } \domTupLabel "XX" \domTupUP { %}
%  up
Q-ah-p\finger \markup { \teeny \smaller \smaller \smaller  "ə"}
                %{ } \domTupLabel "XX" \domTupUP { %}
% o-
`Q-ow --
                } \domTupLabel "[F9]" \domTupUP {
% ver blaow
v-er16\finger \markup { \teeny \smaller \smaller \smaller  "ɚ"}

                    \eB
% ver blaow
@bl-aw16
                  } _16 r16
                \domTupLabel "[g1]" \domTupUP {
% snap
`sn-ae-p8 } |
% Bar 9

                \domTupLabel "[G*2]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[G*GE1]"
% back to re-
@b-ae-k8 t-uw16 r-iy -- }
                \domTupLabel "[G3]" \domTupUP {
% -a-li-ty
`y-ae l-ih -- t-iy }
                \domTupLabel "[E*10]" \domTupUP {
                    \eB
% oh
@Q-ow }
                    r16
                \domTupLabel "[E11]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EGE2]]"
% there
D-eh-r16\finger \markup { \teeny \smaller \smaller \smaller  "ii"}

% goes
`g-ow-z8 }
                \domTupLabel "[G4]" \domTupUP {
% gra-vi-ty
`gr-ae16 -- v-ih -- t-iy }
                \domTupLabel "[E*12]" \domTupUP {
                    \eB
% oh
@Q-ow } |
% Bar 10
                    r16
                \domTupLabel "[E13]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EGE3]]"
% there
D-eh-r16\finger \markup { \teeny \smaller \smaller \smaller  "ii"}
% goes
`g-ow-z8 }
                \domTupLabel "[G5]" \domTupUP {
% rab-bit he
`r-ae16 -- b-ih-t h-iy }
                \domTupLabel "[[E*14]]" \domTupUP {
                    \eB
% choked
@C-ow-kt }
                    r16
                \domTupLabel "[E15]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EGE4]]"
% hes
h-iy-z16\finger \markup { \teeny \smaller \smaller \smaller "i"}

% so
`s-ow8 }
                \domTupLabel "[G*6]" \domTupUP {
% mad but he
`m-ae-d16 b-ah-t h-iy }
                \domTupLabel "[E*16]" \domTupUP {
                    \eB
% wont
@w-ow-nt } |
% Bar 11
                    _16
                \domTupLabel "[E17]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EGE5]]"
% give
g-ih16\finger \markup { \teeny \smaller \smaller \smaller "i"}

% up
`<>v-uh-p8 }
                \domTupLabel "[G7]" \domTupUP {
% that ea-sy
`D-ae-t16 Q-iy -- z-iy }
                \domTupLabel "[E*18]" \domTupUP {
                    \eB
% no
@n-ow }
                    r16
                \domTupLabel "[E19]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EGE6]]"
% he
h-iy\finger \markup { \teeny \smaller \smaller \smaller "i"}

% wont
`w-ow-nt8 }
                \domTupLabel "[G8]" \domTupUP {
% have it he
`h-ae16 v-ih-t h-iy }
                \domTupLabel "[E*20]" \domTupUP {
                    \eB
% knows
@n-ow-z } |
% Bar 12
                    _16
                \domTupLabel "[E21]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EGE7]]"
% hes
h-ih-z\finger \markup { \teeny \smaller \smaller \smaller "i"}

% whole
`h-ow-l8 }
                \domTupLabel "[G*9]" \domTupUP {
% backs to these
`b-ae-ks16 t-ah D-iy-z }
                \domTupLabel "[E*22]" \domTupUP {
                    \eB
% ropes
@r-ow-ps }
                    r16
                \domTupLabel "[E23]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EGE8]]"
% it
Q-ih-t\finger \markup { \teeny \smaller \smaller \smaller "i"}

% dont
`d-ow-nt8 }
                \domTupLabel "[G*10]" \domTupUP {
% mat-ter hes
`m-ae16 -- t-er h-iy-z }
                \domTupLabel "[E*24]" \domTupUP {
                    \eB
% dope
@d-ow-p } |
% Bar 13
                    r16
                \domTupLabel "[E25]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EGE9]]"
% he
h-iy\finger \markup { \teeny \smaller \smaller \smaller "i"}

% knows
`n-ow-z8 }
                \domTupLabel "[G*11]" \domTupUP {
% that but hes
`D-ae-t16 b-ah-t h-iy-z }
                \domTupLabel "[E*26]" \domTupUP {
                    \eB
% broke
@br-ow-k }
                    r16
                \domTupLabel "[E27]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EGE10]]"
% hes
h-iy-z\finger \markup { \teeny \smaller \smaller \smaller "i"}

% so
`s-ow8 }
                \domTupLabel "[G12]" \domTupUP {
% stag-nant he
`st-ae-g16 -- n-ih-nt h-iy }
                \domTupLabel "[E*28]" \domTupUP {
                    \eB
% knows
@n-ow-z } |
% Bar 14
                    _16
                \domTupLabel "[E29]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EGE11]]"
% when he
w-eh-n32\finger \markup { \teeny \smaller \smaller \smaller  "ii"}
h-iy\finger \markup { \teeny \smaller \smaller \smaller "i"}

% goes
`g-ow-z8 }

                \domTupLabel "[G*13]" \domTupUP {
                    \tuplet 3/2 8 {
% back to
`b-ae-k8 t-ah16 }
% this -
D-ih-s16 }
                \domTupLabel "[E*30]" \domTupUP {
                    \eB
%  mo-
@m-ow --
    _16 }
                \domTupLabel "[E31]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EGE12]]"
% -bile
b-el\finger \markup { \teeny \smaller \smaller \smaller  "ɫ̩"}
% home
`h-ow-m8 }

                \domTupLabel "[G14]" \domTupUP {
                    \tuplet 3/2 8 {
% thats when
`D-ae-ts8 w-eh-n16 }
                    \eB
% its
Q-ih-ts16 } r16 |
% Bar 15

                \domTupLabel "[G*15]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[G*EG13]]"
% back to the
@b-ae-k8 t-uw16 D-ah }
                \domTupLabel "[G*16]" \domTupUP {
% lab a-gain
`l-ae b-ah -- g-ih-n }
                \domTupLabel "[E*32]" \domTupUP {
                    \eB
% yo
@y-ow }
                    r16
                \domTupLabel "[E33]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[EGE14]]"
% This
D-ih-s\finger \markup { \teeny \smaller \smaller \smaller "i"}

% whole
`h-ow-l8
              }  \domTupLabel "[G17]" \domTupUP {
% rhap-so-
`r-ae-p16 -- s-ih --
                    \eB
% -dy
d-iy }
                \domTupLabel "[e34]" \domTupUP {
                    %\multiSet
                \innerNestedBracket "[[ege15]]"
% bet-
`b-eh\finger \markup { \teeny \smaller \smaller \smaller  "ii"} -- |
% Bar 16
% -ter go
t-er\finger \markup { \teeny \smaller \smaller \smaller  "ɚ"}
g-ow }
                \domTupLabel "[g*18]" \domTupUP {
% cap-ture
`k-ae-p -- C-er
% This
D-ih-s }
                \domTupLabel "[e35]" \domTupUP {
                    %{ \eB %}
% mo-
`m-ow --
                \eb    %\multiSet
% - ment
m-eh-nt\finger \markup { \teeny \smaller \smaller \smaller  "ii"}
                } \domTupLabel "[e36]" \domTupUP {
                \innerNestedBracket "[[E*EG16]]"
% and
Q-en\finger \markup { \teeny \smaller \smaller \smaller  "n̩"}
% hope
`h-ow }
                \domTupLabel "[e37]" \domTupUP {
%  it
p-ih-t\finger \markup { \teeny \smaller \smaller \smaller "i"}

% dont
`d-ow-nt8 }
                \domTupLabel "[G*19]" \domTupUP {
% pass
@p-ae-s16
                    \eB
% him
h-ih-m r8 } |
  }

\lyricNotation \songLyricsDomains \songSylsNew
%{ \pageBreak %}
%{ \lyricNotation \songLyricsDomains \songSylsOld %}

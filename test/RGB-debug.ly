\version "2.22.0"

\include "../lyric-notation.ily"



debugRGBLyrics =
  \lyricmode {
    \rgbColors
    %{ \shrinkLyrics 1 %}
    GGG\rh GGG0
    GGR\rh GGR0
    GRR\rh GRR0
    RGG\rh RGG0
    RRG\rh RRG0
    RRR\rh RRR0
    RRR\rh RRR0
    RRR\rh RRR0
    RRB\rh RRB0
    RBB\rh RBB0
    BRR\rh BRR0
    BBR\rh BBR0
    BBB\rh BBB0
    %{ BBG\rh BBG0 %}
    %{ BGG\rh BGG0 %}
    %{ GBB\rh GBB0 %}
    %{ GGB\rh GGB0 %}
    %{ GGG\rh GGG0 %}
  }









debugRGBSyls =
  \lyricmode {
      Q-IY8
      Q-IH16
      t-EY
      d-EH
      J-ae
      D-AY
      z-ah
      h-AW
      T-AA
      G-AO
      b-OW
      y-UH
      s-UW8 r16
      |
  }


\lyricNotation \debugRGBLyrics \debugRGBSyls

\version "2.22.0"

\include "../lyric-notation.ily"

%{
PHONEME DEBUG INPUT
I've tried to include every possible combination
although I'm sure I've missed some. Most syllables
start and end with the same consonant, and so should
be symmetrical in that respect. Lowercase vowels in
lyrics mark where diamond noteheads should be.
  %}

tableLyrics =
\lyricmode {
  Ah kAhk gAhg CAhC
  JAhJ fahf vahv mahm |
  lahl el er yah |
}

tableSyllables =
\lyricmode {
  Q-ah4 k-ah-k g-ah-g C-ah-C |
  J-ah-J f-ah-f v-ah-v m-ah-m |
  l-ah-l l-ah-L r-er y-ah |
}

\lyricNotation \tableLyrics \tableSyllables
% \pageBreak
% \oneLineLyricNotation \tableLyrics \tableSyllables

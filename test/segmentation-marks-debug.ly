\version "2.22.0"

\include "../lyric-notation.ily"

\lyricNotation
\lyricmode {
  foo foo
  foo
  foo foo foo foo
  foo foo
}
\lyricmode {
  \rhColor a blue
  \rhColor b green
  @f-UW8 @f-UW]8
  f-UW]4
  f-UW]16\rh a1
  f-UW16\rh a2
  f-UW16\rh a3
  f-UW]16\rh a4
  f-UW8\rh b0
  f-UW]8\rh b0
}
\version "2.22.0"

\include "../lyric-notation.ily"

\lyricNotation
\lyricmode {
  foo!
}
\lyricmode {
  \rhColorGradient a red blue
  `f-UW\rh a0
}
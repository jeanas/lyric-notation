\version "2.22.0"

\include "../lyric-notation.ily"

\lyricNotation
\lyricmode {
  foo
}
\lyricmode {
  f-UW4 \tuplet 3/2 { _8 _8 _8 }
}
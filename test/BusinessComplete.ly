\version "2.22.0"
\include "../lyric-notation.ily"

businessColors = \lyricmode {
  \rhColor a #(x11-color 'Gold2)
    \rhColor A #(x11-color 'Gold2)
    \rhColor b #(x11-color 'DeepPink2)
    \rhColor B #(x11-color 'DeepPink2)
    \rhColor BB #(x11-color 'DeepPink1)
    \rhColor c #(x11-color 'LightSkyBlue)
    \rhColor C #(x11-color 'LightSkyBlue)
    %{ \rhColor CC #"#afeeee" %}
    \rhColor CC #(x11-color 'DeepSkyBlue)
    \rhColor d #(x11-color 'LightSalmon)
    \rhColor D #(x11-color 'Salmon)
    \rhColor e #(x11-color 'GreenYellow)
    \rhColor ee #(x11-color 'GreenYellow)
    \rhColor E #(x11-color 'SpringGreen)
    \rhColor EE #(x11-color 'SpringGreen)
    \rhColor f "#d2d229"
    \rhColor F "#d2d229"
    \rhColor g #(x11-color 'MediumSpringGreen)
    \rhColor G #(x11-color 'MediumSpringGreen)
    \rhColor H #(x11-color 'GreenYellow)
    \rhColor h #(x11-color 'GreenYellow)
    \rhColor i "#7fffd4"
    \rhColor I "#7fffd4"
    }

songLyricsA-MB =
  \lyricmode {
    \businessColors
  You\rh d0 'bout\rh a0 to\rh d0
  wit\rh c0 -- ness\rh C0 hip\rh c0
  -- hop\rh a0 in\rh c0
  its\rh C0 most\rh b0
  pur\rh D0 -- est\rh c0
  Most\rh b0 raw\rh a0
  -- est\rh c0 form,\rh BB0 flow\rh b0
  al\rh a0 -- most\rh b0
  flaw\rh a0 -- less\rh c0
  Most\rh b0 hard\rh a0
  -- est,\rh c0 most\rh b0 ho\rh a0
  -- nest\rh c0 known\rh b0
  ar\rh a0 -- tist\rh c0
  Chip\rh C0 off\rh a0
  the\rh CC0 old\rh b0 block,\rh a0
  but\rh e0 old\rh b0
  Doc\rh a0 is\rh c0 (back)\rh E0
  }

songSylsGroovesA =
  \lyricmode {
    \partial 4
    \genGrooveTupSettings
    \bracketSettings
    \grooveTupletTextBeforeBreak
     %{ \override Stem.details.beamed-lengths = #'(12) %}

      %{ \set TextScript.outside-staff-priority = ##f %}
      %{ \set Script.outside-staff-priority = ##f %}
      %{ \override TupletBracket.padding = #0.6 %}
        %{ \override TupletBracket.direction = #UP %}
        %{ \override Voice.TupletBracket.layer = #3 %}
        %{ \override Voice.HorizontalBracketText.layer = #4 %}
        %{ \override TupletNumber.font-size = #-4 %}
        %{ \override TupletNumber.font-shape = #'upright %}
        %{ \override TupletNumber.font-series = #'bold %}
        %{ \override TupletNumber.whiteout = ##t %}
        %{ \override TupletBracket.edge-height = #'(0.4 . 0.4) %}
        %{ \override TupletBracket.thickness = #1 %}

    \gTwo {
  r16
  y-uw16 }
    \gTwo {
  `b-aw
  t-uw }
  |
    \gClass "<323_2222>-0" {
    \gTre {
  @w-ih-t8
  n-ih-s16 }
    \gTwo {
  `h-ih-p16
    _16 }
    \gTre {
  `h-aa-p8
  Q-ih-n16 }
    \gTwo {
  `Q-ih-ts8 }
    \gTwo {
  \labeledBracket "A*"
  `m-ow-st }
    \gTwo {
  @py-uh-r }
    \gTwo {
  \eB
  r-ih-st } }
  |
    \gTwo {
  \labeledBracket "A1"
  `m-ow-st }
    \gClass "<323_2222>-2" {
    \gTre {
  @r-aa
  \eB
  Q-ih-st16 }
    \gTwo {
  `f-ao-rm8 }
    \gTre {
  `fl-ow16 _16
  Q-aa-l }
    \gTwo {
  \labeledBracket "A1"
  `m-ow-st8 }
    \gTwo {
  @fl-aa }
    \gTwo {
  \eB
  l-ih-s }
    |
    \gTwo {
  \labeledBracket "A1"
  `m-ow-st }
    }
    \gClass "<323_2222>-2" {
    \gTre {
  @h-aa-r
  \eB
  d-ih-st16 }
    \gTwo {
  \labeledBracket "A1"
  `m-ow-st8 }
    \gTre {
  @Q-aa16 _16
  \eB
  n-ih-st }
    \gTwo {
  \labeledBracket "A1"
  `n-ow-n8 }
    \gTwo {
  @Q-aa-r }
    \gTwo {
  \eB
  t-ih-st }
  |
    \gTwo {
  `C-ih-p }
    }
    \gClass "<323_233>-2?" {
    \gTre {
  @Q-aa-f
  D-iy16 }
    \gTwo {
  \labeledBracket "A2"
  `Q-ow-ld8 }
    \gTre {
  @bl-aa-k16 _16
  \eB b-ah-t }
    \gTwo {
  \labeledBracket "A1"
  `Q-ow-ld8 }
    \gTre {
  @d-aa-k8
  \eB
  Q-ih-z16 }
    \once \override TupletBracket.connect-to-neighbor = #'(#f . #t)
    \gTre {
  `b-ae-k
    | } }
  }

songLyricsB-MB =
  \lyricmode {
    \businessColors
  old\rh b0 Doc\rh a0 is\rh c0 (back)\rh E0 |
    Looks\rh d0 like\rh f0 Bat\rh E0 -- man\rh EE0 brought\rh a0 his\rh c0 own\rh b0 Ro\rh a0 -- bin\rh c0 |
    Oh\rh b0 God,\rh a0 Sad\rh e0 -- dam's\rh A0 got\rh a0 his\rh C0 own\rh b0 La\rh a0 -- den\rh C0 |
    With\rh c0 his\rh C0 own\rh b0 pri\rh F0 -- vate\rh E0 plane,\rh g0 his\rh c0 own\rh b0 pi\rh F0 -- lot\rh e0 |
    Set\rh E0 to\rh d0 blow\rh b0   col\rh a0 -- lege\rh c0 dorm\rh BB0 room\rh d0 doors\rh BB0 off\rh a0 the\rh e0 hin\rh c0 |
    }

songSylsGroovesB =
  \lyricmode {
    \partial 4.
      \bracketSettings
      \grooveTupletTextBeforeBreak
      \genGrooveTupSettings
        %{ \override HorizontalBracket.direction = #UP %}
        %{ \override HorizontalBracket.edge-height = #'(0.0 . 0.0) %}
        %{ \override HorizontalBracket.staff-padding = #3.165 %}
        %{ \override HorizontalBracket.thickness = #-0 %}
        %{ \override HorizontalBracketText.font-size = #-2.75 %}
        %{ \override HorizontalBracketText.font-series = #BOLD %}
        %{ \override HorizontalBracket.shorten-pair = #'(2 . 3.85) %}
        %{ \override HorizontalBracketText.font-shape = #italic %}
        %{ \override HorizontalBracket.bracket-visibility = ##f %}
        %{ \override HorizontalBracket.bracket-flare = #'(0.0 . 0.0) %}
      %{ \override TupletBracket.padding = #0.9 %}
      \set Score.currentBarNumber = #5

  \gClass "<333232>-12?" {
      %{ % \once \override HorizontalBracket.shorten-pair = #'(2.3 . 0) %}
    \gTwo {
  \labeledBracket "A1"
  `Q-ow-ld8 }
    \gTre {
  @d-aa-k8
    \eb
  Q-ih-z16 }
    \gTre {
  `b-ae-k
  |
  r16 l-UH-ks16 }
    } \gClass "<332_2222>-4" { \gTwo {
  `l-AY-k8 }
    \gTre {
  `b-AE-t m-AE-n16 }
    \gTre {
          % \once \override HorizontalBracket.shorten-pair = #'(2 . 0)
      % \sG \gC-Four
  @br-AA-t _16 h-IH-z
    } \gTwo {
  \labeledBracket "A1"
  `Q-OW-n8 }
    \gTwo {
  @r-AA }
    \gTwo {
  \eB
  b-IH-n }
  |
    \gTwo {
    \once \override Score.HorizontalBracket.thickness = #5
  \labeledBracket "A1+2"
  Q-OW  }
    } \gClass "<323_2222>-2" { \gTre {
  `g-AA-d s-Ax16 }
    \gTwo {
  `d-AA-z8 }
    \gTre {
      % \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
      % \sG \gA-Two
  `g-AA-t16 _16
  \eB
  h-IH-z }
    \gTwo {
  \labeledBracket "A1⇒2"
  `Q-OW-n8 }
    \gTwo {
  @l-AA }
    \gTwo {
  \eB
  d-EH-n }
  |
    \gTwo {
  `w-IH-D16 h-IH-z }
    }
    \gClass "<332_2222>-4" {
    \gTwo {
  \labeledBracket "A2"
  `Q-OW-n8 }
    \gTre {
  `pr-AY
  \eB
  v-AX-t16 }
    \gTre {
        % \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
    % \sG \gC-Four
  `pl-EY-n _16
  h-IH-z }
    \gTwo {
  \labeledBracket "A2"
  `Q-OW-n8 }
    \gTwo {
  @p-AY }
    \gTwo {
  \eB
  l-AX-t }
  |
    \gTwo {
  `s-EH-t16
  t-UW }
    }
    \gClass "<332_332>-12" {
    \gTwo {
  \labeledBracket "A1"
  `bl-OW8 }
    \gTre {
      % \once \override HorizontalBracket.shorten-pair = #'(0 . 0)
    % \sG \gD-Doz
  `k-AA
  \eB
  l-IH-J16 }
    \gTre {
  `d-AO-rm _16
  r-UW-m }
    \gTwo {
  \labeledBracket "A1+2" % ⇒⤇⇒⇒⇒⥤⥤⇒
  `d-AO-rz8 }
    }

        \once \override TupletBracket.connect-to-neighbor = #'(#f . #t)
    \gClass "<323_2222>-12" {
    \gTre {
        % \once \override HorizontalBracket.shorten-pair = #'(1.5 . 0)
        % \once \override HorizontalBracketText.font-size = #-3.1
    % \sG \gA-Doz
  `Q-AA-f
  \eB
  D-AX16 }
        \tweak TupletBracket.connect-to-neighbor #'(#f . #t)
    \gTwo {
@h-IH-n }
    }
  |
  }

songSylsGroovesOneLineNoLabels =
  \lyricmode {
    \partial 4
    \genGrooveTupSettings
    \oneLineBracketSettings
    \grooveTupletTextBeforeBreak
    %{ \override TupletNumber.font-size = #-4 %}
    %{ \override HorizontalBracketText.font-size = #-8 %}
     %{ \override Stem.details.beamed-lengths = #'(12) %}
      %{ \set TextScript.outside-staff-priority = ##f %}
      %{ \set Script.outside-staff-priority = ##f %}
      %{ \override TupletBracket.padding = #0.6 %}
        %{ \override TupletBracket.direction = #UP %}
        %{ \override Voice.TupletBracket.layer = #3 %}
        %{ \override Voice.HorizontalBracketText.layer = #4 %}
        %{ \override TupletNumber.font-shape = #'upright %}
        %{ \override TupletNumber.font-series = #'bold %}
        %{ \override TupletNumber.whiteout = ##t %}
        %{ \override TupletBracket.edge-height = #'(0.4 . 0.4) %}
        %{ \override TupletBracket.thickness = #1 %}

    \gTwo {
  r16
  y-uw16 }
    \gTwo {
  `b-aw
  t-uw }
  |
    \gClass "<323_2222>-0" {
    \gTre {
  @w-ih-t8
  n-ih-s16 }
    \gTwo {
  `h-ih-p16
    _16 }
    \gTre {
  `h-aa-p8
  Q-ih-n16 }
    \gTwo {
  `Q-ih-ts8 }
    \gTwo {
  \bB
  `m-ow-st }
    \gTwo {
  @py-uh-r }
    \gTwo {
  \eB
  r-ih-st } }
  |
    \gTwo {
  \bB
  `m-ow-st }
    \gClass "<323_2222>-2" {
    \gTre {
  @r-aa
  \eB
  Q-ih-st16 }
    \gTwo {
  `f-ao-rm8 }
    \gTre {
  `fl-ow16 _16
  Q-aa-l }
    \gTwo {
  \bB
  `m-ow-st8 }
    \gTwo {
  @fl-aa }
    \gTwo {
  \eB
  l-ih-s }
    |
    \gTwo {
  \bB
  `m-ow-st }
    }
    \gClass "<323_2222>-2" {
    \gTre {
  @h-aa-r
  \eB
  d-ih-st16 }
    \gTwo {
  \bB
  `m-ow-st8 }
    \gTre {
  @Q-aa16 _16
  \eB
  n-ih-st }
    \gTwo {
  \bB
  `n-ow-n8 }
    \gTwo {
  @Q-aa-r }
    \gTwo {
  \eB
  t-ih-st }
  |
    \gTwo {
  `C-ih-p }
    }
    \gClass "<323_233>-2?" {
    \gTre {
  @Q-aa-f
  D-iy16 }
    \gTwo {
  \bB
  `Q-ow-ld8 }
    \gTre {
  @bl-aa-k16 _16
  \eB b-ah-t }
    \gTwo {
  \bB
  `Q-ow-ld8 }
    \gTre {
  @d-aa-k8
    \eb
  Q-ih-z16 }
    \gTre {
  `b-ae-k
  |
  r16 l-UH-ks16 }
    } \gClass "<332_2222>-4" { \gTwo {
  `l-AY-k8 }
    \gTre {
  `b-AE-t m-AE-n16 }
    \gTre {
          % \once \override HorizontalBracket.shorten-pair = #'(2 . 0)
      % \sG \gC-Four
  @br-AA-t _16 h-IH-z
    } \gTwo {
  \bB
  `Q-OW-n8 }
    \gTwo {
  @r-AA }
    \gTwo {
  \eB
  b-IH-n }
  |
    \gTwo {
  \bB
  Q-OW }
    } \gClass "<323_2222>-2" { \gTre {
  `g-AA-d s-Ax16 }
    \gTwo {
  `d-AA-z8 }
    \gTre {
      % \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
      % \sG \gA-Two
  `g-AA-t16 _16
  \eB
  h-IH-z }
    \gTwo {
  \bB
  `Q-OW-n8 }
    \gTwo {
  @l-AA }
    \gTwo {
  \eB
  d-EH-n }
  |
    \gTwo {
  `w-IH-D16 h-IH-z }
    }
    \gClass "<332_2222>-4" {
    \gTwo {
  \bB
  `Q-OW-n8 }
    \gTre {
  `pr-AY
  \eB
  v-AX-t16 }
    \gTre {
        % \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
    % \sG \gC-Four
  `pl-EY-n _16
  h-IH-z }
    \gTwo {
  \bB
  `Q-OW-n8 }
    \gTwo {
  @p-AY }
    \gTwo {
  \eB
  l-AX-t }
  |
    \gTwo {
  `s-EH-t16
  t-UW }
    }
    \gClass "<332_332>-12" {
    \gTwo {
  \bB
  `bl-OW8 }
    \gTre {
      % \once \override HorizontalBracket.shorten-pair = #'(0 . 0)
    % \sG \gD-Doz
  `k-AA
  \eB
  l-IH-J16 }
    \gTre {
  `d-AO-rm _16
  r-UW-m }
    \gTwo {
  \bB
  `d-AO-rz8 }
    }

        \once \override TupletBracket.connect-to-neighbor = #'(#f . #t)
    \gClass "<323_2222>-12" {
    \gTre {
        % \once \override HorizontalBracket.shorten-pair = #'(1.5 . 0)
        % \once \override HorizontalBracketText.font-size = #-3.1
    % \sG \gA-Doz
  `Q-AA-f
  \eB
  D-AX16 }
        \tweak TupletBracket.connect-to-neighbor #'(#f . #t)
    \gTwo {
  @h-IH-n }
    }
  |
  }

songLyricsOneLine =
  \lyricmode {
    \businessColors
  You\rh d0 'bout\rh a0 to\rh d0
  wit\rh c0 -- ness\rh C0 hip\rh c0
  -- hop\rh a0 in\rh c0
  its\rh C0 most\rh b0
  pur\rh D0 -- est\rh c0
  Most\rh b0 raw\rh a0
  -- est\rh c0 form,\rh BB0 flow\rh b0
  al\rh a0 -- most\rh b0
  flaw\rh a0 -- less\rh c0
  Most\rh b0 hard\rh a0
  -- est,\rh c0 most\rh b0 ho\rh a0
  -- nest\rh c0 known\rh b0
  ar\rh a0 -- tist\rh c0
  Chip\rh C0 off\rh a0
  the\rh CC0 old\rh b0 block,\rh a0
  but\rh e0 old\rh b0
  Doc\rh a0 is\rh c0 (back)\rh E0 |
    Looks\rh d0 like\rh f0 Bat\rh E0 -- man\rh EE0 brought\rh a0 his\rh c0 own\rh b0 Ro\rh a0 -- bin\rh c0 |
    Oh\rh b0 God,\rh a0 Sad\rh e0 -- dam's\rh A0 got\rh a0 his\rh C0 own\rh b0 La\rh a0 -- den\rh C0 |
    With\rh c0 his\rh C0 own\rh b0 pri\rh F0 -- vate\rh E0 plane,\rh g0 his\rh c0 own\rh b0 pi\rh F0 -- lot\rh e0 |
    Set\rh E0 to\rh d0 blow\rh b0   col\rh a0 -- lege\rh c0 dorm\rh BB0 room\rh d0 doors\rh BB0 off\rh a0 the\rh e0 hin\rh c0 |
    }
songSylsGroovesOneLine =
  \lyricmode {
    \partial 4
    \genGrooveTupSettings
    \oneLineBracketSettings
    \grooveTupletTextBeforeBreak
    %{ \override TupletNumber.font-size = #-4 %}
    %{ \override HorizontalBracketText.font-size = #-8 %}
     %{ \override Stem.details.beamed-lengths = #'(12) %}
      %{ \set TextScript.outside-staff-priority = ##f %}
      %{ \set Script.outside-staff-priority = ##f %}
      %{ \override TupletBracket.padding = #0.6 %}
        %{ \override TupletBracket.direction = #UP %}
        %{ \override Voice.TupletBracket.layer = #3 %}
        %{ \override Voice.HorizontalBracketText.layer = #4 %}
        %{ \override TupletNumber.font-shape = #'upright %}
        %{ \override TupletNumber.font-series = #'bold %}
        %{ \override TupletNumber.whiteout = ##t %}
        %{ \override TupletBracket.edge-height = #'(0.4 . 0.4) %}
        %{ \override TupletBracket.thickness = #1 %}

    \gTwo {
  r16
  y-uw16 }
    \gTwo {
  `b-aw
  t-uw }
  |
    \gClass "<323_2222>-0" {
    \gTre {
  @w-ih-t8
  n-ih-s16 }
    \gTwo {
  `h-ih-p16
    _16 }
    \gTre {
  `h-aa-p8
  Q-ih-n16 }
    \gTwo {
  `Q-ih-ts8 }
    \gTwo {
  \labeledBracket "A*"
  `m-ow-st }
    \gTwo {
  @py-uh-r }
    \gTwo {
  \eB
  r-ih-st } }
  |
    \gTwo {
  \labeledBracket "A1"
  `m-ow-st }
    \gClass "<323_2222>-2" {
    \gTre {
  @r-aa
  \eB
  Q-ih-st16 }
    \gTwo {
  `f-ao-rm8 }
    \gTre {
  `fl-ow16 _16
  Q-aa-l }
    \gTwo {
  \labeledBracket "A1"
  `m-ow-st8 }
    \gTwo {
  @fl-aa }
    \gTwo {
  \eB
  l-ih-s }
    |
    \gTwo {
  \labeledBracket "A1"
  `m-ow-st }
    }
    \gClass "<323_2222>-2" {
    \gTre {
  @h-aa-r
  \eB
  d-ih-st16 }
    \gTwo {
  \labeledBracket "A1"
  `m-ow-st8 }
    \gTre {
  @Q-aa16 _16
  \eB
  n-ih-st }
    \gTwo {
  \labeledBracket "A1"
  `n-ow-n8 }
    \gTwo {
  @Q-aa-r }
    \gTwo {
  \eB
  t-ih-st }
  |
    \gTwo {
  `C-ih-p }
    }
    \gClass "<323_233>-2?" {
    \gTre {
  @Q-aa-f
  D-iy16 }
    \gTwo {
  \labeledBracket "A2"
  `Q-ow-ld8 }
    \gTre {
  @bl-aa-k16 _16
  \eB b-ah-t }
    \gTwo {
  \labeledBracket "A1"
  `Q-ow-ld8 }
    \gTre {
  @d-aa-k8
    \eb
  Q-ih-z16 }
    \gTre {
  `b-ae-k
  |
  r16 l-UH-ks16 }
    } \gClass "<332_2222>-4" { \gTwo {
  `l-AY-k8 }
    \gTre {
  `b-AE-t m-AE-n16 }
    \gTre {
          % \once \override HorizontalBracket.shorten-pair = #'(2 . 0)
      % \sG \gC-Four
  @br-AA-t _16 h-IH-z
    } \gTwo {
  \labeledBracket "A1"
  `Q-OW-n8 }
    \gTwo {
  @r-AA }
    \gTwo {
  \eB
  b-IH-n }
  |
    \gTwo {
  \labeledBracket "A1+2"
  Q-OW }
    } \gClass "<323_2222>-2" { \gTre {
  `g-AA-d s-Ax16 }
    \gTwo {
  `d-AA-z8 }
    \gTre {
      % \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
      % \sG \gA-Two
  `g-AA-t16 _16
  \eB
  h-IH-z }
    \gTwo {
  \labeledBracket "A1⇒2"
  `Q-OW-n8 }
    \gTwo {
  @l-AA }
    \gTwo {
  \eB
  d-EH-n }
  |
    \gTwo {
  `w-IH-D16 h-IH-z }
    }
    \gClass "<332_2222>-4" {
    \gTwo {
  \labeledBracket "A2"
  `Q-OW-n8 }
    \gTre {
  `pr-AY
  \eB
  v-AX-t16 }
    \gTre {
        % \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
    % \sG \gC-Four
  `pl-EY-n _16
  h-IH-z }
    \gTwo {
  \labeledBracket "A2"
  `Q-OW-n8 }
    \gTwo {
  @p-AY }
    \gTwo {
  \eB
  l-AX-t }
  |
    \gTwo {
  `s-EH-t16
  t-UW }
    }
    \gClass "<332_332>-12" {
    \gTwo {
  \labeledBracket "A1"
  `bl-OW8 }
    \gTre {
      % \once \override HorizontalBracket.shorten-pair = #'(0 . 0)
    % \sG \gD-Doz
  `k-AA
  \eB
  l-IH-J16 }
    \gTre {
  `d-AO-rm _16
  r-UW-m }
    \gTwo {
  \labeledBracket "A1+2" % ⇒⤇⇒⇒⇒⥤⥤⇒
  `d-AO-rz8 }
    }

        \once \override TupletBracket.connect-to-neighbor = #'(#f . #t)
    \gClass "<323_2222>-12" {
    \gTre {
        % \once \override HorizontalBracket.shorten-pair = #'(1.5 . 0)
        % \once \override HorizontalBracketText.font-size = #-3.1
    % \sG \gA-Doz
  `Q-AA-f
  \eB
  D-AX16 }
    \tweak TupletBracket.connect-to-neighbor #'(#f . #t)
    \gTwo {
  @h-IH-n }
    }
  |
  }



songLyricsC-MB =
  \lyricmode {
    \businessColors
  off\rh a0 the\rh e0 hin\rh c0 |
    -- ges\rh c0 Or\rh BB0 -- an\rh c0 -- ges,\rh C0 peach,\rh CC0 pears,\rh E0 plums,\rh e0 syr\rh e0 -- in\rh c0 |
    -- ges\rh C0 (Vrum,\rh e0 vrum!)\rh e0 Yeah,\rh E0 here\rh I0 I\rh F0 come,\rh e0 I'm\rh F0 in\rh c0 |
    -- ches\rh C0 A\rh e0 -- way\rh g0 from\rh e0 you,\rh d0 dear,\rh i0 fear\rh I0 none\rh e0 Hip\rh c0 |
    -- hop\rh a0 is\rh c0 in\rh C0 a\rh e0 state\rh g0 of\rh e0 nine\rh F0 one\rh e0 one,\rh e0 so...\rh b0 |
  }
songSylsGroovesC =
  \lyricmode {
    \partial 4
      %{ \bracketSettings %}
      \grooveTupletTextAfterBreak
      \genGrooveTupSettings
      %{ \override TupletNumber.before-line-breaking =
      #(lambda (grob)
         (if (not-first-broken-spanner? grob)
             (ly:grob-set-property! grob 'stencil '()))) %}
    %{ \override TupletBracket.padding = #0.1 %}
    %{ \override HorizontalBracket.direction = #UP %}
    %{ \override HorizontalBracket.edge-height = #'(0.0 . 0.0) %}
    %{ \override HorizontalBracket.staff-padding = #3.165 %}
    %{ \override HorizontalBracket.thickness = #-0 %}
    %{ \override HorizontalBracketText.font-size = #-2.75 %}
    %{ \override HorizontalBracketText.font-series = #BOLD %}
    %{ \override TupletBracket.shorten-pair = #'(0.0 . 2.0) %}
    %{ \override HorizontalBracket.shorten-pair = #'(2 . 3.85) %}
  \gClass "<323_2222>-12" {
    %{ \once \override TupletBracket.connect-to-neighbor = #'(#t . #f)  %}
      %{ \tweak TupletBracket.connect-to-neighbor #'(#t . #f) %}
      %{ \tweak TupletBracket.edge-text #'("A1+2" . "") %}
      %{ \tweak TupletBracket.shorten-pair #'(2 . 2) %}
      %{ \uB { %}
    \gTre {
  `Q-AA-f8
  \labeledBracket "B1"
  D-AX16 }
    %{ } %}
    \gTwo {
  @h-IH-n
  |
  _16 }
    \gTre {
  \eB
  `J-IH-z8
  \labeledBracket "B"
  Q-AO-r16 }
    \gTwo {
  @<>r-IH-n8 }
    \gTwo {
  \eb
  `J-IH-z }
    \gTwo {
  \labeledBracket "C"
  `p-IY-C }
    %{ \once \override TupletBracket.shorten-pair = #'(0.0 . 2.0) %}
      \gTwo {
  p-EH-rz } }
    \gClass "<323_2222>-12" {
    \gTre {
  \eb
  `pl-EM-z
  \labeledBracket "B1"
  s-er16 }
    \gTwo {
  @Q-IH-n
  |
   _16 }
    \gTre {
  \eb
  `J-IH-z8 r16 }
    \gTwo {
  `vr-EM8 }
    \gTwo {
  vr-EM }
    \gTwo {
  \labeledBracket "C"
  y-AE }
    \gTwo {
  `h-IH-r16
  Q-AY } }
    \gClass "<323_2222>-12" {
    \gTre {
        % \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
     % \sG \gA-Doz
  \eb
  `k-EM8
  \labeledBracket "B"
  Q-AY-m16 }
    \gTwo {
      @Q-IH-n
  |
  _16 }
    \gTre {
  \eb
  `C-IH-z8
  Q-AX 16 }
    \gTwo {
  `w-EY8 }
    \gTwo {
  `fr-EM16
  y-UW }
    \gTwo {
  \labeledBracket "C"
  `d-IH-r8 }
    \gTwo {
  `f-IH-r }
    }
    \gClass "<323_2222>-12" {
    \gTre {
          % \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
     % \sG \gA-Doz
  \eb
  `n-EN r16 }
    \gTwo {
  `h-IH-p
  |
  _16 }
    \gTre {
  `h-AA-p8
  Q-IH-z16 }
    \gTwo {
  `Q-IH-n
  Q-AH }
    \gTwo {
  `st-EY-t
  Q-AX-v }
    \gTwo {
  \labeledBracket "C"
  `n-AY-n8 }
    \gTwo {
  `w-EN }
    }
    \gTwo {
  \eb @w-EN }
    \gTwo {
  s-OW }
  |
    }
songLyricsBAK =
  \lyricmode {
    Looks like Bat -- man brought his own Ro -- bin
    Oh God, Sad -- dam's got his own La -- den
    With his own pri -- vate plane, his own pi -- lot
    Set to blow col -- lege dorm room doors off the hin

    -- ges Or -- an -- ges, peach, pears, plums, syr -- in
    -- ges (Vrum, vrum!) Yeah, here I come, I'm in
    -- ches A -- way from you, dear, fear none Hip
    -- hop is in a state of nine one one, so...
    }

  songLyricsOh =
    \lyricmode {
      \rhColor a red
      \rhColor b lightblue
      \rhColor c yellow
      \rhColor d green
      You 'bout to wit -- ness hip -- hop\rh a0 in its most\rh b0 pur\rh a0 -- est
      Most\rh b0 raw\rh a0 -- est form,\rh b0 flow\rh b0 al -- most\rh b0 flaw\rh a0 -- less
      Most\rh b0 hard\rh a0 -- est, most\rh b0 ho\rh a0 -- nest known\rh b0 ar\rh a0 -- tist
      Chip off\rh a0 the old\rh b0 block,\rh a0 but old\rh b0 Doc\rh a0 is (back) }
  songLyricsRainbowA =
    \lyricmode {
    % 17 Official HTML Colors + DodgerBlue and RoyalBlue
        %{ \goodColors %}
        \pinkRainbow

    You\rh ER0 'bout\rh AW0 to\rh UW0 wit\rh IH0 -- ness\rh Ih0 hip\rh IH0 -- hop\rh AA0 in\rh IH0 its\rh Ih0 most\rh OW0 pur\rh UH0 -- est\rh IH0
    Most\rh OW0 raw\rh AA0 -- est\rh IH0 form,\rh AA0 flow\rh OW0 al\rh AA0 -- most\rh OW0 flaw\rh AA0 -- less\rh IH0
    Most\rh OW0 hard\rh AA0 -- est,\rh IH0 most\rh OW0 ho\rh AA0 -- nest\rh IH0 known\rh OW0 ar\rh AA0 -- tist\rh IH0
    Chip\rh Ih0 off\rh AO0 the\rh IY0 old\rh OW0 block,\rh AA0 but\rh AX0 old\rh OW0
    Doc\rh AA0 is\rh IH0 (back)\rh AE0 }
  songLyricsRainbowB =
    \lyricmode
      {
        \goodColors
        \pinkRainbow

    Doc\rh AA0 is\rh IH0 (back)\rh AE0
    |
    Looks\rh UH0
      like\rh AY0
      Bat\rh AE0
      --
      man\rh Ae0
      brought\rh AO0
      his\rh IH0
      own\rh OW0
      Ro\rh AA0
      --
      bin\rh IH0
    Oh\rh OW0
      God,\rh AA0
      Sad\rh Aa0
      --
      dam's\rh EM0
      got\rh AA0
      his\rh IH0
      own\rh OW0
      La\rh AA0
      --
      den\rh EN0
    With\rh IH0
      his\rh Ih0
      own\rh OW0
      pri\rh AY0
      --
      vate\rh AX0
      plane,\rh EY0
      his\rh IH0
      own\rh OW0
      pi\rh AY0
      --
      lot\rh AX0
    Set\rh EH0
      to\rh UW0
      blow\rh OW0
      col\rh AA0
      --
      lege\rh IH0
      dorm\rh AOR0
      room\rh UW0
      doors\rh AOR0
      off\rh AO0
      the\rh AX0
      hin\rh IH0
       | }
  songLyricsRainbowC =
    \lyricmode
      {
        \goodColors
        \pinkRainbow

      off\rh AO0
      the\rh AX0
      hin\rh IH0
        |
    -- ges\rh Ih0
      Or\rh AOR0
      --
      an\rh IH0
      --
      ges,\rh Ih0
      peach,\rh IY0
      pears,\rh EHR0
      plums,\rh EM0
      syr\rh ER0
      --
      in\rh IH0
    -- ges\rh Ih0
      (Vrum,\rh EM0
      vrum!)\rh Em0
      Yeah,\rh AE0
      here\rh IHR0
      I\rh AY0
      come,\rh EM0
      I'm\rh AY0
      in\rh IH0
    -- ches\rh Ih0
      A\rh AX0
      --
      way\rh EY0
      from\rh EM0
      you,\rh UW0
      dear,\rh IHR0
      fear\rh Ihr0
      none\rh EN0
      Hip\rh IH0
    -- hop\rh AA0
      is\rh IH0
      in\rh Ih0
      a\rh AX0
      state\rh EY0
      of\rh AX0
      nine\rh AY0
      one\rh AX0
      one,\rh AH0
      so...\rh OW0 |
    }
  songLyricsTwoThreeBak =
    \lyricmode {
      \goodColors \pinkRainbow
    Looks like Bat -- man brought his own Ro -- bin
    Oh God, Sad -- dam's got his own La -- den
    With his own pri -- vate plane, his own pi -- lot
    Set to blow col -- lege dorm room doors off the hin

    -- ges Or -- an -- ges, peach, pears, plums, syr -- in
    -- ges (Vrum, vrum!) Yeah, here I come, I'm in
    -- ches A -- way from you, dear, fear none Hip
    -- hop is in a state of nine one one, so...
    }
  songSylsTwoThreeBak =
    \lyricmode {
    r16 l-UH-ks16 `l-AY-k8 `b-AE-t m-AE-n16 @br-AO-t _16 h-IH-z `Q-OW-n8 @r-AA b-IH-n |
    Q-OW `g-AA-d s-AA16 `d-EM-z8 `g-AA-t16 _16 h-IH-z `Q-OW-n8 @l-AA d-EN |
    `w-IH-D16 h-IH-z `Q-OW-n8 `pr-AY v-AX-t16 `pl-EY-n _16 h-IH-z `Q-OW-n8 @p-AY l-AX-t |
    `s-EH-t16 t-UW `bl-OW8 `k-AA l-IH-J16 `d-AO-rm _16 r-UW-m `d-AO-rz8 `Q-AO-f D-AX16 @h-IH-n |

    _16 `J-IH-z8
  Q-AO16 @r-IH-n8 `J-IH-z `p-IY-C p-EH-rz `pl-EM-z s-er16 @Q-IH-n |
    _16 `J-IH-z8 r16 `vr-EM8 vr-EM y-AE `h-IH-r16 Q-AY `k-EM8
  Q-AY-m16 @Q-IH-n |
    _16 `C-IH-z8
  Q-AX 16 `w-EY8 fr-EM16 y-UW `d-IH-r8 `f-IH-r `n-EN r16 `h-IH-p |
    _16 `h-AA-p8
  Q-IH-z16 `Q-IH-n Q-AH `st-EY-t Q-AX-v `n-AY-n8 w-AX-n @w-AH-n s-OW |
    }
  songSylsMB =
    \lyricmode {
      \partial 4
    r16 y-er16 `b-aw-t t-uw |
    @w-ih-t8 n-eh-s16 `h-ih-p16 _16 h-aa-p8
  Q-ih-n16 Q-ih-ts8 \labeledBracket "𝛂*" `m-ow-st @py-uh-r r-ih-st |
    \labeledBracket "𝛂" `m-ow-st @r-ao Q-ih-st16 `f-ao-rm8 `fl-ow16 _16 Q-aa \labeledBracket "𝛂" `m-ow-st8 @fl-ao l-ih-s |
    \labeledBracket "𝛂" `m-ow-st @h-aa-r d-ih-st16 \labeledBracket "𝛂" `m-ow-st8 @Q-aa16 _16 n-ih-st \labeledBracket "𝛂" `n-ow-n8 @Q-aa-r t-ih-st |
    `C-ih-p @Q-ao-f D-iy16 `Q-ow-ld8 @bl-aa-k16 _16 b-ah-t \labeledBracket "𝛂" `Q-ow-ld8 @d-aa-k Q-ih-z16 `b-ae-k
    | }
  songSylsPedal =
    \lyricmode {
      \set Staff.pedalSustainStyle = #'bracket
      %{ \set PianoPedalBracket.to-barline = #t %}
      %{ \once \override PianoPedalBracket.shorten-pair = #'(0 . 2) %}
      %{ \set SustainPedalLineSpanner.to-barline = #t %}
      \partial 4
    r16 y-uw16 `b-aw-t\sustainOn t-uw\sustainOff |
    @w-ih-t8\sustainOn n-ih-s16 `h-ih-p16\sustainOff\sustainOn
    _16 h-aa-p8\sustainOff\sustainOn Q-ih-n16
    Q-ih-ts8\sustainOff\sustainOn `m-ow-st\sustainOff\sustainOn
    @py-uh-r\sustainOff\sustainOn r-ih-st\sustainOff
    |
    `m-ow-st\sustainOff\sustainOn @r-ao\sustainOff\sustainOn
    Q-ih-st16 `f-ao-rm8\sustainOff\sustainOn `fl-ow16\sustainOff\sustainOn
    _16 Q-aa-l `m-ow-st8\sustainOff\sustainOn
    @fl-ao\sustainOff\sustainOn l-ih-s\sustainOff\sustainOn
    |
    `m-ow-st\sustainOff\sustainOn @h-aa-r\sustainOff\sustainOn
    d-ih-st16 `m-ow-st8\sustainOff\sustainOn @Q-aa16\sustainOff\sustainOn
    _16 n-ih-st `n-ow-n8\sustainOff\sustainOn
    @Q-aa-r\sustainOff\sustainOn t-ih-st\sustainOff\sustainOn
    |
    `C-ih-p\sustainOff\sustainOn @Q-ao-f\sustainOff\sustainOn
    D-iy16 `Q-ow-ld8\sustainOff\sustainOn @bl-aa-k16\sustainOff\sustainOn
    _16 b-ah-t `Q-ow-ld8\sustainOff\sustainOn
    @d-aa-k\sustainOff\sustainOn Q-ih-z16 `b-ae-k\sustainOff
    | }

  songLyricsPlain =
    \lyricmode {

    You 'bout to wit -- ness hip -- hop in its most pur -- est
    Most raw -- est form, flow al -- most flaw -- less
    Most hard -- est, most ho -- nest known ar -- tist
    Chip off the old block, but old Doc is (back) }
  songSylsPlain =
    \lyricmode {
      \partial 4
    r16 y-uw16 `b-aw-t t-uw |
    @w-ih-t8 n-ih-s16 `h-ih-p16
    _16 h-aa-p8
  Q-ih-n16
    Q-ih-ts8 `m-ow-st
    @py-uh-r r-ih-st |
    `m-ow-st @r-ao
    Q-ih-st16 `f-ao-rm8 `fl-ow16
    _16 Q-aa-l `m-ow-st8
    @fl-ao l-ih-s |
    `m-ow-st @h-aa-r
    d-ih-st16 `m-ow-st8 @Q-aa16
    _16 n-ih-st `n-ow-n8
    @Q-aa-r t-ih-st |
    `C-ih-p @Q-ao-f
    D-iy16 `Q-ow-ld8 @bl-aa-k16
    _16 b-ah-t `Q-ow-ld8
    @d-aa-k Q-ih-z16 `b-ae-k
    | }
  songSylsPlainBak =
    \lyricmode {
      \partial 4
    r16 y-uw16 `b-aw-t t-uw |
    @w-ih-t8 n-ih-s16 `h-ih-p16
    _16 h-aa-p8
  Q-ih-n16
    Q-ih-ts8 `m-ow-st
    @py-uh-r r-ih-st |
    `m-ow-st @r-ao
    Q-ih-st16 `f-ao-rm8 `fl-ow16
    _16 Q-aa-l `m-ow-st8
    @fl-ao l-ih-s |
    `m-ow-st @h-aa-r
    d-ih-st16 `m-ow-st8 @Q-aa16
    _16 n-ih-st `n-ow-n8
    @Q-aa-r t-ih-st |
    `C-ih-p @Q-ao-f
    D-iy16 `Q-ow-ld8 @bl-aa-k16
    _16 b-ah-t `Q-ow-ld8
    @d-aa-k Q-ih-z16 `b-ae-k
    | }

\lyricNotation \songLyricsA-MB \songSylsGroovesA
  \pageBreak
\lyricNotation \songLyricsB-MB \songSylsGroovesB
  \pageBreak
\oneLineLyricNotation \songLyricsOneLine \songSylsGroovesOneLineNoLabels
  \pageBreak
\oneLineLyricNotation \songLyricsOneLine \songSylsGroovesOneLine



%{ \lyricNotation \songLyricsC-MB \songSylsGroovesC %}
  \pageBreak
%{ \lyricNotation \songLyricsRainbowA \songSylsGroovesA %}
  \pageBreak
%{ \lyricNotation \songLyricsRainbowB \songSylsGroovesB %}
  \pageBreak
%{ \lyricNotation \songLyricsRainbowC \songSylsGroovesC %}

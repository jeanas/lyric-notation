\version "2.22.0"

\include "../lyric-notation.ily"
%{
  PHONEME DEBUG INPUT
  I've tried to include every possible combination
  although I'm sure I've missed some. Most syllables
  start and end with the same consonant, and so should
  be symmetrical in that respect. Lowercase vowels in
  lyrics mark where diamond noteheads should be.
  %}





debugPhonLyrics =
  \lyricmode {
    \pinkRainbow
    \shrinkLyrics 1
    QIY\rh IY1
    kIY\rh Iy2
    gIH\rh IH3
    pIH\rh Ih4
    gIHr\rh IHR5
    gIHr\rh Ihr6
    bey\rh EY7
    tEY\rh Ey8
    dEH\rh EH9
    CEH\rh Eh10
    CEHr\rh EHR11
    CEHr\rh Ehr12
    Jae\rh AE13
    Jae\rh Ae14
    DAY\rh AY15
    DAY\rh Ay16
    zen\rh EN17
    zen\rh En18
    zah\rh AH19
    zah\rh Ah20
    vax\rh AX21
    vax\rh Ax22
    SER\rh ER23
    SER\rh Er24
    hAW\rh AW25
    hAW\rh Aw26
    xAAr\rh AAR27
    xAAr\rh Aar28
    Taa\rh AA29
    Taa\rh Aa30
    boy\rh OY31
    nOY\rh Oy32
    GAOr\rh AOR33
    GAOr\rh Aor34
    GAO\rh AO35
    mAO\rh Ao36
    rOW\rh OW37
    bOW\rh Ow38
    tEL\rh EL39
    tEL\rh El40
    yUHr\rh UHR41
    yUHr\rh Uhr42
    yUH\rh UH43
    wUH\rh Uh44
    sUW\rh UW45
    sUW\rh Uw46
  }
%{ debugPhonSyllables =
\lyricmode {
  r16
  @Q-IY-y32 k-IY-rk

  `g-IH-g p-IH-rp
  `<>b-EY-yb t-EY-st

  d-EH-d C-EH-rC
  @<>J-AE-J D-AY-D

  `z-EM J-EN
  `z-EG-Z <>`v-AX-Mv

  S-ER-S h-AW-h
  x-AA-rx <>@T-AA-T

  b-oy-nk n-OY-nk
  G-AO-rG m-AO-m

  r-OW-w bl-OW-w
  tr-el-d w-UH-r

  y-UH32 str-UW-GTs r16 |
  } %}







debugPhonSyllables =
  \lyricmode {
      Q-IY16
      k-IY
      g-IH
      p-IH
      g-IH-r
      g-IH-r
      b-ey
      t-EY
      d-EH
      C-EH
      C-EH-r
      C-EH-r
      J-ae
      J-ae
      D-AY
      D-AY |
      z-en
      z-en
      z-ah
      z-ah
      v-ax
      v-ax
      S-ER
      S-ER
      h-AW
      h-AW
      x-AA-r
      x-AA-r
      T-aa
      T-aa
      b-oy
      n-OY |
      G-AO-r
      G-AO-r
      G-AO
      m-AO
      r-OW
      b-OW
      t-EL
      t-EL
      y-UH-r
      y-UH-r
      y-UH
      w-UH
      s-UW
      s-UW r8
      |
  }


\lyricNotation \debugPhonLyrics \debugPhonSyllables

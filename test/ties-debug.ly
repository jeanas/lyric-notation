\version "2.22.0"

\include "../lyric-notation.ily"

tieDebugLyrics =
\lyricmode {
  foo
}

tieDebugSyllables =
\lyricmode {
  `f-UW4 _8. _16 _16 _16 _16 _16
}

\lyricNotation \tieDebugLyrics \tieDebugSyllables
\oneLineLyricNotation \tieDebugLyrics \tieDebugSyllables
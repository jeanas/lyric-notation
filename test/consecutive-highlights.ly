\version "2.22.0"

\include "../lyric-notation.ily"

\lyricNotation
\lyricmode {
  foo foo foo
}
\lyricmode {
  \rhColor a red
  \rhColor A red
  `f-UW1\rh a0
  `f-UW1\rh a0
  `f-UW1\rh A0
}
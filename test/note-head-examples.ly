\version "2.22.0"

\include "../lyric-notation.ily"

%{
PHONEME DEBUG INPUT
I've tried to include every possible combination
although I'm sure I've missed some. Most syllables
start and end with the same consonant, and so should
be symmetrical in that respect. Lowercase vowels in
lyrics mark where diamond noteheads should be.
  %}

tableLyrics =
\lyricmode {
  beat bit beer bait bet bear bat but bird bite how bar hot boy chore cloth boat tour book boot

}

tableSyllables =
\lyricmode {
  \partial 32*20
  b-iy-t32 b-ih-t b-ih-r b-ey-t b-eh-t b-eh-r b-ae-t `b-ah-t `b-er-d b-ay-t h-aw b-aa-r h-aa-t b-oy C-ao-r kl-ao-T b-ou-t t-uh-r b-uh-k b-uw-t |

}

\lyricNotation \tableLyrics \tableSyllables
% \pageBreak
% \oneLineLyricNotation \tableLyrics \tableSyllables

\version "2.22.0"

\include "../lyric-notation.ily"
%{
PHONEME DEBUG INPUT
I've tried to include every possible combination
although I'm sure I've missed some. Most syllables
start and end with the same consonant, and so should
be symmetrical in that respect. Lowercase vowels in
lyrics mark where diamond noteheads should be.
  %}
debugPhonLyrics =
\lyricmode {
  QIYy kIYrk gIHg pIHrp
  beyyb tEYrst dEHd CEHrC
  JaeJ sAErs zAHz fAXf
  vahrv SERS ZAXrZ |
  TaaT DAYD hAWh xAArx
  mAOm nOYn GAOrG lOWrl
  rOWw yUH wUHr strUWGTs slUWr |
}
debugPhonSyllables =
\lyricmode {
  @Q-IY-y16 k-IY-rk `g-IH-g p-IH-rp
  `<>b-EY-yb t-EY-rst `d-EH-d C-EH-rC
  @<>J-AE-J s-AE-rs `z-AH-z f-AX-f
  <>`v-AH-rv S-ER-S @Z-AX-rZ _ |
  <>@T-AA-T16 D-AY-D h-AW-h x-AA-rx
  m-AO-m n-OY-n G-AO-rG l-OW-rl
  r-OW-w y-UH w-UH-r8
  str-UW-GTs16 sl-UW-r r8 |
}

\lyricNotation \debugPhonLyrics \debugPhonSyllables
\pageBreak
\oneLineLyricNotation \debugPhonLyrics \debugPhonSyllables

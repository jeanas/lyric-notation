\version "2.22.0"
%{ \include "../lyric-notation.ily" %}
% Manual recreation of lyric-notation to allow crazy format
\include "/Users/blankens10/GitHub/lyric-notation-master/input.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/spacing.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/general-layout.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/bracket-lyric-support.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/print-note-head.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/align-note-heads.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/draw-staff-symbol.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/proportional-notation.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/process-stress-accent.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/instrument-name.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/partial-indent.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/bar-numbers.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/stems-beams.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/phantom-note-head.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/shift-rests-dots.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/one-line-staff-pitches.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/align-scripts.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/align-ties.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/remove-extra-dots.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/png-resolution.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/breathing-sign.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/language.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/stress-accent-lyrics.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/shrink-lyrics.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/highlight-defs.ily"
  \include "/Users/blankens10/GitHub/lyric-notation-master/highlight-colors.ily"
  %{ \include "/Users/blankens10/GitHub/lyric-notation-master/analysis-tools.ily" %}

% Basic, unlabeled Bracket
bB =
  #(define-music-function
   (note)
   (ly:music?)
  #{
    #note
    %{ -\tweak HorizontalBracket.shorten-pair #'(-1.5 . -1.5) %}
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    -\tweak HorizontalBracket.direction #DOWN
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #1.5 %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #0 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ -\tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.5 . 0.5) %}
    \startGroup
    #})
% end bracket
eB =
  #(define-music-function
   (note)
   (ly:music?)
  #{
    #note
    \stopGroup
    #})
eb =
  #(define-music-function
   (note)
   (ly:music?)
  #{
    #note
    \stopGroup
    #})

  #(define (lyric-notation-helper one-line)
     (define-scheme-function (lyrics phonetic)
                            (ly:music? ly:music?)
       #{
         \score {
           \layout {
             one-line-staff = #one-line
           }
           <<
             \new Voice = "notation" {
               \bar ""
               #(lyric-notation-input one-line phonetic)
             }
             \new Lyrics \lyricsto "notation" {
               #lyrics
             }
           >>
         }
       #}))

  lyricNotation = #(lyric-notation-helper #f)
  oneLineLyricNotation = #(lyric-notation-helper #t)
% Make Slurs break horizontal
#(define (horizontalise-broken-slurs grob)
    (let*
     ((orig (ly:grob-original grob))
      (siblings (if (ly:grob? orig)
                    (ly:spanner-broken-into orig)
                    '()))
      (control-points (ly:grob-property grob 'control-points)))
     (if (>= (length siblings) 2)
         (let ((new-control-points
                (cond
                 ((eq? (first siblings) grob)
                  (list (first control-points)
                        (second control-points)
                        (third control-points)
                        (third control-points)))
                 ((eq? (last siblings) grob)
                  (list (second control-points)
                        (second control-points)
                        (third control-points)
                        (fourth control-points)))
                 (else control-points))))
           (ly:grob-set-property! grob 'control-points
           new-control-points)))))

DOOMTUPLabel =
  #(define-music-function
   (string tuplet)
   (string? ly:music?)
  #{
    %{ #note %}
    %{ \tweak TupletBracket.shorten-pair #'(-0.5 . -0.5) %}
    %{ \tweak TupletNumber.font-size #-4.5 %}
    %{ \tweak TupletNumber.font-series #'bold %}
    %{ \tweak TupletNumber.font-shape #'italics %}
    %{ \tweak TupletNumber.layer #4 %}
    %{ \tweak TupletNumber.whiteout-style #'outline %}
    %{ \tweak TupletNumber.whiteout ##t %}
    %{ \tweak TupletNumber.direction #-1 %}
    %{ \tweak TupletNumber.padding #2 %}
    %{ \tweak TupletNumber.padding #'() %}
    %{ \tweak TupletNumber.staff-padding #2 %}
    %{ \tweak TupletNumber.staff-padding #'() %}
    %{ \tweak TupletNumber.outside-staff-padding #2 %}
    %{ \tweak TupletNumber.outside-staff-padding #'() %}
    %{ \tweak TupletBracket.direction #DOWN %}
    %{ \tweak TupletBracket.stencil ##f %}
    %{ \tweak TupletBracket.tuplet-slur ##t %}
    %{ \tweak TupletBracket.outside-staff-priority #1100 %}
    %{ \tweak TupletBracket.staff-padding #5 %}
    %{ \tweak TupletBracket.outside-staff-padding #5 %}
    %{ \tweak TupletBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ \tweak TupletBracket.connect-to-neighbor #'(#f . #f) %}
    %{ \tweak TupletBracket.edge-height #'(0.6 . 0.6) %}
    %{ \tweak TupletBracket.padding #3 %}
    \tweak TupletNumber.text #string
    %{ \tb %}
    #tuplet
    #})
DOOMTUP =
  \tweak TupletNumber.font-size #-2
  \tweak TupletNumber.font-series #'()
  \tweak TupletNumber.font-shape #'italic
  \tweak TupletNumber.layer #4
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.direction #-1
  %{ \tweak TupletBracket.staff-padding #-4 %}
  %{ \tweak TupletBracket.layer #3 %}
  %{ \tweak TupletBracket.shorten-pair #'(0 . 0) %}
  %{ \tweak TupletBracket.outside-staff-priority ##f %}
  %{ \tweak TupletBracket.outside-staff-priority ##f %}
  %{ \tweak TupletBracket.edge-height #'(0.5 . 0.5) %}
  %{ \tweak TupletBracket.tuplet-slur ##t %}
  %{ %{ \tweak TupletBracket.transparent ##t %}
                      %% \tweak TupletBracket.stencil ##f
 %}
  %{ \tweak TupletNumber.stencil ##f %}
  %{ \tweak TupletBracket.stencil ##f %}
  \tweak TupletBracket.direction #UP
  \tuplet 1/1 \etc

songLyricsKA =
  \lyricmode {
  \goodColors
  \rgbVowelRainbow
  So nas\rh A0 -- ty\rh A0 that it's prob' -- bly some -- what of a tra\rh A0 -- ves\rh A0 -- ty\rh A0
  Ha\rh a0 -- ving\rh a0 me,\rh a0 then he told the peo -- ple You can call me, 'Your Ma\rh A0 -- jes\rh A0 -- ty!'\rh A0
  Keep your bat\rh A0 -- te\rh A0 -- ry\rh A0 charged, you know it won't
  Stick\rh J0 yo,\rh I0 and it's not his fault you kick\rh J0 slow\rh I0
  Shoul -- d've let your trick\rh J0 ho\rh I0 chick\rh J0 hold\rh I0 your sick\rh J0 glow\rh I0
  Plus no -- bo -- dy coul -- dn't do no -- thin' once he let the brick\rh J0 go\rh I0
  And you know\rh I0 I know\rh I0 that's a bunch of snow\rh I0
  The beat is so\rh I0 but\rh D0 -- ter,\rh D0 Peep the slow\rh I0 cut\rh D0 -- ter\rh D0
  As he ut\rh D0 -- ter\rh D0 the calm\rh C0 flow\rh I0 don't talk a -- bout my moms,\rh D0 yo\rh I0
  Some -- times he rhyme quick, some -- times he rhyme\rh D0 slow\rh I0 Or
          %{ songLyricsKA-B =
            \lyricmode {
            \goodColors
            \rgbVowelRainbow %}

  vice\rh P0 ver\rh P0 -- sa,\rh P0 whip up a slice\rh P0 of\rh P0 nice verse\rh P0 pie\rh P0
  Hit it on the first\rh P0 try,\rh P0 vil -- lain the worst\rh P0 guy\rh P0
  Spot\rh N0 hot\rh n0 tracks\rh B0 like spot\rh N0 a pair of fat\rh B0 ass\rh B0 -- es\rh B0
  Shots\rh N0 of the scotch\rh N0 from out of square shot\rh N0 glass\rh B0 -- es\rh B0
  And he won't stop\rh N0 till he got\rh N0 the mass\rh B0 -- es\rh B0
  And show 'em what they know not\rh N0 through flows of hot\rh N0 mo -- lass\rh B0 -- es\rh B0
  Do it like the ro -- bot\rh N0 to head -- spin to boo\rh L0 -- ga\rh L0 -- loo\rh L0
  Took\rh l0 a\rh l0 few\rh l0 min -- utes to con -- vince the ave -- rage bug\rh L0 -- a\rh L0 -- boo\rh L0
  It's ug -- ly, like look\rh L0 at\rh L0 you,\rh L0 it's a damn\rh E0 shame\rh E0
  Just re -- mem -- ber ALL CAPS when you spell the man\rh E0 name\rh E0
}

songSylsKA =
    \lyricmode {
      \override TupletBracket.bracket-visibility = #if-no-beams
      \override TupletBracket.padding = #1
      \override TupletBracket.shorten-pair = #'(-0.75 . -0.75)
      \override TupletBracket.staff-padding = #3
      %{ \override TupletBracket.outside-staff-padding = #4 %}
      \override Slur.after-line-breaking = #horizontalise-broken-slurs
      \override Score.HorizontalBracket.bracket-flare = #'(0.5 . 0.5)
      \override Score.HorizontalBracket.shorten-pair = #'(-1 . -1)
      \override Score.HorizontalBracket.bracket-flare = #'(0.5 . 0.5)
      \override Score.HorizontalBracket.thickness = #2.5
      \override Score.HorizontalBracket.stencil = ##t
      \override Score.HorizontalBracket.edge-height = #'(1 . 1)
        % 1
    \bB `s-ow8 `n-ae8
    st-iy16 `D-ae-t Q-ih-ts `pr-ao
    bl-iy `s-ah-m \once \override TupletBracket.padding = #2.5
    \tuplet 3/2 { w-ah-t16 `Q-ah-v Q-ah }
    `tr-ae16 v-ah st-iy `h-ae | \break \noPageBreak
    % 2
    v-ih-G m-iy `D-eh-n h-iy
    `t-ow-ld d-ah `p-iy p-ah-l
    `y-uw k-ae-n `k-ao-l m-iy
                       %{ \DOOMTUPLabel "3" %}
                       %{ \DOOMTUP { %}
                      %{ \tweak TupletBracket.transparent ##t %}
                                            %{ \tweak TupletBracket.stencil ##f %}
                      %{ \tweak TupletNumber.transparent ##t %}
                                            %{ \tweak TupletNumber.stencil ##f %}

    \once \override TupletNumber.Y-offset = #10.5 \tuplet 3/2 {
    y-ao-r8 @m-ae J-ah } % }
    | \break \noPageBreak
    % 3
    \eb `st-iy16 r8 \once \override HorizontalBracket.shorten-pair = #'(-1 . -1) \bB `k-iy-p16
    y-ao-r `b-ae t-er Q-iy
    \eb C-aa-rJd8 r8
    \bB y-uw16 `n-ow Q-ih-t `w-ow-nt | \break \noPageBreak
    % 4
    `st-ih-k8 y-ow
    \once \override Rest.Y-offset = #7 r16 Q-ah-nd16 `Q-ih-ts n-aa-t \once \override TupletNumber.Y-offset = #9 \tuplet 3/2 {
    `h-ih-z8 f-ao-lt y-uw }
    `k-ih-k8 \eb sl-ow | \break \noPageBreak
    % 5
                    % \DOOMTUPLabel "5" \DOOMTUP {
                      %{ \tweak TupletBracket.transparent ##t %}
                                            %% \tweak TupletBracket.stencil ##f
                      %{ \tweak TupletNumber.transparent ##t %}
                                            %% \tweak TupletNumber.stencil ##f

    \tuplet 5/4 { \once \override Rest.Y-offset = #7 r16 \bB `S-uh16 d-ah `l-eh-t y-ao-r } % }
    \once \override Rest.Y-offset = #7 r16 `tr-ih-k16 h-ow r16
    `C-ih-k h-ow-ld \once \override Rest.Y-offset = #7 r16 y-ah
    `s-ih-k8 \eb gl-ow | \pageBreak

    \once \override Rest.Y-offset = #7 r16 \bB `pl-uh-s16 n-ow `b-aa
                         % \DOOMTUPLabel "5" \DOOMTUP {
                        %{ \tweak TupletBracket.transparent ##t %}
                      %% \tweak TupletBracket.stencil ##f

                        %{ \tweak TupletNumber.transparent ##t %}
                      %% \tweak TupletNumber.stencil ##f
                      \once \override TupletNumber.Y-offset = #9.5
    \tuplet 5/4 {
    d-iy16 `k-uh d-ah-nt d-uw `n-ah } % }
    t-ih-n16 `w-ah-ns h-iy `l-eh-t
    D-ah `br-ih-k8 \eb g-ow16 | \break \noPageBreak

    % 7
    r8. \bB Q-ah-nd16 \tuplet 3/2 {
    `y-uw16 n-ow \once \override Rest.Y-offset = #7 r16 } \tuplet 3/2 {
    `Q-ay16 n-ow \once \override Rest.Y-offset = #7 r16 }
    `D-ae-ts16 Q-ah `b-ah-nC Q-ah-v
    \eb `sn-ow8 r8 | \break \noPageBreak
    % 8
    \bB D-ah16 `b-iy-t Q-ih-z `s-ow
    \once \override Rest.Y-offset = #7 r16 `b-ah \eb t-er r16
    \once \override Rest.Y-offset = #7 r16 \bB `p-iy-p D-ah `sl-ow r16
    `k-ah32 t-er `Q-ae-z16 h-iy | \break \noPageBreak
    % 9
    `Q-ah t-er D-ah `k-aa-m
    \once \override Rest.Y-offset = #7 r16 \eb fl-ow r8
                       % \DOOMTUPLabel "5" \DOOMTUP {
                      %{ \tweak TupletBracket.transparent ##t %}
                      %% \tweak TupletBracket.stencil ##f

                      %{ \tweak TupletNumber.transparent ##t %}
                      %% \tweak TupletNumber.stencil ##f

    \once \override TupletNumber.Y-offset = #9.5 \tuplet 5/4 {
    \bB d-ow-nt16 `t-ao-k Q-ah `b-aw-t m-ay } % }
    `m-aa-mz8 \eb y-ow | \break \noPageBreak

    % 10
    \bB `s-ah-m t-ay-mz16 r16
    h-iy32 `r-ay-m \once \override Rest.Y-offset = #7 r16 kw-ih-k16 `s-ah-m
    t-ay-mz \once \override Rest.Y-offset = #7 r16 h-iy `r-ay-m
    \once \override Rest.Y-offset = #7 r16 `sl-ow Q-ao-r \once \override Rest.Y-offset = #7 r16 | \pageBreak
    %{ } %}

    `v-ay-s \once \override Rest.Y-offset = #7 r16 v-er \eb s-ah r8 \tuplet 3/2 {
    \bB `w-ih-p16 Q-ah-p Q-ah }
    `sl-ay-s16 Q-ah-v `n-ay-s r16
    `v-er-s8 p-ay | \break \noPageBreak
    % 12
    `h-ih-t16 Q-ih-t `Q-aa-n D-ah
    `f-er-st8 tr-ay
    `v-ih16 l-ah-n \once \override Rest.Y-offset = #7 r16 D-ah
    `w-er-st8 \eb g-ay | \break \noPageBreak

    % 13
    \bB `sp-aa-t h-aa-t
    `tr-ae-ks l-ay-k
    `sp-aa-t16 Q-ah `p-eh-r Q-ah-v
    f-ae-t `Q-ae \once \override Rest.Y-offset = #7 r16 \eb s-ah-z | \break \noPageBreak
    % 14
    \bB `S-aa-ts \once \override Rest.Y-offset = #7 r16 Q-ah-v D-ah
    `sk-aa-C r32 `fr-ah-m32 Q-aw-t16 Q-ah-v
    `skw-eh-r8 S-aa-t
    `gl-ae \eb s-ah-z | \break \noPageBreak
    % 15
    r8 \bB `Q-ah-nd16 h-iy
    `w-ow-nt8 st-aa-p
    `t-ih-l16 h-iy `g-aa-t D-ah
    `m-ae8 s-ah-z | \pageBreak
    % 16
    r16. Q-en32 \once \override TupletBracket.padding = #2 \tuplet 3/2 { `S-ow16 Q-em `w-ah-t }
    D-ey16 `n-ow \once \override TupletBracket.padding = #1 \tuplet 3/2 { n-aa-t16 \once \override Rest.Y-offset = #7 r16 Tr-uw16 }
    `fl-ow-z16 Q-ah-v \once \override TupletBracket.padding = #2 \tuplet 3/2 { `h-aa-t16 \once \override Rest.Y-offset = #7 r16 m-ah16 }
    `l-ae8 \eb s-ah-z | \break \noPageBreak
    % 17
    \bB `d-uw16 Q-ih-t `l-ay-k D-ah
    `r-ow b-aa-t \once \override Rest.Y-offset = #7 r16 t-uw
    `h-eh-d sp-ih-n \once \override Rest.Y-offset = #7 r16 t-uw
    `b-uw g-ah \eb `l-uw \once \override Rest.Y-offset = #7 r16 | \break \noPageBreak
    % 18
    \once \override TupletBracket.padding = #2 \tuplet 3/2 { \once \override Rest.Y-offset = #7 r16 \once \override Rest.Y-offset = #7 r16 \bB `t-uh-k16 } \once \override TupletBracket.padding = #2 \tuplet 3/2 { Q-ah16 `fy-uw16 \once \override Rest.Y-offset = #7 r16 }
    `m-ih16 n-ah-ts `t-uw k-ah-n
    `v-ih-ns D-ah `Q-ae-v r-ih-J
    `b-ah g-ah `b-uw \once \override Rest.Y-offset = #7 r16 | \break \noPageBreak

    % 19
    r8 Q-ih-ts16 `Q-ah
    gl-iy \once \override Rest.Y-offset = #7 r16 l-ay-k `l-uh-k
    Q-ae-t `y-uw Q-ih-ts Q-ah
    `d-ae-m8 \eb S-ey-m | \break \noPageBreak
    % 20
          \set Timing.measureLength = #(ly:make-moment 9/8)
    \once \override Rest.Y-offset = #7 r16 \bB J-ah-st r-ih `m-eh-m
    \once \override Rest.Y-offset = #7 r16 b-er Q-ao-l r16
    `k-ae-ps \once \override Rest.Y-offset = #7 r16 `w-eh-n y-uw
    `sp-eh-l D-ah `m-ae-n r16
    % 21
    \bar "!" \noBreak
    \eb `n-ey-m8 } |

songSylsOL =
    \lyricmode {
            \override TupletBracket.bracket-visibility = ##t
            \override Slur.after-line-breaking = #horizontalise-broken-slurs
            \override Score.HorizontalBracket.bracket-flare = #'(0.5 . 0.5)
            \override Score.HorizontalBracket.thickness = #2.5
            \override Score.HorizontalBracket.stencil = ##t
            \override Score.HorizontalBracket.edge-height = #'(1 . 1)

    \bB `s-ow8 `n-ae8
    st-iy16 `D-ae-t Q-ih-ts `pr-ao
    bl-iy `s-ah-m \tuplet 3/2 { w-ah-t16 `Q-ah-v Q-ah }
    `tr-ae16 v-ah st-iy `h-ae | \break \noPageBreak
    % 2
    v-ih-G m-iy `D-eh-n h-iy
    `t-ow-ld d-ah `p-iy p-ah-l
    `y-uw k-ae-n `k-ao-l m-iy
                       %{ \DOOMTUPLabel "3" %}
                       %{ \DOOMTUP { %}
                      %{ \tweak TupletBracket.transparent ##t %}
                      %% \tweak TupletBracket.stencil ##f

                      %{ \tweak TupletNumber.transparent ##t %}
                      %% \tweak TupletNumber.stencil ##f

    \tuplet 3/2 {
    y-ao-r8 @m-ae J-ah } % }
    | \break \noPageBreak
    % 3
    \eb `st-iy16 r8 \bB `k-iy-p16
    y-ao-r `b-ae t-er Q-iy
    \eb C-aa-rJd8 r8
    \bB y-uw16 `n-ow Q-ih-t `w-ow-nt | \break \noPageBreak
    % 4
    `st-ih-k8 y-ow
    \once \override Rest.Y-offset = #7 r16 Q-ah-nd16 `Q-ih-ts n-aa-t \tuplet 3/2 {
    `h-ih-z8 f-ao-lt y-uw }
    `k-ih-k8 \eb sl-ow | \break \noPageBreak
    % 5
                    % \DOOMTUPLabel "5" \DOOMTUP {
    %{ \tweak TupletBracket.transparent ##t %}
                      %% \tweak TupletBracket.stencil ##f

    %{ \tweak TupletNumber.transparent ##t %}
                      %% \tweak TupletNumber.stencil ##f

    \tuplet 5/4 { \once \override Rest.Y-offset = #7 r16 \bB `S-uh16 d-ah `l-eh-t y-ao-r } % }
    \once \override Rest.Y-offset = #7 r16 `tr-ih-k16 h-ow r16
    `C-ih-k h-ow-ld \once \override Rest.Y-offset = #7 r16 y-ah
    `s-ih-k8 \eb gl-ow | \break \noPageBreak

    \once \override Rest.Y-offset = #7 r16 \bB `pl-uh-s16 n-ow `b-aa
                         % \DOOMTUPLabel "5" \DOOMTUP {
                        %{ \tweak TupletBracket.transparent ##t %}
                      %% \tweak TupletBracket.stencil ##f

                        %{ \tweak TupletNumber.transparent ##t %}
                      %% \tweak TupletNumber.stencil ##f

    \tuplet 5/4 {
    d-iy16 `k-uh d-ah-nt d-uw `n-ah } % }
    t-ih-n16 `w-ah-ns h-iy `l-eh-t
    D-ah `br-ih-k8 \eb g-ow16 | \break \noPageBreak

    % 7
    r8. \bB Q-ah-nd16 \tuplet 3/2 {
    `y-uw16 n-ow \once \override Rest.Y-offset = #7 r16 } \tuplet 3/2 {
    `Q-ay16 n-ow r16}
    `D-ae-ts16 Q-ah `b-ah-nC Q-ah-v
    \eb `sn-ow8 r8 | \break \noPageBreak
    % 8
    \bB D-ah16 `b-iy-t Q-ih-z `s-ow
    \once \override Rest.Y-offset = #7 r16 `b-ah \eb t-er r16
    \once \override Rest.Y-offset = #7 r16 \bB `p-iy-p D-ah `sl-ow r16
    `k-ah32 t-er `Q-ae-z16 h-iy | \break \noPageBreak
    % 9
    `Q-ah t-er D-ah `k-aa-m
    \once \override Rest.Y-offset = #7 r16 \eb fl-ow r8
                       % \DOOMTUPLabel "5" \DOOMTUP {
                      %{ \tweak TupletBracket.transparent ##t %}
                      %% \tweak TupletBracket.stencil ##f

                      %{ \tweak TupletNumber.transparent ##t %}
                      %% \tweak TupletNumber.stencil ##f

    \tuplet 5/4 {
    \bB d-ow-nt16 `t-ao-k Q-ah `b-aw-t m-ay } % }
    `m-aa-mz8 \eb y-ow | \break \noPageBreak

    % 10
    \bB `s-ah-m t-ay-mz16 r16
    h-iy32 `r-ay-m \once \override Rest.Y-offset = #7 r16 kw-ih-k16 `s-ah-m
    t-ay-mz \once \override Rest.Y-offset = #7 r16 h-iy `r-ay-m
    \once \override Rest.Y-offset = #7 r16 `sl-ow Q-ao-r \once \override Rest.Y-offset = #7 r16 | \pageBreak
    %{ } %}

    `v-ay-s \once \override Rest.Y-offset = #7 r16 v-er \eb s-ah r8 \tuplet 3/2 {
    \bB `w-ih-p16 Q-ah-p Q-ah }
    `sl-ay-s16 Q-ah-v `n-ay-s r16
    `v-er-s8 p-ay | \break \noPageBreak
    % 12
    `h-ih-t16 Q-ih-t `Q-aa-n D-ah
    `f-er-st8 tr-ay
    `v-ih16 l-ah-n \once \override Rest.Y-offset = #7 r16 D-ah
    `w-er-st8 \eb g-ay | \break \noPageBreak

    % 13
    \bB `sp-aa-t h-aa-t
    `tr-ae-ks l-ay-k
    `sp-aa-t16 Q-ah `p-eh-r Q-ah-v
    f-ae-t `Q-ae \once \override Rest.Y-offset = #7 r16 \eb s-ah-z | \break \noPageBreak
    % 14
    \bB `S-aa-ts \once \override Rest.Y-offset = #7 r16 Q-ah-v D-ah
    `sk-aa-C r32 `fr-ah-m32 Q-aw-t16 Q-ah-v
    `skw-eh-r8 S-aa-t
    `gl-ae \eb s-ah-z | \break \noPageBreak
    % 15
    r8 \bB `Q-ah-nd16 h-iy
    `w-ow-nt8 st-aa-p
    `t-ih-l16 h-iy `g-aa-t D-ah
    `m-ae8 s-ah-z | \break \noPageBreak

    r16. Q-en32 \tuplet 3/2 { `S-ow16 Q-em `w-ah-t }
    D-ey16 `n-ow \tuplet 3/2 { n-aa-t16 \once \override Rest.Y-offset = #7 r16 Tr-uw16 }
    `fl-ow-z16 Q-ah-v \tuplet 3/2 { `h-aa-t16 \once \override Rest.Y-offset = #7 r16 m-ah16 }
    `l-ae8 \eb s-ah-z | \break \noPageBreak
    % 17
    \bB `d-uw16 Q-ih-t `l-ay-k D-ah
    `r-ow b-aa-t \once \override Rest.Y-offset = #7 r16 t-uw
    `h-eh-d sp-ih-n \once \override Rest.Y-offset = #7 r16 t-uw
    `b-uw g-ah \eb `l-uw \once \override Rest.Y-offset = #7 r16 | \break \noPageBreak
    % 18
    \tuplet 3/2 { \tweak Rest.extra-offset #-0.5 \once \override Rest.Y-offset = #7 r16 \once \override Rest.Y-offset = #7 r16 \bB `t-uh-k16 } \tuplet 3/2 { Q-ah16 `fy-uw16 \once \override Rest.Y-offset = #7 r16 }
    `m-ih16 n-ah-ts `t-uw k-ah-n
    `v-ih-ns D-ah `Q-ae-v r-ih-J
    `b-ah g-ah `b-uw \once \override Rest.Y-offset = #7 r16 | \break \noPageBreak

    % 19
    r8 Q-ih-ts16 `Q-ah
    gl-iy \once \override Rest.Y-offset = #7 r16 l-ay-k `l-uh-k
    Q-ae-t `y-uw Q-ih-ts Q-ah
    `d-ae-m8 \eb S-ey-m | \break \noPageBreak
    % 20
          \set Timing.measureLength = #(ly:make-moment 9/8)
    \once \override Rest.Y-offset = #7 r16 \bB J-ah-st r-ih `m-eh-m
    \once \override Rest.Y-offset = #7 r16 b-er Q-ao-l r16
    `k-ae-ps \once \override Rest.Y-offset = #7 r16 `w-eh-n y-uw
    `sp-eh-l D-ah `m-ae-n r16
    % 21
    \bar "!" \noBreak
    \eb `n-ey-m8 } |

\lyricNotation \songLyricsKA \songSylsKA
        \pageBreak
\oneLineLyricNotation \songLyricsKA \songSylsOL

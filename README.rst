##############
Lyric Notation
##############

Automated special notation of lyrics using LilyPond_ for
Michael Blankenship's Ph.D. dissertation. MIT-licensed
(see ``LICENSE.rst``).

Software requirements to use this are LilyPond_, and
optionally Python_ version 3. The framework was tested
using version 2.22.0 of LilyPond and version 3.8 of Python.
For editing LilyPond files, Frescobaldi_ is very highly
recommended. Include ``lyric-notation.ily`` in your LilyPond
source, and enjoy! Look at ``example.ly`` for inspiration
or read the
rest of this documentation.

.. _LilyPond: https://lilypond.org
.. _Python: https://python.org
.. _Frescobaldi: https://frescobaldi.org/


Using the LilyPond framework
============================

Basics
------

Here is the template for producing lyric notation
in a LilyPond input file::

   \version "2.22.0"
   \include "lyric-notation.ily"

   \lyricNotation
   \lyricmode {
     [Lyrics go here...]
   }
   \lyricmode {
     [Phonetic input goes here...]
   }

The two main components are the lyrics, and the phonetic
notation. The lyrics are just English words; when they
have several syllables, these are separated with a double
hyphen, like this::

   rap mu -- sic

The phonetic notation contains information about the
pronunciation as well as the rhythm. The unit here is
the syllable. To enter one, separate the onset, nucleus
and coda with hyphens. The coda is optional. Phonemes
are entered using the ARPAbet system. In consonant parts
(onset and coda), every character is considered a phoneme
and the one-letter, case-sensitive version of ARPAbet
is used. Most of the consonant characters are just the
normal lowercase letters that would usually make that sound,
i.e. /p, b, t, d, k, g, s, z, f, v, n, m, l, r, w, y/
The rest of the consonant sounds are represented by capital
letters relating to that sound, plus a few non-phonemic sounds
are also triggered with capitals.
The nucleus, by contrast, is one vowel represented
by two characters (occasionally three). Double hyphens
between syllables in a word are customary in this part,
but not required. For example::

   r-ae-p my-uw -- z-ih-k

Just like in regular LilyPond input syntax, durations
are added using numbers: 1 for a whole note, 2 for
a half note, 4 for a quarter note, etc. ::

   r-ae-p8 my-uw16 -- z-ih-k16


Modifiers
---------

Modifiers are added at the beginning of the syllable
to indicate special handling. For stress, use the backtick:
`````. Musical accent is denoted by the at sign: ``@``. Finally,
``<>`` is used for non-standard pronunciation. ::

   <>`r-ae-p8 @my-uw16 -- z-ih-k16



Regular commands
----------------

All LilyPond commands, such as ``\partial`` or ``\tuplet``,
can be used. When they apply to notes, they go in the phonetic
input. ::

   \lyricmode {
    \partial 4
    \tuplet 3/4 { r-ae-p8 `my-uw8 -- z-ih-k }
   }


Slurs and phrasing slurs
------------------------

You can use normal LilyPond syntax to add both
slurs and phrasing slurs in the phonetic input::

   r-ae-p8\( my-uw16( -- z-ih-k16)\)


Breathing sign
--------------

To insert a breathe mark, use the single apostrophe as
a standalone syllable. ::

   \lyricmode {
     r-ae-p ' my-uw -- z-ih-k
   }


Angle bracket
-------------

This segmentation marker is added with a closing
bracket on the syllable (no space in between). ::

   \lyricmode {
     r-ae-p] my-uw -- z-ih-k
   }


Analysis highlights
-------------------

These go in the lyrics (by convention). To declare
a highlight color, use:

.. parsed-literal::

   \\rhColor *x* *color*

where *x* is the highlight's *id* (which can be
any letter, case sensitive) and *color* is any
kind of color recognized by LilyPond (see
`The color property`__ in the documentation).

__ https://lilypond.org/doc/v2.22/Documentation/learning/visibility-and-color-of-objects.html#the-color-property

X11 colors can also be used, using the following syntax:

.. parsed-literal::

   \rhColor *x* #(x11-color '*X11ColorName*)

where *x* is the *id* and *X11ColorName* is the x11 color
name using capital letters and no spaces.

E.G. ::

   \rhColor a #(x11-color 'LawnGreen)

Afterwards, you can highlight a passage and add
an analysis number using the ``\rh`` command as an
articulation on the lyric word. The command takes
two arguments, the id, and the number. To suppress
the number, pass 0. It is convenient to let the id
and number not be separated by any space. When several
consecutive ``\rh`` commands are passed the same id,
their highlights are merged into one single spanning
highlight. ::

   \lyricmode {
     \rhColor a blue
     rap\rh a1 mu\rh a2 -- sic
   }

With ``\rhColorGradient``, the highlight gradually changes color::

   \lyricNotation
   \lyricmode {
     foo!
   }
   \lyricmode {
     \rhColorGradient a red blue
     `f-UW\rh a0
   }


Analysis brackets
-----------------

Analytical brackets can be applied, and nested with or
without string labels, using ``\sB`` (start bracket) followed
by a label string in quotes, (leave blank for no label)
and ``\eB`` (end bracket) as follows. ::

   \lyricmode {
     \bracket { \sB "𝛂" r-ae-p8 my-uw16 -- \eB z-ih-k16 }
   }




To group notes together in a tuplet-like bracket,
use the ``\bracket`` command in the phonetic input. ::

   \lyricmode {
     \bracket { r-ae-p8 my-uw16 -- z-ih-k16 }
   }


Lyrics size
------------

Often, the lyrics are dense enough to distort the
proportional spacing. A convenience function is
available to reduce their size (for use in the
lyrics, naturally):

.. parsed-literal::

   \\shrinkLyrics *x*

This decreases the font size of the text by *x*.
To return to normal size, use ``\shrinkLyrics 0``.


Moving objects around
---------------------

Use standard LilyPond syntax for this:

.. parsed-literal::

   \\once \\override *ContextName*.\ *ObjectName*.extra-offset #'(*a* . *b*)

where *a* and *b* are the horizontal and vertical
amounts by which the object is moved. This happens
*after* the positioning, so the typesetting engine
is oblivious to the offset (hence the name
*extra*-offset).

The context name may be *Voice*, *Staff*, *Lyrics*,
or even *Score*. Here are some of the most common
objects in the system:

- Voice.CustomizedBreathingSign,
- Score.Highlight,
- Score.AnalysisNumber,
- Score.AngleBracket,
- Lyrics.LyricText
- Score.TextScript (for stress and accent),
- Voice.Rest,
- Voice.Dots.


Suppressing headphones
----------------------

To stop imprinting headphones temporarily, put ``\headphonesOff``
in the phonetic input. You can bring them back with ``\headphonesOn``.


One-line mode
-------------

In one-line mode, the staff is collapsed into a single
line. There are some others differences as well:

- For diphthongs, the second note is on the right
  of the stem, not centered (that would lead to collisions
  obviously).

- Ties are not duplicated,

- There is no instrument name.

To activate it, simply replace ``\lyricNotation`` with
``\oneLineLyricNotation``.




Using the input preparator script
=================================

This script takes normal, English lyric input and
outputs an automatic syllabification where you can
just fill in the durations and other commands. It
is written in Python and based on the CMU dictionary.
It was tested with Python 3.8.

Here is a sample lyric input file::

   rap mu -- sic

Assuming that this is called ``mylyrics.txt``,
the following command calls the script:

.. code-block:: shell

   $ python3 syllabify.py mylyrics.txt

Output is written to ``mylyrics.ly``.

The script has options for setting output directory and
dictionary file names. Please read the output of
``python3 syllabify.py --help`` for information.

Standard globbing patterns are supported. For instance, here is how
you would convert all ".txt" files in ``lyric-preparator/lyric-files/``,
writing ".ly" files in ``lyric-preparator/ly-files/``:

.. code-block:: shell

   $ python3 syllabify.py -o lyric-preparator/ly-files lyric-preparator/lyric-files/*.txt

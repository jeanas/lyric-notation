\version "2.22.0"

#(define (a-format obj)
   (format #f "~a" obj))

#(define (color->list color)
   (if (string? color)
       (css->colorlist color)
       color))

#(define (format-color color)
   (string-join
     (map a-format (color->list color))
     " "))

#(define (format-path-command command)
   (string-join
     (map
       a-format
       (append (cdr command)
               (list (car command))))
     " "))

#(define-markup-command (gradient-path layout props left-color right-color path)
                        (color? color? list?)
   #:properties ((gradient-angle 45)
                 (transition-fraction 0.1))
   (let* ((normal-path (interpret-markup layout props (make-path-markup 0 path)))
          (X-extent (ly:stencil-extent normal-path X))
          (X-start (interval-start X-extent))
          (X-end (interval-end X-extent))
          (Y-extent (ly:stencil-extent normal-path Y))
          (Y-start (interval-start Y-extent))
          (Y-end (interval-end Y-extent))
          (ps-path (string-join (map format-path-command path)
                                "\n"))
          (sin-alpha (sin (degrees->radians gradient-angle)))
          (delta-x (interval-length X-extent))
          (delta-y (interval-length Y-extent))
          (a (/ (+ delta-x (* delta-y sin-alpha))
                (+ 1 (expt sin-alpha 2))))
          (strip-start-fraction (/ (- 1 transition-fraction)
                                   2))
          (strip-end-fraction (/ (+ 1 transition-fraction)
                                 2))
          (strip-start-X (+ X-start (* a strip-start-fraction)))
          (strip-start-Y (+ Y-start (* a strip-start-fraction sin-alpha)))
          (strip-end-X (+ X-start (* a strip-end-fraction)))
          (strip-end-Y (+ Y-start (* a strip-end-fraction sin-alpha)))
          (ps-code
            (format
              #f
              "
              gsave
              currentpoint
              translate
              newpath
              ~a
              clip
              <<
                /ShadingType 2
                /Extend [true true]
                /ColorSpace /DeviceRGB
                /Coords [~a ~a ~a ~a]
                /Function <<
                  /FunctionType 2
                  /Domain [0 1]
                  /C0 [~a]
                  /C1 [~a]
                  /N 1>>
              >>
              shfill
              grestore
              "
              ps-path
              strip-start-X
              strip-start-Y
              strip-end-X
              strip-end-Y
              (format-color left-color)
              (format-color right-color))))
     (ly:make-stencil
      `(embedded-ps ,ps-code)
      X-extent
      Y-extent)))



%% Testing
%{
samplePath =
  #'((moveto 0 0)
     (lineto 10 0)
     (lineto 10 10)
     (lineto 0 10)
     (closepath))

\markup \gradient-path "orange" "cornflowerblue" #samplePath
%}

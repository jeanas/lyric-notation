\version "2.22.0"

% - In twelve-line mode, remove dots on visible heads.
% - In one-line mode, remove those on the second head
%   when there is one.

#(set-object-property! 'slated-for-suicide 'backend-type? boolean?)

#(define (Remove_extra_dots_engraver context)
   (make-engraver
     (acknowledgers
       ((dots-interface engraver grob source-engraver)
          (let* ((layout (ly:grob-layout grob))
                 (one-line (ly:output-def-lookup layout 'one-line-staff))
                 (note-head (ly:grob-property grob 'cause))
                 (note-event (event-cause note-head))
                 (is-phantom (ly:event-property note-event 'is-phantom))
                 (index (ly:event-property note-event 'index)))
            (if (or
                  (and (not one-line)
                       (not is-phantom))
                  (and one-line
                       (equal? index 2)))
                (ly:grob-set-property! grob 'slated-for-suicide #t)))))))
                
#(define (suicide-if-appropriate dots)
   (if (eq? (ly:grob-property dots 'slated-for-suicide)
            #t)
       (ly:grob-suicide! dots)))

\layout {
  \context {
    \Voice
    \consists #Remove_extra_dots_engraver
    \override Dots.before-line-breaking = #suicide-if-appropriate
  }
}
 i\version "2.22.0"
\include "../lyric-notation.ily"

songLyrics =
\lyricmode {
  \rhColor a #(x11-color 'LightSeaGreen)
  \rhColor A #(x11-color 'LightSeaGreen)
  \rhColor b #(x11-color 'LightGreen)
  \rhColor B #(x11-color 'LightGreen)
  \rhColor c #(x11-color 'LawnGreen)
  \rhColor C #(x11-color 'LawnGreen)
  \rhColor i #(x11-color 'GreenYellow)
  \rhColor iii #(x11-color 'MediumSeaGreen)
  \rhColor d #(x11-color 'Gold)
  \rhColor ax #(x11-color 'Khaki)
  \rhColor ii #(x11-color 'DarkKhaki)
  \rhColor f #(x11-color 'Tomato)
  \rhColor g #(x11-color 'IndianRed)
  \rhColor h #(x11-color 'Magenta)
  \rhColor H #(x11-color 'Orchid)
  \rhColor HH #(x11-color 'Orchid)

% Pickup
His\rh i0 palms\rh a0
% Bar 1
are\rh b0 swea\rh b0 -- ty,\rh b0
knees\rh c0 weak,\rh C0
arms\rh a0 are\rh b0 hea\rh b0 --
vy\rh b0 There's\rh ii0 vo\rh a0 -- mit\rh i0
% Bar 2
on\rh a0 his\rh i0 swea\rh B2 -- ter\rh B1
al\rh b0 -- rea\rh b0 -- dy,\rh b0
mom's\rh a0 spa\rh b0 -- ghe\rh b0 --
tti\rh b0 He's\rh i0 ner\rh d0 -- vous\rh d0
% Bar 3
but\rh ax0 on\rh a0 the\rh ax0
sur\rh d0 -- face\rh d0 he\rh c0 looks\rh iii0
calm\rh a0 and\rh b0 rea\rh b0 --
dy\rh b0 To\rh iii0 drop\rh a0
% Bar 4
bombs\rh A0
but\rh b1 he\rh b3 keeps\rh b3
on\rh a0 for\rh b0 -- ge\rh b0 --
tting\rh b0 What\rh ax0 he\rh i0 wrote\rh f0
% Bar 5
down,\rh g0 the\rh ax0
whole\rh f0 crowd\rh g0
goes\rh f0 so\rh f0 loud\rh g0
He\rh i0 o\rh f0 --
% Bar 6
pens\rh i0 his\rh i0 mouth,\rh g0
but\rh ax0 the\rh ax0 words\rh ax0
won't\rh f0 come\rh ax0 out\rh g0
He's\rh i0 cho\rh f0 --
% Bar 7
king,\rh i0 how?\rh g0
Eve\rh ii0 -- ry\rh i0 -- bo\rh ax0 -- dy's\rh i0
jo\rh f0 -- king\rh i0 now\rh g0
The\rh ax0 clock's\rh f0
% Bar 8
run\rh ax0 out,\rh g0
time's\rh i0 up,\rh ax0
o\rh f0 -- ver,\rh ax0 blaow!\rh g0
Snap\rh h1
% Bar 9
back\rh H1 to\rh H2 re\rh H3 --
a\rh h1 -- li\rh h2 -- ty,\rh h3 oh\rh f0
there\rh ii0 goes\rh f0
gra\rh h1 -- vi\rh h2 -- ty,\rh h3 oh\rh f0
% Bar 10
There\rh ii0 goes\rh f0
Ra\rh h1 -- bbit,\rh h2 he\rh h3 choked,\rh f0
he's\rh i0 so\rh f0
mad\rh H1 but\rh H2 he\rh H3 won't\rh f0
% Bar 11
Give\rh i0 up\rh f0
that\rh h1 ea\rh h2 -- sy,\rh h3 no,\rh f0
he\rh i0 won't\rh f0
have\rh h1 it,\rh h2 he\rh h3 knows\rh f0
% Bar 12
His\rh i0 whole\rh f0
back's\rh H1 to\rh H2 these\rh H3 ropes,\rh f0
it\rh i0 don't\rh f0
ma\rh H1 -- tter,\rh H2 he's\rh H3 dope\rh f0
% Bar 13
He\rh i0 knows\rh f0
that\rh H1 but\rh H2 he's\rh H3 broke,\rh f0
he's\rh i0 so\rh f0
stag\rh h1 -- nant,\rh h2 he\rh h3 knows\rh f0
% Bar 14
When\rh ii0 he\rh i0 goes\rh f0
back\rh H1 to\rh H2 this\rh H3 mo\rh f0 --
bile\rh iii0 home,\rh f0
that's\rh h1 when\rh h2 it's\rh h3
% Bar 15
Back\rh H1 to\rh H2 the\rh H3
lab\rh HH1 a\rh HH2 -- gain\rh HH3 yo,\rh f0
this\rh i0 whole\rh f0
rhap\rh h1 -- so\rh h2 -- dy,\rh h3 Bet\rh ii0 --
% Bar 16
ter\rh ax0 go\rh f0 cap\rh H1 -- ture\rh H2
this\rh H3 mo\rh f0 -- ment\rh ii0 and\rh ax1
hope\rh f0 it\rh c0 don't\rh f0
pass\rh h1 him\rh h3
}
songSyllables =
\lyricmode {
\phrasingSlurDotted \slurDashed
\partial 4
% Pickup
r16 \bracket { h-ih-z16\(( @p-aa-lmz8)  |
% Bar 1
Q-aa-r16 `sw-eh -- t-iy8 }
@n-iy-z( `w-iy-k)
\bracket { @Q-aa-rm z-aa-r16 `h-eh --
v-iy\) } \bracket { D-eh-rz\(\startGroup `v-aa -- m-ih-t\stopGroup |
% Bar 2
`Q-aa-n\startGroup h-ih-z\stopGroup \slurDashed \slurUp `sw-eh -- t-er
Q-aa-l -- `r-eh -- d-iy8 }
\bracket { @m-aa-mz sp-ah16 -- `g-eh --
t-iy16\) } h-iy-z\(( `n-er -- v-ah-s) |
% Bar 3
r16. b-ah-t32( `Q-aa-n16) D-ah(
`s-er -- f-ah-s) `h-iy( l-uh-ks)
\bracket { @k-aa-lm8 Q-ah-nd16 `r-eh --
d-iy\) } \bracket { t-uw\(( `dr-aa-p8) |
% Bar 4
@b-aa-mz8  r8
`b-ah-t16 h-iy `k-iy-ps8 }
\bracket { @Q-aa-n f-er16 -- `g-eh --
t-ih-G16\) } \bracket { `w-ah-t( h-iy `r-ow-t) |
% Bar 5
_16 @d-aw-n8 } \bracket { D-ah16(
`h-ow-l8) @kr-aw-d }
\bracket { `g-ow-z( s-ow16) `l-aw-d
_16 } \bracket { h-iy( `y-ow8) -- |
% Bar 6
p-ih-nz16( h-ih-z `m-aw-T8) }
\bracket { `b-ah-t16( D-ah `w-er-dz8
`w-ow-nt) k-ah16( `m-aw-t) }
r16 \bracket { h-iy-z( `C-ow8) -- |
% Bar 7
k-ih-G16( @h-aw8) } r16
\bracket { `Q-eh-v16( -- r-iy -- `b-ah -- d-iy-z
`J-ow8) -- k-ih-G16( @n-aw) }
r16 \bracket { D-ah( `kl-aa-ks8) |
% Bar 8
r-ah-n16( @Q-aw-t8) } r16
\bracket { `t-ay-mz8( Q-ah-p
`Q-ow) -- v-er16( @bl-aw16)
_16 } r16 \bracket { `sn-ae-p8 |
% Bar 9
@b-ae-k8 t-uw16 r-iy --
`y-ae l-ih -- t-iy @Q-ow }
r16 \bracket { D-eh-r16( `g-ow-z8)
`gr-ae16 -- v-ih -- t-iy @Q-ow } |
% Bar 10
r16 \bracket { D-eh-r16( `g-ow-z8)
`r-ae16 -- b-ih-t h-iy @C-ow-kt }
r16 \bracket { h-iy-z16( `s-ow8)
`m-ae-d16 b-ah-t h-iy @w-ow-nt } |
% Bar 11
_16 \bracket { g-ih16( `<>v-uh-p8)
`D-ae-t16 Q-iy -- z-iy @n-ow }
r16 \bracket { h-iy( `w-ow-nt8)
`h-ae16 v-ih-t h-iy @n-ow-z } |
% Bar 12
_16 \bracket { h-ih-z( `h-ow-l8)
`b-ae-ks16 t-ah D-iy-z @r-ow-ps }
r16 \bracket { Q-ih-t( `d-ow-nt8)
`m-ae16 -- t-er h-iy-z @d-ow-p } |
% Bar 13
r16 \bracket { h-iy( `n-ow-z8)
`D-ae-t16 b-ah-t h-iy-z @br-ow-k }
r16 \bracket { h-iy-z( `s-ow8)
`st-ae-g16 -- n-ih-nt h-iy @n-ow-z } |
% Bar 14
_16 \bracket { w-eh-n32( h-iy `g-ow-z8)
\tuplet 3/2 8 { `b-ae-k8 t-ah16 } D-ih-s16 @m-ow --
_16 } \bracket { b-el( `h-ow-m8)
\tuplet 3/2 8 { `D-ae-ts8 w-eh-n16 } Q-ih-ts16 } r16 |
% Bar 15
\bracket { @b-ae-k8 t-uw16 D-ah
`l-ae b-ah -- g-ih-n @y-ow }
r16 \bracket { D-ih-s( `h-ow-l8)
`r-ae-p16 -- s-ih -- d-iy } \bracket { `b-eh( -- |
% Bar 16
t-er g-ow) `k-ae-p -- C-er
D-ih-s `m-ow( } -- \bracket { m-eh-nt) Q-ax-nd(
`h-ow) p-ih-t( `d-ow-nt8)
@p-ae-s16 h-ih-m } r8 |
}
\lyricNotation \songLyrics \songSyllables

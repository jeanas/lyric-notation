\version "2.22.0"
\include "../lyric-notation.ily"

songLyrics =
\lyricmode {
  \rhColor a lightblue
  \rhColor b red
  \rhColor c yellow
  \rhColor d green
Back when Mark Wahl -- berg was Mar -- ky Mark 
This is how we used to make the par -- ty start
We used to mix Hen with Ba -- car -- di Dark
And when it kicks in you can hard -- ly talk
And by the sixth gin you're gon' prob' -- bly crawl
And you'll be sick then and you'll prob' -- bly barf
And my pre -- dic -- tion is you're gon' prob' -- bly fall
Ei -- ther some -- where in the lob -- by or the hall -- way wall
And eve -- ry -- thing's spin -- nin', you're be -- gin -- nin' to think wo -- men
Are swim -- min' in pink lin -- en a -- gain in the sink, then in
A cou -- ple of mi -- nutes that bott -- le of Guin -- ness is fin -- ished
You are now al -- lowed to o -- ffi -- cial -- ly slap bit -- ches
You have the right to re -- main vio -- lent and start wi -- lin'
Start a fight with the same guy that was smart eye -- in' you
Get in your car, start it, and start dri -- vin'
O -- ver the is -- land and cause a for -- ty two car pile up
Earth cal -- ling, pi -- lot to co- pi -- lot
Loo -- king for life on this pla -- net, sir, no sign of it
All I can see is a bunch of smoke fly -- in'
And I'm so high that I might die if I go by it
Let me out -- ta this place, I'm out -- ta place
I'm in ou -- ter space, I've just va -- nished wi -- thout a trace
I'm go -- ing to a pre -- tty place now where the flo -- wers grow
I'll be back in an hour(1) or so}
songSyllables =
\lyricmode {
b-ae-k w-eh-n m-aa-rk `w-aa-l b-er-g w-aa-z `m-aa-r k-iy m-aa-rk D-ih-s Q-ih-z h-aw w-iy y-uw-zd t-uw m-ey-k D-ah `p-aa-r t-iy st-aa-rt w-iy y-uw-zd t-uw m-ih-ks h-eh-n w-ih-D b-ah `k-aa-r d-iy d-aa-rk Q-ah-nd w-eh-n Q-ih-t k-ih-ks Q-ih-n y-uw k-ae-n `h-aa-rd l-iy t-ao-k Q-ah-nd b-ay D-ah s-ih-ksT J-ih-n y-uh-r g-ao-n `pr-ao bl-iy kr-ao-l Q-ah-nd y-uw-l b-iy s-ih-k D-eh-n Q-ah-nd y-uw-l `pr-ao bl-iy b-aa-rf Q-ah-nd m-ay pr-iy `d-ih-k S-ah-n Q-ih-z y-uh-r g-ao-n `pr-ao bl-iy f-ao-l `Q-iy D-er `s-ah-m `w-eh-r Q-ih-n D-ah `l-aa b-iy Q-ao-r D-ah `h-ao-l `w-ey w-ao-l Q-ah-nd `Q-eh-v r-iy `T-ih-Gz `sp-ih n-ih-n ' y-uh-r `b-iy `g-ih n-ih-n t-uw T-ih-Gk `w-ih m-ah-n Q-aa-r `sw-ih m-ih-n Q-ih-n p-ih-Gk `l-ih n-ah-n Q-ah `g-eh-n Q-ih-n D-ah s-ih-Gk ' D-eh-n Q-ih-n Q-ah `k-ah p-ah-l Q-ah-v `m-ih n-ah-ts D-ae-t `b-aa t-ah-l Q-ah-v `g-ih n-ah-s Q-ih-z `f-ih n-ih-St y-uw Q-aa-r n-aw Q-ah `l-aw-d t-uw Q-ah `f-ih S-ah l-iy sl-ae-p `b-ih C-ih-z y-uw h-ae-v D-ah r-ay-t t-uw r-ih `m-ey-n `v-ay l-ah-nt Q-ah-nd st-aa-rt `w-ay l-ih-n st-aa-rt Q-ah f-ay-t w-ih-D D-ah s-ey-m g-ay D-ae-t w-aa-z sm-aa-rt `Q-ay Q-ih-n y-uw g-eh-t Q-ih-n y-ao-r k-aa-r ' st-aa-rt Q-ih-t ' Q-ah-nd st-aa-rt `dr-ay v-ih-n `Q-ow v-er D-ah `Q-ay l-ah-nd Q-ah-nd k-ah-z Q-ah `f-ao-r t-iy t-uw k-aa-r p-ay-l Q-ah-p Q-er-T `k-ao l-ih-G ' `p-ay l-ah-t t-uw CO- `p-ay l-ah-t `l-uh k-ih-G f-ao-r l-ay-f Q-aa-n D-ih-s `pl-ae n-ah-t ' s-er ' n-ow s-ay-n Q-ah-v Q-ih-t Q-ao-l Q-ay k-ae-n s-iy Q-ih-z Q-ah b-ah-nC Q-ah-v sm-ow-k `fl-ay Q-ih-n Q-ah-nd Q-ay-m s-ow h-ay D-ae-t Q-ay m-ay-t d-ay Q-ih-f Q-ay g-ow b-ay Q-ih-t l-eh-t m-iy `Q-uw t-ah D-ih-s pl-ey-s ' Q-ay-m `Q-uw t-ah pl-ey-s Q-ay-m Q-ih-n `Q-aw t-er sp-ey-s ' Q-ay-v J-ah-st `v-ae n-ih-St w-ih `T-aw-t Q-ah tr-ey-s Q-ay-m `g-ow Q-ih-G t-uw Q-ah `pr-ih t-iy pl-ey-s n-aw w-eh-r D-ah `fl-aw Q-er-z gr-ow Q-ay-l b-iy b-ae-k Q-ih-n Q-ae-n HOUR(1) Q-ao-r s-ow } \lyricNotation \songLyrics \songSyllables

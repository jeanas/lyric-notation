\version "2.22.0"
\include "../lyric-notation.ily"

songLyrics =
\lyricmode {
%Vowel Rainbow
  \rhColor uw #(x11-color 'DeepPink2)
  \rhColor uh #(x11-color 'Red)
  \rhColor el #(x11-color 'Red)
  \rhColor ow #(x11-color 'DarkOrange2)
  \rhColor ao #(x11-color 'Gold2)
  \rhColor oy #(x11-color 'LightSeaGreen)
  \rhColor aa #(x11-color 'Yellow)
  \rhColor aw #(x11-color 'RosyBrown)
  \rhColor ah #(x11-color 'Khaki)
  \rhColor ax #(x11-color 'PaleGoldenrod)
  \rhColor er #(x11-color 'BurlyWood2)
  \rhColor ay #(x11-color 'OliveDrab)
  \rhColor ae #(x11-color 'YellowGreen)
  \rhColor eh #(x11-color 'Green)
  \rhColor ey #(x11-color 'MediumSpringGreen)
  \rhColor ih #(x11-color 'Turquoise)
  \rhColor iy #(x11-color 'DeepSkyBlue)

  \rhColor UW #(x11-color 'DeepPink2)
  \rhColor UH #(x11-color 'Red)
  \rhColor EL #(x11-color 'Red)
  \rhColor OW #(x11-color 'DarkOrange2)
  \rhColor AO #(x11-color 'Gold2)
  \rhColor OY #(x11-color 'LightSeaGreen)
  \rhColor AA #(x11-color 'Yellow)
  \rhColor AW #(x11-color 'RosyBrown)
  \rhColor AH #(x11-color 'Khaki)
  \rhColor AX #(x11-color 'PaleGoldenrod)
  \rhColor ER #(x11-color 'BurlyWood2)
  \rhColor AY #(x11-color 'MediumSeaGreen)
  \rhColor AE #(x11-color 'YellowGreen)
  \rhColor EH #(x11-color 'Green)
  \rhColor EY #(x11-color 'MediumSpringGreen)
  \rhColor IH #(x11-color 'Turquoise)
  \rhColor IY #(x11-color 'DeepSkyBlue)

  \rhColor em #(x11-color 'GreenYellow)
  \rhColor en #(x11-color 'GreenYellow)
  \rhColor eg #(x11-color 'GreenYellow)

Till\rh ih0
I\rh ay0
col\rh el0
-- lapse\rh ae0
I'm\rh AY0
spil\rh ih0
-- lin'\rh IH0
these\rh iy0
raps\rh ae0
long\rh ao0
as\rh ax0
you\rh uw0
feel\rh ih0
'em\rh em0
Till\rh ih0
the\rh ax0
day\rh ey0
that\rh ax0
I\rh ay0
drop\rh aa0
you'll\rh el0
ne\rh eh0
-- ver\rh er0
say\rh ey0
that\rh ax0
I'm\rh AY0
not\rh aa0
kil\rh ih0
-- lin'\rh IH0
'em\rh em0
‘Cause\rh ax0
when\rh eh0
I\rh ay0
am\rh ae0
not,\rh aa0
then\rh eh0
I'm\rh AY0
-- a\rh ax0
stop\rh aa0
pen\rh ih0
-- nin'\rh IH0
'em\rh em0
And\rh ax0
I\rh ay0
am\rh ax0
not\rh aa0
hip\rh ih0
-- hop\rh aa0
an'\rh en0
I'm\rh AY0
just\rh ax0
not\rh aa0
E\rh eh0
-- mi\rh ih0
-- nem\rh eh0

Sub\rh ax0
-- li\rh ih0
-- mi\rh IH0
-- nal\rh el0
thoughts,\rh ao0
when\rh eh0
I'm\rh AY0
-- a\rh ax0
stop\rh aa0
send\rh ih0
-- in'\rh IH0
'em?\rh em0
Wo\rh ih0
-- men\rh IH0
are\rh er0
caught\rh aa0
in\rh ih0
webs,\rh eh0
spin\rh ih0
'em\rh em0
and\rh ax0
hock\rh aa0
ve\rh ih0
-- nom\rh em0
A\rh ax0
-- dre\rh ih0
-- na\rh ax0
-- line\rh ih0
shots\rh aa0
of\rh ax0
pe\rh eh0
-- ni\rh ih0
-- cil\rh IH0
-- lin\rh eh0
could\rh uh0
not\rh aa0
get\rh ih0
the\rh ax0
il\rh ih0
-- lin'\rh IH0
to\rh ax0
stop\rh aa0
A\rh ax0
-- mo\rh aa0
-- xi\rh ih0
-- cil\rh IH0
-- lin's\rh ih0
just\rh ax0
not\rh aa0
real\rh ih0
e\rh ih0
-- nough\rh ax0


%{ The cri\rh ih0
-- min\rh ih0
-- al,\rh el0
cop\rh aa0
kil\rh ih0
-- lin',\rh IH0
hip \rh ih0
-- hop\rh aa0
vil\rh ih0
-- lain\rh IH0
A\rh ax0
mi\rh ih0
-- ni\rh IH0
-- mal\rh el0
swap\rh aa0
to\rh ax0
cop\rh aa0
mil\rh ih0
-- lions\rh en0
of\rh ax0
Pac\rh aa0
list\rh ih0
-- en\rh IH0
-- ers\rh er0
You're\rh ER0
co\rh ah0
-- min'\rh ih0
with\rh IH0
me,\rh iy0
feel\rh IH0
it\rh ih0
or\rh  ao0
not\rh aa0
You're\rh  er0
gon\rh  ah0
-- na\rh  ax0
fear\rh  IH0
it\rh ih0
like\rh AY0
I\rh ay0
showed\rh ow0
ya\rh ah0
the\rh ax0
spi\rh iy0
-- rit\rh ih0
of\rh ax0
God\rh aa0
lives\rh ih0
in\rh IH0
us\rh ah0

You\rh uw0
hear\rh ih0
it\rh ih0
a\rh ax0
lot,\rh aa0
ly\rh IH0
-- rics\rh ih0
to\rh ax0
shock\rh aa0
Is\rh ih0
it\rh IH0
a\rh ax0
mi\rh iy0
-- ra\rh ih0
-- cle\rh el0
or\rh ao0
am\rh ae0
I\rh AY0
just\rh ah0
pro\rh aa0
-- duct\rh ah0
of\rh ax0
pop\rh aa0
fiz\rh ih0
-- zin'\rh IH0
up?\rh ah0
Fo'\rh ah0
shiz\rh ih0
-- zle,\rh el0
my\rh AY0
wiz\rh ih0
-- zle,\rh el0
this\rh ih0
is\rh IH0
the\rh ax0
plot,\rh aa0
list\rh ih0
-- en\rh IH0
up\rh ah0
You\rh uw0
biz\rh ih0
-- zles\rh el0
for\rh er0
-- got,\rh aa0
Sliz\rh ih0
-- zle\rh el0
does\rh ah0
not\rh aa0
give\rh ih0
a\rh ax0
fuck!\rh ah0 %}
}
songSyllables =
\lyricmode {
  \partial 4
r16 t-ih-l `Q-ay k-el |
@l-ae-ps8 Q-ay-m16 `sp-ih-l l-ih-n D-iy-z @r-ae-ps8 `l-ao-G16 Q-ax-z y-uw @<>f-ih-l Q-em `t-ih-l D-ah @d-ey |
D-ah-t Q-ay @dr-aa-p8 y-uh-L16 `n-eh v-er `s-ey D-ae-t Q-ay-m @n-aa-t8 `k-ih-l16 l-ih-n Q-em8 |
r16 k-ah-z16 `w-eh-n Q-ay Q-ae-m @n-aa-t8 D-eh-n16 `Q-ay m-ah @st-aa-p8 `<>p-ih-n16 n-ih-n Q-em Q-ah-nd |
`Q-ay Q-ae-m @n-aa-t8 h-ih-p16 `h-aa-p8 Q-en16 `Q-ay-m J-ah-st @n-aa-t8 `Q-eh16 m-ih n-eh-m8 |

r16 s-ah16 `^bl-ih m-ih n-el @T-ao-ts8 w-eh-n16 `Q-ay m-ah @st-aa-p8 `<>s-ih-n16 d-ih-n Q-em `w-ih |
m-ih-n Q-aa-r @k-aa-t8 Q-ih-n16 `w-eh-bz8 `sp-ih-n16 n-em Q-ah-nd @x-aa-k8 `<>v-ih16 n-em Q-ah `<>dr-ih |
n-ah l-ih-n @S-aa-ts8 Q-ah-v16 `p-eh n-ih `s-ih l-ih-n k-uh-d @n-aa-t8 `g-ih-t16 D-ah `Q-ih l-ih-n |
t-ax @st-aa-p8 Q-ah16 `m-aa-k8 s-ih16 `s-ih l-ih-nz J-ah-st @n-aa-t8 `<>r-ih-l16 Q-ih n-ah-f8 |

%{ r16 D-ah `kr-ih m-ah n-el `k-aa-p8 `k-ih16 l-ih-n h-ih-p `h-aa-p8 `v-ih16 l-ah-n Q-ah `m-ih |
n-ah m-ah-l `sw-aa-p8 t-uw16 `k-aa-p8 `m-ih-l16 y-ah-nz Q-ah-v @p-aa-k8 `l-ih16 <>s-ih n-er-z8 |
r16 y-uh-r `k-ah m-ih-n w-ih-D @m-iy8 `f-iy-l16 Q-ih-t Q-ao-r @n-aa-t8 y-uh-r16 `g-aa n-ah `f-ih-r |
Q-ih-t l-ay-k @Q-ay8 `S-ow-d16 y-aa D-ah `sp-ih r-ah-t Q-ah-v @g-aa-d8 `l-ih-v16 z-ih n-ah-s8 |

r16 y-uw16 `h-iy-r Q-ih-t Q-ah `l-aa-t8 `l-ih16 r-ih-ks t-uw `S-aa-k8 `Q-ih-z16 Q-ih-t Q-ah `m-ih |
<>r-ih k-el `Q-ao-r Q-ae-m Q-ay `J-ah-st8 `pr-aa16 d-ah-kt Q-ah-v @p-aa-p8 `f-ih16 z-ih-n Q-ah-p8 |
r16 f-ah @S-ih z-el m-ay `wh-ih z-el `D-ih-s Q-ih-z D-ah `pl-aa-t8 `l-ih16 s-ah-n Q-ah-p8 |
r16 y-uw `b-ih z-el-z f-er @g-aa-t8 `sl-ih16 z-el d-ah-z @n-aa-t8 `g-ih-v16 Q-ah @f-ah-k8 |  %}
}

\lyricNotation \songLyrics \songSyllables
\pageBreak
\oneLineLyricNotation \songLyrics \songSyllables

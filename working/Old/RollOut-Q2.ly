\version "2.22.0"
\include "../lyric-notation.ily"

songLyricsPlain =
\lyricmode {
  \rhColor a #(x11-color 'PeachPuff1)
  \rhColor A #(x11-color 'PeachPuff1)
  \rhColor b #(x11-color 'LawnGreen)
  \rhColor B #(x11-color 'LightGreen)
  \rhColor c #(x11-color 'Tomato)
  \rhColor C #(x11-color 'LawnGreen)
  \rhColor i #(x11-color 'GreenYellow)
  \rhColor iii #(x11-color 'MediumSeaGreen)
  \rhColor d #(x11-color 'IndianRed)
  \rhColor ax #(x11-color 'Khaki)
  \rhColor ii #(x11-color 'DarkKhaki)
  \rhColor f #(x11-color 'Tomato)
  \rhColor g #(x11-color 'IndianRed)
  \rhColor h #(x11-color 'Orchid2)
  \rhColor H #(x11-color 'Orchid2)
  \rhColor HH #(x11-color 'Orchid)
Tell me who's your weed man, and how do you smoke so good?
You's a su -- per -- star, boy, why you still up in the hood?
What in the world is in that bag? What you got in that bag?
A cou -- ple of cans of whoop ass, you did a good ass job of just eye -- in' me, spy -- in' me }

songLyrics =
\lyricmode {
\rhColor ax #(x11-color 'Khaki)
\rhColor a #(x11-color 'Maroon)
\rhColor b #(x11-color 'Red)
\rhColor c #(x11-color 'Orange)
\rhColor d #(x11-color 'Yellow)
\rhColor e #(x11-color 'Olive)
\rhColor f #(x11-color 'Green)
\rhColor g #(x11-color 'Lime)
\rhColor h #(x11-color 'Cyan)
\rhColor i #(x11-color 'Teal)
\rhColor j #(x11-color 'DodgerBlue)
\rhColor k #(x11-color 'RoyalBlue)
\rhColor l #(x11-color 'Blue)
\rhColor m #(x11-color 'Purple)
\rhColor n #(x11-color 'Fuchsia)

\rhColor AX #(x11-color 'Khaki)
\rhColor A #(x11-color 'Maroon)
\rhColor B #(x11-color 'Red)
\rhColor C #(x11-color 'Orange)
\rhColor D #(x11-color 'Yellow)
\rhColor E #(x11-color 'Olive)
\rhColor F #(x11-color 'Green)
\rhColor G #(x11-color 'Lime)
\rhColor H #(x11-color 'Cyan)
\rhColor I #(x11-color 'Teal)
\rhColor J #(x11-color 'DodgerBlue)
\rhColor K #(x11-color 'RoyalBlue)
\rhColor L #(x11-color 'Blue)
\rhColor M #(x11-color 'Purple)
\rhColor N #(x11-color 'Fuchsia)
Tell me
who's your weed man, and how do you smoke so good?\rh b0
You's a su -- per -- star, boy, why you still up in the hood?\rh b0
What in the world is in\rh a0 that\rh a0 bag?\rh c0 What you got in\rh a0 that\rh a0 bag?\rh c0 \shrinkLyrics 2 A\rh ax0 cou\rh ax0 -- ple\rh d0 o'\rh ax0 cans\rh c0
o'\rh ax0 whoop\rh d0 ass,\rh c0 you\rh d0 did\rh a0 a\rh ax0 good\rh d0 ass\rh c0 job\rh d0 o'\rh ax0 just\rh ax0 eye\rh H0 -- in'\rh H0 me,\rh H0 spy\rh h0 -- in'\rh h0 me\rh h0 }

songSyllables =
\lyricmode {
  \partial 8
`t-eh-l16 m-iy |
`h-uw-z y-ao-r @w-iy-d8 `m-ae-n16. Q-ah-nd32 `h-aw d-uw y-uw16 `sm-ow-k16. s-ow @g-uh-d16 _16 r16 `y-uw-z16 Q-ah |
`s-uw p-er `st-aa-r8 @b-oy w-ay16 y-uw `st-ih-l32 Q-ah-p `Q-ih-n16 D-ah @h-uh-d _16 r16 `w-ah-t32 Q-ih-n D-ah16 |
`w-er-ld16. Q-ih-z32 `Q-ih-n16 D-ae-t @b-ae-g8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @b-ae-g _16 r32 Q-ah32 `k-ah p-el Q-ah `k-ae-nz |
_32 Q-ah `w-uw-p Q-ae-s16 `y-uw32 d-ih-d Q-ah `g-uh-d Q-ae-s16 `J-ao-b Q-ah-v32 `J-ah-st16 @Q-ay Q-ih-n32 `m-iy16 @sp-ay Q-ih-n32 `m-iy16 r16 r8 | }
\lyricNotation \songLyrics \songSyllables

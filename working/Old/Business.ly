\version "2.22.0"
\include "../lyric-notation.ily"

songLyrics =
\lyricmode {
  \rhColor a lightblue
  \rhColor b red
  \rhColor c yellow
  \rhColor d green
You're 'bout to wit -- ness hip -- hop in its most pur -- est
Most raw -- est form, flow al -- most flaw -- less
Most hard -- est, most ho -- nest known ar -- tist
Chip off the old block, but old Doc is (back) }
songSyllables =
\lyricmode {
  \partial 4
r16 y-er16 `b-aw-t t-uw |
@w-ih-t8 n-eh-s16 `h-ih-p16 _16 h-aa-p8 Q-ih-n16 Q-ih-ts8 `m-ow-st @py-uh-r r-ih-st |
`m-ow-st @r-ao Q-ih-st16 `f-ao-rm8 `fl-ow16 _16 Q-aa `m-ow-st8 @fl-ao l-ih-s |
`m-ow-st @h-aa-r d-ih-st16 `m-ow-st8 @Q-aa16 _16 n-ih-st `n-ow-n8 @Q-aa-r t-ih-st |
`C-ih-p @Q-ao-f D-iy16 `Q-ow-ld8 @bl-aa-k16 _16 b-ah-t `Q-ow-ld8 @d-aa-k Q-ih-z16 `b-ae-k | }
\lyricNotation \songLyrics \songSyllables

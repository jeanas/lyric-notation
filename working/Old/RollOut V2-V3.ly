\version "2.22.0"
\include "../lyric-notation.ily"

\lyricNotation
\lyricmode {
%Q3:
Man, that car don't
come out un -- til next year, where in the fuck did you get it? That's eigh -- ty
thou -- sand bucks gone, where in the fuck did you spend it? You must have
eyes on your back, ‘cause you got mo -- ney to the cei -- lin’ \shrinkLyrics 2 And the big -- ger the cap,
the big -- ger the pee -- ling, the bet -- ter I'm fee -- ling, the more that I'm chil -- ling, wil -- ling, dril -- ling and kil -- ling the fee -- ling \shrinkLyrics 0

%Q4:
Now who's that
buc -- ked na -- ked cook fi -- xin’ three course meals? Get -- ting
goose -- bumps when her bo -- dy taps the six inch heels What in the
world is in that room, what you got in that room? \shrinkLyrics 2 A cou -- ple o' gats,
a cou -- ple o' knives, a cou -- ple o' rats, a cou -- ple o' wives now it's time to choose
\shrinkLyrics 0

%Q5:
Are you cus -- tom
made, cus -- tom paid, or you just cus -- tom fit -- ted? Play -- sta -- tion
2 up in the ride and is that Lo -- ren -- zo kit -- ted? Is that your
wife, your girl -- friend or just your main bitch? \shrinkLyrics 2 You take a pic,
while I'm rub -- bing the hips, touch the lips to the top of the dick and then whew!
\shrinkLyrics 0

%Q6:
And tell me
who's your house -- kee -- per and what you keep in your house? What a -- bout
dia -- monds and gold, is that what you keep in your mouth? What in the
world is in that case, what you got in that case? \shrinkLyrics 2 Get up out my face,
you coul -- dn't re -- late, Wait to take place at a si -- mi -- lar pace So shake, shake it!
%\shrinkLyrics 0

%Outro:
%{ Get out my busi -- ness, my busi -- ness
Stay the fuck up out my busi -- ness, ah
'Cause these nig -- gas all up in my shit and it's my busi -- ness, my busi -- ness
Stay the fuck up out my busi -- ness, 'cause it's mine, oh mine
My busi -- ness, my busi -- ness
Stay the fuck up out my busi -- ness
'Cause these nig -- gas all up in my shit
And it's my busi -- ness, my busi -- ness
Stay the fuck up out my busi -- ness, 'cause it's mine, oh mine %}
}

\lyricmode {
  \partial 4.
  % Q3:
r16 @m-ae-n16 _16 D-ae-t `k-aa-r d-ow-nt |
`k-ah-m32 Q-aw-16 Q-ah-32 `t-ih-l16 n-eh-kst `y-ih-r8 `w-eh-r32 Q-ih-n D-ah16 @f-ah-k16. d-ih-d32 y-uw16 `g-eh-t32 Q-ih-t \breathe r16

D-ae-ts16 `Q-ey t-iy |
`T-aw z-ah-nd `b-ah-ks8 @g-ao-n `w-eh-r32 Q-ih-n D-ah16 @f-ah-k16. d-ih-d32 y-uw16 `sp-eh-nd32 Q-ih-t \breathe r16.

y-uw32 `m-ah-st16 h-ae-v |
`Q-ay-z8 Q-aa-n16 y-ao-r @b-ae-k16. k-ah-z32 `y-uw16 g-aa-t `m-ah32 n-iy16 `t-uw32 D-ah16 `<>s-ih32 l-ih-n \breathe r16

Q-ah-nd32 D-ah `b-ih g-er D-ah `k-ae-p |
_32 D-ah `b-ih g-er D-ah `<>p-ih l-ih-n D-ah `b-eh t-er Q-ay-m `<>f-ih l-ih-n D-ah `m-ao-r D-ae-t Q-ay-m `C-ih l-ih-n `w-ih l-ih-n `dr-ih l-ih-n Q-ah-nd `k-ih l-ih-n D-ah `f-ih l-ih-n16 r16 |

%Q4:
r16 n-aw16 `h-uw-z D-ae-t |
`b-ah-k <>k-eh-d `n-ey k-eh-d @k-uh-k8 `f-ih-k16 s-ih-n `Tr-iy16. k-ao-rs `m-iy-lz16 _16 \breathe r16

`g-eh t-ih-n |
@g-uw-s `b-ah-mps8 w-eh-n32 h-er `b-aa16 d-iy `t-ae-ps D-ah `s-ih-ks16. Q-ih-nC `h-iy-lz16 _16 \breathe r16

`w-ah-t32 Q-ih-n D-ah16 |
`w-er-ld16. Q-ih-z32 Q-ih-n16 D-ae-t @r-uw-m8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @r-uw-m _16 \breathe r32

Q-ah32 `k-ah p-el Q-ah `g-ae-ts |
_32 Q-ah `k-ah p-el Q-ah n-ay-vz _32 Q-ah `k-ah p-el Q-ah `r-ae-ts _32 Q-ah `k-ah p-el Q-ah `w-ay-vz _32 n-aw16 Q-ih-ts32 `t-ay-m t-uw `C-uw-z8 r8 |

%Q5:
r32 Q-aa-r32 `y-uw16 `k-ah st-ah-m |
@m-ey-d8 `k-ah16 st-ah-m `p-ey-d16. Q-ao-r32 `y-uw16 J-ah-st `k-ah16. st-ah-m `f-ih32 <>t-ih-d \breathe r16

pl-ey16 `st-ey S-ah-n |
`t-uw Q-ah-p `Q-ih-n D-ah32 `r-ay-d _32 Q-ah-nd32 Q-ih-z `D-ae-t16 l-ao-r `Q-eh-n16. z-ow `k-ih32 <>t-ih-d \breathe r16

Q-ih-z16 `D-ae-t y-ao-r |
`w-ay-f y-ao-r `g-er-l8 `fr-eh-nd16 Q-ao-r `J-ah-st y-ao-r @m-ey-n8. `b-ih-C16 \breathe r16

y-uw `t-ey-k Q-ah32 `p-ih-k |
_16 `w-ay-l32 Q-ay-m `r-ah b-ih-n D-ah `h-ih-ps _32 `t-ah-C16 D-ah32 `l-ih-ps16 t-uw32 D-ah `t-aa-p Q-ah-v D-ah `d-ih-k _32 Q-ah-nd D-eh-n @w-uw16 \breathe r8. |

%Q6:
r16 Q-ah-nd16 `t-eh-l m-iy |
`h-uw-z y-ao-r `h-aw-s8 `k-iy16 p-er32 Q-ah-nd `w-ah-t16 y-uw `k-iy-p Q-ih-n y-ao-r @h-aw-s _16 \breathe r16

`w-ah-t32 Q-ah b-aw-t16 |
`d-ay m-ah-ndz Q-ah-nd `g-ow-ld _16 Q-ih-z `D-ae-t w-ah-t32 y-uw `k-iy-p16 Q-ih-n y-ao-r @m-aw-T _16 \breathe r16

`w-ah-t32 Q-ih-n D-ah16 |
`w-er-ld16. Q-ih-z32 `Q-ih-n16 D-ae-t @k-ey-s8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @k-ey-s _32 \breathe r32

`g-eh-t32 Q-ah-p `Q-aw-t16 m-ay32 `f-ey-s |
_32 y-uw `k-uh d-ah-nt r-ih `l-ey-t16 `w-ey-t32 _32 t-uw `t-ey-k16 `pl-ey-s Q-ae-t32 Q-ah `s-ih m-ih l-er `p-ey-s _32 s-ow `S-ey-k16 `S-ey-k Q-ih-t r8 |

%[OUTRO]
%{ g-eh-t Q-aw-t m-ay `b-ih-z n-ih-s m-ay `b-ih-z <>@n-ae-s \breathe st-ey D-ah f-ah-k Q-ah-p Q-aw-t m-ay `b-ih-z <>@n-ae-s \breathe Q-aa k-ah-z D-iy-z `n-ih g-uh-z Q-ao-l Q-ah-p Q-ih-n m-ay S-ih-t Q-ah-nd Q-ih-ts m-ay `b-ih-z n-ih-s m-ay `b-ih-z <>@n-ae-s \breathe st-ey D-ah @f-ah-k Q-ah-p Q-aw-t m-ay `b-ih-z <>@n-ae-s \breathe k-ah-z Q-ih-ts m-ay-n Q-ow m-ay-n

m-ay `b-ih-z n-ih-s m-ay `b-ih-z <>@n-ae-s \breathe st-ey D-ah @f-ah-k Q-ah-p Q-aw-t m-ay `b-ih-z <>@n-ae-s \breathe k-ah-z D-iy-z `n-ih g-uh-z Q-ao-l Q-ah-p Q-ih-n m-ay S-ih-t Q-ah-nd Q-ih-ts m-ay `b-ih-z n-ih-s m-ay `b-ih-z <>@n-ae-s \breathe st-ey D-ah f-ah-k Q-ah-p Q-aw-t m-ay `b-ih-z <>@n-ae-s \breathe k-ah-z Q-ih-ts m-ay-n Q-ow m-ay-n  %}
}

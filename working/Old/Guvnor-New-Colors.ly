\version "2.22.0"
\include "../lyric-notation.ily"

songLyrics =
\lyricmode {
  % 17 Official HTML Colors + DodgerBlue and RoyalBlue
    %{ \rhColor a #(x11-color 'Red)
    \rhColor b #(x11-color 'Orange)
    \rhColor c #(x11-color 'Yellow)
    \rhColor d #(x11-color 'YellowGreen)
    \rhColor e #(x11-color 'Green)
    \rhColor f #(x11-color 'LimeGreen)
    \rhColor g #(x11-color 'Cyan)
    \rhColor h #(x11-color 'Teal)
    \rhColor i #(x11-color 'DodgerBlue)
    \rhColor j #(x11-color 'RoyalBlue)
    \rhColor k #(x11-color 'Blue)
    \rhColor l #(x11-color 'Purple)
    \rhColor m #(x11-color 'Fuchsia)
    \rhColor n #(x11-color 'Maroon)

    \rhColor A #(x11-color 'Red)
    \rhColor B #(x11-color 'Orange)
    \rhColor C #(x11-color 'Yellow)
    \rhColor D #(x11-color 'YellowGreen)
    \rhColor E #(x11-color 'Green)
    \rhColor F #(x11-color 'LimeGreen)
    \rhColor G #(x11-color 'Cyan)
    \rhColor H #(x11-color 'Teal)
    \rhColor I #(x11-color 'DodgerBlue)
    \rhColor J #(x11-color 'RoyalBlue)
    \rhColor K #(x11-color 'Blue)
    \rhColor L #(x11-color 'Purple)
    \rhColor M #(x11-color 'Fuchsia)
    \rhColor N #(x11-color 'Maroon) %}
  %{ % NEW Vowel Rainbow Pink
    \rhColor uw #(x11-color 'DeepPink2)
    \rhColor uh #(x11-color 'Red)
    \rhColor el #(x11-color 'Red)
    \rhColor ow #(x11-color 'DarkOrange2)
    \rhColor ao #(x11-color 'Gold2)
    \rhColor oy #(x11-color 'LightSeaGreen)
    \rhColor aa #(x11-color 'Yellow)
    \rhColor aw #(x11-color 'RosyBrown)
    \rhColor ah #(x11-color 'Khaki)
    \rhColor ax #(x11-color 'PaleGoldenrod)
    \rhColor er #(x11-color 'BurlyWood2)
    \rhColor ay #(x11-color 'OliveDrab)
    \rhColor ae #(x11-color 'YellowGreen)
    \rhColor eh #(x11-color 'Green)
    \rhColor ey #(x11-color 'MediumSpringGreen)
    \rhColor ih #(x11-color 'Turquoise)
    \rhColor iy #(x11-color 'DeepSkyBlue)

    \rhColor UW #(x11-color 'DeepPink2)
    \rhColor UH #(x11-color 'Red)
    \rhColor EL #(x11-color 'Red)
    \rhColor OW #(x11-color 'DarkOrange2)
    \rhColor AO #(x11-color 'Gold2)
    \rhColor OY #(x11-color 'LightSeaGreen)
    \rhColor AA #(x11-color 'Yellow)
    \rhColor AW #(x11-color 'RosyBrown)
    \rhColor AH #(x11-color 'Khaki)
    \rhColor AX #(x11-color 'PaleGoldenrod)
    \rhColor ER #(x11-color 'BurlyWood2)
    \rhColor AY #(x11-color 'OliveDrab)
    \rhColor AE #(x11-color 'YellowGreen)
    \rhColor EH #(x11-color 'Green)
    \rhColor EY #(x11-color 'MediumSpringGreen)
    \rhColor IH #(x11-color 'Turquoise)
    \rhColor IY #(x11-color 'DeepSkyBlue)

    \rhColor em #(x11-color 'GreenYellow)
    \rhColor en #(x11-color 'GreenYellow)
    \rhColor eg #(x11-color 'GreenYellow) %}
% vowel rainbow
    \goodColors
    \pinkRainbow

  Catch\rh EH0
  a\rh AX0
  throat\rh OW0
   -- ful\rh EL0
  from\rh EM0
  the\rh AX0
  fi\rh AY0
   -- re\rh ER0
  vo\rh OW0
   -- caled\rh EL0
  With\rh Ih0
  ash\rh AE0
  and\rh EN0
  mol\rh OW0
   -- ten\rh IH0
  glass\rh AE0
  like\rh AY0
  Ey\rh Ay0
   -- jaf\rh AH0
   -- jal\rh Ay0
   -- la\rh AH0
   -- jö\rh OW0
   -- kull\rh EL0
% Normal labels
%{ Catch\rh d0
a\rh ax0
throat\rh b1
-- ful\rh b2
from\rh em0
the\rh ax0
fi\rh a1
-- re\rh a2
vo\rh b1
-- caled\rh b2
With\rh c0
ash\rh d0
and\rh ax0
mol\rh b1
 -- ten\rh c0
glass\rh d0
like\rh A1
Ey\rh a1
-- jaf\rh a2
-- jal\rh A1
-- la\rh A2
-- jö\rh b1
-- kull\rh b2 %}
}
songSylsLabeled =
\lyricmode {
  \slurDashed
  r8 \sB "" `k-eh-C16(\( Q-ah @Tr-ow-t8 \eb f-el) \sb "" `fr-em16( D-ah `f-ay Q-er @v-ow8 \eB k-el-d16)\) \sB "" w-ih-D(\( |
  @Q-ae-S8) Q-en16( \eb `m-ow-L \sb "" t-ih-n16 @gl-ae-s8) l-ay-k16( `Q-ay y-ax-f `J-ay l-ax @J-ow \eB k-el)\) r8 |
  }

songSyllables =
\lyricmode {
r8 `k-ae-C16 Q-ah @Tr-ow-t8 f-el `fr-ah-m16 D-ah `f-ay Q-er @v-ow8 k-el16 w-ih-D |
@Q-ae-S8 Q-ah-nd16 `m-ow-l t-eh-n16 @gl-ae-s8 l-ay-k16 `Q-ay y-ax-f `J-ay l-ax @J-ow k-el r8 | }
\lyricNotation \songLyrics \songSyllables
\pageBreak
\oneLineLyricNotation \songLyrics \songSyllables
\pageBreak
\lyricNotation \songLyrics \songSylsLabeled
\pageBreak
\oneLineLyricNotation \songLyrics \songSylsLabeled

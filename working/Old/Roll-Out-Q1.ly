\version "2.22.00"

\include "../lyric-notation.ily"

rollOutLyrics =
\lyricmode {
  \rhColor ax #(x11-color 'Khaki)
  \rhColor a #(x11-color 'Maroon)
  \rhColor b #(x11-color 'Red)
  \rhColor c #(x11-color 'Orange)
  \rhColor d #(x11-color 'Yellow)
  \rhColor e #(x11-color 'Olive)
  \rhColor f #(x11-color 'Green)
  \rhColor g #(x11-color 'Lime)
  \rhColor h #(x11-color 'Cyan)
  \rhColor i #(x11-color 'Teal)
  \rhColor j #(x11-color 'DodgerBlue)
  \rhColor k #(x11-color 'RoyalBlue)
  \rhColor l #(x11-color 'Blue)
  \rhColor m #(x11-color 'Purple)
  \rhColor n #(x11-color 'Fuchsia)

  \rhColor AX #(x11-color 'Khaki)
  \rhColor A #(x11-color 'Maroon)
  \rhColor B #(x11-color 'Red)
  \rhColor C #(x11-color 'Orange)
  \rhColor D #(x11-color 'Yellow)
  \rhColor E #(x11-color 'Olive)
  \rhColor F #(x11-color 'Green)
  \rhColor G #(x11-color 'Lime)
  \rhColor H #(x11-color 'Cyan)
  \rhColor I #(x11-color 'Teal)
  \rhColor J #(x11-color 'DodgerBlue)
  \rhColor K #(x11-color 'RoyalBlue)
  \rhColor L #(x11-color 'Blue)
  \rhColor M #(x11-color 'Purple)
  \rhColor N #(x11-color 'Fuchsia)
  %{ \rhColor a lightblue
  \rhColor b red
  \rhColor c yellow
  \rhColor d green %}
  % Pickup
  Now where'd\rh a1 you\rh a2 |
  % 1
  get\rh a3 that\rh a4 plat’ -- num
  chain with\rh a5 them\rh a6
  dia -- monds in\rh b1 it?\rh b2
  Where'd\rh a1 you\rh a2 |
  % 2
  get\rh a3 that\rh a4 mat -- chin’
  Benz with\rh a5 them\rh a6
  win -- dows tin\rh b1 -- ted?\rh b2
  Who them |
  % 3
  girls you be
  wit’ when you be
  ri\rh c1 -- din’\rh c2 through?\rh d0
  Man\rh c1 I\rh c1 ain't\rh c2 got\rh c3 |
  % 4
  \shrinkLyrics 2
  no\rh c1 -- thin’\rh c2 to\rh d0 prove,\rh d0 I\rh c1 paid\rh c2
  my\rh c3 dues,\rh d0 brea\rh c1 -- kin’\rh c2 the\rh c3 rules,\rh d0
  I\rh c1 shake\rh c2 fools\rh d0 while\rh c1 I'm\rh c1
  ta\rh c2 -- kin’\rh c2 a\rh c3 cruise!\rh d0 |
}

rollOutSyllables =
\lyricmode {
  \partial 4
  % Pickup
  r16 n-aw16( `w-ey-rd y-uw |
  % 1
  <>`g-ih-t16 D-ae-t `pl-ae-t n-ah-m
  C-ey-n8 w-ih-T16 D-eh-m
  <>@d-aa8 m-ah-ndz16 `Q-ih-n32 Q-ih-t) '
  r8 `w-ey-rd16( y-uw |
  % 2
  <>`g-ih-t16 D-ae-t `m-ae-t C-ih-n
  <>@b-ih-nz8 w-ih-T16 D-eh-m
  `w-ih-n16. d-ow-z `t-ih-n32 <>t-ih-d) '
  r8 `h-uw16( D-eh-m |
  % 3
  <>@g-el-z8 y-uw16 b-iy
  `w-ih-t16. w-eh-n32 `y-uw16 b-iy
  \sB "" <>@r-aa8 d-ih-n16 \eB `Tr-uw16)
  _16 ' r32 \sB "" m-ae-n32 `Q-ay Q-ey-nt g-aa-t16 |
  % 4
  `n-ah32 T-ih-n t-uw \eB `pr-uw-v16 \sB "" Q-ay32 `p-ey-d16
  m-ah32 \eB `d-uw-z16. \sB "" `br-ey32 k-ih-n D-ah `r-uw-Lz32
  \eB _32 \sB "" Q-ay32 `S-ey-k16 \eB `f-uw-Lz \sB "" w-ay-l32 Q-ay-m
  `t-ey k-ih-n Q-ah \eb @kr-uw-z8 r32 |
}


\lyricNotation \rollOutLyrics \rollOutSyllables
\pageBreak
\oneLineLyricNotation \rollOutLyrics \rollOutSyllables

\version "2.22.0"
\include "../lyric-notation.ily"


songLyricsPlain =
  \lyricmode {
    \rhColor a #(x11-color 'LightSeaGreen)
    \rhColor A #(x11-color 'LightSeaGreen)
    \rhColor b #(x11-color 'LightGreen)
    \rhColor B #(x11-color 'LightGreen)
    \rhColor c #(x11-color 'LawnGreen)
    \rhColor C #(x11-color 'LawnGreen)
    \rhColor i #(x11-color 'GreenYellow)
    \rhColor iii #(x11-color 'MediumSeaGreen)
    \rhColor d #(x11-color 'Gold)
    \rhColor ax #(x11-color 'Khaki)
    \rhColor ii #(x11-color 'DarkKhaki)
    \rhColor f #(x11-color 'Tomato)
    \rhColor g #(x11-color 'IndianRed)
    \rhColor h #(x11-color 'Magenta)
    \rhColor H #(x11-color 'Orchid)
    \rhColor HH #(x11-color 'Orchid)

  % Pickup
  His palms
  % Bar
  are swea -- ty,
  knees weak,
  arms are hea --
  vy There's vo -- mit
  % Bar
  on his swea -- ter
  al -- rea -- dy,
  mom's spa -- ghe --
  tti He's ner -- vous
  % Bar
  but on the
  sur -- face he looks
  calm and rea --
  dy To drop
  % Bar
  bombs
  but he keeps
  on for -- ge --
  tting What he wrote
  % Bar
  down, the
  whole crowd
  goes so loud
  He o --
  % Bar 6
  pens his mouth,
  but the words
  won't come out
  He's cho --
  % Bar 7
  king, how?
  Eve -- ry -- bo -- dy's
  jo -- king now
  The clock's
  % Bar 8
  run out,
  time's up,
  o -- ver, blaow!
  Snap
  % Bar 9
  back to re --
  a -- li -- ty, oh
  there goes
  gra -- vi -- ty, oh
  % Bar 0
  There goes
  Ra -- bbit, he choked,
  he's so
  mad but he won't
  % Bar 1
  Give up
  that ea -- sy, no,
  he won't
  have it, he knows
  % Bar
  His whole
  back's to these ropes,
  it don't
  ma -- tter, he's dope
  % Bar
  He knows
  that but he's broke,
  he's so
  stag -- nant, he knows
  % Bar
  When he goes
  back to this mo --
  bile home,
  that's when it's
  % Bar
  Back to the
  lab a -- gain yo,
  this whole
  rhap -- so -- dy, Bet --
  % Bar 6
  ter go cap -- ture
  this mo -- ment and
  hope it don't
  pass him
  }

songSylsPlain =
  \lyricmode {
  \partial 4
  % Pickup
  r16 h-ih-z16 @p-aa-lmz8 |
  % Bar 1
  Q-aa-r16 `sw-eh -- t-iy8
  @n-iy-z `w-iy-k
  @Q-aa-rm z-aa-r16 `h-eh --
  v-iy D-eh-rz `v-aa -- m-ih-t |
  % Bar 2
  `Q-aa-n h-ih-z `sw-eh -- t-er
  Q-aa-l -- `r-eh -- d-iy8
  @m-aa-mz sp-ah16 -- `g-eh --
  t-iy16 h-iy-z `n-er -- v-ah-s |
  % Bar 3
  r16. b-ah-t32 `Q-aa-n16 D-ah
  `s-er -- f-ah-s `h-iy l-uh-ks
  @k-aa-lm8 Q-ah-nd16 `r-eh --
  d-iy t-uw `dr-aa-p8 |
  % Bar 4
  @b-aa-mz8 r8
  `b-ah-t16 h-iy `k-iy-ps8
  @Q-aa-n f-er16 -- `g-eh --
  t-ih-G16 `w-ah-t h-iy `r-ow-t |
  % Bar 5
  _16 @d-aw-n8 D-ah16
  `h-ow-l8 @kr-aw-d
  `g-ow-z s-ow16 `l-aw-d
  _16  h-iy `y-ow8 -- |
  % Bar 6
  p-ih-nz16 h-ih-z `m-aw-T8
  `b-ah-t16 D-ah `w-er-dz8
  `w-ow-nt k-ah16 `m-aw-t
  r16 h-iy-z `C-ow8 -- |
  % Bar 7
  k-ih-G16 @h-aw8 r16
  `Q-eh-v16 -- r-iy -- `b-ah -- d-iy-z
  `J-ow8 -- k-ih-G16 @n-aw
  r16 D-ah `kl-aa-ks8 |
  % Bar 8
  r-ah-n16 @Q-aw-t8 r16
  `t-ay-mz8 Q-ah-p
  `Q-ow -- v-er16 @bl-aw16
  _16 r16 `sn-ae-p8 |
  % Bar 9
  @b-ae-k8 t-uw16 r-iy --
  `y-ae l-ih -- t-iy @Q-ow
  r16 D-eh-r16 `g-ow-z8
  `gr-ae16 -- v-ih -- t-iy @Q-ow |
  % Bar 10
  r16 D-eh-r16 `g-ow-z8
  `r-ae16 -- b-ih-t h-iy @C-ow-kt
  r16 h-iy-z16 `s-ow8
  `m-ae-d16 b-ah-t h-iy @w-ow-nt |
  % Bar 11
  _16   g-ih16 `<>v-uh-p8
  `D-ae-t16 Q-iy -- z-iy @n-ow
  r16 h-iy `w-ow-nt8
  `h-ae16 v-ih-t h-iy @n-ow-z |
  % Bar 12
  _16 h-ih-z `h-ow-l8
  `b-ae-ks16 t-ah D-iy-z @r-ow-ps
  r16 Q-ih-t `d-ow-nt8
  `m-ae16 -- t-er h-iy-z @d-ow-p |
  % Bar 13
  r16 h-iy `n-ow-z8
  `D-ae-t16 b-ah-t h-iy-z @br-ow-k
  r16 h-iy-z `s-ow8
  `st-ae-g16 -- n-ih-nt h-iy @n-ow-z |
  % Bar 14
  _16 w-eh-n32 h-iy `g-ow-z8
  \tuplet 3/2 8 { `b-ae-k8 t-ah16 } D-ih-s16 @m-ow --
  _16 b-el `h-ow-m8
  \tuplet 3/2 8 { `D-ae-ts8 w-eh-n16 } Q-ih-ts16 r16 |
  % Bar 15
  @b-ae-k8 t-uw16 D-ah
  `l-ae b-ah -- g-ih-n @y-ow
  r16 D-ih-s `h-ow-l8
  `r-ae-p16 -- s-ih -- d-iy `b-eh -- |
  % Bar 16
  t-er g-ow `k-ae-p -- C-er
  D-ih-s `m-ow -- m-eh-nt Q-ax-nd
  `h-ow p-ih-t `d-ow-nt8
  @p-ae-s16 h-ih-m r8 |
  }

songLyricsDomains =
  \lyricmode {
    \rhColor a #(x11-color 'LightSeaGreen)
    \rhColor A #(x11-color 'LightSeaGreen)
    \rhColor b #(x11-color 'LightGreen)
    \rhColor B #(x11-color 'LightGreen)
    \rhColor c #(x11-color 'LawnGreen)
    \rhColor C #(x11-color 'LawnGreen)
    \rhColor i #(x11-color 'GreenYellow)
    \rhColor iii #(x11-color 'MediumSeaGreen)
    \rhColor d #(x11-color 'Gold)
    \rhColor ax #(x11-color 'Khaki)
    \rhColor ii #(x11-color 'DarkKhaki)
    \rhColor f #(x11-color 'Tomato)
    \rhColor g #(x11-color 'IndianRed)
    \rhColor h #(x11-color 'Magenta)
    \rhColor H #(x11-color 'Orchid)
    \rhColor HH #(x11-color 'Orchid)

  % Pickup
  His\rh i0 palms\rh a0
  % Bar 1
  are\rh b0 swea\rh b0 -- ty,\rh b0
  knees\rh c0 weak,\rh C0
  arms\rh a0 are\rh b0 hea\rh b0 --
  vy\rh b0 There's\rh ii0 vo\rh a0 -- mit\rh i0
  % Bar 2
  on\rh a0 his\rh i0 swea\rh B2 -- ter\rh B1
  al\rh b0 -- rea\rh b0 -- dy,\rh b0
  mom's\rh a0 spa\rh b0 -- ghe\rh b0 --
  tti\rh b0 He's\rh i0 ner\rh d0 -- vous\rh d0
  % Bar 3
  but\rh ax0 on\rh a0 the\rh ax0
  sur\rh d0 -- face\rh d0 he\rh c0 looks\rh iii0
  calm\rh a0 and\rh b0 rea\rh b0 --
  dy\rh b0 To\rh iii0
%%% XXX
  drop\rh a0
  % Bar 4
%%% CAPITAL "A" Required for "bombs"
%%% or no highlight appears
  bombs\rh a0
  but\rh b1 he\rh b3 keeps\rh b3
  on\rh a0 for\rh b0 -- ge\rh b0 --
  tting\rh b0 What\rh ax0 he\rh i0 wrote\rh f0
  % Bar 5
  down,\rh g0 the\rh ax0
  whole\rh f0 crowd\rh g0
  goes\rh f0 so\rh f0 loud\rh g0
  He\rh i0 o\rh f0 --
  % Bar 6
  pens\rh i0 his\rh i0 mouth,\rh g0
  but\rh ax0 the\rh ax0 words\rh ax0
  won't\rh f0 come\rh ax0 out\rh g0
  He's\rh i0 cho\rh f0 --
  % Bar 7
  king,\rh i0 how?\rh g0
  Eve\rh ii0 -- ry\rh i0 -- bo\rh ax0 -- dy's\rh i0
  jo\rh f0 -- king\rh i0 now\rh g0
  The\rh ax0 clock's\rh f0
  % Bar 8
  run\rh ax0 out,\rh g0
  time's\rh i0 up,\rh ax0
  o\rh f0 -- ver,\rh ax0 blaow!\rh g0
  Snap\rh h0
  % Bar 9
  back\rh H0 to\rh H0 re\rh H0 --
  a\rh h0 -- li\rh h0 -- ty,\rh h0 oh\rh f0
  there\rh ii0 goes\rh f0
  gra\rh h0 -- vi\rh h0 -- ty,\rh h0 oh\rh f0
  % Bar 10
  There\rh ii0 goes\rh f0
  Ra\rh h0 -- bbit,\rh h0 he\rh h0 choked,\rh f0
  he's\rh i0 so\rh f0
  mad\rh H0 but\rh H0 he\rh H0 won't\rh f0
  % Bar 11
  Give\rh i0 up\rh f0
  that\rh h0 ea\rh h0 -- sy,\rh h0 no,\rh f0
  he\rh i0 won't\rh f0
  have\rh h0 it,\rh h0 he\rh h0 knows\rh f0
  % Bar 12
  His\rh i0 whole\rh f0
  back's\rh H0 to\rh H0 these\rh H0 ropes,\rh f0
  it\rh i0 don't\rh f0
  ma\rh H0 -- tter,\rh H0 he's\rh H0 dope\rh f0
  % Bar 13
  He\rh i0 knows\rh f0
  that\rh H0 but\rh H0 he's\rh H0 broke,\rh f0
  he's\rh i0 so\rh f0
  stag\rh h0 -- nant,\rh h0 he\rh h0 knows\rh f0
  % Bar 14
  When\rh ii0 he\rh i0 goes\rh f0
  back\rh H0 to\rh H0 this\rh H0 mo\rh f0 --
  bile\rh iii0 home,\rh f0
  that's\rh h0 when\rh h0 it's\rh h0
  % Bar 15
  Back\rh H0 to\rh H0 the\rh H0
  lab\rh HH0 a\rh HH0 -- gain\rh HH0 yo,\rh f0
  this\rh i0 whole\rh f0
  rhap\rh h0 -- so\rh h0 -- dy,\rh h0 Bet\rh ii0 --
  % Bar 16
  ter\rh ax0 go\rh f0 cap\rh H0 -- ture\rh H0
  this\rh H0 mo\rh f0 -- ment\rh ii0 and\rh ax0
  hope\rh f0 it\rh c0 don't\rh f0
  pass\rh h1 him\rh h3
  }

songSylsLabeled =
  \lyricmode {
    \partial 4
  % Pickup
    \phrasingSlurDotted \slurDashed
  r16 \labeledBracket "𝛂1" h-ih-z16( @p-aa-lmz8)  |
  % Bar 1
  Q-aa-r16 `sw-eh -- \eB t-iy8
  @n-iy-z( `w-iy-k)
  \labeledBracket "𝛂2" @Q-aa-rm z-aa-r16 `h-eh --
  \eB v-iy \slurDown \labeledBracket "𝛃1" \labeledBracket "𝛂3" D-eh-rz( `v-aa -- m-ih-t) |
  % Bar 2
  `Q-aa-n( \eB h-ih-z) `sw-eh -- t-er \slurUp
  Q-aa-l -- `r-eh -- \eB d-iy8
  \labeledBracket "𝛂4" @m-aa-mz sp-ah16 -- `g-eh --
  \eB t-iy16 h-iy-z( `n-er -- v-ah-s) |
  % Bar 3
  r16. b-ah-t32( `Q-aa-n16) D-ah(
  `s-er -- f-ah-s) `h-iy( l-uh-ks)
  \labeledBracket "𝛂5" @k-aa-lm8 Q-ah-nd16 `r-eh --
  \eB d-iy \labeledBracket "𝛃2" \labeledBracket "𝛂6" t-uw( `dr-aa-p8) |
  % Bar 4
  \eB @b-aa-mz8 r8
  `b-ah-t16 h-iy \eB `k-iy-ps8
  \labeledBracket "𝛂7" @Q-aa-n f-er16 -- `g-eh --
  \eB t-ih-G16 \labeledBracket "𝛄1" `w-ah-t( h-iy `r-ow-t) |
  % Bar 5
  _16 \eB @d-aw-n8 \labeledBracket "𝛄2" D-ah16(
  `h-ow-l8) \eB @kr-aw-d
  \labeledBracket "𝛄3" `g-ow-z( s-ow16) `l-aw-d
  \eB _16 \labeledBracket "𝛄4" h-iy( `y-ow8) -- |
  % Bar 6
  p-ih-nz16( h-ih-z \eB `m-aw-T8)
  \labeledBracket "𝛄5" `b-ah-t16( D-ah `w-er-dz8
  `w-ow-nt) k-ah16( \eB `m-aw-t)
  r16 \labeledBracket "𝛄6" h-iy-z( `C-ow8) -- |
  % Bar 7
  k-ih-G16( \eB @h-aw8) r16
  \labeledBracket "𝛄7" `Q-eh-v16( -- r-iy -- `b-ah -- d-iy-z
  `J-ow8) -- k-ih-G16( \eB @n-aw)
  r16 \labeledBracket "𝛄8" D-ah( `kl-aa-ks8) |
  % Bar 8
  r-ah-n16( \eB @Q-aw-t8) r16
  \labeledBracket "𝛄9" `t-ay-mz8( Q-ah-p
  `Q-ow) -- v-er16( @bl-aw16)
  \eB _16 r16 `sn-ae-p8 |
  % Bar 9
  \labeledBracket "𝛅*1" @b-ae-k8 t-uw16 r-iy --
  `y-ae l-ih -- t-iy \eB @Q-ow
  r16 \labeledBracket "𝛅2" D-eh-r16( `g-ow-z8)
  `gr-ae16 -- v-ih -- t-iy \eB @Q-ow |
  % Bar 10
  r16 \labeledBracket "𝛅3" D-eh-r16( `g-ow-z8)
  `r-ae16 -- b-ih-t h-iy \eB @C-ow-kt
  r16 \labeledBracket "𝛅4" h-iy-z16( `s-ow8)
  `m-ae-d16 b-ah-t h-iy \eB @w-ow-nt |
  % Bar 11
  _16 \labeledBracket "𝛅5" g-ih16( `<>v-uh-p8)
  `D-ae-t16 Q-iy -- z-iy \eB @n-ow
  r16 \labeledBracket "𝛅6" h-iy( `w-ow-nt8)
  `h-ae16 v-ih-t h-iy \eB @n-ow-z |
  % Bar 12
  _16 \labeledBracket "𝛅7" h-ih-z( `h-ow-l8)
  `b-ae-ks16 t-ah D-iy-z \eB @r-ow-ps
  r16 \labeledBracket "𝛅8" Q-ih-t( `d-ow-nt8)
  `m-ae16 -- t-er h-iy-z \eB @d-ow-p |
  % Bar 13
  r16 \labeledBracket "𝛅9" h-iy( `n-ow-z8)
  `D-ae-t16 b-ah-t h-iy-z \eB @br-ow-k
  r16 \labeledBracket "𝛅10" h-iy-z( `s-ow8)
  `st-ae-g16 -- n-ih-nt h-iy \eB @n-ow-z |
  % Bar 14
  _16 \labeledBracket "𝛅11" w-eh-n32( h-iy `g-ow-z8)
  \tuplet 3/2 8 { `b-ae-k8 t-ah16 } D-ih-s16 @m-ow --
  \eB _16 \labeledBracket "𝛅12" b-el( `h-ow-m8)
  \tuplet 3/2 8 { `D-ae-ts8 w-eh-n16 } \eB Q-ih-ts16 r16 |
  % Bar 15
  \labeledBracket "𝛅*13" @b-ae-k8 t-uw16 D-ah
  `l-ae b-ah -- g-ih-n \eB @y-ow
  r16 \labeledBracket "𝛅14" D-ih-s( `h-ow-l8)
  `r-ae-p16 -- s-ih -- \eB d-iy \labeledBracket "𝛅15" `b-eh( -- |
  % Bar 16
  t-er g-ow) `k-ae-p -- C-er
  D-ih-s \eB `m-ow( -- \labeledBracket "𝛅16" m-eh-nt) Q-ax-nd(
  `h-ow) p-ih-t( `d-ow-nt8)
  @p-ae-s16 \eB h-ih-m r8 |
  }
\lyricNotation \songLyricsDomains \songSylsLabeled
%{ \pageBreak %}
%{ \lyricNotation \songLyricsPlain \songSylsPlain %}

\version "2.22.0"
\include "../lyric-notation.ily"

songLyrics =
\lyricmode {
  \rhColor a red
  \rhColor b lightblue
  \rhColor c yellow
  \rhColor d green
You're 'bout to wit -- ness hip -- hop\rh a0 in its most\rh b0 pur\rh a0 -- est
Most\rh b0 raw\rh a0 -- est form,\rh b0 flow\rh b0 al -- most\rh b0 flaw\rh a0 -- less
Most\rh b0 hard\rh a0 -- est, most\rh b0 ho\rh a0 -- nest known\rh b0 ar\rh a0 -- tist
Chip off\rh a0 the old\rh b0 block,\rh a0 but old\rh b0 Doc\rh a0 is (back) }
songSyllables =
\lyricmode {
  \partial 4
r16 y-er16 `b-aw-t t-uw |
@w-ih-t8 n-eh-s16 `h-ih-p16 _16 h-aa-p8 Q-ih-n16 Q-ih-ts8 `m-ow-st @py-uu-r r-ih-st |
`m-ow-st @r-ao Q-ih-st16 `f-ao-rm8 `fl-ow16 _16 Q-aa `m-ow-st8 @fl-ao l-ih-s |
`m-ow-st @h-aa-r d-ih-st16 `m-ow-st8 @Q-aa16 _16 n-ih-st `n-ow-n8 @Q-aa-r t-ih-st |
`C-ih-p @Q-ao-f D-iy16 `Q-ow-ld8 @bl-aa-k16 _16 b-ah-t `Q-ow-ld8 @d-aa-k Q-ih-z16 `b-ae-k | } \lyricNotation \songLyrics \songSyllables

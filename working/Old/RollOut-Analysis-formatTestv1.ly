\version "2.22.0"
\include "../lyric-notation.ily"

qTwoLyrics =
  \lyricmode
  {
    \goodColors
      %{ \override Staff.MeasureCounter.count-from = #5
      \startMeasureCount %}
  % 1
  Tell me
      %{ }
      \lyricmode {
        \goodColors
        \override Staff.MeasureCounter.count-from = #5
        \startMeasureCount %}
  who's your weed man, and how do you smoke so good?\rh G0
  % 2
  You's a
    su -- per -- star, boy, why you still up in the hood?\rh G0
  % 3
  What\rh I1 in the
    world is in\rh I2 that\rh I3 bag?\rh D0 What\rh I1 you got in\rh I2 that\rh I3 bag?\rh D0
  % 4
  \shrinkLyrics 2
  A cou -- ple\rh B0 of cans\rh D0
    of whoop\rh B0 ass,\rh D0 you did a good\rh B0 ass\rh D0 job of just eye\rh P1 -- in'\rh P2 me,\rh P3 spy\rh PP1 -- in'\rh PP2 me\rh PP3 |
    \shrinkLyrics 0

  }
    %{ \stopMeasureCount %}

qTwoSyls =
  \lyricmode
  {
    \override Staff.MeasureCounter.count-from = #4
    \startMeasureCount
  \partial 8
  % 1
  `t-eh-l16 m-iy |
      %{ }
      \lyricmode {
       %}
  `h-uw-z y-ao-r @w-iy-d8 `m-ae-n16. Q-ah-nd32 `h-aw d-uw y-uw16 `sm-ow-k16. s-ow @g-uh-d16 _16 ' r16
  % 2
  `y-uw-z16 Q-ah |
  `s-uw p-er `st-aa-r8 @b-oy w-ay16 y-uw `st-ih-l32 Q-ah-p `Q-ih-n16 D-ah @h-uh-d _16 ' r16
  % 3
  `w-ah-t32 Q-ih-n D-ah16 |
  `w-er-ld16. Q-ih-z32 `Q-ih-n16 D-ae-t @b-ae-g8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @b-ae-g _16 ' r32
  % 4
  Q-ah32 `k-ah p-el Q-ah `k-ae-nz |
  _32 Q-ah `w-uw-p Q-ae-s16 `y-uw32 d-ih-d Q-ah `g-uh-d Q-ae-s16 `J-ao-b Q-ah-v32 `J-ah-st16 @Q-ay Q-ih-n32 `m-iy16 @sp-ay Q-ih-n32 `m-iy16 ' r16 r8 |
  \stopMeasureCount
  }

\lyricNotation \qTwoLyrics \qTwoSyls
\pageBreak
\oneLineLyricNotation \qTwoLyrics \qTwoSyls

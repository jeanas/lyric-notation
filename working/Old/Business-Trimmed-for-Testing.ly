\version "2.22.0"
\include "../lyric-notation.ily"
%{
gA = "<323_2222>-0"
  gA-Two = "<323_2222>-2"
  gA-Doz = "<323_2222>-12"
  gB-Doz = "<333232>-12"
  gB-Alt = "<332_323>-15"
  gC-Four = "<332_2222>-4"
  gD-Doz = "<332_332>-12" %}

businessColors = \lyricmode {
  \rhColor a #(x11-color 'Gold2)
    \rhColor A #(x11-color 'Gold2)
    \rhColor b #(x11-color 'DeepPink2)
    \rhColor B #(x11-color 'DeepPink2)
    \rhColor BB #(x11-color 'DeepPink1)
    \rhColor c #(x11-color 'LightSkyBlue)
    \rhColor C #(x11-color 'LightSkyBlue)
    %{ \rhColor CC #"#afeeee" %}
    \rhColor CC #(x11-color 'DeepSkyBlue)
    \rhColor d #(x11-color 'LightSalmon)
    \rhColor D #(x11-color 'Salmon)
    \rhColor e #(x11-color 'GreenYellow)
    \rhColor ee #(x11-color 'GreenYellow)
    \rhColor E #(x11-color 'SpringGreen)
    \rhColor EE #(x11-color 'SpringGreen)
    \rhColor f #"#d2d229"
    \rhColor F #"#d2d229"
    \rhColor g #(x11-color 'MediumSpringGreen)
    \rhColor G #(x11-color 'MediumSpringGreen)
    \rhColor H #(x11-color 'GreenYellow)
    \rhColor h #(x11-color 'GreenYellow)
    \rhColor i #"#7fffd4"
    \rhColor I #"#7fffd4"

}

songLyricsA-MB =
  \lyricmode {
    \businessColors
  You\rh d0 'bout\rh a0 to\rh d0
  wit\rh c0 -- ness\rh C0 hip\rh c0
  -- hop\rh a0 in\rh c0
  its\rh C0 most\rh b0
  pur\rh D0 -- est\rh c0
  Most\rh b0 raw\rh a0
  -- est\rh c0 form,\rh BB0 flow\rh b0
  al\rh a0 -- most\rh b0
  flaw\rh a0 -- less\rh c0
  Most\rh b0 hard\rh a0
  -- est,\rh c0 most\rh b0 ho\rh a0
  -- nest\rh c0 known\rh b0
  ar\rh a0 -- tist\rh c0
  Chip\rh C0 off\rh a0
  the\rh CC0 old\rh b0 block,\rh a0
  but\rh e0 old\rh b0
  Doc\rh a0 is\rh c0 (back\rh E0)
  }

songSylsGroovesA =
  \lyricmode {
    \partial 4
    \genGrooveTupSettings
    \bracketSettings
     %{ \override Stem.details.beamed-lengths = #'(12) %}

      %{ \set TextScript.outside-staff-priority = ##f %}
      %{ \set Script.outside-staff-priority = ##f %}
      %{ \override TupletBracket.padding = #0.6 %}
        %{ \override TupletBracket.direction = #UP %}
        %{ \override Voice.TupletBracket.layer = #3 %}
        %{ \override Voice.HorizontalBracketText.layer = #4 %}
        %{ \override TupletNumber.font-size = #-4 %}
        %{ \override TupletNumber.font-shape = #'upright %}
        %{ \override TupletNumber.font-series = #'bold %}
        %{ \override TupletNumber.whiteout = ##t %}
        %{ \override TupletBracket.edge-height = #'(0.4 . 0.4) %}
        %{ \override TupletBracket.thickness = #1 %}


  r16
  y-uw16
    \gTwo {
  `b-aw
  t-uw }
  |
    \gClass "<323_2222>-0" {
    \gTre {
  @w-ih-t8
  n-ih-s16 }
    \gTwo {
  `h-ih-p16
    _16 }
    \gTre {
  `h-aa-p8
  Q-ih-n16 }
    \gTwo {
  `Q-ih-ts8 }
    \gTwo {
  \labeledBracket "A*"
  `m-ow-st }
    \gTwo {
  @py-uh-r }
    \gTwo {
  \eB
  r-ih-st } }
  |
    \gTwo {
  \labeledBracket "A1"
  `m-ow-st }
    \gClass "<323_2222>-2" {
    \gTre {
  @r-aa
  \eB
  Q-ih-st16 }
    \gTwo {
  `f-ao-rm8 }
    \gTre {
  `fl-ow16 _16
  Q-aa-l }
    \gTwo {
  \labeledBracket "A1"
  `m-ow-st8 }
    \gTwo {
  @fl-aa }
    \gTwo {
  \eB
  l-ih-s }
    |
    \gTwo {
  \labeledBracket "A1"
  `m-ow-st }
    }
    \gClass "<323_2222>-2" {
    \gTre {
  @h-aa-r
  \eB
  d-ih-st16 }
    \gTwo {
  \labeledBracket "A1"
  `m-ow-st8 }
    \gTre {
  @Q-aa16 _16
  \eB
  n-ih-st }
    \gTwo {
  \labeledBracket "A1"
  `n-ow-n8 }
    \gTwo {
  @Q-aa-r }
    \gTwo {
  \eB
  t-ih-st }
  |
    \gTwo {
  `C-ih-p }
    }
    \gClass "<332_323>-15?" {
    \gTre {
  @Q-aa-f
  D-iy16 }
    \gTwo {
  \labeledBracket "A2"
  `Q-ow-ld8 }
    \gTre {
  @bl-aa-k16 _16
  \eB b-ah-t }
    \gTwo {
  \labeledBracket "A1"
  `Q-ow-ld8 }
    \gTre {
  @d-aa-k8
  \eB
  Q-ih-z16 }
    \once \override TupletBracket.connect-to-neighbor = #'(#f . #t)
    \gTre {
  `b-ae-k
    | } }
  }

songLyricsB-MB =
  \lyricmode {
    \businessColors
  Doc\rh a0 is\rh c0 (back)\rh E0 |
    Looks\rh d0 like\rh f0 Bat\rh E0 -- man\rh EE0 brought\rh a0 his\rh c0 own\rh b0 Ro\rh a0 -- bin\rh c0 |
    Oh\rh b0 God,\rh a0 Sad\rh e0 -- dam's\rh A0 got\rh a0 his\rh C0 own\rh b0 La\rh a0 -- den\rh E0 |
    With\rh c0 his\rh C0 own\rh b0 pri\rh F0 -- vate\rh E0 plane,\rh g0 his\rh c0 own\rh b0 pi\rh F0 -- lot\rh e0 |
    Set\rh E0 to\rh d0 blow\rh b0   col\rh a0 -- lege\rh c0 dorm\rh BB0 room\rh d0 doors\rh BB0 off\rh a0 the\rh e0 hin\rh c0 |
    }

songSylsGroovesB =
  \lyricmode {
    \partial 4
      \genGrooveTupSettings
    \bracketSettings
        %{ \override HorizontalBracket.direction = #UP %}
        %{ \override HorizontalBracket.edge-height = #'(0.0 . 0.0) %}
        %{ \override HorizontalBracket.staff-padding = #3.165 %}
        %{ \override HorizontalBracket.thickness = #-0 %}
        %{ \override HorizontalBracketText.font-size = #-2.75 %}
        %{ \override HorizontalBracketText.font-series = #BOLD %}
        %{ \override HorizontalBracket.shorten-pair = #'(2 . 3.85) %}
        %{ \override HorizontalBracketText.font-shape = #italic %}
        %{ \override HorizontalBracket.bracket-visibility = ##f %}
        %{ \override HorizontalBracket.bracket-flare = #'(0.0 . 0.0) %}
      %{ \override TupletBracket.padding = #0.9 %}
  \gClass "<332_323>-15?" { \gTre {
      %{ % \once \override HorizontalBracket.shorten-pair = #'(2.3 . 0) %}
      %{ % \sG \gB-Doz %}
  @d-aa-k8
  Q-ih-z16 }
    \gTre {   `b-ae-k
  |
  r16 l-UH-ks16 }
    } \gClass "<332_2222>-4" { \gTwo {
  `l-AY-k8 }
    \gTre {
  `b-AE-t m-AE-n16 }
    \gTre {
          % \once \override HorizontalBracket.shorten-pair = #'(2 . 0)
      % \sG \gC-Four
  @br-AA-t _16 h-IH-z
    } \gTwo {
  \labeledBracket "A1"
  `Q-OW-n8 }
    \gTwo {
  @r-AA }
    \gTwo {
  \eB
  b-IH-n }
  |
    \gTwo {
  \labeledBracket "A1⇒2"
  Q-OW }
    } \gClass "<323_2222>-2" { \gTre {
  `g-AA-d s-Ax16 }
    \gTwo {
  `d-AA-z8 }
    \gTre {
      % \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
      % \sG \gA-Two
  `g-AA-t16 _16
  \eB
  h-IH-z }
    \gTwo {
  \labeledBracket "A2"
  `Q-OW-n8 }
    \gTwo {
  @l-AA }
    \gTwo {
  \eB
  d-EHN }
  |
    \gTwo {
  `w-IH-D16 h-IH-z }
    }
    \gClass "<332_2222>-4" {
    \gTwo {
  \labeledBracket "A2*"
  `Q-OW-n8 }
    \gTre {
  `pr-AY
  \eB
  v-AX-t16 }
    \gTre {
        % \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
    % \sG \gC-Four
  `pl-EY-n _16
  h-IH-z }
    \gTwo {
  \labeledBracket "A2*"
  `Q-OW-n8 }
    \gTwo {
  @p-AY }
    \gTwo {
  \eB
  l-AX-t }
  |
    \gTwo {
  `s-EH-t16
  t-UW }
    }
    \gClass "<332_332>-12" {
    \gTwo {
  \labeledBracket "A1"
  `bl-OW8 }
    \gTre {
      % \once \override HorizontalBracket.shorten-pair = #'(0 . 0)
    % \sG \gD-Doz
  `k-AA
  \eB
  l-IH-J16 }
    \gTre {
  `d-AO-rm _16
  r-UW-m }
    \gTwo {
  \labeledBracket "A1⇒2" % ⇒⤇⇒⇒⇒⥤⥤⇒
  `d-AO-rz8 }
    }
    {
        \once \override TupletBracket.connect-to-neighbor = #'(#f . #t)
    \gClass "<323_2222>-12" {
    \gTre {
        % \once \override HorizontalBracket.shorten-pair = #'(1.5 . 0)
        % \once \override HorizontalBracketText.font-size = #-3.1
    % \sG \gA-Doz
  `Q-AA-f
  \eB
  D-AX16 }
    \gTwo   @h-IH-n }
    }
  |
  }

songLyricsC-MB =
  \lyricmode {
    \businessColors
  off\rh a0 the\rh e0 hin\rh c0 |
    -- ges\rh c0 Or\rh BB0 -- an\rh c0 -- ges,\rh C0 peach,\rh CC0 pears,\rh E0 plums,\rh e0 syr\rh e0 -- in\rh c0 |
    -- ges\rh C0 (Vrum,\rh e0 vrum!)\rh e0 Yeah,\rh E0 here\rh I0 I\rh F0 come,\rh e0 I'm\rh F0 in\rh c0 |
    -- ches\rh C0 A\rh e0 -- way\rh g0 from\rh e0 you,\rh d0 dear,\rh i0 fear\rh I0 none\rh e0 Hip\rh c0 |
    -- hop\rh a0 is\rh c0 in\rh C0 a\rh e0 state\rh g0 of\rh e0 nine\rh F0 one\rh e0 one,\rh e0 so...\rh b0 |
  }

songSylsGroovesC =
  \lyricmode {
    \partial 4
      \bracketSettings
      \genGrooveTupSettingsAlt
      %{ \override TupletNumber.before-line-breaking =
      #(lambda (grob)
         (if (not-first-broken-spanner? grob)
             (ly:grob-set-property! grob 'stencil '()))) %}
    %{ \override TupletBracket.padding = #0.1 %}
    %{ \override HorizontalBracket.direction = #UP %}
    %{ \override HorizontalBracket.edge-height = #'(0.0 . 0.0) %}
    %{ \override HorizontalBracket.staff-padding = #3.165 %}
    %{ \override HorizontalBracket.thickness = #-0 %}
    %{ \override HorizontalBracketText.font-size = #-2.75 %}
    %{ \override HorizontalBracketText.font-series = #BOLD %}
    %{ \override TupletBracket.shorten-pair = #'(0.0 . 2.0) %}
    %{ \override HorizontalBracket.shorten-pair = #'(2 . 3.85) %}
  \gClass "<323_2222>-12" {
    %{ \once \override TupletBracket.connect-to-neighbor = #'(#t . #f)  %}
      %{ \tweak TupletBracket.connect-to-neighbor #'(#t . #f) %}
      %{ \tweak TupletBracket.edge-text #'("A1⇒2" . "") %}
      %{ \tweak TupletBracket.shorten-pair #'(2 . 2) %}
      %{ \uB { %}
    \gTre {
  `Q-AA-f8
  \labeledBracket "B1"
  D-AX16 }
    %{ } %}
    \gTwo {
  @h-IH-n
  |
  _16 }
    \gTre {
  \eB
  `J-IH-z8
  \labeledBracket "B"
  Q-AO-r16 }
    \gTwo {
  @<>r-IH-n8 }
    \gTwo {
  \eb
  `J-IH-z }
    \gTwo {
  \labeledBracket "C"
  `p-IY-C }
    %{ \once \override TupletBracket.shorten-pair = #'(0.0 . 2.0) %}
      \gTwo {
  p-EH-rz } }
    \gClass "<323_2222>-12" {
    \gTre {
  \eb
  `pl-EM-z
  \labeledBracket "B1"
  s-er16 }
    \gTwo {
  @Q-IH-n
  |
   _16 }
    \gTre {
  \eb
  `J-IH-z8 r16 }
    \gTwo {
  `vr-EM8 }
    \gTwo {
  vr-EM }
    \gTwo {
  \labeledBracket "C"
  y-AE }
    \gTwo {
  `h-IH-r16
  Q-AY } }
    \gClass "<323_2222>-12" {
    \gTre {
        % \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
     % \sG \gA-Doz
  \eb
  `k-EM8
  \labeledBracket "B"
  Q-AY-m16 }
    \gTwo {
      @Q-IH-n
  |
  _16 }
    \gTre {
  \eb
  `C-IH-z8
  Q-AX 16 }
    \gTwo {
  `w-EY8 }
    \gTwo {
  `fr-EM16
  y-UW }
    \gTwo {
  \labeledBracket "C"
  `d-IH-r8 }
    \gTwo {
  `f-IH-r }
    }
    \gClass "<323_2222>-12" {
    \gTre {
          % \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
     % \sG \gA-Doz
  \eb
  `n-EN r16 }
    \gTwo {
  `h-IH-p
  |
  _16 }
    \gTre {
  `h-AA-p8
  Q-IH-z16 }
    \gTwo {
  `Q-IH-n
  Q-AH }
    \gTwo {
  `st-EY-t
  Q-AX-v }
    \gTwo {
  \labeledBracket "C"
  `n-AY-n8 }
    \gTwo {
  `w-EN }
    }
    \gTwo {
  \eb @w-EN }
    \gTwo {
  s-OW }
  |
    }


  songLyricsPlain =
    \lyricmode {

    You 'bout to wit -- ness hip -- hop in its most pur -- est
    Most raw -- est form, flow al -- most flaw -- less
    Most hard -- est, most ho -- nest known ar -- tist
    Chip off the old block, but old Doc is (back) }
  songSylsPlain =
    \lyricmode {
      \partial 4
    r16 y-uw16 `b-aw-t t-uw |
    @w-ih-t8 n-ih-s16 `h-ih-p16
    _16 h-aa-p8
  Q-ih-n16
    Q-ih-ts8 `m-ow-st
    @py-uh-r r-ih-st |
    `m-ow-st @r-ao
    Q-ih-st16 `f-ao-rm8 `fl-ow16
    _16 Q-aa-l `m-ow-st8
    @fl-ao l-ih-s |
    `m-ow-st @h-aa-r
    d-ih-st16 `m-ow-st8 @Q-aa16
    _16 n-ih-st `n-ow-n8
    @Q-aa-r t-ih-st |
    `C-ih-p @Q-ao-f
    D-iy16 `Q-ow-ld8 @bl-aa-k16
    _16 b-ah-t `Q-ow-ld8
    @d-aa-k Q-ih-z16 `b-ae-k
    | }

\lyricNotation \songLyricsA-MB \songSylsGroovesA
  \pageBreak
\lyricNotation \songLyricsB-MB \songSylsGroovesB
  \pageBreak
\lyricNotation \songLyricsC-MB \songSylsGroovesC

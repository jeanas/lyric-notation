\version "2.22.0"
\include "../lyric-notation.ily"

songLyrics =
\lyricmode {
  % 17 Official HTML Colors + DodgerBlue and RoyalBlue
  \rhColor a #(x11-color 'Red)
  \rhColor b #(x11-color 'Orange)
  \rhColor c #(x11-color 'Yellow)
  \rhColor d #(x11-color 'YellowGreen)
  \rhColor e #(x11-color 'Green)
  \rhColor f #(x11-color 'LimeGreen)
  \rhColor g #(x11-color 'Cyan)
  \rhColor h #(x11-color 'Teal)
  \rhColor i #(x11-color 'DodgerBlue)
  \rhColor j #(x11-color 'RoyalBlue)
  \rhColor k #(x11-color 'Blue)
  \rhColor l #(x11-color 'Purple)
  \rhColor m #(x11-color 'Fuchsia)
  \rhColor n #(x11-color 'Maroon)

  \rhColor A #(x11-color 'Red)
  \rhColor B #(x11-color 'Orange)
  \rhColor C #(x11-color 'Yellow)
  \rhColor D #(x11-color 'YellowGreen)
  \rhColor E #(x11-color 'Green)
  \rhColor F #(x11-color 'LimeGreen)
  \rhColor G #(x11-color 'Cyan)
  \rhColor H #(x11-color 'Teal)
  \rhColor I #(x11-color 'DodgerBlue)
  \rhColor J #(x11-color 'RoyalBlue)
  \rhColor K #(x11-color 'Blue)
  \rhColor L #(x11-color 'Purple)
  \rhColor M #(x11-color 'Fuchsia)
  \rhColor N #(x11-color 'Maroon)


  % Vowel Rainbow
  \rhColor uw #(x11-color 'DeepPink2)
  \rhColor uh #(x11-color 'Red)
  \rhColor el #(x11-color 'Red)
  \rhColor ow #(x11-color 'DarkOrange2)
  \rhColor ao #(x11-color 'Gold2)
  \rhColor oy #(x11-color 'LightSeaGreen)
  \rhColor aa #(x11-color 'Yellow)
  \rhColor aw #(x11-color 'RosyBrown)
  \rhColor ah #(x11-color 'Khaki)
  \rhColor ax #(x11-color 'PaleGoldenrod)
  \rhColor er #(x11-color 'BurlyWood2)
  \rhColor ay #(x11-color 'OliveDrab)
  \rhColor ae #(x11-color 'YellowGreen)
  \rhColor eh #(x11-color 'Green)
  \rhColor ey #(x11-color 'MediumSpringGreen)
  \rhColor ih #(x11-color 'Turquoise)
  \rhColor iy #(x11-color 'DeepSkyBlue)

  \rhColor UW #(x11-color 'DeepPink2)
  \rhColor UH #(x11-color 'Red)
  \rhColor EL #(x11-color 'Red)
  \rhColor OW #(x11-color 'DarkOrange2)
  \rhColor AO #(x11-color 'Gold2)
  \rhColor OY #(x11-color 'LightSeaGreen)
  \rhColor AA #(x11-color 'Yellow)
  \rhColor AW #(x11-color 'RosyBrown)
  \rhColor AH #(x11-color 'Khaki)
  \rhColor AX #(x11-color 'PaleGoldenrod)
  \rhColor ER #(x11-color 'BurlyWood2)
  \rhColor AY #(x11-color 'MediumSeaGreen)
  \rhColor AE #(x11-color 'YellowGreen)
  \rhColor EH #(x11-color 'Green)
  \rhColor EY #(x11-color 'MediumSpringGreen)
  \rhColor IH #(x11-color 'Turquoise)
  \rhColor IY #(x11-color 'DeepSkyBlue)

  \rhColor em #(x11-color 'GreenYellow)
  \rhColor en #(x11-color 'GreenYellow)
  \rhColor eg #(x11-color 'GreenYellow)

You're 'bout to wit\rh c0 -- ness\rh C0 hip\rh c0 -- hop\rh a0 in\rh c0 its\rh c0 most\rh b0 pur\rh d0 -- est\rh c0
Most\rh b0 raw\rh a0 -- est\rh c0 form,\rh a0 flow\rh b0 al\rh a0 -- most\rh b0 flaw\rh a0 -- less\rh c0
Most\rh b0 hard\rh a0 -- est,\rh c0 most\rh b0 ho\rh a0 -- nest\rh c0 known\rh b0 ar\rh a0 -- tist\rh c0
Chip\rh C0 off\rh a0 the\rh c0 old\rh b0 block,\rh a0 but old\rh b0 Doc\rh a0 is\rh c0 (back) }

songSyllables =
\lyricmode {
  \partial 4
r16 y-er16 `b-aw-t t-uw |
@w-ih-t8 n-eh-s16 `h-ih-p16 _16 h-aa-p8 Q-ih-n16 Q-ih-ts8 \sB "𝛂*" `m-ow-st @py-uuh-r \eB r-ih-st |
\sB "𝛂" `m-ow-st @r-ao \eB Q-ih-st16 `f-ao-rm8 `fl-ow16 _16 Q-aa \sB "𝛂" `m-ow-st8 @fl-ao \eB l-ih-s |
\sB "𝛂" `m-ow-st @h-aa-r \eB d-ih-st16 \sB "𝛂" `m-ow-st8 @Q-aa16 _16 \eB n-ih-st \sB "𝛂" `n-ow-n8 @Q-aa-r \eB t-ih-st |
`C-ih-p @Q-ao-f D-iy16 `Q-ow-ld8 @bl-aa-k16 _16 b-ah-t \sB "𝛂" `Q-ow-ld8 @d-aa-k \eB Q-ih-z16 `b-ae-k | }
\lyricNotation \songLyrics \songSyllables
\pageBreak
\oneLineLyricNotation \songLyrics \songSyllables

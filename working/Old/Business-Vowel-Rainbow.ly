\version "2.22.0"
\include "../lyric-notation.ily"

songLyricsPlain =
  \lyricmode {

  You're 'bout to wit -- ness hip -- hop in its most pur -- est
  Most raw -- est form, flow al -- most flaw -- less
  Most hard -- est, most ho -- nest known ar -- tist
  Chip off the old block, but old Doc is (back) }

songLyricsOh =
  \lyricmode {
    \rhColor a red
    \rhColor b lightblue
    \rhColor c yellow
    \rhColor d green
    You're 'bout to wit -- ness hip -- hop\rh a0 in its most\rh b0 pur\rh a0 -- est
    Most\rh b0 raw\rh a0 -- est form,\rh b0 flow\rh b0 al -- most\rh b0 flaw\rh a0 -- less
    Most\rh b0 hard\rh a0 -- est, most\rh b0 ho\rh a0 -- nest known\rh b0 ar\rh a0 -- tist
    Chip off\rh a0 the old\rh b0 block,\rh a0 but old\rh b0 Doc\rh a0 is (back) }

songLyricsRainbow =
  \lyricmode {
  % 17 Official HTML Colors + DodgerBlue and RoyalBlue
      \goodColors
      \pinkRainbow

  You're\rh ER0 'bout\rh AW0 to\rh UW0 wit\rh IH0 -- ness\rh EH0 hip\rh IH0 -- hop\rh AA0 in\rh IH0 its\rh IH0 most\rh OW0 pur\rh UH0 -- est\rh IH0
  Most\rh OW0 raw\rh AA0 -- est\rh IH0 form,\rh AA0 flow\rh OW0 al\rh AA0 -- most\rh OW0 flaw\rh AA0 -- less\rh IH0
  Most\rh OW0 hard\rh AA0 -- est,\rh IH0 most\rh OW0 ho\rh AA0 -- nest\rh IH0 known\rh OW0 ar\rh AA0 -- tist\rh IH0
  Chip\rh Ih0 off\rh AO0 the\rh IY0 old\rh OW0 block,\rh AA0 but\rh AX0 old\rh OW0 Doc\rh AA0 is\rh IH0 (back)\rh AE0 }

songLyricsMB =
  \lyricmode {
    \rhColor a red
    \rhColor b lightblue
    \rhColor c gold
    \rhColor C gold
    \rhColor e yellow
    \rhColor d pink

  You're 'bout to wit\rh c0 -- ness\rh e0 hip\rh c0 -- hop\rh a0 in\rh c0 its\rh c0 most\rh b0 pur\rh d0 -- est\rh c0
  Most\rh b0 raw\rh a0 -- est\rh c0 form,\rh a0 flow\rh b0 al\rh a0 -- most\rh b0 flaw\rh a0 -- less\rh c0
  Most\rh b0 hard\rh a0 -- est,\rh c0 most\rh b0 ho\rh a0 -- nest\rh c0 known\rh b0 ar\rh a0 -- tist\rh c0
  Chip\rh C0 off\rh a0 the\rh c0 old\rh b0 block,\rh a0 but old\rh b0 Doc\rh a0 is\rh c0 (back) }
songSyllablesMB =
  \lyricmode {
    \partial 4
  r16 y-er16 `b-aw-t t-uw |
  @w-ih-t8 n-eh-s16 `h-ih-p16 _16 h-aa-p8 Q-ih-n16 Q-ih-ts8 \sB "𝛂*" `m-ow-st @py-uuh-r \eB r-ih-st |
  \sB "𝛂" `m-ow-st @r-ao \eB Q-ih-st16 `f-ao-rm8 `fl-ow16 _16 Q-aa \sB "𝛂" `m-ow-st8 @fl-ao \eB l-ih-s |
  \sB "𝛂" `m-ow-st @h-aa-r \eB d-ih-st16 \sB "𝛂" `m-ow-st8 @Q-aa16 _16 \eB n-ih-st \sB "𝛂" `n-ow-n8 @Q-aa-r \eB t-ih-st |
  `C-ih-p @Q-ao-f D-iy16 `Q-ow-ld8 @bl-aa-k16 _16 b-ah-t \sB "𝛂" `Q-ow-ld8 @d-aa-k \eB Q-ih-z16 `b-ae-k | }

songSylsLabeled =
  \lyricmode {
    \partial 4
  r16 y-er16 `b-aw-t t-uw |
  @w-ih-t8 n-eh-s16 `h-ih-p16 _16 h-aa-p8 Q-ih-n16 Q-ih-ts8 \sB "𝛂*" `m-ow-st @py-uh-r \eB r-ih-st |
  \sB "𝛂" `m-ow-st @r-ao \eB Q-ih-st16 `f-ao-rm8 `fl-ow16 _16 Q-aa \sB "𝛂" `m-ow-st8 @fl-ao \eB l-ih-s |
  \sB "𝛂" `m-ow-st @h-aa-r \eB d-ih-st16 \sB "𝛂" `m-ow-st8 @Q-aa16 _16 \eB n-ih-st \sB "𝛂" `n-ow-n8 @Q-aa-r \eB t-ih-st |
  `C-ih-p @Q-ao-f D-iy16 `Q-ow-ld8 @bl-aa-k16 _16 b-ah-t \sB "𝛂" `Q-ow-ld8 @d-aa-k \eB Q-ih-z16 `b-ae-k | }

songSylsPlain =
  \lyricmode {
    \partial 4
  r16 y-er16 `b-aw-t t-uw |
  @w-ih-t8 n-eh-s16 `h-ih-p16 _16 h-aa-p8 Q-ih-n16 Q-ih-ts8 `m-ow-st @py-uh-r r-ih-st |
  `m-ow-st @r-ao Q-ih-st16 `f-ao-rm8 `fl-ow16 _16 Q-aa `m-ow-st8 @fl-ao l-ih-s |
  `m-ow-st @h-aa-r d-ih-st16 `m-ow-st8 @Q-aa16 _16 n-ih-st `n-ow-n8 @Q-aa-r t-ih-st |
  `C-ih-p @Q-ao-f D-iy16 `Q-ow-ld8 @bl-aa-k16 _16 b-ah-t `Q-ow-ld8 @d-aa-k Q-ih-z16 `b-ae-k | }
\lyricNotation \songLyricsRainbow \songSylsLabeled
\pageBreak
\oneLineLyricNotation \songLyricsRainbow \songSylsLabeled
\pageBreak
\lyricNotation \songLyricsPlain \songSylsPlain
\pageBreak
\oneLineLyricNotation \songLyricsPlain \songSylsPlain

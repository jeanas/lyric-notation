\version "2.22.0"
\include "../lyric-notation.ily"
% Make Slurs break horizontal
%{ #(define (horizontalise-broken-slurs grob)
    (let*
     ((orig (ly:grob-original grob))
      (siblings (if (ly:grob? orig)
                    (ly:spanner-broken-into orig)
                    '()))
      (control-points (ly:grob-property grob 'control-points)))
     (if (>= (length siblings) 2)
         (let ((new-control-points
                (cond
                 ((eq? (first siblings) grob)
                  (list (first control-points)
                        (second control-points)
                        (third control-points)
                        (third control-points)))
                 ((eq? (last siblings) grob)
                  (list (second control-points)
                        (second control-points)
                        (third control-points)
                        (fourth control-points)))
                 (else control-points))))
           (ly:grob-set-property! grob 'control-points
           new-control-points))))) %}

songLyrics =
    \lyricmode {
    \goodColors
    \rgbVowelRainbow
  So nas -- ty that it's prob' -- bly some -- what of a tra -- ves -- ty
  Ha -- ving me, then he told the peo -- ple 'You can call me, Your Ma -- jes -- ty!'
  Keep your bat -- te -- ry charged, you know it won't
  Stick yo, and it's not his fault you kick slow
  Shoul -- d've let your trick ho chick hold your sick glow
  Plus no -- bo -- dy coul -- dn't do no -- thin' once he let the brick go
  And you know I know that's a bunch of snow
  The beat is so but -- ter, Peep the slow cut -- ter
  As he ut -- ter the calm flow don't talk a -- bout my moms, yo
  Some -- times he rhyme quick, some -- times he rhyme slow
  Or vice ver -- sa, whip up a slice of nice verse pie
  Hit it on the first try, vil -- lain the worst guy
  Spot hot tracks like spot a pair of fat ass -- es
  Shots of the scotch from out of square shot glass -- es
  And he won't stop till he got the mass -- es
  And show 'em what they know not through flows of hot mo -- lass -- es
  Do it like the ro -- bot to head -- spin to boo -- ga -- loo
  Took a few min -- utes to con -- vince the ave -- rage bug -- a -- boo
  It's ug -- ly, like look at you, it's a damn shame
  Just re -- mem -- ber ALL CAPS when you spell the man name
  }
songSyllables =
    \lyricmode {
        \bracketSettings
        \bracketTextBeforeBreak
        %{ \genGrooveTupSettings %}
        %{ \grooveTupletTextBeforeBreak %}
        %{ \partial 4 %}

  `s-ow8\( `n-ae8
  st-iy16 `D-ae-t Q-ih-ts `pr-ao
  bl-iy `s-ah-m \tuplet 3/2 8 { w-ah-t16 `Q-ah-v Q-ah }
  `tr-ae16 v-ah st-iy `h-ae |

  v-ih-G m-iy `D-eh-n h-iy
  `t-ow-ld d-ah `p-iy p-ah-l
  `y-uw k-ae-n `k-ao-l m-iy \tuplet 3/2 4 {
  y-ao-r8 @m-ae J-ah } |
  `st-iy16\) r8 `k-iy-p16\(
  y-ao-r `b-ae t-er Q-iy
  C-aa-rJd8\) r8
  y-uw16\( `n-ow Q-ih-t `w-ow-nt |

  `st-ih-k8 y-ow
  r16 Q-ah-nd16 `Q-ih-ts n-aa-t \tuplet 3/2 4 {
  `h-ih-z f-ao-lt y-uw }
  `k-ih-k8 sl-ow\) |

  \tuplet 5/4 8 { r16 `S-uh16\( d-ah `l-eh-t y-ao-r }
  r16 `tr-ih-k16 h-ow r16
  `C-ih-k h-ow-ld r16 y-ah
  `s-ih-k8 gl-ow\) |

  r16 `pl-uh-s16\( n-ow `b-aa \tuplet 5/4 8 {
  d-iy16 `k-uh d-ah-nt d-uw `n-ah }
  t-ih-n16 `w-ah-ns h-iy `l-eh-t
  D-ah `br-ih-k8 g-ow16\) |


  r8. Q-ah-nd\( \tuplet 3/2 8 {
  `y-uw16 n-ow r16 } \tuplet 3/2 8 {
  `Q-ay16 n-ow r16}
  `D-ae-ts16 Q-ah `b-ah-nC Q-ah-v
  `sn-ow8\) r8 |

  D-ah16\( `b-iy-t Q-ih-z `s-ow
  r16 `b-ah t-er\) r16
  r16 `p-iy-p\( D-ah `sl-ow r16
  `k-ah32 t-er `Q-ae-z16 h-iy |

  `Q-ah t-er D-ah `k-aa-m
  r16 fl-ow\) r8 \tuplet 5/4 8 {
  d-ow-nt\( `t-ao-k Q-ah `b-aw-t m-ay }
  `m-aa-mz8 y-ow\) |


  `s-ah-m\( t-ay-mz16 r16
  h-iy32 `r-ay-m r16 kw-ih-k16 `s-ah-m
  t-ay-mz r16 h-iy `r-ay-m
  r16 `sl-ow Q-ao-r r16 |

  `v-ay-s r16 v-er s-ah\) r8 \tuplet 3/2 8 {
  `w-ih-p16\( Q-ah-p Q-ah }
  `sl-ay-s16 Q-ah-v `n-ay-s r16
  `v-er-s8 p-ay |

  `h-ih-t16 Q-ih-t `Q-aa-n D-ah
  `f-er-st8 tr-ay
  `v-ih16 l-ah-n r16 D-ah
  `w-er-st8 g-ay\) |


  `sp-aa-t\( h-aa-t
  `tr-ae-ks l-ay-k
  `sp-aa-t16 Q-ah `p-eh-r Q-ah-v
  f-ae-t `Q-ae r16 s-ah-z\) |

  `S-aa-ts\( r16 Q-ah-v D-ah
  `sk-aa-C r32 `fr-ah-m32 Q-aw-t16 Q-ah-v
  `skw-eh-r8 S-aa-t
  `gl-ae s-ah-z\) |

  r8 `Q-ah-nd16\( h-iy
  `w-ow-nt8 st-aa-p
  `t-ih-l16 h-iy `g-aa-t D-ah
  `m-ae8 s-ah-z |


  r16. Q-en \tuplet 3/2 8 { `S-ow16 Q-em `w-ah-t }
  D-ey `n-ow \tuplet 3/2 8 { n-aa-t16 r16 Tr-uw16 }
  f`l-ow-z Q-ah-v \tuplet 3/2 8 { `h-aa-t16 r16 m-ah16 }
  `l-ae8 s-ah-z\) |

  `d-uw16\( Q-ih-t `l-ay-k D-ah
  `r-ow b-aa-t r16 t-uw
  `h-eh-d sp-ih-n r16 t-uw
  `b-uw g-ah `l-uw\) |

  \tuplet 3/2 8 { r16 r16 `t-uh-k16\( } \tuplet 3/2 8 { Q-ah16 `fy-uw16 r16 }
  `m-ih16 n-ah-ts `t-uw k-ah-n
  `v-ih-ns D-ah `Q-ae-v r-ih-J
  `b-ah g-ah `b-uw r16 |


  r8 Q-ih-ts16 `Q-ah
  gl-iy r16 l-ay-k `l-uh-k
  Q-ae-t `y-uw Q-ih-ts Q-ah
  `d-ae-m8 S-ey-m\) |

  r16 J-ah-st\( r-ih `m-eh-m
  r16 b-er Q-ao-l r16
  `k-ae-ps r16 `w-eh-n y-uw
  `sp-eh-l D-ah `m-ae-n r16 |

  `n-ey-m8\) }

songLyricsKA-A =
  \lyricmode {
  \goodColors
  \rgbVowelRainbow
  So nas\rh A0 -- ty\rh A0 that it's prob' -- bly some -- what of a tra\rh A0 -- ves\rh A0 -- ty\rh A0
  Ha\rh a0 -- ving\rh a0 me,\rh a0 then he told the peo -- ple You can call me, 'Your Ma\rh A0 -- jes\rh A0 -- ty!'\rh A0
  Keep your bat\rh A0 -- te\rh A0 -- ry\rh A0 charged, you know it won't
  Stick\rh J0 yo,\rh I0 and it's not his fault you kick\rh J0 slow\rh I0
  Shoul -- d've let your trick\rh J0 ho\rh I0 chick\rh J0 hold\rh I0 your sick\rh J0 glow\rh I0
  Plus no -- bo -- dy coul -- dn't do no -- thin' once he let the brick\rh J0 go\rh I0
  And you know\rh I0 I know\rh I0 that's a bunch of snow\rh I0
  The beat is so\rh I0 but\rh D0 -- ter,\rh D0 Peep the slow\rh I0 cut\rh D0 -- ter\rh D0
  As he ut\rh D0 -- ter\rh D0 the calm\rh C0 flow\rh I0 don't talk a -- bout my moms,\rh D0 yo\rh I0
  Some -- times he rhyme quick, some -- times he rhyme\rh D0 slow\rh I0 Or
}
songLyricsKA-B =
  \lyricmode {
  \goodColors
  \rgbVowelRainbow

  vice\rh P0 ver\rh P0 -- sa,\rh P0 whip up a slice\rh P0 of\rh P0 nice verse\rh P0 pie\rh P0
  Hit it on the first\rh P0 try,\rh P0 vil -- lain the worst\rh P0 guy\rh P0
  Spot\rh N0 hot\rh n0 tracks\rh B0 like spot\rh N0 a pair of fat\rh B0 ass\rh B0 -- es\rh B0
  Shots\rh N0 of the scotch\rh N0 from out of square shot\rh N0 glass\rh B0 -- es\rh B0
  And he won't stop\rh N0 till he got\rh N0 the mass\rh B0 -- es\rh B0
  And show 'em what they know not\rh N0 through flows of hot\rh N0 mo -- lass\rh B0 -- es\rh B0
  Do it like the ro -- bot\rh N0 to head -- spin to boo\rh L0 -- ga\rh L0 -- loo\rh L0
  Took\rh l0 a\rh l0 few\rh l0 min -- utes to con -- vince the ave -- rage bug\rh L0 -- a\rh L0 -- boo\rh L0
  It's ug -- ly, like look\rh L0 at\rh L0 you,\rh L0 it's a damn\rh E0 shame\rh E0
  Just re -- mem -- ber ALL CAPS when you spell the man\rh E0 name\rh E0
}
songLyricsQA =
  \lyricmode {
    \goodColors
    \rgbVowelRainbow
        \bracketSettings
        \bracketTextBeforeBreak
        \override Slur.after-line-breaking = #horizontalise-broken-slurs
        %{ \genGrooveTupSettings %}
        %{ \grooveTupletTextBeforeBreak %}
        %{ \partial 4 %}
  So nas\rh A0 -- ty\rh A0 that it's prob' -- bly some -- what of a tra\rh A0 -- ves\rh A0 -- ty\rh A0
  Ha\rh a0 -- ving\rh a0 me,\rh a0 then he told the peo -- ple You can call me, 'Your Ma\rh A0 -- jes\rh A0 -- ty!'\rh A0
  Keep your bat\rh A0 -- te\rh A0 -- ry\rh A0 charged, you know it won't
  Stick\rh J0 yo,\rh I0 and it's not his fault you kick\rh J0 slow\rh I0
  Shoul -- d've let your trick\rh J0 ho\rh I0 chick\rh J0 hold\rh I0 your sick\rh J0 glow\rh I0
}
songLyricsQB =
  \lyricmode {
    \goodColors
    \rgbVowelRainbow
        \bracketSettings
        \bracketTextBeforeBreak
        \override Slur.after-line-breaking = #horizontalise-broken-slurs
        %{ \genGrooveTupSettings %}
        %{ \grooveTupletTextBeforeBreak %}
        %{ \partial 4 %}
  Plus no -- bo -- dy coul -- dn't do no -- thin' once he let the brick\rh J0 go\rh I0
  And you know\rh I0 I know\rh I0 that's a bunch of snow\rh I0
  The beat is so\rh I0 but\rh D0 -- ter,\rh D0 Peep the slow\rh I0 cut\rh D0 -- ter\rh D0
  As he ut\rh D0 -- ter\rh D0 the calm\rh C0 flow\rh I0 don't talk a -- bout my moms,\rh D0 yo\rh I0
  Some -- times he rhyme quick, some -- times he rhyme\rh D0 slow\rh I0 Or
}
songLyricsQC =
  \lyricmode {
  \goodColors
  \rgbVowelRainbow
  vice\rh P0 ver\rh P0 -- sa,\rh P0 whip up a slice\rh P0 of\rh P0 nice verse\rh P0 pie\rh P0
  Hit it on the first\rh P0 try,\rh P0 vil -- lain the worst\rh P0 guy\rh P0
  Spot\rh N0 hot\rh n0 tracks\rh B0 like spot\rh N0 a pair of fat\rh B0 ass\rh B0 -- es\rh B0
  Shots\rh N0 of the scotch\rh N0 from out of square shot\rh N0 glass\rh B0 -- es\rh B0
  And he won't stop\rh N0 till he got\rh N0 the mass\rh B0 -- es\rh B0
}
songLyricsQD =
  \lyricmode {
    \goodColors
    \rgbVowelRainbow
        \bracketSettings
        \bracketTextBeforeBreak
        \override Slur.after-line-breaking = #horizontalise-broken-slurs
        %{ \genGrooveTupSettings %}
        %{ \grooveTupletTextBeforeBreak %}
        %{ \partial 4 %}
  And show 'em what they know not\rh N0 through flows of hot\rh N0 mo -- lass\rh B0 -- es\rh B0
  Do it like the ro -- bot\rh N0 to head -- spin to boo\rh L0 -- ga\rh L0 -- loo\rh L0
  Took\rh l0 a\rh l0 few\rh l0 min -- utes to con -- vince the ave -- rage bug\rh L0 -- a\rh L0 -- boo\rh L0
  It's ug -- ly, like look\rh L0 at\rh L0 you,\rh L0 it's a damn\rh E0 shame\rh E0
  Just re -- mem -- ber ALL CAPS when you spell the man\rh E0 name\rh E0
}

songSylsQA =
    \lyricmode {
        \bracketSettings
        \bracketTextBeforeBreak
        \override Slur.after-line-breaking = #horizontalise-broken-slurs
        %{ \genGrooveTupSettings %}
        %{ \grooveTupletTextBeforeBreak %}
        %{ \partial 4 %}
  % 1
  \bB `s-ow8\( `n-ae8
  st-iy16 `D-ae-t Q-ih-ts `pr-ao
  bl-iy `s-ah-m \tuplet 3/2 8 { w-ah-t16 `Q-ah-v Q-ah }
  `tr-ae16 v-ah st-iy `h-ae |
  % 2
  v-ih-G m-iy `D-eh-n h-iy
  `t-ow-ld d-ah `p-iy p-ah-l
  `y-uw k-ae-n `k-ao-l m-iy
                     \domTupLabel "3"
                     \domTupUP {
                    \tweak TupletBracket.transparent ##t
                    \tweak TupletNumber.transparent ##t
  \tuplet 3/2 4 {
  y-ao-r8 @m-ae J-ah } } |
  % 3
  \eb `st-iy16\) r8 \bB `k-iy-p16\(
  y-ao-r `b-ae t-er Q-iy
  \eb C-aa-rJd8\) r8
  \bB y-uw16\( `n-ow Q-ih-t `w-ow-nt |
  % 4
  `st-ih-k8 y-ow
  r16 Q-ah-nd16 `Q-ih-ts n-aa-t \tuplet 3/2 4 {
  `h-ih-z8 f-ao-lt y-uw }
  `k-ih-k8 \eb sl-ow\) |
  % 5
                  \domTupLabel "5" \domTupUP {
  \tweak TupletBracket.transparent ##t
  \tweak TupletNumber.transparent ##t
  \tuplet 5/4 8 { r16 \bB `S-uh16\( d-ah `l-eh-t y-ao-r } }
  r16 `tr-ih-k16 h-ow r16
  `C-ih-k h-ow-ld r16 y-ah
  `s-ih-k8 \eb gl-ow\) |
  }
songSylsQB =
      \lyricmode {
        \bracketSettings
        \bracketTextBeforeBreak
        \override Slur.after-line-breaking = #horizontalise-broken-slurs
        %{ \genGrooveTupSettings %}
        %{ \grooveTupletTextBeforeBreak %}
        %{ \partial 4 %}
  % 6
    r16 \bB `pl-uh-s16\( n-ow `b-aa
                       \domTupLabel "5" \domTupUP {
                      \tweak TupletBracket.transparent ##t
                      \tweak TupletNumber.transparent ##t
    \tuplet 5/4 8 {
    d-iy16 `k-uh d-ah-nt d-uw `n-ah } }
    t-ih-n16 `w-ah-ns h-iy `l-eh-t
    D-ah `br-ih-k8 \eb g-ow16\) |

    % 7
    r8. \bB Q-ah-nd16\( \tuplet 3/2 8 {
    `y-uw16 n-ow r16 } \tuplet 3/2 8 {
    `Q-ay16 n-ow r16}
    `D-ae-ts16 Q-ah `b-ah-nC Q-ah-v
    \eb `sn-ow8\) r8 |
    % 8
    \bB D-ah16\( `b-iy-t Q-ih-z `s-ow
    r16 `b-ah \eb t-er\) r16
    r16 \bB `p-iy-p\( D-ah `sl-ow r16
    `k-ah32 t-er `Q-ae-z16 h-iy |
    % 9
    `Q-ah t-er D-ah `k-aa-m
    r16 \eb fl-ow\) r8
                       \domTupLabel "5" \domTupUP {
                      \tweak TupletBracket.transparent ##t
                      \tweak TupletNumber.transparent ##t
    \tuplet 5/4 8 {
    \bB d-ow-nt16\( `t-ao-k Q-ah `b-aw-t m-ay } }
    `m-aa-mz8 \eb y-ow\) |

    % 10
    \bB `s-ah-m\( t-ay-mz16 r16
    h-iy32 `r-ay-m r16 kw-ih-k16 `s-ah-m
    t-ay-mz r16 h-iy `r-ay-m
    r16 `sl-ow Q-ao-r r16 |
    %{ } %}
%{ songSylsQC = %}
    %{ \lyricmode { %}
        %{ \bracketSettings %}
        %{ \bracketTextBeforeBreak %}
        %{ \override Slur.after-line-breaking = #horizontalise-broken-slurs %}
        %{ \genGrooveTupSettings %}
        %{ \grooveTupletTextBeforeBreak %}
        %{ \partial 4 %}
    % 11
  `v-ay-s r16 v-er \eb s-ah\) r8 \tuplet 3/2 8 {
  \bB `w-ih-p16\( Q-ah-p Q-ah }
  `sl-ay-s16 Q-ah-v `n-ay-s r16
  `v-er-s8 p-ay |
  % 12
  `h-ih-t16 Q-ih-t `Q-aa-n D-ah
  `f-er-st8 tr-ay
  `v-ih16 l-ah-n r16 D-ah
  `w-er-st8 \eb g-ay\) |

  % 13
  \bB `sp-aa-t\( h-aa-t
  `tr-ae-ks l-ay-k
  `sp-aa-t16 Q-ah `p-eh-r Q-ah-v
  f-ae-t `Q-ae r16 \eb s-ah-z\) |
  % 14
  \bB `S-aa-ts\( r16 Q-ah-v D-ah
  `sk-aa-C r32 `fr-ah-m32 Q-aw-t16 Q-ah-v
  `skw-eh-r8 S-aa-t
  `gl-ae \eb s-ah-z\) |
  % 15
  r8 \bB `Q-ah-nd16\( h-iy
  `w-ow-nt8 st-aa-p
  `t-ih-l16 h-iy `g-aa-t D-ah
  `m-ae8 s-ah-z |
  }
songSylsQD =
    \lyricmode {
        \bracketSettings
        \bracketTextBeforeBreak
        \override Slur.after-line-breaking = #horizontalise-broken-slurs
        %{ \genGrooveTupSettings %}
        %{ \grooveTupletTextBeforeBreak %}
        %{ \partial 4 %}
  % 16
  r16. Q-en32 \tuplet 3/2 8 { `S-ow16 Q-em `w-ah-t }
  D-ey16 `n-ow \tuplet 3/2 8 { n-aa-t16 r16 Tr-uw16 }
  `fl-ow-z16 Q-ah-v \tuplet 3/2 8 { `h-aa-t16 r16 m-ah16 }
  `l-ae8 \eb s-ah-z\) |
  % 17
  \bB `d-uw16\( Q-ih-t `l-ay-k D-ah
  `r-ow b-aa-t r16 t-uw
  `h-eh-d sp-ih-n r16 t-uw
  `b-uw g-ah \eb `l-uw\) r16 |
  % 18
  \tuplet 3/2 8 { r16 r16 \bB `t-uh-k16\( } \tuplet 3/2 8 { Q-ah16 `fy-uw16 r16 }
  `m-ih16 n-ah-ts `t-uw k-ah-n
  `v-ih-ns D-ah `Q-ae-v r-ih-J
  `b-ah g-ah `b-uw r16 |

  % 19
  r8 Q-ih-ts16 `Q-ah
  gl-iy r16 l-ay-k `l-uh-k
  Q-ae-t `y-uw Q-ih-ts Q-ah
  `d-ae-m8 \eb S-ey-m\) |
  % 20
        \set Timing.measureLength = #(ly:make-moment 9/8)
  r16 \bB J-ah-st\( r-ih `m-eh-m
  r16 b-er Q-ao-l r16
  `k-ae-ps r16 `w-eh-n y-uw
  `sp-eh-l D-ah `m-ae-n r16
  % 21
  %{ \bar "!" %}
  \eb `n-ey-m8\) } |

songSyllablesKA-A =
    \lyricmode {
        \bracketSettings
        \bracketTextBeforeBreak
        \override Slur.after-line-breaking = #horizontalise-broken-slurs
                %{ \override Score.PhrasingSlur.stencil = ##f %}
                %{ \override Score.TupletBracket.stencil = ##f %}
                %{ \override Score.TupletNumber.stencil = ##f %}
                \phrasingSlurDashed
        %{ \genGrooveTupSettings %}
        %{ \grooveTupletTextBeforeBreak %}
  % 1
  \bB `s-ow8\( `n-ae8
  st-iy16 `D-ae-t Q-ih-ts `pr-ao
  bl-iy `s-ah-m \tuplet 3/2 8 { w-ah-t16 `Q-ah-v Q-ah }
  `tr-ae16 v-ah st-iy `h-ae |
  % 2
  v-ih-G m-iy `D-eh-n h-iy
  `t-ow-ld d-ah `p-iy p-ah-l
  `y-uw k-ae-n `k-ao-l m-iy
                     %{ \domTupLabel "3" %}
                     %{ \domTup { %}
                    %{ \tweak TupletBracket.transparent ##t %}
                    %{ \tweak TupletNumber.transparent ##t %}
  \tuplet 3/2 4 {
  y-ao-r8 @m-ae J-ah }
  %{ }   %}
  |
  % 3
  \eb `st-iy16\) r8 \bB `k-iy-p16\(
  y-ao-r `b-ae t-er Q-iy
  \eb C-aa-rJd8\) r8
  y-uw16\( `n-ow Q-ih-t `w-ow-nt |
  % 4
  `st-ih-k8 y-ow
  r16 Q-ah-nd16 `Q-ih-ts n-aa-t \tuplet 3/2 4 {
  `h-ih-z8 f-ao-lt y-uw }
  `k-ih-k8 \eb sl-ow\) |
  % 5
                  %{ \domTupLabel "5" \domTupUP { %}
  %{ \tweak TupletBracket.transparent ##t %}
  %{ \tweak TupletNumber.transparent ##t %}
  \tuplet 5/4 8 { r16 \bB `S-uh16\( d-ah `l-eh-t y-ao-r } % }
  r16 `tr-ih-k16 h-ow r16
  `C-ih-k h-ow-ld r16 y-ah
  `s-ih-k8 \eb gl-ow\) |
  % 6
  r16 \bB `pl-uh-s16\( n-ow `b-aa
                     %{ \domTupLabel "5" \domTupUP { %}
                    %{ \tweak TupletBracket.transparent ##t %}
                    %{ \tweak TupletNumber.transparent ##t %}
  \tuplet 5/4 8 {
  d-iy16 `k-uh d-ah-nt d-uw `n-ah } %}
  t-ih-n16 `w-ah-ns h-iy `l-eh-t
  D-ah `br-ih-k8 \eb g-ow16\) |

  % 7
  r8. \bB Q-ah-nd16\( \tuplet 3/2 8 {
  `y-uw16 n-ow r16 } \tuplet 3/2 8 {
  `Q-ay16 n-ow r16}
  `D-ae-ts16 Q-ah `b-ah-nC Q-ah-v
  \eb `sn-ow8\) r8 |
  % 8
  \bB D-ah16\( `b-iy-t Q-ih-z `s-ow
  r16 `b-ah \eb t-er\) r16
  r16 \bB `p-iy-p\( D-ah `sl-ow r16
  `k-ah32 t-er `Q-ae-z16 h-iy |
  % 9
  `Q-ah t-er D-ah `k-aa-m
  r16 \eb fl-ow\) r8
                     %{ \domTupLabel "5" \domTupUP { %}
                    %{ \tweak TupletBracket.transparent ##t %}
                    %{ \tweak TupletNumber.transparent ##t %}
  \tuplet 5/4 8 {
  \bB d-ow-nt16\( `t-ao-k Q-ah `b-aw-t m-ay } %}
  `m-aa-mz8 \eb y-ow\) |

  % 10
  \bB `s-ah-m\( t-ay-mz16 r16
  h-iy32 `r-ay-m r16 kw-ih-k16 `s-ah-m
  t-ay-mz r16 h-iy `r-ay-m
  r16 `sl-ow Q-ao-r r16 |
  }
songSyllablesKA-B =
    \lyricmode {
        \bracketSettings
        \bracketTextBeforeBreak
        #(set-global-staff-size 20)
        \override Slur.after-line-breaking = #horizontalise-broken-slurs
                %{ \override Score.PhrasingSlur.stencil = ##f %}
                \override Score.TupletBracket.stencil = ##f
                \override Score.TupletNumber.stencil = ##f
                \phrasingSlurDashed
        %{ \genGrooveTupSettings %}
        %{ \grooveTupletTextBeforeBreak %}
  % 11
  `v-ay-s r16 v-er \eb s-ah\) r8 \tuplet 3/2 8 {
  \bB `w-ih-p16\( Q-ah-p Q-ah }
  `sl-ay-s16 Q-ah-v `n-ay-s r16
  `v-er-s8 p-ay |
  % 12
  `h-ih-t16 Q-ih-t `Q-aa-n D-ah
  `f-er-st8 tr-ay
  `v-ih16 l-ah-n r16 D-ah
  `w-er-st8 \eb g-ay\) |

  % 13
  \bB `sp-aa-t\( h-aa-t
  `tr-ae-ks l-ay-k
  `sp-aa-t16 Q-ah `p-eh-r Q-ah-v
  f-ae-t `Q-ae r16 \eb s-ah-z\) |
  % 14
  \bB `S-aa-ts\( r16 Q-ah-v D-ah
  `sk-aa-C r32 `fr-ah-m32 Q-aw-t16 Q-ah-v
  `skw-eh-r8 S-aa-t
  `gl-ae \eb s-ah-z\) |
  % 15
  r8 \bB `Q-ah-nd16\( h-iy
  `w-ow-nt8 st-aa-p
  `t-ih-l16 h-iy `g-aa-t D-ah
  `m-ae8 s-ah-z |

  % 16
  r16. Q-en32 \tuplet 3/2 8 { `S-ow16 Q-em `w-ah-t }
  D-ey16 `n-ow \tuplet 3/2 8 { n-aa-t16 r16 Tr-uw16 }
  `fl-ow-z16 Q-ah-v \tuplet 3/2 8 { `h-aa-t16 r16 m-ah16 }
  `l-ae8 \eb s-ah-z\) |
  % 17
  \bB `d-uw16\( Q-ih-t `l-ay-k D-ah
  `r-ow b-aa-t r16 t-uw
  `h-eh-d sp-ih-n r16 t-uw
  `b-uw g-ah \eb `l-uw\) r16 |
  % 18
  \tuplet 3/2 8 { r16 r16 \bB `t-uh-k16\( } \tuplet 3/2 8 { Q-ah16 `fy-uw16 r16 }
  `m-ih16 n-ah-ts `t-uw k-ah-n
  `v-ih-ns D-ah `Q-ae-v r-ih-J
  `b-ah g-ah `b-uw r16 |

  % 19
  r8 Q-ih-ts16 `Q-ah
  gl-iy r16 l-ay-k `l-uh-k
  Q-ae-t `y-uw Q-ih-ts Q-ah
  `d-ae-m8 \eb S-ey-m\) |
  %{ \layout { %}
    %{ \context { %}
      %{ \Voice %}
      \set Timing.measureLength = #(ly:make-moment 9/8)
      %{ \time 9/8 %}
  % 20
  r16 \bB J-ah-st\( r-ih `m-eh-m
  r16 b-er Q-ao-l r16
  `k-ae-ps r16 `w-eh-n y-uw
  %{ \cadenzaOn %}
  `sp-eh-l D-ah `m-ae-n r16
  %{ \override PaperColumn.forbidBreak = ##t %}
  %{ \bar "!" %}
  % 21
  \eb `n-ey-m8\)
  %{ \cadenzaOff %}
  | }

\lyricNotation \songLyricsQA \songSylsQA
        \pageBreak
\lyricNotation \songLyricsQB \songSylsQB
        %{ \pageBreak
\lyricNotation \songLyricsQC \songSylsQC %}
        \pageBreak
\lyricNotation \songLyricsQD \songSylsQD
        \pageBreak

\oneLineLyricNotation \songLyricsKA-A \songSyllablesKA-A
        \pageBreak
\oneLineLyricNotation \songLyricsKA-B \songSyllablesKA-B

\version "2.22.0"
\include "../lyric-notation.ily"

songLyricsPlain =
\lyricmode {
I'm be -- gin -- ning to write like An -- na Ka -- re -- ni -- na, give me a mi -- nute, a mic
A li -- ttle to like, get rid of the spite, a bit of the pride to fight
}

songLyrics =
\lyricmode {
  %{ \rhColor a #(x11-color 'LightGreen)
  \rhColor A #(x11-color 'LightGreen)
  \rhColor b #(x11-color 'LightSeaGreen)
  \rhColor B #(x11-color 'LightSeaGreen)
  \rhColor c #(x11-color 'LawnGreen)
  \rhColor C #(x11-color 'LawnGreen)
  \rhColor i #(x11-color 'GreenYellow)
  \rhColor iii #(x11-color 'MediumSeaGreen)
  \rhColor d #(x11-color 'Gold)
  \rhColor ax #(x11-color 'Khaki)
  \rhColor ii #(x11-color 'DarkKhaki)
  \rhColor f #(x11-color 'Tomato)
  \rhColor g #(x11-color 'IndianRed)
  \rhColor h #(x11-color 'Magenta)
  \rhColor H #(x11-color 'Orchid)
  \rhColor HH #(x11-color 'Orchid) %}
  \rhColor ax #(x11-color 'Khaki)
  \rhColor a #(x11-color 'Maroon)
  \rhColor b #(x11-color 'Red)
  \rhColor c #(x11-color 'Orange)
  \rhColor d #(x11-color 'Yellow)
  \rhColor e #(x11-color 'Olive)
  \rhColor f #(x11-color 'Green)
  \rhColor g #(x11-color 'Lime)
  \rhColor h #(x11-color 'Cyan)
  \rhColor i #(x11-color 'Teal)
  \rhColor j #(x11-color 'DodgerBlue)
  \rhColor k #(x11-color 'RoyalBlue)
  \rhColor l #(x11-color 'Blue)
  \rhColor m #(x11-color 'Purple)
  \rhColor n #(x11-color 'Fuchsia)

  \rhColor AX #(x11-color 'Khaki)
  \rhColor A #(x11-color 'Maroon)
  \rhColor B #(x11-color 'Red)
  \rhColor C #(x11-color 'Orange)
  \rhColor D #(x11-color 'Yellow)
  \rhColor E #(x11-color 'Olive)
  \rhColor F #(x11-color 'Green)
  \rhColor G #(x11-color 'Lime)
  \rhColor H #(x11-color 'Cyan)
  \rhColor I #(x11-color 'Teal)
  \rhColor J #(x11-color 'DodgerBlue)
  \rhColor K #(x11-color 'RoyalBlue)
  \rhColor L #(x11-color 'Blue)
  \rhColor M #(x11-color 'Purple)
  \rhColor N #(x11-color 'Fuchsia)

I'm\rh c2 be\rh b2 --
gin\rh b2 -- ning\rh b2 to\rh d1 write\rh d2
like\rh c2 An\rh b1 -- na\rh b1 Ka\rh b1 --
re\rh b2 -- ni\rh b2 -- na,\rh B1 give\rh B2 me\rh B2 a\rh b1
mi\rh b2 -- nute,\rh b3 a\rh c1 mic\rh c2 A\rh b1 |
li\rh b2 -- ttle\rh b3 to\rh d1 like,\rh d2 get\rh b2
rid\rh b2 of\rh b3 the\rh c1 spite,\rh c2 a\rh b1
bit\rh b2 of\rh b3 the\rh c1 pride\rh c2 to\rh d1
fight\rh d2 |
}
songSyllables =
\lyricmode {
  \partial 8
\tuplet 3/2 8 { Q-ay-m8 b-iy16 } |
\tuplet 3/2 8 { `g-ih n-ih-G16 t-ax } `r-ay-t8
`l-ay-k \tuplet 3/2 8 { `Q-ae16 n-ah k-ah }
\tuplet 3/2 8 { `r-eh16 n-ih n-ah } \tuplet 3/2 8 { `g-ih-v16 m-iy Q-ah }
\tuplet 3/2 8 { `m-ih16 n-ah-t Q-ah } \tuplet 3/2 8 { `m-ay-k8 Q-ah16 } |
\tuplet 3/2 8 { `l-ih16 d-el t-ax } \tuplet 3/2 8 { `l-ay-k8 g-eh-t16 }
\tuplet 3/2 8 { `r-ih-d16 Q-ah-v D-ah } \tuplet 3/2 8 { `sp-ay-t8 Q-ah16 }
\tuplet 3/2 8 { `b-ih-t16 Q-ah-v D-ah } \tuplet 3/2 8 { `pr-ay-d8 t-ax16 }
`f-ay-t8 r8 |
}
\lyricNotation \songLyrics \songSyllables
%\pageBreak
\oneLineLyricNotation \songLyrics \songSyllables

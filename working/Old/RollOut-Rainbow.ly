\version "2.22.0"
\include "../lyric-notation.ily"

qOneLyricsPlain =
  \lyricmode {
    % \goodColors
    \pinkRainbow
  % Q1:
  Now where'd you |
  % 1
  get that plat’ -- num
  chain with them
  dia -- monds in  it?
  Where'd you |
  % 2
  get that mat -- chin’
  Benz with them
  win -- dows tin  -- ted?
  Who them |
  % 3
  girls you be
  wit’ when you be
  ri -- din’ through?
  Man I ain't got |
  % 4
  \shrinkLyrics 2
  no -- thin’ to prove,  I paid
  my dues,  brea -- kin’ the rules,
  I shake fools  while I'm
  ta -- kin’ a cruise!  |
  \shrinkLyrics 0
  }
qTwoLyricsPlain =
    \lyricmode
    {
    % \goodColors
    \pinkRainbow
  % 1
  Tell me who's your weed man, and how do you smoke so good?
  % 2
  You's a su -- per -- star, boy, why you still up in the hood?
  % 3
  What in the world is in that bag? What you got in that bag?
    \shrinkLyrics 2
  % 4
  A cou -- ple of cans of whoop ass, you did a good ass job of just eye -- in' me, spy -- in' me
    | \shrinkLyrics 0
  }
qThreeLyricsPlain =
  \lyricmode
    {
    \goodColors
    \pinkRainbow
  % 1
  Man, that car don't
  come out un -- til next year, where in the fuck did you get it?
  % 2
  That's eigh -- ty
  thou -- sand bucks gone, where in the fuck did you spend it?
  % 3
  You must have
  eyes on your back, ‘cause you got mo -- ney to the cei -- lin’
  \shrinkLyrics 2
  % 4
  And the big -- ger the cap,
  \shrinkLyrics 4
  the big -- ger the pee -- ling, the bet -- ter I'm fee -- ling, the more that I'm chil -- ling, wil -- ling, dril -- ling and kil -- ling the fee -- ling
  \shrinkLyrics 0
  }
qFourLyricsPlain =
  \lyricmode
    {
    % \goodColors
    \pinkRainbow
  % 1
  Now who's that
  buc -- ked na -- ked cook fi -- xin’ three course meals?
  % 2
  Get -- ting
  goose -- bumps when her bo -- dy taps the six inch heels
  % 3
  What in the
  world is in that room, what you got in that room?
  % 4
  \shrinkLyrics 2
  A cou -- ple o' gats,
  a cou -- ple o' knives, a cou -- ple o' rats, a cou -- ple o' wives now it's time to choose
  \shrinkLyrics 0
  }
qFiveLyricsPlain =
  \lyricmode
    {
    % \goodColors
    \pinkRainbow
  % 1
  Are you cus -- tom
  made, cus -- tom paid, or you just cus -- tom fit -- ted?
  % 2
  Play -- sta -- tion
  "2" up in the ride and is that Lo -- ren -- zo kit -- ted?
  % 3
  Is that your
  wife, your girl -- friend or just your main bitch?
  \shrinkLyrics 2
  % 4
  You take a pic,
  while I'm rub -- bing the hips, touch the lips to the top of the dick and then whew!
  \shrinkLyrics 0
  }
qSixLyricsPlain =
  \lyricmode
    {
    % \goodColors
    \pinkRainbow
  % 1
  And tell me
  who's your house -- kee -- per and what you keep in your house?
  % 2
  What a -- bout
  dia -- monds and gold, is that what you keep in your mouth?
  % 3
  What in the
  world is in that case, what you got in that case?
  % 4
  \shrinkLyrics 2
  Get up out my face,
  you coul -- dn't re -- late, Wait to take place at a si -- mi -- lar pace So shake, shake it!
  \shrinkLyrics 0
  }
%Outro:
  %{ Get out my busi --
    ness, my busi --
    ness
  Stay the fuck up out my busi --
    ness, ah
  'Cause these nig --
    gas all up in my shit and it's my busi --
    ness, my busi --
    ness
  Stay the fuck up out my busi --
    ness, 'cause it's mine, oh mine
  My busi --
    ness, my busi --
    ness
  Stay the fuck up out my busi --
    ness
  'Cause these nig --
    gas all up in my shit
  And it's my busi --
    ness, my busi --
    ness
  Stay the fuck up out my busi --
    ness, 'cause it's mine, oh mine } %}
qOneSylsPlain =
  \lyricmode
  {
  \partial 4
  % 1
  r16 n-aw16 `w-ey-rd y-uw |
  <>`g-ih-t16 D-ae-t `pl-ae-t n-em
  C-ey-n8 w-ih-T16 D-eh-m
  <>@d-aa8 m-EN-dz16 `Q-ih32 n-ih-t '

  % 2
  r8 `w-ey-rd16 y-uw |
  <>`g-ih-t16 D-ae-t `m-ae-t C-ih-n
  <>@b-ih-nz8 w-ih-T16 D-eh-m
  `w-ih-n16. d-ow-z `t-ih-n32 <>t-ih-d '

  % 3
  r8 `h-uw16 D-eh-m |
  <>@g-el-z8 y-uw16 b-iy
  `w-ih-t16. w-eh-n32 `y-uw16 b-iy
  <>@r-aa8 d-ih-n16 `Tr-uw16 _16 '
  % 4
  r32 m-ae-n32 `Q-ay Q-ey-nt g-aa-t16 |
  `n-ah32 T-ih-n t-ax `pr-uw-v16 Q-ay32 `p-ey-d16
  m-ah32 `d-uw-z16. `br-ey32 k-ih-n D-ah `r-uw-Lz
  _32 Q-ay32 `S-ey-k16 `f-uw-Lz w-ay-l32 Q-ay-m
  `t-ey k-ih-n Q-ah @kr-uw-z8 r32 |
  }
qTwoSylsPlain =
  \lyricmode
  {
  \partial 8
  % 1
  `t-eh-l16 m-iy |
  `h-uw-z y-ao-r @w-iy-d8 `m-ae-n16. Q-ae-nd32 `h-aw d-uw y-uw16 `sm-ow-k16. s-ow @g-uh-d16 _16 ' r16
  % 1
  `y-uw-z16 Q-ah |
  `s-uw p-er `st-aa-r8 @b-oy w-ay16 y-uw `st-ih-l32 Q-ah-p `Q-ih-n16 D-ah @h-uh-d _16 ' r16
  % 1
  `w-ah-t32 Q-ih-n D-ah16 |
  `w-er-ld16. Q-ih-z32 `Q-ih-n16 D-ae-t @b-ae-g8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @b-ae-g _16 ' r32
  % 1
  Q-ah32 `k-ah p-el Q-ah `k-ae-nz |
  _32 Q-ah `w-uh-p Q-ae-s16 `y-uw32 d-ih-d Q-ah `g-uh-d Q-ae-s16 `J-ao-b Q-ah-v32 `J-ah-st16 @Q-ay Q-ih-n32 `m-iy16 @sp-ay Q-ih-n32 `m-iy16 ' r16 r8 |
  }
qThreeSylsPlain =
  \lyricmode
  {
    \partial 4.
  r16 @m-ae-n16 _16 D-ae-t `k-aa-r d-ow-nt |
  `k-em32 Q-aw-16 Q-en32 `t-ih-l16 n-eh-kst `y-ih-r8 `w-eh-r32 Q-ih-n D-ah16 @f-ah-k16. d-ih-d32 y-uw16 `<>g-ih-t32 Q-ih-t ' r16

  D-ae-ts16 `Q-ey t-iy |
  `T-aw z-en-d `b-ah-ks8 @g-ao-n `w-eh-r32 Q-ih-n D-ah16 @f-ah-k16. d-ih-d32 y-uw16 `<>sp-ih-nd32 Q-ih-t ' r16.

  y-uw32 `m-ah-st16 h-ae-v |
  `Q-ay-z8 Q-ao-n16 y-ao-r @b-ae-k16. k-ah-z32 `y-uw16 g-aa-t `m-ah32 n-iy16 `t-ax32 D-ah16 `<>s-ih32 l-ih-n ' r16

  Q-ae-nd32 D-ah `b-ih g-er D-ah `k-ae-p |
  _32 D-ah `b-ih g-er D-ah `<>p-ih l-ih-n D-ah `b-eh t-er Q-ay-m `<>f-ih l-ih-n D-ah `m-ao-r D-ae-t Q-ay-m `C-ih l-ih-n `w-ih l-ih-n `dr-ih l-ih-n Q-en-d `k-ih l-ih-n D-ah `f-ih l-ih-n16 ' r16 |
  }
qFourSylsPlain =
  \lyricmode
  {
    \partial 4
  r16 n-aw16 `h-uw-z D-ae-t |
  `b-ah-k <>k-eh-d `n-ey k-eh-d @k-uh-k8 `f-ih-k16 s-ih-n `Tr-iy16. k-ao-rs `m-iy-Lz16 _16 ' r16

  `g-eh t-ih-n |
  @g-uw-s `b-em-ps8 w-eh-n32 h-er `b-aa16 d-iy `t-ae-ps D-ah `s-ih-ks16. Q-ih-nC `h-iy-Lz16 _16 ' r16

  `w-ah-t32 Q-ih-n D-ah16 |
  `w-er-ld16. Q-ih-z32 Q-ih-n16 D-ae-t @r-uw-m8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @r-uw-m _16 ' r32

  Q-ah32 `k-ah p-el Q-ah `g-ae-ts |
  _32 Q-ah `k-ah p-el Q-ah n-ay-vz _32 Q-ah `k-ah p-el Q-ah `r-ae-ts _32 Q-ah `k-ah p-el Q-ah `w-ay-vz _32 n-aw16 Q-ih-ts32 `t-ay-m t-uw `C-uw-z8 ' r8 |
  }
qFiveSylsPlain =
  \lyricmode
  {
  \partial 4
  r32 Q-aa-r32 `y-uw16 `k-ah st-em |
    @m-ey-d8 `k-ah16 st-em `p-ey-d16. Q-ao-r32 `y-uw16 J-ah-st `k-ah16. st-em `f-ih32 <>t-ih-d ' r16

  pl-ey16 `st-ey S-en |
  `t-uw Q-ah-p `Q-ih-n D-ah32 `r-ay-d _16 Q-en-d32 Q-ih-z `D-ae-t16 l-ao-r `Q-eh-n16. z-ow `k-ih32 <>t-ih-d ' r16

  Q-ih-z16 `D-ae-t y-ao-r |
  `w-ay-f y-ao-r `g-er-l8 `fr-eh-nd16 Q-ao-r `J-ah-st y-ao-r @m-ey-n8. `b-ih-C16 ' r16

  y-uw `t-ey-k Q-ah32 `p-ih-k |
  _16 `w-ay-l32 Q-ay-m `r-ah b-ih-n D-ah `h-ih-ps _32 `t-ah-C16 D-ah32 `l-ih-ps16 t-uw32 D-ah `t-aa-p Q-ah-v D-ah `d-ih-k _32 Q-en-d D-eh-n @w-uw32 _16 ' r8. |
  }
qSixSylsPlain =
  \lyricmode
  {
  \partial 4
  r16 Q-en-d16 `t-eh-l m-iy |
  `h-uw-z y-ao-r `h-aw-s8 `k-iy16 p-er32 Q-en-d `w-ah-t16 y-uw `k-iy-p Q-ih-n y-ao-r @h-aw-s _16 ' r16

  `w-ah-t32 Q-ah b-aw-t16 |
  `d-ay m-en-dz Q-en-d `g-ow-Ld _16 Q-ih-z `D-ae-t w-ah-t32 y-uw `k-iy-p16 Q-ih-n y-ao-r @m-aw-T _16 ' r16

  `w-ah-t32 Q-ih-n D-ah16 |
  `w-er-ld16. Q-ih-z32 `Q-ih-n16 D-ae-t @k-ey-s8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @k-ey-s _32 ' r32

  `g-ih-t32 Q-ah-p `Q-aw-t16 m-ay32 `f-ey-s |
  _32 y-uw `k-uh d-en-t r-ih `l-ey-t16 `w-ey-t32 _32 t-ax `t-ey-k16 `pl-ey-s Q-ae-t32 Q-ah `s-ih m-ih l-er `p-ey-s _32 s-ow `S-ey-k16 `S-ey-k Q-ih-t ' r8 |
  }

qOneLyricsRainbow =
  \lyricmode {
    % \goodColors
    \pinkRainbow
  % 1
  Now\rh AW0
    where'd\rh EHR0
    you\rh UW0
    get\rh IH0
    that\rh AE0
    plat’\rh AE0
    --
    num\rh EM0
    chain\rh SB0
    with\rh IH0
    them\rh EH0
    dia\rh AA0
    --
    monds\rh EN0
    in\rh IH0
    it?\rh IH0
  % 2
  Where'd\rh EHR0
    you\rh UW0
    get\rh IH0
    that\rh AE0
    mat\rh AE0
    --
    chin’\rh IH0
    Benz\rh IH0
    with\rh IH0
    them\rh EH0
    win\rh IH0
    --
    dows\rh OW0
    tin\rh IH0
    --
    ted?\rh IH0
  % 3
  Who\rh UW0
    them\rh EH0
    girls\rh EL0
    you\rh UW0
    be\rh IY0
    wit’\rh IH0
    when\rh EH0
    you\rh UW0
    be\rh IY0
    ri\rh AA0
    --
    din’\rh IH0
    through?\rh UW0
      \shrinkLyrics 2
  % 4
  Man\rh AE0
    I\rh AY0
    ain't\rh EY0
    got\rh AA0
    no\rh AH0
    --
    thin’\rh IH0
    to\rh UW0
    prove,\rh UW0
    I\rh AY0
    paid\rh EY0
    my\rh AY0
    dues,\rh UW0
    brea\rh EY0
    --
    kin’\rh IH0
    the\rh AX0
    rules,\rh UWL0
    I\rh AY0
    shake\rh EY0
    fools\rh UWL0
    while\rh AY0
    I'm\rh AY0
    ta\rh EY0
    --
    kin’\rh IH0
    a\rh AX0
    cruise!\rh UW0
    \shrinkLyrics 0
    }
qTwoLyricsRainbow =
    \lyricmode
    {
    % \goodColors
    \pinkRainbow
  % 1
  Tell\rh EH0
    me\rh IY0
    who's\rh UW0
    your\rh ER0
    weed\rh IY0
    man,\rh AE0
    and\rh AE0
    how\rh AW0
    do\rh UW0
    you\rh UW0
    smoke\rh OW0
    so\rh OW0
    good?\rh UH0
  % 2
  You's\rh UW0
    a\rh AX0
    su\rh UW0
    --
    per\rh ER0
    --
    star,\rh AAR0
    boy,\rh OY0
    why\rh AY0
    you\rh UW0
    still\rh IH0
    up\rh AX0
    in\rh IH0
    the\rh AX0
    hood?\rh UH0
  % 3
  What\rh AH0
    in\rh IH0
    the\rh AX0
    world\rh ER0
    is\rh IH0
    in\rh IH0
    that\rh AE0
    bag?\rh AE0
    What\rh AH0
    you\rh UW0
    got\rh AA0
    in\rh IH0
    that\rh AE0
    bag?\rh AE0
    \shrinkLyrics 2
  % 4
  A\rh AX0
    cou\rh AH0
    --
    ple\rh EL0
    of\rh AX0
    cans\rh AE0
    of\rh AX0
    whoop\rh UH0
    ass,\rh AE0
    you\rh UW0
    did\rh IH0
    a\rh AX0
    good\rh UH0
    ass\rh AE0
    job\rh AO0
    of\rh AX0
    just\rh AH0
    eye\rh AY0
    --
    in'\rh IH0
    me,\rh IY0
    spy\rh AY0
    --
    in'\rh IH0
    me\rh IY0
    | \shrinkLyrics 0
    }
qThreeLyricsRainbow =
  \lyricmode
    {
    \goodColors
    \pinkRainbow
  % 1
  Man,\rh AE0
    that\rh AE0
    car\rh AAR0
    don't\rh OW0
    come\rh EM0
    out\rh AW0
    un\rh EN0
    --
    til\rh IH0
    next\rh EH0
    year,\rh IHR0
    where\rh EHR0
    in\rh IH0
    the\rh AX0
    fuck\rh AH0
    did\rh IH0
    you\rh UW0
    get\rh IH0
    it?\rh IH0
  % 2
  That's\rh AE0
    eigh\rh EY0
    --
    ty\rh IY0
    thou\rh AW0
    --
    sand\rh EN0
    bucks\rh AH0
    gone,\rh AO0
    where\rh EHR0
    in\rh IH0
    the\rh AX0
    fuck\rh AH0
    did\rh IH0
    you\rh UW0
    spend\rh IH0
    it?\rh IH0
  % 3
  You\rh UW0
    must\rh AH0
    have\rh AE0
    eyes\rh AY0
    on\rh A0
    your\rh AOR0
    back,\rh AE0
    ‘cause\rh AX0
    you\rh UW0
    got\rh AA0
    mo\rh AH0
    --
    ney\rh IY0
    to\rh AH0
    the\rh AX0
    cei\rh IH0
    --
    lin’\rh IH0
    \shrinkLyrics 2
  % 4
  And\rh AE0
    the\rh AX0
    big\rh IH0
    --
    ger\rh ER0
    the\rh AX0
    cap,\rh AE0
    \shrinkLyrics 4
    the\rh AX0
    big\rh IH0
    --
    ger\rh ER0
    the\rh AX0
    pee\rh IH0
    --
    ling,\rh IH0
    the\rh AX0
    bet\rh EH0
    --
    ter\rh ER0
    I'm\rh AY0
    fee\rh IH0
    --
    ling,\rh IH0
    the\rh AX0
    more\rh AOR0
    that\rh AE0
    I'm\rh AY0
    chil\rh IH0
    --
    ling,\rh IH0
    wil\rh Ih0
    --
    ling,\rh Ih0
    dril\rh IH0
    --
    ling\rh IH0
    and\rh EN0
    kil\rh IH0
    --
    ling\rh IH0
    the\rh AX0
    fee\rh IH0
    --
    ling\rh IH0
    \shrinkLyrics 0
  }
qFourLyricsRainbow =
  \lyricmode
    {
    % \goodColors
    \pinkRainbow
  % 1
  Now\rh AW0
    who's\rh UW0
    that\rh AE0
    buc\rh AH0
    --
    ked\rh EH0
    na\rh EY0
    --
    ked\rh EH0
    cook\rh UH0
    fi\rh IH0
    --
    xin’\rh IH0
    three\rh IY0
    course\rh AOR0
    meals?\rh IYL0
  % 2
  Get\rh EH0
    --
    ting\rh IH0
    goose\rh UW0
    --
    bumps\rh AH0
    when\rh EH0
    her\rh ER0
    bo\rh AA0
    --
    dy\rh IY0
    taps\rh AE0
    the\rh AX0
    six\rh IH0
    inch\rh IH0
    heels\rh IYL0
  % 3
  What\rh AH0
    in\rh IH0
    the\rh AX0
    world\rh ER0
    is\rh IH0
    in\rh IH0
    that\rh AE0
    room,\rh UW0
    what\rh AH0
    you\rh UW0
    got\rh AA0
    in\rh IH0
    that\rh AE0
    room?\rh UW0
    \shrinkLyrics 2
  % 4
  A\rh AX0
    cou\rh AH0
    --
    ple\rh EL0
    o'\rh AX0
    gats,\rh AE0
    a\rh AX0
    cou\rh AH0
    --
    ple\rh EL0
    o'\rh AX0
    knives,\rh AY0
    a\rh AX0
    cou\rh AH0
    --
    ple\rh EL0
    o'\rh AX0
    rats,\rh AE0
    a\rh AX0
    cou\rh AH0
    --
    ple\rh EL0
    o'\rh AX0
    wives\rh AY0
    now\rh AW0
    it's\rh IH0
    time\rh AY0
    to\rh AX0
    choose!\rh UW0
    \shrinkLyrics 0
  }
qFiveLyricsRainbow =
  \lyricmode
    {
    % \goodColors
    \pinkRainbow
  % 1
  Are\rh AAR0
    you\rh UW0
    cus\rh AH0
    --
    tom\rh EM0
    made,\rh AY0
    cus\rh AH0
    --
    tom\rh EM0
    paid,\rh AY0
    or\rh AOR0
    you\rh UW0
    just\rh AX0
    cus\rh AH0
    --
    tom\rh EM0
    fit\rh IH0
    --
    ted?\rh IH0
  % 2
  Play\rh EY0
    --
    sta\rh EY0
    --
    tion\rh EN0
    "2"\rh UW0
    up\rh AX0
    in\rh IH0
    the\rh AX0
    ride\rh AY0
    and\rh EN0
    is\rh IH0
    that\rh AE0
    Lor\rh AOR0
    --
    en\rh EH0
    --
    zo\rh OW0
    kit\rh IH0
    --
    ted?\rh IH0
  % 3
  Is\rh IH0
    that\rh AE0
    your\rh AOR0
    wife,\rh AY0
    your\rh AOR0
    girl\rh ER0
    --
    friend\rh EH0
    or\rh AOR0
    just\rh AH0
    your\rh AOR0
    main\rh EY0
    bitch?\rh IH0
    \shrinkLyrics 2
  % 4
  You\rh UW0
    take\rh EY0
    a\rh AX0
    pic,\rh IH0
    while\rh AY0
    I'm\rh AY0
    rub\rh AH0
    --
    bing\rh IH0
    the\rh AX0
    hips,\rh IH0
    touch\rh AH0
    the\rh AX0
    lips\rh IH0
    to\rh UW0
    the\rh AX0
    top\rh AA0
    of\rh AX0
    the\rh AX0
    dick\rh IH0
    and\rh EN0
    then\rh EH0
    whew!\rh UW0
    \shrinkLyrics 0
  }
qSixLyricsRainbow =
  \lyricmode
    {
    % \goodColors
    \pinkRainbow
  % 1
  And\rh EN0
    tell\rh EH0
    me\rh IY0
    who's\rh UW0
    your\rh AOR0
    house\rh AW0
    --
    kee\rh IY0
    --
    per\rh ER0
    and\rh EN0
    what\rh AH0
    you\rh UW0
    keep\rh IY0
    in\rh IH0
    your\rh AOR0
    house?\rh AW0
  % 2
  What\rh AH0
    a\rh AX0
    --
    bout\rh AW0
    dia\rh AY0
    --
    monds\rh EN0
    and\rh EN0
    gold,\rh OWL0
    is\rh IH0
    that\rh AE0
    what\rh AX0
    you\rh UW0
    keep\rh IY0
    in\rh IH0
    your\rh AOR0
    mouth?\rh AW0
  % 3
  What\rh AH0
    in\rh IH0
    the\rh AX0
    world\rh ER0
    is\rh IH0
    in\rh IH0
    that\rh AE0
    case,\rh EY0
    what\rh AH0
    you\rh UW0
    got\rh AA0
    in\rh IH0
    that\rh AE0
    case?\rh EY0
    \shrinkLyrics 2
  % 4
  Get\rh IH0
    up\rh AX0
    out\rh AW0
    my\rh AY0
    face,\rh EY0
    you\rh UW0
    coul\rh UH0
    --
    dn't\rh EN0
    re\rh IH0
    --
    late,\rh EY0
    Wait\rh EY0
    to\rh AX0
    take\rh EY0
    place\rh EY0
    at\rh AE0
    a\rh AX0
    si\rh IH0
    --
    mi\rh IH0
    --
    lar\rh ER0
    pace\rh EY0
    So\rh OW0
    shake,\rh EY0
    shake\rh EY0
    it!\rh IH0
    \shrinkLyrics 0
  }
%Outro:
  %{ Get out my busi --
    ness, my busi --
    ness
  Stay the fuck up out my busi --
    ness, ah
  'Cause these nig --
    gas all up in my shit and it's my busi --
    ness, my busi --
    ness
  Stay the fuck up out my busi --
    ness, 'cause it's mine, oh mine
  My busi --
    ness, my busi --
    ness
  Stay the fuck up out my busi --
    ness
  'Cause these nig --
    gas all up in my shit
  And it's my busi --
    ness, my busi --
    ness
  Stay the fuck up out my busi --
    ness, 'cause it's mine, oh mine } %}

qOneSylsRainbow =
  \lyricmode
  {
  \partial 4
  % 1
  r16 n-aw16 `w-ey-rd y-uw |
  <>`g-ih-t16 D-ae-t `pl-ae-t n-em
  C-ey-n8 w-ih-T16 D-eh-m
  <>@d-aa8 m-EN-dz16 `Q-ih32 n-ih-t '

  % 2
  r8 `w-ey-rd16 y-uw |
  <>`g-ih-t16 D-ae-t `m-ae-t C-ih-n
  <>@b-ih-nz8 w-ih-T16 D-eh-m
  `w-ih-n16. d-ow-z `t-ih-n32 <>t-ih-d '

  % 3
  r8 `h-uw16 D-eh-m |
  <>@g-el-z8 y-uw16 b-iy
  `w-ih-t16. w-eh-n32 `y-uw16 b-iy
  <>@r-aa8 d-ih-n16 `Tr-uw16 _16 '
  % 4
  r32 m-ae-n32 `Q-ay Q-ey-nt g-aa-t16 |
  `n-ah32 T-ih-n t-ax `pr-uw-v16 Q-ay32 `p-ey-d16
  m-ah32 `d-uw-z16. `br-ey32 k-ih-n D-ah `r-uw-Lz
  _32 Q-ay32 `S-ey-k16 `f-uw-Lz w-ay-l32 Q-ay-m
  `t-ey k-ih-n Q-ah @kr-uw-z8 r32 |
  }
qTwoSylsRainbow =
  \lyricmode
  {
  \partial 8
  % 1
  `t-eh-l16 m-iy |
  `h-uw-z y-ao-r @w-iy-d8 `m-ae-n16. Q-ae-nd32 `h-aw d-uw y-uw16 `sm-ow-k16. s-ow @g-uh-d16 _16 ' r16
  % 1
  `y-uw-z16 Q-ah |
  `s-uw p-er `st-aa-r8 @b-oy w-ay16 y-uw `st-ih-l32 Q-ah-p `Q-ih-n16 D-ah @h-uh-d _16 ' r16
  % 1
  `w-ah-t32 Q-ih-n D-ah16 |
  `w-er-ld16. Q-ih-z32 `Q-ih-n16 D-ae-t @b-ae-g8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @b-ae-g _16 ' r32
  % 1
  Q-ah32 `k-ah p-el Q-ah `k-ae-nz |
  _32 Q-ah `w-uh-p Q-ae-s16 `y-uw32 d-ih-d Q-ah `g-uh-d Q-ae-s16 `J-ao-b Q-ah-v32 `J-ah-st16 @Q-ay Q-ih-n32 `m-iy16 @sp-ay Q-ih-n32 `m-iy16 ' r16 r8 |
  }
qThreeSylsRainbow =
  \lyricmode
  {
    \partial 4.
  r16 @m-ae-n16 _16 D-ae-t `k-aa-r d-ow-nt |
  `k-em32 Q-aw-16 Q-en32 `t-ih-l16 n-eh-kst `y-ih-r8 `w-eh-r32 Q-ih-n D-ah16 @f-ah-k16. d-ih-d32 y-uw16 `<>g-ih-t32 Q-ih-t ' r16

  D-ae-ts16 `Q-ey t-iy |
  `T-aw z-en-d `b-ah-ks8 @g-ao-n `w-eh-r32 Q-ih-n D-ah16 @f-ah-k16. d-ih-d32 y-uw16 `<>sp-ih-nd32 Q-ih-t ' r16.

  y-uw32 `m-ah-st16 h-ae-v |
  `Q-ay-z8 Q-ao-n16 y-ao-r @b-ae-k16. k-ah-z32 `y-uw16 g-aa-t `m-ah32 n-iy16 `t-ax32 D-ah16 `<>s-ih32 l-ih-n ' r16

  Q-ae-nd32 D-ah `b-ih g-er D-ah `k-ae-p |
  _32 D-ah `b-ih g-er D-ah `<>p-ih l-ih-n D-ah `b-eh t-er Q-ay-m `<>f-ih l-ih-n D-ah `m-ao-r D-ae-t Q-ay-m `C-ih l-ih-n `w-ih l-ih-n `dr-ih l-ih-n Q-en-d `k-ih l-ih-n D-ah `f-ih l-ih-n16 ' r16 |
  }
qFourSylsRainbow =
  \lyricmode
  {
    \partial 4
  r16 n-aw16 `h-uw-z D-ae-t |
  `b-ah-k <>k-eh-d `n-ey k-eh-d @k-uh-k8 `f-ih-k16 s-ih-n `Tr-iy16. k-ao-rs `m-iy-Lz16 _16 ' r16

  `g-eh t-ih-n |
  @g-uw-s `b-em-ps8 w-eh-n32 h-er `b-aa16 d-iy `t-ae-ps D-ah `s-ih-ks16. Q-ih-nC `h-iy-Lz16 _16 ' r16

  `w-ah-t32 Q-ih-n D-ah16 |
  `w-er-ld16. Q-ih-z32 Q-ih-n16 D-ae-t @r-uw-m8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @r-uw-m _16 ' r32

  Q-ah32 `k-ah p-el Q-ah `g-ae-ts |
  _32 Q-ah `k-ah p-el Q-ah n-ay-vz _32 Q-ah `k-ah p-el Q-ah `r-ae-ts _32 Q-ah `k-ah p-el Q-ah `w-ay-vz _32 n-aw16 Q-ih-ts32 `t-ay-m t-uw `C-uw-z8 ' r8 |
  }
qFiveSylsRainbow =
  \lyricmode
  {
  \partial 4
  r32 Q-aa-r32 `y-uw16 `k-ah st-em |
    @m-ey-d8 `k-ah16 st-em `p-ey-d16. Q-ao-r32 `y-uw16 J-ah-st `k-ah16. st-em `f-ih32 <>t-ih-d ' r16

  pl-ey16 `st-ey S-en |
  `t-uw Q-ah-p `Q-ih-n D-ah32 `r-ay-d _16 Q-en-d32 Q-ih-z `D-ae-t16 l-ao-r `Q-eh-n16. z-ow `k-ih32 <>t-ih-d ' r16

  Q-ih-z16 `D-ae-t y-ao-r |
  `w-ay-f y-ao-r `g-er-l8 `fr-eh-nd16 Q-ao-r `J-ah-st y-ao-r @m-ey-n8. `b-ih-C16 ' r16

  y-uw `t-ey-k Q-ah32 `p-ih-k |
  _16 `w-ay-l32 Q-ay-m `r-ah b-ih-n D-ah `h-ih-ps _32 `t-ah-C16 D-ah32 `l-ih-ps16 t-uw32 D-ah `t-aa-p Q-ah-v D-ah `d-ih-k _32 Q-en-d D-eh-n @w-uw32 _16 ' r8. |
  }
qSixSylsRainbow =
  \lyricmode
  {
  \partial 4
  r16 Q-en-d16 `t-eh-l m-iy |
  `h-uw-z y-ao-r `h-aw-s8 `k-iy16 p-er32 Q-en-d `w-ah-t16 y-uw `k-iy-p Q-ih-n y-ao-r @h-aw-s _16 ' r16

  `w-ah-t32 Q-ah b-aw-t16 |
  `d-ay m-en-dz Q-en-d `g-ow-Ld _16 Q-ih-z `D-ae-t w-ah-t32 y-uw `k-iy-p16 Q-ih-n y-ao-r @m-aw-T _16 ' r16

  `w-ah-t32 Q-ih-n D-ah16 |
  `w-er-ld16. Q-ih-z32 `Q-ih-n16 D-ae-t @k-ey-s8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @k-ey-s _32 ' r32

  `g-ih-t32 Q-ah-p `Q-aw-t16 m-ay32 `f-ey-s |
  _32 y-uw `k-uh d-en-t r-ih `l-ey-t16 `w-ey-t32 _32 t-ax `t-ey-k16 `pl-ey-s Q-ae-t32 Q-ah `s-ih m-ih l-er `p-ey-s _32 s-ow `S-ey-k16 `S-ey-k Q-ih-t ' r8 |
  }
%[OUTRO]

qOneLyricsDomains =
  \lyricmode {
    % \goodColors
    \pinkRainbow
  % Q1:
  Now where'd you |
  % 1
  get that plat’ -- num
  chain with them
  dia -- monds in  it?
  Where'd you |
  % 2
  get that mat -- chin’
  Benz with them
  win -- dows tin  -- ted?
  Who them |
  % 3
  girls you be
  wit’ when you be
  ri -- din’ through?
  Man I ain't got |
  % 4
  \shrinkLyrics 2
  no -- thin’ to prove,  I paid
  my dues,  brea -- kin’ the rules,
  I shake fools  while I'm
  ta -- kin’ a cruise!  |
  \shrinkLyrics 0
  }
qTwoLyricsDomains =
    \lyricmode
    {
    % \goodColors
    \pinkRainbow
  % 1
  Tell me who's your weed man, and how do you smoke so good?
  % 2
  You's a su -- per -- star, boy, why you still up in the hood?
  % 3
  What in the world is in that bag? What you got in that bag?
    \shrinkLyrics 2
  % 4
  A cou -- ple of cans of whoop ass, you did a good ass job of just eye -- in' me, spy -- in' me
    | \shrinkLyrics 0
  }
qThreeLyricsDomains =
  \lyricmode
    {
    \goodColors
    \pinkRainbow
  % 1
  Man, that car don't
  come out un -- til next year, where in the fuck did you get it?
  % 2
  That's eigh -- ty
  thou -- sand bucks gone, where in the fuck did you spend it?
  % 3
  You must have
  eyes on your back, ‘cause you got mo -- ney to the cei -- lin’
  \shrinkLyrics 2
  % 4
  And the big -- ger the cap,
  \shrinkLyrics 4
  the big -- ger the pee -- ling, the bet -- ter I'm fee -- ling, the more that I'm chil -- ling, wil -- ling, dril -- ling and kil -- ling the fee -- ling
  \shrinkLyrics 0
  }
qFourLyricsDomains =
  \lyricmode
    {
    % \goodColors
    \pinkRainbow
  % 1
  Now who's that
  buc -- ked na -- ked cook fi -- xin’ three course meals?
  % 2
  Get -- ting
  goose -- bumps when her bo -- dy taps the six inch heels
  % 3
  What in the
  world is in that room, what you got in that room?
  % 4
  \shrinkLyrics 2
  A cou -- ple o' gats,
  a cou -- ple o' knives, a cou -- ple o' rats, a cou -- ple o' wives now it's time to choose
  \shrinkLyrics 0
  }
qFiveLyricsDomains =
  \lyricmode
    {
    % \goodColors
    \pinkRainbow
  % 1
  Are you cus -- tom
  made, cus -- tom paid, or you just cus -- tom fit -- ted?
  % 2
  Play -- sta -- tion
  "2" up in the ride and is that Lo -- ren -- zo kit -- ted?
  % 3
  Is that your
  wife, your girl -- friend or just your main bitch?
  \shrinkLyrics 2
  % 4
  You take a pic,
  while I'm rub -- bing the hips, touch the lips to the top of the dick and then whew!
  \shrinkLyrics 0
  }
qSixLyricsDomains =
  \lyricmode
    {
    % \goodColors
    \pinkRainbow
  % 1
  And tell me
  who's your house -- kee -- per and what you keep in your house?
  % 2
  What a -- bout
  dia -- monds and gold, is that what you keep in your mouth?
  % 3
  What in the
  world is in that case, what you got in that case?
  % 4
  \shrinkLyrics 2
  Get up out my face,
  you coul -- dn't re -- late, Wait to take place at a si -- mi -- lar pace So shake, shake it!
  \shrinkLyrics 0
  }
%Outro:
  %{ Get out my busi --
    ness, my busi --
    ness
  Stay the fuck up out my busi --
    ness, ah
  'Cause these nig --
    gas all up in my shit and it's my busi --
    ness, my busi --
    ness
  Stay the fuck up out my busi --
    ness, 'cause it's mine, oh mine
  My busi --
    ness, my busi --
    ness
  Stay the fuck up out my busi --
    ness
  'Cause these nig --
    gas all up in my shit
  And it's my busi --
    ness, my busi --
    ness
  Stay the fuck up out my busi --
    ness, 'cause it's mine, oh mine } %}
qOneSylsDomains =
  \lyricmode
  {
  \partial 4
  % 1
  r16 n-aw16 `w-ey-rd y-uw |
  <>`g-ih-t16 D-ae-t `pl-ae-t n-em
  C-ey-n8 w-ih-T16 D-eh-m
  <>@d-aa8 m-EN-dz16 `Q-ih32 n-ih-t '

  % 2
  r8 `w-ey-rd16 y-uw |
  <>`g-ih-t16 D-ae-t `m-ae-t C-ih-n
  <>@b-ih-nz8 w-ih-T16 D-eh-m
  `w-ih-n16. d-ow-z `t-ih-n32 <>t-ih-d '

  % 3
  r8 `h-uw16 D-eh-m |
  <>@g-el-z8 y-uw16 b-iy
  `w-ih-t16. w-eh-n32 `y-uw16 b-iy
  <>@r-aa8 d-ih-n16 `Tr-uw16 _16 '
  % 4
  r32 m-ae-n32 `Q-ay Q-ey-nt g-aa-t16 |
  `n-ah32 T-ih-n t-ax `pr-uw-v16 Q-ay32 `p-ey-d16
  m-ah32 `d-uw-z16. `br-ey32 k-ih-n D-ah `r-uw-Lz
  _32 Q-ay32 `S-ey-k16 `f-uw-Lz w-ay-l32 Q-ay-m
  `t-ey k-ih-n Q-ah @kr-uw-z8 r32 |
  }
qTwoSylsDomains =
  \lyricmode
  {
  \partial 8
  % 1
  `t-eh-l16 m-iy |
  `h-uw-z y-ao-r @w-iy-d8 `m-ae-n16. Q-ae-nd32 `h-aw d-uw y-uw16 `sm-ow-k16. s-ow @g-uh-d16 _16 ' r16
  % 1
  `y-uw-z16 Q-ah |
  `s-uw p-er `st-aa-r8 @b-oy w-ay16 y-uw `st-ih-l32 Q-ah-p `Q-ih-n16 D-ah @h-uh-d _16 ' r16
  % 1
  `w-ah-t32 Q-ih-n D-ah16 |
  `w-er-ld16. Q-ih-z32 `Q-ih-n16 D-ae-t @b-ae-g8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @b-ae-g _16 ' r32
  % 1
  Q-ah32 `k-ah p-el Q-ah `k-ae-nz |
  _32 Q-ah `w-uh-p Q-ae-s16 `y-uw32 d-ih-d Q-ah `g-uh-d Q-ae-s16 `J-ao-b Q-ah-v32 `J-ah-st16 @Q-ay Q-ih-n32 `m-iy16 @sp-ay Q-ih-n32 `m-iy16 ' r16 r8 |
  }
qThreeSylsDomains =
  \lyricmode
  {
    \partial 4.
  r16 @m-ae-n16 _16 D-ae-t `k-aa-r d-ow-nt |
  `k-em32 Q-aw-16 Q-en32 `t-ih-l16 n-eh-kst `y-ih-r8 `w-eh-r32 Q-ih-n D-ah16 @f-ah-k16. d-ih-d32 y-uw16 `<>g-ih-t32 Q-ih-t ' r16

  D-ae-ts16 `Q-ey t-iy |
  `T-aw z-en-d `b-ah-ks8 @g-ao-n `w-eh-r32 Q-ih-n D-ah16 @f-ah-k16. d-ih-d32 y-uw16 `<>sp-ih-nd32 Q-ih-t ' r16.

  y-uw32 `m-ah-st16 h-ae-v |
  `Q-ay-z8 Q-ao-n16 y-ao-r @b-ae-k16. k-ah-z32 `y-uw16 g-aa-t `m-ah32 n-iy16 `t-ax32 D-ah16 `<>s-ih32 l-ih-n ' r16

  Q-ae-nd32 D-ah `b-ih g-er D-ah `k-ae-p |
  _32 D-ah `b-ih g-er D-ah `<>p-ih l-ih-n D-ah `b-eh t-er Q-ay-m `<>f-ih l-ih-n D-ah `m-ao-r D-ae-t Q-ay-m `C-ih l-ih-n `w-ih l-ih-n `dr-ih l-ih-n Q-en-d `k-ih l-ih-n D-ah `f-ih l-ih-n16 ' r16 |
  }
qFourSylsDomains =
  \lyricmode
  {
    \partial 4
  r16 n-aw16 `h-uw-z D-ae-t |
  `b-ah-k <>k-eh-d `n-ey k-eh-d @k-uh-k8 `f-ih-k16 s-ih-n `Tr-iy16. k-ao-rs `m-iy-Lz16 _16 ' r16

  `g-eh t-ih-n |
  @g-uw-s `b-em-ps8 w-eh-n32 h-er `b-aa16 d-iy `t-ae-ps D-ah `s-ih-ks16. Q-ih-nC `h-iy-Lz16 _16 ' r16

  `w-ah-t32 Q-ih-n D-ah16 |
  `w-er-ld16. Q-ih-z32 Q-ih-n16 D-ae-t @r-uw-m8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @r-uw-m _16 ' r32

  Q-ah32 `k-ah p-el Q-ah `g-ae-ts |
  _32 Q-ah `k-ah p-el Q-ah n-ay-vz _32 Q-ah `k-ah p-el Q-ah `r-ae-ts _32 Q-ah `k-ah p-el Q-ah `w-ay-vz _32 n-aw16 Q-ih-ts32 `t-ay-m t-uw `C-uw-z8 ' r8 |
  }
qFiveSylsDomains =
  \lyricmode
  {
  \partial 4
  r32 Q-aa-r32 `y-uw16 `k-ah st-em |
    @m-ey-d8 `k-ah16 st-em `p-ey-d16. Q-ao-r32 `y-uw16 J-ah-st `k-ah16. st-em `f-ih32 <>t-ih-d ' r16

  pl-ey16 `st-ey S-en |
  `t-uw Q-ah-p `Q-ih-n D-ah32 `r-ay-d _16 Q-en-d32 Q-ih-z `D-ae-t16 l-ao-r `Q-eh-n16. z-ow `k-ih32 <>t-ih-d ' r16

  Q-ih-z16 `D-ae-t y-ao-r |
  `w-ay-f y-ao-r `g-er-l8 `fr-eh-nd16 Q-ao-r `J-ah-st y-ao-r @m-ey-n8. `b-ih-C16 ' r16

  y-uw `t-ey-k Q-ah32 `p-ih-k |
  _16 `w-ay-l32 Q-ay-m `r-ah b-ih-n D-ah `h-ih-ps _32 `t-ah-C16 D-ah32 `l-ih-ps16 t-uw32 D-ah `t-aa-p Q-ah-v D-ah `d-ih-k _32 Q-en-d D-eh-n @w-uw32 _16 ' r8. |
  }
qSixSylsDomains =
  \lyricmode
  {
  \partial 4
  r16 Q-en-d16 `t-eh-l m-iy |
  `h-uw-z y-ao-r `h-aw-s8 `k-iy16 p-er32 Q-en-d `w-ah-t16 y-uw `k-iy-p Q-ih-n y-ao-r @h-aw-s _16 ' r16

  `w-ah-t32 Q-ah b-aw-t16 |
  `d-ay m-en-dz Q-en-d `g-ow-Ld _16 Q-ih-z `D-ae-t w-ah-t32 y-uw `k-iy-p16 Q-ih-n y-ao-r @m-aw-T _16 ' r16

  `w-ah-t32 Q-ih-n D-ah16 |
  `w-er-ld16. Q-ih-z32 `Q-ih-n16 D-ae-t @k-ey-s8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @k-ey-s _32 ' r32

  `g-ih-t32 Q-ah-p `Q-aw-t16 m-ay32 `f-ey-s |
  _32 y-uw `k-uh d-en-t r-ih `l-ey-t16 `w-ey-t32 _32 t-ax `t-ey-k16 `pl-ey-s Q-ae-t32 Q-ah `s-ih m-ih l-er `p-ey-s _32 s-ow `S-ey-k16 `S-ey-k Q-ih-t ' r8 |
  }
%[OUTRO]
% OUTPUT Rainbow
\lyricNotation \qOneLyricsRainbow \qOneSylsRainbow
  \pageBreak
    \lyricNotation \qTwoLyricsRainbow \qTwoSylsRainbow
    \pageBreak
    \lyricNotation \qThreeLyricsRainbow \qThreeSylsRainbow
    \pageBreak
    \lyricNotation \qFourLyricsRainbow \qFourSylsRainbow
    \pageBreak
    \lyricNotation \qFiveLyricsRainbow \qFiveSylsRainbow
    \pageBreak
    \lyricNotation \qSixLyricsRainbow \qSixSylsRainbow
    \pageBreak

    \lyricNotation \qOneLyricsRainbow \qOneSylsRainbow
    \pageBreak
    \lyricNotation \qThreeLyricsRainbow \qThreeSylsRainbow
    \pageBreak
    \lyricNotation \qFiveLyricsRainbow \qFiveSylsRainbow
    \pageBreak

    \lyricNotation \qTwoLyricsRainbow \qTwoSylsRainbow
    \pageBreak
    \lyricNotation \qFourLyricsRainbow \qFourSylsRainbow
    \pageBreak
    \lyricNotation \qSixLyricsRainbow \qSixSylsRainbow
    \pageBreak
% OUTPUT Plain
\lyricNotation \qOneLyricsPlain \qOneSylsPlain
  \pageBreak
    \lyricNotation \qTwoLyricsPlain \qTwoSylsPlain
    \pageBreak
    \lyricNotation \qThreeLyricsPlain \qThreeSylsPlain
    \pageBreak
    \lyricNotation \qFourLyricsPlain \qFourSylsPlain
    \pageBreak
    \lyricNotation \qFiveLyricsPlain \qFiveSylsPlain
    \pageBreak
    \lyricNotation \qSixLyricsPlain \qSixSylsPlain
    \pageBreak

    \lyricNotation \qOneLyricsPlain \qOneSylsPlain
    \pageBreak
    \lyricNotation \qThreeLyricsPlain \qThreeSylsPlain
    \pageBreak
    \lyricNotation \qFiveLyricsPlain \qFiveSylsPlain
    \pageBreak

    \lyricNotation \qTwoLyricsPlain \qTwoSylsPlain
    \pageBreak
    \lyricNotation \qFourLyricsPlain \qFourSylsPlain
    \pageBreak
    \lyricNotation \qSixLyricsPlain \qSixSylsPlain
    \pageBreak
% OUTPUT

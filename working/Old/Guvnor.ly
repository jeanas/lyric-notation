\version "2.22.0"
\include "../lyric-notation.ily"

songLyricsPlain =
\lyricmode {
  \rhColor a lightblue
  \rhColor b red
  \rhColor c yellow
  \rhColor d green
Catch a throat -- ful from the fi -- re vo -- caled
With ash and mol -- ten glass like Ey -- jaf -- jal -- la -- jö -- kull}

songLyrics =
\lyricmode {
  \rhColor ax #(x11-color 'Khaki)
  \rhColor a #(x11-color 'Tomato)
  \rhColor A #(x11-color 'Tomato)
  \rhColor b #(x11-color 'IndianRed)
  \rhColor c #(x11-color 'LightGreen)
  \rhColor d #(x11-color 'LightSeaGreen)

Catch\rh d0 a\rh ax0 throat\rh b1 -- ful\rh b2 from\rh ax0 the\rh ax0 fi\rh a1 -- re\rh a2 vo\rh b1 -- caled\rh b2
With\rh c0 ash\rh d0 and\rh ax0 mol\rh b1  -- ten\rh c0 glass\rh d0 like\rh A1 Ey\rh a1 -- jaf\rh a2 -- jal\rh A1 -- la\rh A2 -- jö\rh b1 -- kull\rh b2}


songSyllables =
\lyricmode {
r8 `k-ae-C16 Q-ah @Tr-ow-t8 f-el `fr-ah-m16 D-ah `f-ay Q-er @v-ow8 k-el16 w-ih-D |
@Q-ae-S8 Q-ah-nd16 `m-ow-l t-eh-n16 @gl-ae-s8 l-ay-k16 `Q-ay y-ax-f `J-ay l-ax @J-ow k-el r8 | }
\lyricNotation \songLyrics \songSyllables
%\pageBreak
\oneLineLyricNotation \songLyrics \songSyllables

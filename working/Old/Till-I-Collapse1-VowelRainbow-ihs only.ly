\version "2.22.0"
\include "../lyric-notation.ily"

songLyrics =
\lyricmode {
  % 17 Official HTML Colors + DodgerBlue and RoyalBlue
  \rhColor a #(x11-color 'Red)
  \rhColor b #(x11-color 'Orange)
  \rhColor c #(x11-color 'Yellow)
  \rhColor d #(x11-color 'YellowGreen)
  \rhColor e #(x11-color 'Green)
  \rhColor f #(x11-color 'LimeGreen)
  \rhColor g #(x11-color 'Cyan)
  \rhColor h #(x11-color 'Teal)
  \rhColor i #(x11-color 'DodgerBlue)
  \rhColor j #(x11-color 'RoyalBlue)
  \rhColor k #(x11-color 'Blue)
  \rhColor l #(x11-color 'Purple)
  \rhColor m #(x11-color 'Fuchsia)
  \rhColor n #(x11-color 'Maroon)

  \rhColor A #(x11-color 'Red)
  \rhColor B #(x11-color 'Orange)
  \rhColor C #(x11-color 'Yellow)
  \rhColor D #(x11-color 'YellowGreen)
  \rhColor E #(x11-color 'Green)
  \rhColor F #(x11-color 'LimeGreen)
  \rhColor G #(x11-color 'Cyan)
  \rhColor H #(x11-color 'Teal)
  \rhColor I #(x11-color 'DodgerBlue)
  \rhColor J #(x11-color 'RoyalBlue)
  \rhColor K #(x11-color 'Blue)
  \rhColor L #(x11-color 'Purple)
  \rhColor M #(x11-color 'Fuchsia)
  \rhColor N #(x11-color 'Maroon)

  % Vowel Rainbow
  \rhColor uw #(x11-color 'DeepPink2)
  \rhColor uh #(x11-color 'Red1)
  \rhColor el #(x11-color 'Red1)
  \rhColor ow #(x11-color 'Coral)
  \rhColor oy #(x11-color 'MediumPurple)
  \rhColor ao #(x11-color 'Orange)
  \rhColor aw #(x11-color 'RosyBrown)
  \rhColor aa #(x11-color 'Gold)
  \rhColor ah #(x11-color 'Khaki3)
  \rhColor ax #(x11-color 'Khaki2)
  \rhColor er #(x11-color 'BurlyWood2)
  \rhColor ay #(x11-color 'MediumSeaGreen)
  \rhColor ae #(x11-color 'YellowGreen)
  \rhColor eh #(x11-color 'LawnGreen)
  \rhColor ey #(x11-color 'MediumSpringGreen)
  \rhColor ih #(x11-color 'Turquoise)
  \rhColor iy #(x11-color 'DeepSkyBlue)

  \rhColor UW #(x11-color 'DeepPink2)
  \rhColor UH #(x11-color 'Red1)
  \rhColor EL #(x11-color 'Red1)
  \rhColor OW #(x11-color 'Coral)
  \rhColor OY #(x11-color 'MediumPurple)
  \rhColor AO #(x11-color 'Orange)
  \rhColor AW #(x11-color 'RosyBrown)
  \rhColor AA #(x11-color 'Gold)
  \rhColor AH #(x11-color 'Khaki3)
  \rhColor AX #(x11-color 'Khaki2)
  \rhColor ER #(x11-color 'BurlyWood2)
  \rhColor AY #(x11-color 'MediumSeaGreen)
  \rhColor AE #(x11-color 'YellowGreen)
  \rhColor EH #(x11-color 'LawnGreen)
  \rhColor EY #(x11-color 'MediumSpringGreen)
  \rhColor IH #(x11-color 'Turquoise)
  \rhColor IY #(x11-color 'DeepSkyBlue)

Till\rh ih0 I col -- lapse I'm spil\rh ih0 -- lin'\rh IH0 these raps long as you feel\rh ih0 'em
Till\rh ih0 the day that I drop you'll ne -- ver say that I'm not kil\rh ih0 -- lin'\rh IH0 'em
‘Cause when I am not, then I'm -- a stop pen\rh ih0 -- nin'\rh IH0 'em
And I am not hip\rh ih0 -- hop then I'm just not E -- mi\rh ih0 -- nem
Sub -- li\rh ih0 -- mi\rh IH0 -- nal thoughts, when I'm -- a stop send\rh ih0 -- in'\rh ih0 'em?
Wo\rh ih0 -- men\rh ih0 are caught in\rh ih0 webs, spin\rh ih0 'em and hock ve\rh ih0 -- nom
A -- dre -- na -- line\rh ih0 shots of pe -- ni\rh ih0 -- cil\rh IH0 -- lin\rh ih0 could not get the il\rh ih0 -- lin'\rh IH0 to stop
A -- mo -- xi\rh ih0 -- cil\rh IH0 -- lin's\rh ih0 just not real\rh ih0 e\rh ih0 -- nough
The cri\rh ih0 -- min\rh ih0 -- al, cop kil\rh ih0 -- lin',\rh IH0 hip -- hop vil\rh ih0 -- lain\rh IH0
A mi\rh ih0 -- ni\rh IH0 -- mal swap to cop mil\rh ih0 -- lions of Pac list\rh ih0 -- en\rh IH0 -- ers
You're co -- min'\rh ih0 with\rh IH0 me, feel it\rh ih0 or not
You're gon -- na fear it\rh ih0 like I showed ya the spi -- rit\rh ih0 of God lives\rh ih0 in\rh IH0 us
You hear it\rh ih0 a lot, ly -- rics\rh ih0 to shock
Is\rh ih0 it\rh IH0 a mi -- ra\rh ih0 -- cle or am I just pro -- duct of pop fiz\rh ih0 -- zin'\rh IH0 up?
Fo' shiz\rh ih0 -- zle, my wiz\rh ih0 -- zle, this\rh ih0 is\rh IH0 the plot, list\rh ih0 -- en\rh IH0 up
You biz\rh ih0 -- zles for -- got, Sliz\rh ih0 -- zle does not give\rh ih0 a fuck!
}
songSyllables =
\lyricmode {
  \partial 4
r16 t-ih-l `Q-ay k-ao |
@l-ae-ps8 Q-ay-m16 `sp-ih l-ih-n D-iy-z @r-ae-ps8 `l-ao-G16 Q-ae-z y-uw @<>f-ih-l Q-ah-m `t-ih-l D-ah @d-ey |
D-ae-t Q-ay @dr-aa-p8 y-uh-L16 `n-eh v-er `s-ey D-ae-t Q-ay-m @n-aa-t8 `k-ih16 l-ih-n Q-ah-m8 |
r16 k-ah-z16 `w-eh-n Q-ay Q-ae-m @n-aa-t8 D-eh-n16 `Q-ay m-ah @st-aa-p8 `<>p-ih16 n-ih-n Q-ah-m Q-ah-nd |
`Q-ay Q-ae-m @n-aa-t8 h-ih-p16 `h-aa-p8 D-eh-n16 `Q-ay-m J-ah-st @n-aa-t8 `Q-eh16 m-ih n-eh-m8 |

r16 s-ah16 `^bl-ih m-ih n-ah-l @T-ao-ts8 w-eh-n16 `Q-ay m-ah @st-aa-p8 `<>s-ih-n16 d-ih-n Q-eh-m `w-ih |
m-ah-n Q-aa-r @k-aa-t8 Q-ih-n16 `w-eh-bz8 `sp-ih-n16 Q-eh-m Q-ah-nd @x-aa-k8 `<>v-ih16 n-ah-m Q-ah `dr-eh |
n-ah l-ah-n @S-aa-ts8 Q-ah-v16 `p-eh n-ah `s-ih l-ah-n k-uh-d @n-aa-t8 `g-eh-t16 D-ah `Q-ih l-ih-n |
t-uw @st-aa-p8 Q-ah16 `m-aa-k8 s-ah16 `s-ih l-ih-nz J-ah-st @n-aa-t8 `<>r-ih-l16 Q-ih n-ah-f8 |

r16 D-ah `kr-ih m-ah n-el `k-aa-p8 `k-ih16 l-ih-n h-ih-p `h-aa-p8 `v-ih16 l-ah-n Q-ah `m-ih |
n-ah m-ah-l `sw-aa-p8 t-uw16 `k-aa-p8 `m-ih-l16 y-ah-nz Q-ah-v @p-aa-k8 `l-ih16 <>s-ih n-er-z8 |
r16 y-uh-r `k-ah m-ih-n w-ih-D @m-iy8 `f-iy-l16 Q-ih-t Q-ao-r @n-aa-t8 y-uh-r16 `g-aa n-ah `f-ih-r |
Q-ih-t l-ay-k @Q-ay8 `S-ow-d16 y-aa D-ah `sp-ih r-ah-t Q-ah-v @g-aa-d8 `l-ih-v16 z-ih n-ah-s8 |

r16 y-uw16 `h-iy-r Q-ih-t Q-ah `l-aa-t8 `l-ih16 r-ih-ks t-uw `S-aa-k8 `Q-ih-z16 Q-ih-t Q-ah `m-ih |
<>r-ih k-el `Q-ao-r Q-ae-m Q-ay `J-ah-st8 `pr-aa16 d-ah-kt Q-ah-v @p-aa-p8 `f-ih16 z-ih-n Q-ah-p8 |
r16 f-ah @S-ih z-el m-ay `wh-ih z-el `D-ih-s Q-ih-z D-ah `pl-aa-t8 `l-ih16 s-ah-n Q-ah-p8 |
r16 y-uw `b-ih z-el-z f-er @g-aa-t8 `sl-ih16 z-el d-ah-z @n-aa-t8 `g-ih-v16 Q-ah @f-ah-k8 | }

\lyricNotation \songLyrics \songSyllables

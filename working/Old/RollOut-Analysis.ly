\version "2.22.0"
\include "../lyric-notation.ily"

q-One-Lyrics =
  \lyricmode {
    % Good Colors
    \rhColor a #(x11-color 'Red)
    \rhColor b #(x11-color 'Orange)
    \rhColor c #(x11-color 'Yellow)
    \rhColor d #(x11-color 'YellowGreen)
    \rhColor e #(x11-color 'Green)
    \rhColor f #(x11-color 'LimeGreen)
    \rhColor g #(x11-color 'Cyan)
    \rhColor h #(x11-color 'DarkCyan)
    \rhColor i #(x11-color 'LightSkyBlue)
    \rhColor j #(x11-color 'DodgerBlue)
    \rhColor k #(x11-color 'Blue)
    \rhColor l #(x11-color 'Purple)
    \rhColor m #(x11-color 'Fuchsia)
    \rhColor n #(x11-color 'Maroon)
    \rhColor o #(x11-color 'DarkOrange2)
    \rhColor p #(x11-color 'DeepPink2)


    \rhColor A #(x11-color 'Red)
    \rhColor B #(x11-color 'DarkOrange)
    \rhColor C #(x11-color 'Gold2)
    \rhColor D #(x11-color 'YellowGreen)
    \rhColor E #(x11-color 'Green)
    \rhColor F #(x11-color 'LimeGreen)
    \rhColor G #(x11-color 'Cyan)
    \rhColor H #(x11-color 'DarkCyan)
    \rhColor I #(x11-color 'LightSkyBlue)
    \rhColor J #(x11-color 'DodgerBlue)
    \rhColor K #(x11-color 'Blue)
    \rhColor L #(x11-color 'Purple)
    \rhColor M #(x11-color 'Fuchsia)
    \rhColor N #(x11-color 'Maroon)
    \rhColor O #(x11-color 'DarkOrange2)
    \rhColor P #(x11-color 'DeepPink2)

    % NEW Vowel Rainbow Pink
    \rhColor uw #(x11-color 'DeepPink2)
    \rhColor uh #(x11-color 'Red)
    \rhColor el #(x11-color 'Red)
    \rhColor ow #(x11-color 'DarkOrange2)
    \rhColor ao #(x11-color 'Gold2)
    \rhColor oy #(x11-color 'LightSeaGreen)
    \rhColor aa #(x11-color 'Yellow)
    \rhColor aw #(x11-color 'RosyBrown)
    \rhColor ah #(x11-color 'Khaki)
    \rhColor ax #(x11-color 'PaleGoldenrod)
    \rhColor er #(x11-color 'BurlyWood2)
    \rhColor ay #(x11-color 'OliveDrab)
    \rhColor ae #(x11-color 'YellowGreen)
    \rhColor eh #(x11-color 'Green)
    \rhColor ey #(x11-color 'MediumSpringGreen)
    \rhColor ih #(x11-color 'Turquoise)
    \rhColor iy #(x11-color 'DeepSkyBlue)

    \rhColor UW #(x11-color 'DeepPink2)
    \rhColor UH #(x11-color 'Red)
    \rhColor EL #(x11-color 'Red)
    \rhColor OW #(x11-color 'DarkOrange2)
    \rhColor AO #(x11-color 'Gold2)
    \rhColor OY #(x11-color 'LightSeaGreen)
    \rhColor AA #(x11-color 'Yellow)
    \rhColor AW #(x11-color 'RosyBrown)
    \rhColor AH #(x11-color 'Khaki)
    \rhColor AX #(x11-color 'PaleGoldenrod)
    \rhColor ER #(x11-color 'BurlyWood2)
    \rhColor AY #(x11-color 'OliveDrab)
    \rhColor AE #(x11-color 'YellowGreen)
    \rhColor EH #(x11-color 'Green)
    \rhColor EY #(x11-color 'MediumSpringGreen)
    \rhColor IH #(x11-color 'Turquoise)
    \rhColor IY #(x11-color 'DeepSkyBlue)

    \rhColor em #(x11-color 'GreenYellow)
    \rhColor en #(x11-color 'GreenYellow)
    \rhColor eg #(x11-color 'GreenYellow)
  % Q1:
  Now where'd\rh i1 you\rh i2 |
  % 1
  get\rh i3 that\rh i4 plat’ -- num
  chain with\rh i5 them\rh i6
  dia -- monds in\rh h1 it?\rh h2
  Where'd\rh i1 you\rh i2 |
  % 2
  get\rh i3 that\rh i4 mat -- chin’
  Benz with\rh i5 them\rh i6
  win -- dows tin\rh h1 -- ted?\rh h2
  Who them |
  % 3
  girls you be
  wit’ when you be
  ri\rh c1 -- din’\rh c2 through?\rh a0
  Man\rh c1 I\rh c1 ain't\rh c2 got\rh c3 |
  % 4
  \shrinkLyrics 2
  no\rh c1 -- thin’\rh c2 to\rh c3 prove,\rh a0 I\rh c1 paid\rh c2
  my\rh c3 dues,\rh a0 brea\rh c2 -- kin’\rh c2 the\rh c3 rules,\rh a0
  I\rh c1 shake\rh c2 fools\rh a0 while\rh c1 I'm\rh c1
  ta\rh c2 -- kin’\rh c2 a\rh c3 cruise!\rh a0 |
  \shrinkLyrics 0
  }
q-Two-Lyrics =
  \lyricmode {
    % Good Colors
    \rhColor a #(x11-color 'Red)
    \rhColor b #(x11-color 'Orange)
    \rhColor c #(x11-color 'Yellow)
    \rhColor d #(x11-color 'YellowGreen)
    \rhColor e #(x11-color 'Green)
    \rhColor f #(x11-color 'LimeGreen)
    \rhColor g #(x11-color 'Cyan)
    \rhColor h #(x11-color 'DarkCyan)
    \rhColor i #(x11-color 'LightSkyBlue)
    \rhColor j #(x11-color 'DodgerBlue)
    \rhColor k #(x11-color 'Blue)
    \rhColor l #(x11-color 'Purple)
    \rhColor m #(x11-color 'Fuchsia)
    \rhColor n #(x11-color 'Maroon)
    \rhColor o #(x11-color 'DarkOrange2)
    \rhColor p #(x11-color 'DeepPink2)


    \rhColor A #(x11-color 'Red)
    \rhColor B #(x11-color 'DarkOrange)
    \rhColor C #(x11-color 'Gold2)
    \rhColor D #(x11-color 'YellowGreen)
    \rhColor E #(x11-color 'Green)
    \rhColor F #(x11-color 'LimeGreen)
    \rhColor G #(x11-color 'Cyan)
    \rhColor H #(x11-color 'DarkCyan)
    \rhColor I #(x11-color 'LightSkyBlue)
    \rhColor J #(x11-color 'DodgerBlue)
    \rhColor K #(x11-color 'Blue)
    \rhColor L #(x11-color 'Purple)
    \rhColor M #(x11-color 'Fuchsia)
    \rhColor N #(x11-color 'Maroon)
    \rhColor O #(x11-color 'DarkOrange2)
    \rhColor P #(x11-color 'DeepPink2)

    % NEW Vowel Rainbow Pink
    \rhColor uw #(x11-color 'DeepPink2)
    \rhColor uh #(x11-color 'Red)
    \rhColor el #(x11-color 'Red)
    \rhColor ow #(x11-color 'DarkOrange2)
    \rhColor ao #(x11-color 'Gold2)
    \rhColor oy #(x11-color 'LightSeaGreen)
    \rhColor aa #(x11-color 'Yellow)
    \rhColor aw #(x11-color 'RosyBrown)
    \rhColor ah #(x11-color 'Khaki)
    \rhColor ax #(x11-color 'PaleGoldenrod)
    \rhColor er #(x11-color 'BurlyWood2)
    \rhColor ay #(x11-color 'OliveDrab)
    \rhColor ae #(x11-color 'YellowGreen)
    \rhColor eh #(x11-color 'Green)
    \rhColor ey #(x11-color 'MediumSpringGreen)
    \rhColor ih #(x11-color 'Turquoise)
    \rhColor iy #(x11-color 'DeepSkyBlue)

    \rhColor UW #(x11-color 'DeepPink2)
    \rhColor UH #(x11-color 'Red)
    \rhColor EL #(x11-color 'Red)
    \rhColor OW #(x11-color 'DarkOrange2)
    \rhColor AO #(x11-color 'Gold2)
    \rhColor OY #(x11-color 'LightSeaGreen)
    \rhColor AA #(x11-color 'Yellow)
    \rhColor AW #(x11-color 'RosyBrown)
    \rhColor AH #(x11-color 'Khaki)
    \rhColor AX #(x11-color 'PaleGoldenrod)
    \rhColor ER #(x11-color 'BurlyWood2)
    \rhColor AY #(x11-color 'OliveDrab)
    \rhColor AE #(x11-color 'YellowGreen)
    \rhColor EH #(x11-color 'Green)
    \rhColor EY #(x11-color 'MediumSpringGreen)
    \rhColor IH #(x11-color 'Turquoise)
    \rhColor IY #(x11-color 'DeepSkyBlue)

    \rhColor em #(x11-color 'GreenYellow)
    \rhColor en #(x11-color 'GreenYellow)
    \rhColor eg #(x11-color 'GreenYellow)
  % Q2:
  % 1
  Tell me
  who's your weed man, and how do you smoke so good?\rh g0
  % 2
  You's a
  su -- per -- star, boy, why you still up in the hood?\rh g0
  % 3
  What\rh i1 in the
  world is in\rh i2 that\rh i3 bag?\rh j0 What\rh i1 you got in\rh i2 that\rh i3 bag?\rh j0
  % 4
  \shrinkLyrics 2
  A cou -- ple of cans
  of whoop ass, you did a good ass job of just eye -- in' me, spy -- in' me
  \shrinkLyrics 0
  }
q-Three-Lyrics =
  \lyricmode {
    % Good Colors
    \rhColor a #(x11-color 'Red)
    \rhColor b #(x11-color 'Orange)
    \rhColor c #(x11-color 'Yellow)
    \rhColor d #(x11-color 'YellowGreen)
    \rhColor e #(x11-color 'Green)
    \rhColor f #(x11-color 'LimeGreen)
    \rhColor g #(x11-color 'Cyan)
    \rhColor h #(x11-color 'DarkCyan)
    \rhColor i #(x11-color 'LightSkyBlue)
    \rhColor j #(x11-color 'DodgerBlue)
    \rhColor k #(x11-color 'Blue)
    \rhColor l #(x11-color 'Purple)
    \rhColor m #(x11-color 'Fuchsia)
    \rhColor n #(x11-color 'Maroon)
    \rhColor o #(x11-color 'DarkOrange2)
    \rhColor p #(x11-color 'DeepPink2)


    \rhColor A #(x11-color 'Red)
    \rhColor B #(x11-color 'DarkOrange)
    \rhColor C #(x11-color 'Gold2)
    \rhColor D #(x11-color 'YellowGreen)
    \rhColor E #(x11-color 'Green)
    \rhColor F #(x11-color 'LimeGreen)
    \rhColor G #(x11-color 'Cyan)
    \rhColor H #(x11-color 'DarkCyan)
    \rhColor I #(x11-color 'LightSkyBlue)
    \rhColor J #(x11-color 'DodgerBlue)
    \rhColor K #(x11-color 'Blue)
    \rhColor L #(x11-color 'Purple)
    \rhColor M #(x11-color 'Fuchsia)
    \rhColor N #(x11-color 'Maroon)
    \rhColor O #(x11-color 'DarkOrange2)
    \rhColor P #(x11-color 'DeepPink2)

    % NEW Vowel Rainbow Pink
    \rhColor uw #(x11-color 'DeepPink2)
    \rhColor uh #(x11-color 'Red)
    \rhColor el #(x11-color 'Red)
    \rhColor ow #(x11-color 'DarkOrange2)
    \rhColor ao #(x11-color 'Gold2)
    \rhColor oy #(x11-color 'LightSeaGreen)
    \rhColor aa #(x11-color 'Yellow)
    \rhColor aw #(x11-color 'RosyBrown)
    \rhColor ah #(x11-color 'Khaki)
    \rhColor ax #(x11-color 'PaleGoldenrod)
    \rhColor er #(x11-color 'BurlyWood2)
    \rhColor ay #(x11-color 'OliveDrab)
    \rhColor ae #(x11-color 'YellowGreen)
    \rhColor eh #(x11-color 'Green)
    \rhColor ey #(x11-color 'MediumSpringGreen)
    \rhColor ih #(x11-color 'Turquoise)
    \rhColor iy #(x11-color 'DeepSkyBlue)

    \rhColor UW #(x11-color 'DeepPink2)
    \rhColor UH #(x11-color 'Red)
    \rhColor EL #(x11-color 'Red)
    \rhColor OW #(x11-color 'DarkOrange2)
    \rhColor AO #(x11-color 'Gold2)
    \rhColor OY #(x11-color 'LightSeaGreen)
    \rhColor AA #(x11-color 'Yellow)
    \rhColor AW #(x11-color 'RosyBrown)
    \rhColor AH #(x11-color 'Khaki)
    \rhColor AX #(x11-color 'PaleGoldenrod)
    \rhColor ER #(x11-color 'BurlyWood2)
    \rhColor AY #(x11-color 'OliveDrab)
    \rhColor AE #(x11-color 'YellowGreen)
    \rhColor EH #(x11-color 'Green)
    \rhColor EY #(x11-color 'MediumSpringGreen)
    \rhColor IH #(x11-color 'Turquoise)
    \rhColor IY #(x11-color 'DeepSkyBlue)

    \rhColor em #(x11-color 'GreenYellow)
    \rhColor en #(x11-color 'GreenYellow)
    \rhColor eg #(x11-color 'GreenYellow)
  %Q3:
  % 1
  Man, that car don't
  come out un -- til next year, where\rh i1 in\rh i2 the\rh i3 fuck\rh i4 did\rh i5 you\rh i6 get\rh g1 it?\rh g2
  % 2
  That's eigh -- ty
  thou -- sand bucks gone, where\rh i1 in\rh i2 the\rh i3 fuck\rh i4 did\rh i5 you\rh i6 spend\rh g1 it?\rh g2
  % 3
  You must have
  eyes on your back, ‘cause you got mo -- ney to the cei -- lin’
  \shrinkLyrics 2
  % 4
  And the big -- ger the cap,
  \shrinkLyrics 4
  the big -- ger the pee -- ling, the bet -- ter I'm fee -- ling, the more that I'm chil -- ling, wil -- ling, dril -- ling and kil -- ling the fee -- ling
  \shrinkLyrics 0
  }
q-Four-Lyrics =
  \lyricmode {

    \rhColor a #(x11-color 'Red)
    \rhColor b #(x11-color 'Orange)
    \rhColor c #(x11-color 'Yellow)
    \rhColor d #(x11-color 'YellowGreen)
    \rhColor e #(x11-color 'Green)
    \rhColor f #(x11-color 'LimeGreen)
    \rhColor g #(x11-color 'Cyan)
    \rhColor h #(x11-color 'DarkCyan)
    \rhColor i #(x11-color 'LightSkyBlue)
    \rhColor j #(x11-color 'DodgerBlue)
    \rhColor k #(x11-color 'Blue)
    \rhColor l #(x11-color 'Purple)
    \rhColor m #(x11-color 'Fuchsia)
    \rhColor n #(x11-color 'Maroon)
    \rhColor o #(x11-color 'DarkOrange2)
    \rhColor p #(x11-color 'DeepPink2)


    \rhColor A #(x11-color 'Red)
    \rhColor B #(x11-color 'DarkOrange)
    \rhColor C #(x11-color 'Gold2)
    \rhColor D #(x11-color 'YellowGreen)
    \rhColor E #(x11-color 'Green)
    \rhColor F #(x11-color 'LimeGreen)
    \rhColor G #(x11-color 'Cyan)
    \rhColor H #(x11-color 'DarkCyan)
    \rhColor I #(x11-color 'LightSkyBlue)
    \rhColor J #(x11-color 'DodgerBlue)
    \rhColor K #(x11-color 'Blue)
    \rhColor L #(x11-color 'Purple)
    \rhColor M #(x11-color 'Fuchsia)
    \rhColor N #(x11-color 'Maroon)
    \rhColor O #(x11-color 'DarkOrange2)
    \rhColor P #(x11-color 'DeepPink2)

    % NEW Vowel Rainbow Pink
    \rhColor uw #(x11-color 'DeepPink2)
    \rhColor uh #(x11-color 'Red)
    \rhColor el #(x11-color 'Red)
    \rhColor ow #(x11-color 'DarkOrange2)
    \rhColor ao #(x11-color 'Gold2)
    \rhColor oy #(x11-color 'LightSeaGreen)
    \rhColor aa #(x11-color 'Yellow)
    \rhColor aw #(x11-color 'RosyBrown)
    \rhColor ah #(x11-color 'Khaki)
    \rhColor ax #(x11-color 'PaleGoldenrod)
    \rhColor er #(x11-color 'BurlyWood2)
    \rhColor ay #(x11-color 'OliveDrab)
    \rhColor ae #(x11-color 'YellowGreen)
    \rhColor eh #(x11-color 'Green)
    \rhColor ey #(x11-color 'MediumSpringGreen)
    \rhColor ih #(x11-color 'Turquoise)
    \rhColor iy #(x11-color 'DeepSkyBlue)

    \rhColor UW #(x11-color 'DeepPink2)
    \rhColor UH #(x11-color 'Red)
    \rhColor EL #(x11-color 'Red)
    \rhColor OW #(x11-color 'DarkOrange2)
    \rhColor AO #(x11-color 'Gold2)
    \rhColor OY #(x11-color 'LightSeaGreen)
    \rhColor AA #(x11-color 'Yellow)
    \rhColor AW #(x11-color 'RosyBrown)
    \rhColor AH #(x11-color 'Khaki)
    \rhColor AX #(x11-color 'PaleGoldenrod)
    \rhColor ER #(x11-color 'BurlyWood2)
    \rhColor AY #(x11-color 'OliveDrab)
    \rhColor AE #(x11-color 'YellowGreen)
    \rhColor EH #(x11-color 'Green)
    \rhColor EY #(x11-color 'MediumSpringGreen)
    \rhColor IH #(x11-color 'Turquoise)
    \rhColor IY #(x11-color 'DeepSkyBlue)

    \rhColor em #(x11-color 'GreenYellow)
    \rhColor en #(x11-color 'GreenYellow)
    \rhColor eg #(x11-color 'GreenYellow)
  %Q4:
  % 1
  Now who's that
  buc -- ked na -- ked cook fi -- xin’ three course meals?
  % 2
  Get -- ting
  goose -- bumps when her bo -- dy taps the six inch heels
  % 3
  What in the
  world is in that room, what you got in that room?
  % 4
  \shrinkLyrics 2
  A cou -- ple o' gats,
  a cou -- ple o' knives, a cou -- ple o' rats, a cou -- ple o' wives now it's time to choose
  \shrinkLyrics 0
  }
q-Five-Lyrics =
  \lyricmode {

    \rhColor a #(x11-color 'Red)
    \rhColor b #(x11-color 'Orange)
    \rhColor c #(x11-color 'Yellow)
    \rhColor d #(x11-color 'YellowGreen)
    \rhColor e #(x11-color 'Green)
    \rhColor f #(x11-color 'LimeGreen)
    \rhColor g #(x11-color 'Cyan)
    \rhColor h #(x11-color 'DarkCyan)
    \rhColor i #(x11-color 'LightSkyBlue)
    \rhColor j #(x11-color 'DodgerBlue)
    \rhColor k #(x11-color 'Blue)
    \rhColor l #(x11-color 'Purple)
    \rhColor m #(x11-color 'Fuchsia)
    \rhColor n #(x11-color 'Maroon)
    \rhColor o #(x11-color 'DarkOrange2)
    \rhColor p #(x11-color 'DeepPink2)


    \rhColor A #(x11-color 'Red)
    \rhColor B #(x11-color 'DarkOrange)
    \rhColor C #(x11-color 'Gold2)
    \rhColor D #(x11-color 'YellowGreen)
    \rhColor E #(x11-color 'Green)
    \rhColor F #(x11-color 'LimeGreen)
    \rhColor G #(x11-color 'Cyan)
    \rhColor H #(x11-color 'DarkCyan)
    \rhColor I #(x11-color 'LightSkyBlue)
    \rhColor J #(x11-color 'DodgerBlue)
    \rhColor K #(x11-color 'Blue)
    \rhColor L #(x11-color 'Purple)
    \rhColor M #(x11-color 'Fuchsia)
    \rhColor N #(x11-color 'Maroon)
    \rhColor O #(x11-color 'DarkOrange2)
    \rhColor P #(x11-color 'DeepPink2)

    % NEW Vowel Rainbow Pink
    \rhColor uw #(x11-color 'DeepPink2)
    \rhColor uh #(x11-color 'Red)
    \rhColor el #(x11-color 'Red)
    \rhColor ow #(x11-color 'DarkOrange2)
    \rhColor ao #(x11-color 'Gold2)
    \rhColor oy #(x11-color 'LightSeaGreen)
    \rhColor aa #(x11-color 'Yellow)
    \rhColor aw #(x11-color 'RosyBrown)
    \rhColor ah #(x11-color 'Khaki)
    \rhColor ax #(x11-color 'PaleGoldenrod)
    \rhColor er #(x11-color 'BurlyWood2)
    \rhColor ay #(x11-color 'OliveDrab)
    \rhColor ae #(x11-color 'YellowGreen)
    \rhColor eh #(x11-color 'Green)
    \rhColor ey #(x11-color 'MediumSpringGreen)
    \rhColor ih #(x11-color 'Turquoise)
    \rhColor iy #(x11-color 'DeepSkyBlue)

    \rhColor UW #(x11-color 'DeepPink2)
    \rhColor UH #(x11-color 'Red)
    \rhColor EL #(x11-color 'Red)
    \rhColor OW #(x11-color 'DarkOrange2)
    \rhColor AO #(x11-color 'Gold2)
    \rhColor OY #(x11-color 'LightSeaGreen)
    \rhColor AA #(x11-color 'Yellow)
    \rhColor AW #(x11-color 'RosyBrown)
    \rhColor AH #(x11-color 'Khaki)
    \rhColor AX #(x11-color 'PaleGoldenrod)
    \rhColor ER #(x11-color 'BurlyWood2)
    \rhColor AY #(x11-color 'OliveDrab)
    \rhColor AE #(x11-color 'YellowGreen)
    \rhColor EH #(x11-color 'Green)
    \rhColor EY #(x11-color 'MediumSpringGreen)
    \rhColor IH #(x11-color 'Turquoise)
    \rhColor IY #(x11-color 'DeepSkyBlue)

    \rhColor em #(x11-color 'GreenYellow)
    \rhColor en #(x11-color 'GreenYellow)
    \rhColor eg #(x11-color 'GreenYellow)
  %Q5:
  % 1
  Are you cus -- tom
  made, cus -- tom paid, or you just cus -- tom fit -- ted?
  % 2
  Play -- sta -- tion
  "2" up in the ride and is that Lo -- ren -- zo kit -- ted?
  % 3
  Is that your
  wife, your girl -- friend or just your main bitch?
  \shrinkLyrics 2
  % 4
  You take a pic,
  while I'm rub -- bing the hips, touch the lips to the top of the dick and then whew!
  \shrinkLyrics 0
  }
q-Six-Lyrics =
  \lyricmode {

    \rhColor a #(x11-color 'Red)
    \rhColor b #(x11-color 'Orange)
    \rhColor c #(x11-color 'Yellow)
    \rhColor d #(x11-color 'YellowGreen)
    \rhColor e #(x11-color 'Green)
    \rhColor f #(x11-color 'LimeGreen)
    \rhColor g #(x11-color 'Cyan)
    \rhColor h #(x11-color 'DarkCyan)
    \rhColor i #(x11-color 'LightSkyBlue)
    \rhColor j #(x11-color 'DodgerBlue)
    \rhColor k #(x11-color 'Blue)
    \rhColor l #(x11-color 'Purple)
    \rhColor m #(x11-color 'Fuchsia)
    \rhColor n #(x11-color 'Maroon)
    \rhColor o #(x11-color 'DarkOrange2)
    \rhColor p #(x11-color 'DeepPink2)


    \rhColor A #(x11-color 'Red)
    \rhColor B #(x11-color 'DarkOrange)
    \rhColor C #(x11-color 'Gold2)
    \rhColor D #(x11-color 'YellowGreen)
    \rhColor E #(x11-color 'Green)
    \rhColor F #(x11-color 'LimeGreen)
    \rhColor G #(x11-color 'Cyan)
    \rhColor H #(x11-color 'DarkCyan)
    \rhColor I #(x11-color 'LightSkyBlue)
    \rhColor J #(x11-color 'DodgerBlue)
    \rhColor K #(x11-color 'Blue)
    \rhColor L #(x11-color 'Purple)
    \rhColor M #(x11-color 'Fuchsia)
    \rhColor N #(x11-color 'Maroon)
    \rhColor O #(x11-color 'DarkOrange2)
    \rhColor P #(x11-color 'DeepPink2)

    % NEW Vowel Rainbow Pink
    \rhColor uw #(x11-color 'DeepPink2)
    \rhColor uh #(x11-color 'Red)
    \rhColor el #(x11-color 'Red)
    \rhColor ow #(x11-color 'DarkOrange2)
    \rhColor ao #(x11-color 'Gold2)
    \rhColor oy #(x11-color 'LightSeaGreen)
    \rhColor aa #(x11-color 'Yellow)
    \rhColor aw #(x11-color 'RosyBrown)
    \rhColor ah #(x11-color 'Khaki)
    \rhColor ax #(x11-color 'PaleGoldenrod)
    \rhColor er #(x11-color 'BurlyWood2)
    \rhColor ay #(x11-color 'OliveDrab)
    \rhColor ae #(x11-color 'YellowGreen)
    \rhColor eh #(x11-color 'Green)
    \rhColor ey #(x11-color 'MediumSpringGreen)
    \rhColor ih #(x11-color 'Turquoise)
    \rhColor iy #(x11-color 'DeepSkyBlue)

    \rhColor UW #(x11-color 'DeepPink2)
    \rhColor UH #(x11-color 'Red)
    \rhColor EL #(x11-color 'Red)
    \rhColor OW #(x11-color 'DarkOrange2)
    \rhColor AO #(x11-color 'Gold2)
    \rhColor OY #(x11-color 'LightSeaGreen)
    \rhColor AA #(x11-color 'Yellow)
    \rhColor AW #(x11-color 'RosyBrown)
    \rhColor AH #(x11-color 'Khaki)
    \rhColor AX #(x11-color 'PaleGoldenrod)
    \rhColor ER #(x11-color 'BurlyWood2)
    \rhColor AY #(x11-color 'OliveDrab)
    \rhColor AE #(x11-color 'YellowGreen)
    \rhColor EH #(x11-color 'Green)
    \rhColor EY #(x11-color 'MediumSpringGreen)
    \rhColor IH #(x11-color 'Turquoise)
    \rhColor IY #(x11-color 'DeepSkyBlue)

    \rhColor em #(x11-color 'GreenYellow)
    \rhColor en #(x11-color 'GreenYellow)
    \rhColor eg #(x11-color 'GreenYellow)
  %Q6:
  % 1
  And tell me
  who's your house -- kee -- per and what you keep in your house?
  % 2
  What a -- bout
  dia -- monds and gold, is that what you keep in your mouth?
  % 3
  What in the
  world is in that case, what you got in that case?
  % 4
  \shrinkLyrics 2
  Get up out my face,
  you coul -- dn't re -- late, Wait to take place at a si -- mi -- lar pace So shake, shake it!
  \shrinkLyrics 0
  }
%Outro:
  %{ Get out my busi -- ness, my busi -- ness
  Stay the fuck up out my busi -- ness, ah
  'Cause these nig -- gas all up in my shit and it's my busi -- ness, my busi -- ness
  Stay the fuck up out my busi -- ness, 'cause it's mine, oh mine
  My busi -- ness, my busi -- ness
  Stay the fuck up out my busi -- ness
  'Cause these nig -- gas all up in my shit
  And it's my busi -- ness, my busi -- ness
  Stay the fuck up out my busi -- ness, 'cause it's mine, oh mine } %}

q-One-Syllables =
  \lyricmode {
    \partial 4
    % 1
    r16 n-aw16 `w-ey-rd y-uw |
    <>`g-ih-t16 D-ae-t `pl-ae-t n-ah-m
    C-ey-n8 w-ih-T16 D-eh-m
    <>@d-aa8 m-ah-ndz16 `Q-ih-n32 Q-ih-t '

    % 2
    r8 `w-ey-rd16 y-uw |
    <>`g-ih-t16 D-ae-t `m-ae-t C-ih-n
    <>@b-ih-nz8 w-ih-T16 D-eh-m
    `w-ih-n16. d-ow-z `t-ih-n32 <>t-ih-d '

    % 3
    r8 `h-uw16 D-eh-m |
    <>@g-el-z8 y-uw16 b-iy
    `w-ih-t16. w-eh-n32 `y-uw16 b-iy
    <>@r-aa8 d-ih-n16 `Tr-uw16 _16 '
    % 4
    r32 m-ae-n32 `Q-ay Q-ey-nt g-aa-t16 |
    `n-ah32 T-ih-n t-ax `pr-uw-v16 Q-ay32 `p-ey-d16
    m-ah32 `d-uw-z16. `br-ey32 k-ih-n D-ah `r-uw-Lz32
    _32 Q-ay32 `S-ey-k16 `f-uw-Lz w-ay-l32 Q-ay-m
    `t-ey k-ih-n Q-ah @kr-uw-z8 r32 |
  }
q-Two-Syllables =
  \lyricmode {
  %Q2:
    \partial 8
    % 1
  `t-eh-l16 m-iy |
  `h-uw-z y-ao-r @w-iy-d8 `m-ae-n16. Q-ah-nd32 `h-aw d-uw y-uw16 `sm-ow-k16. s-ow @g-uh-d16 _16 ' r16
  % 1
  `y-uw-z16 Q-ah |
  `s-uw p-er `st-aa-r8 @b-oy w-ay16 y-uw `st-ih-l32 Q-ah-p `Q-ih-n16 D-ah @h-uh-d _16 ' r16
  % 1
  `w-ah-t32 Q-ih-n D-ah16 |
  `w-er-ld16. Q-ih-z32 `Q-ih-n16 D-ae-t @b-ae-g8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @b-ae-g _16 ' r32
  % 1
  Q-ah32 `k-ah p-el Q-ah `k-ae-nz |
  _32 Q-ah `w-uw-p Q-ae-s16 `y-uw32 d-ih-d Q-ah `g-uh-d Q-ae-s16 `J-ao-b Q-ah-v32 `J-ah-st16 @Q-ay Q-ih-n32 `m-iy16 @sp-ay Q-ih-n32 `m-iy16 ' r16 r8 |
  }
q-Three-Syllables =
  \lyricmode {
  % Q3:
    \partial 4.
  r16 @m-ae-n16 _16 D-ae-t `k-aa-r d-ow-nt |
  `k-ah-m32 Q-aw-16 Q-ah-32 `t-ih-l16 n-eh-kst `y-ih-r8 `w-eh-r32 Q-ih-n D-ah16 @f-ah-k16. d-ih-d32 y-uw16 `g-eh-t32 Q-ih-t ' r16

  D-ae-ts16 `Q-ey t-iy |
  `T-aw z-ah-nd `b-ah-ks8 @g-ao-n `w-eh-r32 Q-ih-n D-ah16 @f-ah-k16. d-ih-d32 y-uw16 `sp-eh-nd32 Q-ih-t ' r16.

  y-uw32 `m-ah-st16 h-ae-v |
  `Q-ay-z8 Q-aa-n16 y-ao-r @b-ae-k16. k-ah-z32 `y-uw16 g-aa-t `m-ah32 n-iy16 `t-uw32 D-ah16 `<>s-ih32 l-ih-n ' r16

  Q-ah-nd32 D-ah `b-ih g-er D-ah `k-ae-p |
  _32 D-ah `b-ih g-er D-ah `<>p-ih l-ih-n D-ah `b-eh t-er Q-ay-m `<>f-ih l-ih-n D-ah `m-ao-r D-ae-t Q-ay-m `C-ih l-ih-n `w-ih l-ih-n `dr-ih l-ih-n Q-ah-nd `k-ih l-ih-n D-ah `f-ih l-ih-n16 ' r16 |
  }
q-Four-Syllables =
  \lyricmode {
  %Q4:
    \partial 4
  r16 n-aw16 `h-uw-z D-ae-t |
  `b-ah-k <>k-eh-d `n-ey k-eh-d @k-uh-k8 `f-ih-k16 s-ih-n `Tr-iy16. k-ao-rs `m-iy-lz16 _16 ' r16

  `g-eh t-ih-n |
  @g-uw-s `b-ah-mps8 w-eh-n32 h-er `b-aa16 d-iy `t-ae-ps D-ah `s-ih-ks16. Q-ih-nC `h-iy-lz16 _16 ' r16

  `w-ah-t32 Q-ih-n D-ah16 |
  `w-er-ld16. Q-ih-z32 Q-ih-n16 D-ae-t @r-uw-m8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @r-uw-m _16 ' r32

  Q-ah32 `k-ah p-el Q-ah `g-ae-ts |
  _32 Q-ah `k-ah p-el Q-ah n-ay-vz _32 Q-ah `k-ah p-el Q-ah `r-ae-ts _32 Q-ah `k-ah p-el Q-ah `w-ay-vz _32 n-aw16 Q-ih-ts32 `t-ay-m t-uw `C-uw-z8 ' r8 |
  }
q-Five-Syllables =
  \lyricmode {
  %Q5:
  \partial 4
  r32 Q-aa-r32 `y-uw16 `k-ah st-ah-m |
  @m-ey-d8 `k-ah16 st-ah-m `p-ey-d16. Q-ao-r32 `y-uw16 J-ah-st `k-ah16. st-ah-m `f-ih32 <>t-ih-d ' r16

  pl-ey16 `st-ey S-ah-n |
  `t-uw Q-ah-p `Q-ih-n D-ah32 `r-ay-d _16 Q-ah-nd32 Q-ih-z `D-ae-t16 l-ao-r `Q-eh-n16. z-ow `k-ih32 <>t-ih-d ' r16

  Q-ih-z16 `D-ae-t y-ao-r |
  `w-ay-f y-ao-r `g-er-l8 `fr-eh-nd16 Q-ao-r `J-ah-st y-ao-r @m-ey-n8. `b-ih-C16 ' r16

  y-uw `t-ey-k Q-ah32 `p-ih-k |
  _16 `w-ay-l32 Q-ay-m `r-ah b-ih-n D-ah `h-ih-ps _32 `t-ah-C16 D-ah32 `l-ih-ps16 t-uw32 D-ah `t-aa-p Q-ah-v D-ah `d-ih-k _32 Q-ah-nd D-eh-n @w-uw32 _16 ' r8. |
  }
q-Six-Syllables =
  \lyricmode {
  %Q6:
  \partial 4
  r16 Q-ah-nd16 `t-eh-l m-iy |
  `h-uw-z y-ao-r `h-aw-s8 `k-iy16 p-er32 Q-ah-nd `w-ah-t16 y-uw `k-iy-p Q-ih-n y-ao-r @h-aw-s _16 ' r16

  `w-ah-t32 Q-ah b-aw-t16 |
  `d-ay m-ah-ndz Q-ah-nd `g-ow-ld _16 Q-ih-z `D-ae-t w-ah-t32 y-uw `k-iy-p16 Q-ih-n y-ao-r @m-aw-T _16 ' r16

  `w-ah-t32 Q-ih-n D-ah16 |
  `w-er-ld16. Q-ih-z32 `Q-ih-n16 D-ae-t @k-ey-s8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @k-ey-s _32 ' r32

  `g-eh-t32 Q-ah-p `Q-aw-t16 m-ay32 `f-ey-s |
  _32 y-uw `k-uh d-ah-nt r-ih `l-ey-t16 `w-ey-t32 _32 t-uw `t-ey-k16 `pl-ey-s Q-ae-t32 Q-ah `s-ih m-ih l-er `p-ey-s _32 s-ow `S-ey-k16 `S-ey-k Q-ih-t ' r8 |
  }
%[OUTRO]




\lyricNotation \q-One-Lyrics \q-One-Syllables
\pageBreak
\lyricNotation \q-Two-Lyrics \q-Two-Syllables
\pageBreak
\lyricNotation \q-Three-Lyrics \q-Three-Syllables
\pageBreak
\lyricNotation \q-Four-Lyrics \q-Four-Syllables
\pageBreak
\lyricNotation \q-Five-Lyrics \q-Five-Syllables
\pageBreak
\lyricNotation \q-Six-Lyrics \q-Six-Syllables

\oneLineLyricNotation \q-One-Lyrics \q-One-Syllables
\oneLineLyricNotation \q-Two-Lyrics \q-Two-Syllables
\pageBreak
\oneLineLyricNotation \q-Three-Lyrics \q-Three-Syllables
\oneLineLyricNotation \q-Four-Lyrics \q-Four-Syllables
\pageBreak
\oneLineLyricNotation \q-Five-Lyrics \q-Five-Syllables
\oneLineLyricNotation \q-Six-Lyrics \q-Six-Syllables

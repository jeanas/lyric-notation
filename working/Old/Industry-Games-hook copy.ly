\version "2.22.0"
\include "lyric-notation.ily"

\lyricNotation
  \lyricmode {
  Nig -- gas be playin', yeah
  In -- dus -- try games, yeah
  Rap -- pin' but they not in love with it
  Think it's a shame, yeah
  I'm 'bout to pop on some other shit
  Think it's a game?
  They don't con -- trol where I land
  Roy -- al flush, nig -- ga the cards in my hand }
\lyricmode {
n-ih b-iy pl-ey \breathe y-ae Q-ih-n g-ey-mz \breathe y-ae r-ae b-ah-t D-ey n-aa-t Q-ih-n l-ah-v w-ih-D Q-ih-t T-ih-Gk Q-ih-ts Q-ah S-ey-m \breathe y-ae Q-ay-m b-aw-t t-uw p-aa-p Q-aa-n s-ah-m Q-ah S-ih-t T-ih-Gk Q-ih-ts Q-ah g-ey-m \breathe D-ey d-ow-nt k-ah-n w-eh-r Q-ay l-ae-nd r-oy fl-ah-S \breathe n-ih D-ah k-aa-rdz Q-ih-n m-ay h-ae-nd }

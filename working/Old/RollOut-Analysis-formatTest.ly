\version "2.22.0"
\include "../lyric-notation.ily"

qOneLyrics =
  \lyricmode
  {
  \goodColors
  % 1
  Now where'd\rh I0 you\rh I0 |
    get\rh I0 that\rh I0 plat’\rh MSB0 -- num\rh MSB0
    chain\rh SB0 with\rh I0 them\rh I0
    dia\rh C0 -- monds\rh C0 in\rh G0 it?\rh G0
  % 2
  Where'd\rh I0 you\rh I0 |
    get\rh I0 that\rh I0 mat\rh MSB0 -- chin’\rh MSB0
    Benz\rh SB0 with\rh I0 them\rh I0
    win -- dows tin\rh G0 -- ted?\rh G0
  % 3
  Who them |
    girls you be
    wit’\rh SB0 when you be
    ri\rh C0 -- din’\rh C0 through?\rh A0
  % 4
  \shrinkLyrics 2
  Man\rh C0 I\rh C0 ain't\rh C0 got\rh C0 |
      no\rh C0 -- thin’\rh C0 to\rh C0 prove,\rh A0 I\rh C0 paid\rh C0
      my\rh C0 dues,\rh A0 brea\rh C0 -- kin’\rh C0 the\rh C0 rules,\rh A0
      I\rh C0 shake\rh C0 fools\rh A0 while\rh C0 I'm\rh C0
      ta\rh C0 -- kin’\rh C0 a\rh C0 cruise!\rh A0 |
      \shrinkLyrics 0
    }
qTwoLyrics =
  \lyricmode
  {
  \goodColors
  % 1
  Tell me
    who's your weed man, and how do you smoke so good?\rh SG0
  % 2
  You's a
    su -- per -- star, boy, why you still up in the hood?\rh SG0
  % 3
  What\rh Y0 in\rh Y0 the\rh Y0
    world\rh Y0 is\rh Y0 in\rh Y0 that\rh Y0 bag?\rh OD0 What\rh Y0 you\rh Y0 got\rh Y0 in\rh Y0 that\rh Y0 bag?\rh OD0
  % 4
  \shrinkLyrics 2
  A\rh M0 cou\rh M0 -- ple\rh BB0 of\rh M0 cans\rh OD0
    of\rh M0 whoop\rh BB0 ass,\rh OD0 you\rh OR0 did a\rh M0 good\rh BB0 ass\rh OD0 job\rh B0 of\rh M0 just\rh M0 eye\rh P0 -- in'\rh P0 me,\rh P0 spy\rh PP0 -- in'\rh PP0 me\rh PP0 |
    \shrinkLyrics 0
  }
qThreeLyrics =
  \lyricmode
  {
  \goodColors
  % 1
  Man, that car don't
    come out un -- til next year, where\rh I0 in\rh I0 the\rh I0 fuck\rh I0 did\rh I0 you\rh I0 get\rh H0 it?\rh H0
  % 2
  That's eigh -- ty
    thou -- sand bucks gone, where\rh I0 in\rh I0 the\rh I0 fuck\rh I0 did\rh I0 you\rh I0 spend\rh H0 it?\rh H0
  % 3
  You must have
    eyes on your back,\rh OD0 ‘cause you got mo -- ney to the cei\rh LSG0 -- lin’\rh LSG0
    \shrinkLyrics 2
  % 4
  And the\rh LS0 big\rh IR0 -- ger\rh LS0 the\rh LS0 cap,\rh OD0
    \shrinkLyrics 4
    the\rh LS0 big\rh IR0 -- ger\rh LS0 the\rh LS0 pee\rh LSG0 -- ling,\rh LSG0 the\rh LS0 bet\rh IR0 -- ter\rh LS0 I'm\rh LS0 fee\rh LSG0 -- ling,\rh LSG0 the\rh LS0 more\rh FB0 that\rh LS0 I'm\rh LS0 chil\rh LSG0 -- ling,\rh LSG0 wil\rh LSGLSG0 -- ling,\rh LSGLSG0 dril\rh LSG0 -- ling\rh LSG0 and\rh LS0 kil\rh LSG0 -- ling\rh LSG0 the\rh LS0 fee\rh LSG0 -- ling\rh LSG0
    \shrinkLyrics 0
  }
qFourLyrics =
  \lyricmode
  {
  \goodColors
  % 1
  Now who's that
    buc -- ked na -- ked cook fi -- xin’ three course meals?\rh LG0
  % 2
  Get -- ting
    goose -- bumps when her bo -- dy taps the six inch heels\rh LG0
  % 3
  What\rh Y0 in\rh Y0 the\rh Y0
    world\rh Y0 is\rh Y0 in\rh Y0 that\rh Y0 room,\rh DOG0 what\rh Y0 you\rh Y0 got\rh Y0 in\rh Y0 that\rh Y0 room?\rh DOG0
  % 4
  \shrinkLyrics 2
  A cou -- ple o' gats,
    a cou -- ple o' knives, a cou -- ple o' rats, a cou -- ple o' wives now it's time to choose!\rh DOG0
    \shrinkLyrics 0
  }
qFiveLyrics =
  \lyricmode
  {
  \goodColors
  % 1
  Are you cus\rh I0 -- tom\rh I0
    made, cus\rh I0 -- tom\rh I0 paid, or you just cus\rh I0 -- tom\rh I0 fit\rh G0 -- ted?\rh G0
  % 2
  Play -- sta -- tion
    "2" up in the ride and is that Lo -- ren -- zo kit\rh G0 -- ted?\rh G0
  % 3
  Is that your
    wife, your girl -- friend or just\rh PT0 your main\rh F0 bitch?\rh DT0
    \shrinkLyrics 2
  % 4
  You take\rh F0 a\rh PT0 pic,\rh DT0
    while\rh DT0 I'm\rh DT0 rub\rh PT0 -- bing\rh DT0 the\rh PT0 hips,\rh DT0 touch\rh PT0 the\rh PT0 lips\rh DT0 to\rh DOG0 the\rh PT0 top\rh PT0 of\rh PT0 the\rh PT0 dick\rh DT0 and\rh PT0 then whew!\rh DOG0
    \shrinkLyrics 0
  }
qSixLyrics =
  \lyricmode
  {
  \goodColors
  % 1
  And tell me
    who's your house -- kee -- per and what\rh I0 you\rh I0 keep\rh I0 in\rh I0 your\rh I0 house?\rh MSG0
  % 2
  What a -- bout
    dia -- monds and gold, is that what\rh I0 you\rh I0 keep\rh I0 in\rh I0 your\rh I0 mouth?\rh MSG0
  % 3
  What\rh Y0 in\rh Y0 the\rh Y0
    world\rh Y0 is\rh Y0 in\rh Y0 that\rh Y0 case,\rh OD0 what\rh Y0 you\rh Y0 got\rh Y0 in\rh Y0 that\rh Y0 case?\rh OD0
      \shrinkLyrics 2
  % 4
  Get up out my face,
    you coul -- dn't re -- late, Wait to take place at a si -- mi -- lar pace So shake, shake it!
    \shrinkLyrics 0
  }
%Outro:
  %{ Get out my busi -- ness, my busi -- ness
  Stay the fuck up out my busi -- ness, ah
  'Cause these nig -- gas all up in my shit and it's my busi -- ness, my busi -- ness
  Stay the fuck up out my busi -- ness, 'cause it's mine, oh mine
  My busi -- ness, my busi -- ness
  Stay the fuck up out my busi -- ness
  'Cause these nig -- gas all up in my shit
  And it's my busi -- ness, my busi -- ness
  Stay the fuck up out my busi -- ness, 'cause it's mine, oh mine } %}

qOneSyls =
  \lyricmode
  {
  \partial 4
  % 1
  r16 n-aw16 `w-ey-rd y-uw |
  <>`g-ih-t16 D-ae-t `pl-ae-t n-ah-m
  C-ey-n8 w-ih-T16 D-eh-m
  <>@d-aa8 m-EN-dz16 `Q-ih-n32 Q-ih-t '

  % 2
  r8 `w-ey-rd16 y-uw |
  <>`g-ih-t16 D-ae-t `m-ae-t C-ih-n
  <>@b-ih-nz8 w-ih-T16 D-eh-m
  `w-ih-n16. d-ow-z `t-ih-n32 <>t-ih-d '

  % 3
  r8 `h-uw16 D-eh-m |
  <>@g-el-z8 y-uw16 b-iy
  `w-ih-t16. w-eh-n32 `y-uw16 b-iy
  <>@r-aa8 d-ih-n16 `Tr-uw16 _16 '
  % 4
  r32 m-ae-n32 `Q-ay Q-ey-nt g-aa-t16 |
  `n-ah32 T-ih-n t-ax `pr-uw-v16 Q-ay32 `p-ey-d16
  m-ah32 `d-uw-z16. `br-ey32 k-ih-n D-ah `r-uw-Lz
  _32 Q-ay32 `S-ey-k16 `f-uw-Lz w-ay-l32 Q-ay-m
  `t-ey k-ih-n Q-ah @kr-uw-z8 r32 |
  }

qTwoSyls =
  \lyricmode
  {
  \partial 8
  % 1
  `t-eh-l16 m-iy |
  `h-uw-z y-ao-r @w-iy-d8 `m-ae-n16. Q-ah-nd32 `h-aw d-uw y-uw16 `sm-ow-k16. s-ow @g-uh-d16 _16 ' r16
  % 1
  `y-uw-z16 Q-ah |
  `s-uw p-er `st-aa-r8 @b-oy w-ay16 y-uw `st-ih-l32 Q-ah-p `Q-ih-n16 D-ah @h-uh-d _16 ' r16
  % 1
  `w-ah-t32 Q-ih-n D-ah16 |
  `w-er-ld16. Q-ih-z32 `Q-ih-n16 D-ae-t @b-ae-g8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @b-ae-g _16 ' r32
  % 1
  Q-ah32 `k-ah p-el Q-ah `k-ae-nz |
  _32 Q-ah `w-uh-p Q-ae-s16 `y-uw32 d-ih-d Q-ah `g-uh-d Q-ae-s16 `J-ao-b Q-ah-v32 `J-ah-st16 @Q-ay Q-ih-n32 `m-iy16 @sp-ay Q-ih-n32 `m-iy16 ' r16 r8 |
  }

qThreeSyls =
  \lyricmode
  {
    \partial 4.
  r16 @m-ae-n16 _16 D-ae-t `k-aa-r d-ow-nt |
  `k-ah-m32 Q-aw-16 Q-en32 `t-ih-l16 n-eh-kst `y-ih-r8 `w-eh-r32 Q-ih-n D-ah16 @f-ah-k16. d-ih-d32 y-uw16 `g-eh-t32 Q-ih-t ' r16

  D-ae-ts16 `Q-ey t-iy |
  `T-aw z-en-d `b-ah-ks8 @g-ao-n `w-eh-r32 Q-ih-n D-ah16 @f-ah-k16. d-ih-d32 y-uw16 `sp-eh-nd32 Q-ih-t ' r16.

  y-uw32 `m-ah-st16 h-ae-v |
  `Q-ay-z8 Q-ao-n16 y-ao-r @b-ae-k16. k-ah-z32 `y-uw16 g-aa-t `m-ah32 n-iy16 `t-uw32 D-ah16 `<>s-ih32 l-ih-n ' r16

  Q-ah-nd32 D-ah `b-ih g-er D-ah `k-ae-p |
  _32 D-ah `b-ih g-er D-ah `<>p-ih l-ih-n D-ah `b-eh t-er Q-ay-m `<>f-ih l-ih-n D-ah `m-ao-r D-ae-t Q-ay-m `C-ih l-ih-n `w-ih l-ih-n `dr-ih l-ih-n Q-ah-nd `k-ih l-ih-n D-ah `f-ih l-ih-n16 ' r16 |
  }

qFourSyls =
  \lyricmode
  {
    \partial 4
  r16 n-aw16 `h-uw-z D-ae-t |
  `b-ah-k <>k-eh-d `n-ey k-eh-d @k-uh-k8 `f-ih-k16 s-ih-n `Tr-iy16. k-ao-rs `m-iy-Lz16 _16 ' r16

  `g-eh t-ih-n |
  @g-uw-s `b-ah-mps8 w-eh-n32 h-er `b-aa16 d-iy `t-ae-ps D-ah `s-ih-ks16. Q-ih-nC `h-iy-Lz16 _16 ' r16

  `w-ah-t32 Q-ih-n D-ah16 |
  `w-er-ld16. Q-ih-z32 Q-ih-n16 D-ae-t @r-uw-m8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @r-uw-m _16 ' r32

  Q-ah32 `k-ah p-el Q-ah `g-ae-ts |
  _32 Q-ah `k-ah p-el Q-ah n-ay-vz _32 Q-ah `k-ah p-el Q-ah `r-ae-ts _32 Q-ah `k-ah p-el Q-ah `w-ay-vz _32 n-aw16 Q-ih-ts32 `t-ay-m t-uw `C-uw-z8 ' r8 |
  }

qFiveSyls =
  \lyricmode
  {
  \partial 4
  r32 Q-aa-r32 `y-uw16 `k-ah st-ah-m |
    @m-ey-d8 `k-ah16 st-ah-m `p-ey-d16. Q-ao-r32 `y-uw16 J-ah-st `k-ah16. st-ah-m `f-ih32 <>t-ih-d ' r16

  pl-ey16 `st-ey S-ah-n |
  `t-uw Q-ah-p `Q-ih-n D-ah32 `r-ay-d _16 Q-ah-nd32 Q-ih-z `D-ae-t16 l-ao-r `Q-eh-n16. z-ow `k-ih32 <>t-ih-d ' r16

  Q-ih-z16 `D-ae-t y-ao-r |
  `w-ay-f y-ao-r `g-er-l8 `fr-eh-nd16 Q-ao-r `J-ah-st y-ao-r @m-ey-n8. `b-ih-C16 ' r16

  y-uw `t-ey-k Q-ah32 `p-ih-k |
  _16 `w-ay-l32 Q-ay-m `r-ah b-ih-n D-ah `h-ih-ps _32 `t-ah-C16 D-ah32 `l-ih-ps16 t-uw32 D-ah `t-aa-p Q-ah-v D-ah `d-ih-k _32 Q-ah-nd D-eh-n @w-uw32 _16 ' r8. |
  }
qSixSyls =
  \lyricmode
  {
  \partial 4
  r16 Q-ah-nd16 `t-eh-l m-iy |
  `h-uw-z y-ao-r `h-aw-s8 `k-iy16 p-er32 Q-ah-nd `w-ah-t16 y-uw `k-iy-p Q-ih-n y-ao-r @h-aw-s _16 ' r16

  `w-ah-t32 Q-ah b-aw-t16 |
  `d-ay m-ah-ndz Q-ah-nd `g-ow-ld _16 Q-ih-z `D-ae-t w-ah-t32 y-uw `k-iy-p16 Q-ih-n y-ao-r @m-aw-T _16 ' r16

  `w-ah-t32 Q-ih-n D-ah16 |
  `w-er-ld16. Q-ih-z32 `Q-ih-n16 D-ae-t @k-ey-s8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @k-ey-s _32 ' r32

  `g-eh-t32 Q-ah-p `Q-aw-t16 m-ay32 `f-ey-s |
  _32 y-uw `k-uh d-en-t r-ih `l-ey-t16 `w-ey-t32 _32 t-ax `t-ey-k16 `pl-ey-s Q-ae-t32 Q-ah `s-ih m-ih l-er `p-ey-s _32 s-ow `S-ey-k16 `S-ey-k Q-ih-t ' r8 |
  }
%[OUTRO]

%{ verseOneLyrics = {
  \qOneLyrics
  \qTwoLyrics
} %}
%{ verseTwoLyrics = {
  \qThreeLyrics
  \qFourLyrics
} %}
%{ verseThreeLyrics = {
  \qFiveLyrics
  \qSixLyrics
} %}

%{ verseOneSyls = {
  \qOneSyls
  \qTwoSyls
} %}
%{ verseTwoSyls = {
  \qThreeSyls
  \qFourSyls
} %}
%{ verseThreeSyls = {
  \qFiveSyls
  \qSixSyls
} %}

%{ songLyrics = {
  \qOneLyrics
  \qTwoLyrics
  \qThreeLyrics
  \qFourLyrics
  \qFiveLyrics
  \qSixLyrics
  } %}

%{ songSyls = {
    \qOneSyls
    \qTwoSyls
    \qThreeSyls
    \qFourSyls
    \qFiveSyls
    \qSixSyls
  } %}

%{ \lyricNotation \songLyrics \songSyls %}
\lyricNotation \qOneLyrics \qOneSyls
\pageBreak
\lyricNotation \qThreeLyrics \qThreeSyls
\pageBreak
\lyricNotation \qFiveLyrics \qFiveSyls
\pageBreak

\lyricNotation \qTwoLyrics \qTwoSyls
\pageBreak
\lyricNotation \qFourLyrics \qFourSyls
\pageBreak
\lyricNotation \qSixLyrics \qSixSyls
\pageBreak

\lyricNotation \qOneLyrics \qOneSyls
\pageBreak
\lyricNotation \qTwoLyrics \qTwoSyls
\pageBreak
\lyricNotation \qThreeLyrics \qThreeSyls
\pageBreak
\lyricNotation \qFourLyrics \qFourSyls
\pageBreak
\lyricNotation \qFiveLyrics \qFiveSyls
\pageBreak
\lyricNotation \qSixLyrics \qSixSyls
\pageBreak
%{ \oneLineLyricNotation \qOneLyrics \qOneSyls
\pageBreak
\oneLineLyricNotation \qTwoLyrics \qTwoSyls
\pageBreak
\oneLineLyricNotation \qThreeLyrics \qThreeSyls
\pageBreak
\oneLineLyricNotation \qFourLyrics \qFourSyls
\pageBreak
\oneLineLyricNotation \qFiveLyrics \qFiveSyls
\pageBreak
\oneLineLyricNotation \qSixLyrics \qSixSyls %}

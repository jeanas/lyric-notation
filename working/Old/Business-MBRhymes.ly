\version "2.22.0"
\include "../lyric-notation.ily"

songLyricsMB =
  \lyricmode {
    \rhColor a red
    \rhColor b lightblue
    \rhColor c gold
    \rhColor C gold
    \rhColor e yellow
    \rhColor d pink

  You're 'bout to wit\rh c0 -- ness\rh e0 hip\rh c0 -- hop\rh a0 in\rh c0 its\rh c0 most\rh b0 pur\rh d0 -- est\rh c0
  Most\rh b0 raw\rh a0 -- est\rh c0 form,\rh a0 flow\rh b0 al\rh a0 -- most\rh b0 flaw\rh a0 -- less\rh c0
  Most\rh b0 hard\rh a0 -- est,\rh c0 most\rh b0 ho\rh a0 -- nest\rh c0 known\rh b0 ar\rh a0 -- tist\rh c0
  Chip\rh C0 off\rh a0 the\rh c0 old\rh b0 block,\rh a0 but old\rh b0 Doc\rh a0 is\rh c0 (back) }
songSyllablesMB =
  \lyricmode {
    \partial 4
  r16 y-er16 `b-aw-t t-uw |
  @w-ih-t8 n-eh-s16 `h-ih-p16 _16 h-aa-p8 Q-ih-n16 Q-ih-ts8 \sB "𝛂*" `m-ow-st @py-uuh-r \eB r-ih-st |
  \sB "𝛂" `m-ow-st @r-ao \eB Q-ih-st16 `f-ao-rm8 `fl-ow16 _16 Q-aa \sB "𝛂" `m-ow-st8 @fl-ao \eB l-ih-s |
  \sB "𝛂" `m-ow-st @h-aa-r \eB d-ih-st16 \sB "𝛂" `m-ow-st8 @Q-aa16 _16 \eB n-ih-st \sB "𝛂" `n-ow-n8 @Q-aa-r \eB t-ih-st |
  `C-ih-p @Q-ao-f D-iy16 `Q-ow-ld8 @bl-aa-k16 _16 b-ah-t \sB "𝛂" `Q-ow-ld8 @d-aa-k \eB Q-ih-z16 `b-ae-k | }
\lyricNotation \songLyrics \songSyllables
\pageBreak
\oneLineLyricNotation \songLyrics \songSyllables

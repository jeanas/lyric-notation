\version "2.22.0"
\include "lyric-notation.ily"

\lyricNotation
\lyricmode {
Q3: 
Man, that car don't come out un -- til next year, where in the fuck did you get it?
That's eigh -- ty thou -- sand bucks gone, where in the fuck did you spend it?
You must have eyes on your back, ‘cause you got mo -- ney to the cei -- lin’
And the big -- ger the cap, the big -- ger the pee -- ling, the bet -- ter I'm fee -- ling, the more that I'm chil -- ling, wil -- ling, dril -- ling and kil -- ling the fee -- ling

Q4: 
Now who's that buck na -- ked cook fi -- xin’ three course meals?
Get -- ting goose -- bumps when her bo -- dy taps the six inch heels
What in the world is in that room, what you got in that room?
A cou -- ple o' gats, a cou -- ple o' knives, a cou -- ple o' rats, a cou -- ple o' wives
Now it's time to choose

Q5:  
Are you cus -- tom made, cus -- tom paid, or you just cus -- tom fit -- ted?
Play -- sta -- tion 2 up in the ride and is that Lo -- ren -- zo kit -- ted?
Is that your wife, your girl -- friend or just your main bitch?
You take a pic, while I'm rub -- bing the hips,
Touch the lips to the top of the dick and then whew!

Q6: 
And tell me who's your house -- keeper and what you keep in your house?
What about dia -- monds and gold, is that what you keep in your mouth?
What in the world is in that case, what you got in that case?
Get up out my face, you coul -- dn't re -- late,
Wait to take place at a si -- mi -- lar pace
So shake, shake it

Outro: 
Get out my busi -- ness, my busi -- ness
Stay the fuck up out my busi -- ness, ah
'Cause these nig -- gas all up in my shit and it's my busi -- ness, my busi -- ness
Stay the fuck up out my busi -- ness, 'cause it's mine, oh mine
My busi -- ness, my busi -- ness
Stay the fuck up out my busi -- ness
'Cause these nig -- gas all up in my shit
And it's my busi -- ness, my busi -- ness
Stay the fuck up out my busi -- ness, 'cause it's mine, oh mine}
\lyricmode {
[] m-ae-n \breathe D-ae-t k-aa-r d-ow-nt k-ah-m Q-aw-t Q-ah-n `t-ih-l n-eh-kst y-ih-r \breathe w-eh-r Q-ih-n D-ah f-ah-k d-ih-d y-uw g-eh-t Q-ih-t \breathe D-ae-ts `Q-ey t-iy `T-aw z-ah-nd b-ah-ks g-ao-n \breathe w-eh-r Q-ih-n D-ah f-ah-k d-ih-d y-uw sp-eh-nd Q-ih-t \breathe y-uw m-ah-st h-ae-v Q-ay-z Q-aa-n y-ao-r b-ae-k \breathe ‘CAUSE y-uw g-aa-t `m-ah n-iy t-uw D-ah [CEI] Q-ah-nd D-ah `b-ih g-er D-ah k-ae-p \breathe D-ah `b-ih g-er D-ah `p-iy l-ih-G \breathe D-ah `b-eh t-er Q-ay-m `f-iy l-ih-G \breathe D-ah m-ao-r D-ae-t Q-ay-m `C-ih l-ih-G \breathe `w-ih l-ih-G \breathe `dr-ih l-ih-G Q-ah-nd `k-ih l-ih-G D-ah `f-iy l-ih-G Q4: n-aw h-uw-z D-ae-t b-ah-k `n-ey k-ah-d k-uh-k f-ay Tr-iy k-ao-rs m-iy-lz \breathe `g-eh t-ih-G [GOOSEBUMPS] w-eh-n h-er `b-aa d-iy t-ae-ps D-ah s-ih-ks Q-ih-nC h-iy-lz w-ah-t Q-ih-n D-ah w-er-ld Q-ih-z Q-ih-n D-ae-t r-uw-m \breathe w-ah-t y-uw g-aa-t Q-ih-n D-ae-t r-uw-m \breathe Q-ah `k-ah p-ah-l Q-ow g-ae-ts \breathe Q-ah `k-ah p-ah-l Q-ow n-ay-vz \breathe Q-ah `k-ah p-ah-l Q-ow r-ae-ts \breathe Q-ah `k-ah p-ah-l Q-ow w-ay-vz n-aw Q-ih-ts t-ay-m t-uw C-uw-z Q5: Q-aa-r y-uw `k-ah st-ah-m m-ey-d \breathe `k-ah st-ah-m p-ey-d \breathe Q-ao-r y-uw J-ah-st `k-ah st-ah-m `f-ih t-ah-d \breathe `pl-ey `st-ey S-ah-n 2 Q-ah-p Q-ih-n D-ah r-ay-d Q-ah-nd Q-ih-z D-ae-t l-er `Q-eh-n z-ow [KITTED] Q-ih-z D-ae-t y-ao-r w-ay-f \breathe y-ao-r `g-er-l `fr-eh-nd Q-ao-r J-ah-st y-ao-r m-ey-n b-ih-C \breathe y-uw t-ey-k Q-ah p-ih-k \breathe w-ay-l Q-ay-m `r-ah b-ih-G D-ah h-ih-ps \breathe t-ah-C D-ah l-ih-ps t-uw D-ah t-aa-p Q-ah-v D-ah d-ih-k Q-ah-nd D-eh-n w-uw \breathe Q6: Q-ah-nd t-eh-l m-iy h-uw-z y-ao-r `h-aw `sk-iy Q-ah-nd w-ah-t y-uw k-iy-p Q-ih-n y-ao-r h-aw-s \breathe w-ah-t Q-ah `d-ay m-ah-ndz Q-ah-nd g-ow-ld \breathe Q-ih-z D-ae-t w-ah-t y-uw k-iy-p Q-ih-n y-ao-r m-aw-T \breathe w-ah-t Q-ih-n D-ah w-er-ld Q-ih-z Q-ih-n D-ae-t k-ey-s \breathe w-ah-t y-uw g-aa-t Q-ih-n D-ae-t k-ey-s \breathe g-eh-t Q-ah-p Q-aw-t m-ay f-ey-s \breathe y-uw `k-uh d-ah-nt r-ih `l-ey-t \breathe w-ey-t t-uw t-ey-k pl-ey-s Q-ae-t Q-ah `s-ih m-ah l-er p-ey-s s-ow S-ey-k \breathe S-ey-k Q-ih-t [OUTRO] g-eh-t Q-aw-t m-ay `b-ih-z n-ah-s \breathe m-ay `b-ih-z n-ah-s st-ey D-ah f-ah-k Q-ah-p Q-aw-t m-ay `b-ih-z n-ah-s \breathe Q-aa k-uh-z D-iy-z `n-ih g-uh-z Q-ao-l Q-ah-p Q-ih-n m-ay S-ih-t Q-ah-nd Q-ih-ts m-ay `b-ih-z n-ah-s \breathe m-ay `b-ih-z n-ah-s st-ey D-ah f-ah-k Q-ah-p Q-aw-t m-ay `b-ih-z n-ah-s \breathe k-uh-z Q-ih-ts m-ay-n \breathe Q-ow m-ay-n m-ay `b-ih-z n-ah-s \breathe m-ay `b-ih-z n-ah-s st-ey D-ah f-ah-k Q-ah-p Q-aw-t m-ay `b-ih-z n-ah-s k-uh-z D-iy-z `n-ih g-uh-z Q-ao-l Q-ah-p Q-ih-n m-ay S-ih-t Q-ah-nd Q-ih-ts m-ay `b-ih-z n-ah-s \breathe m-ay `b-ih-z n-ah-s st-ey D-ah f-ah-k Q-ah-p Q-aw-t m-ay `b-ih-z n-ah-s \breathe k-uh-z Q-ih-ts m-ay-n \breathe Q-ow m-ay-n }

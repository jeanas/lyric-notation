\version "2.22.0"
\include "../lyric-notation.ily"

songLyricsPlain =
  \lyricmode {
  I'm be -- gin -- ning to write like An -- na Ka -- re -- ni -- na, give me a mi -- nute, a mic
  A li -- ttle to like, get rid of the spite, a bit of the pride to fight
  }

songSylsPlain =
  \lyricmode {
    \partial 8
  \tuplet 3/2 8 { `Q-ay-m8 b-iy16 } |
  \tuplet 3/2 8 { `g-ih n-ih-G16 t-ax } `r-ay-t8
  `l-ay-k \tuplet 3/2 8 { `Q-ae16 n-ah k-ah }
  \tuplet 3/2 8 { `r-eh16 n-ih n-ah } \tuplet 3/2 8 { `g-ih-v16 m-iy Q-ah }
  \tuplet 3/2 8 { `m-ih16 n-ah-t Q-ah } \tuplet 3/2 8 { `m-ay-k8 Q-ah16 } |
  \tuplet 3/2 8 { `l-ih16 d-el t-ax } \tuplet 3/2 8 { `l-ay-k8 g-eh-t16 }
  \tuplet 3/2 8 { `r-ih-d16 Q-ah-v D-ah } \tuplet 3/2 8 { `sp-ay-t8 Q-ah16 }
  \tuplet 3/2 8 { `b-ih-t16 Q-ah-v D-ah } \tuplet 3/2 8 { `pr-ay-d8 t-ax16 }
  `f-ay-t8 r8 |
  }
\lyricNotation \songLyricsPlain \songSyls
%{ \pageBreak %}
%{ \lyricNotation \songLyricsMBOG \songSyls %}


\version "2.22.0"
\include "../lyric-notation.ily"

songLyricsB-MB =
  \lyricmode {
    \rhColor a red
    \rhColor b lightblue
    \rhColor c gold
    \rhColor C gold
    \rhColor e yellow
    \rhColor d pink
        \rhColor a gold
        \rhColor A gold
        \rhColor b red
        \rhColor B red
        \rhColor c lightblue
        \rhColor C lightblue
        \rhColor d #(x11-color 'DarkOrange)
        \rhColor D #(x11-color 'DarkOrange)
        \rhColor e #(x11-color 'LightSeaGreen)
        \rhColor E #(x11-color 'LightGreen)
        \rhColor EE #(x11-color 'LightGreen)
        \rhColor f #(x11-color 'YellowGreen)
        \rhColor F #(x11-color 'YellowGreen)
        \rhColor g #(x11-color 'MediumSpringGreen)
        \rhColor G #(x11-color 'MediumSpringGreen)

  Doc\rh a0 is\rh c0 (back)\rh E0 |
    Looks\rh d0 like Bat\rh E0 -- man\rh EE0 brought\rh a0 his\rh c0 own\rh b0 Ro\rh a0 -- bin\rh c0 |
    Oh\rh b0 God,\rh a0 Sad\rh a0 -- dam's\rh e0 got\rh a0 his own\rh b0 La\rh a0 -- den\rh e0 |
    With\rh c0 his\rh C0 own\rh b0 pri -- vate\rh E0 plane, his\rh c0 own\rh b0 pi -- lot |
    Set to\rh d0 blow\rh b0 col -- lege\rh c0 dorm room\rh d0 doors off the hin\rh c0 |
    }
songLyricsC-MB =
  \lyricmode {
    \rhColor a red
    \rhColor b lightblue
    \rhColor c gold
    \rhColor C gold
    \rhColor e yellow
    \rhColor d pink
        \rhColor a gold
        \rhColor A gold
        \rhColor b red
        \rhColor B red
        \rhColor c lightblue
        \rhColor C lightblue
        \rhColor d #(x11-color 'DarkOrange)
        \rhColor D #(x11-color 'DarkOrange)
        \rhColor e #(x11-color 'LightSeaGreen)
        \rhColor E #(x11-color 'LightGreen)
        \rhColor EE #(x11-color 'LightGreen)
        \rhColor f #(x11-color 'YellowGreen)
        \rhColor F #(x11-color 'YellowGreen)
        \rhColor g #(x11-color 'MediumSpringGreen)
        \rhColor G #(x11-color 'MediumSpringGreen)

  off the hin |
    -- ges\rh c0 Or -- an\rh c0 -- ges,\rh C0 peach, pears, plums, syr -- in\rh c0 |
    -- ges\rh C0 (Vrum, vrum!) Yeah, here I come, I'm in\rh c0 |
    -- ches\rh C0 A -- way from you,\rh d0 dear, fear none Hip\rh c0 |
    -- hop is\rh c0 in\rh C0 a state of nine one one, so...\rh b0 |
  }

songSylsGroovesB =
  \lyricmode {
    \partial 4
      \set tupletFullLength = ##t
      \override HorizontalBracket.direction = #UP
      \override HorizontalBracket.edge-height = #'(0.0 . 0.0)
      \override HorizontalBracket.staff-padding = #3.165
      \override HorizontalBracket.thickness = #-0
      \override HorizontalBracketText.font-size = #-2.75
      %{ \override HorizontalBracketText.font-series = #BOLD %}
      \override TupletBracket.shorten-pair = #'(0.0 . 2.0)
      %{ \override HorizontalBracket.shorten-pair = #'(2 . 3.85) %}
      \once \override TupletBracket.connect-to-neighbor = #'(#t . #f)
  \grooveB { \gTre {
      \once \override HorizontalBracket.shorten-pair = #'(2.3 . 0)
  \sG \gB-Doz @d-aa-k8 Q-ih-z16 } \gTre { \eG `b-ae-k
  |
    r16 l-UH-ks16 } } \grooveB { \gTwo { `l-AY-k8 } \gTre { `b-AE-t m-AE-n16 } \gTre {
            \once \override HorizontalBracket.shorten-pair = #'(2 . 0)
    \sG \gC-Four @br-AO-t _16 h-IH-z } \gTwo { \eG `Q-OW-n8 } \gTwo { @r-AA } \gTwo { b-IH-n }
  |
    \gTwo { Q-OW } } \grooveB { \gTre { `g-AA-d s-AA16 } \gTwo { `d-EM-z8 } \gTre {
            \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
    \sG \gA-Two `g-AA-t16 _16 h-IH-z } \gTwo { \eG `Q-OW-n8 } \gTwo { @l-AA } \gTwo { d-EN }
  |
    \gTwo { `w-IH-D16 h-IH-z } } \grooveB { \gTwo { `Q-OW-n8 } \gTre { `pr-AY v-AX-t16 } \gTre {
            \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
    \sG \gC-Four `pl-EY-n _16 h-IH-z } \gTwo { \eG `Q-OW-n8 } \gTwo { @p-AY } \gTwo { l-AX-t }
  |
    \gTwo { `s-EH-t16 t-UW } } \grooveB { \gTwo { `bl-OW8 } \gTre {
            \once \override HorizontalBracket.shorten-pair = #'(0 . 0)
    \sG \gD-Doz `k-AA l-IH-J16 } \gTre { \eG `d-AO-rm _16 r-UW-m } \gTwo { `d-AO-rz8 } } {
            \once \override TupletBracket.connect-to-neighbor = #'(#f . #t)
    \grooveB { \gTre {
            \once \override HorizontalBracket.shorten-pair = #'(1.5 . 0)
            \once \override HorizontalBracketText.font-size = #-3.1
    \sG \gA-Doz `Q-AO-f D-AX16 } \gTwo \eG @h-IH-n } }
  |
  }
songSylsGroovesC =
  \lyricmode {
    \partial 4
    \set tupletFullLength = ##t
    %{ \override TupletBracket.padding = #0.1 %}
    \override HorizontalBracket.direction = #UP
    \override HorizontalBracket.edge-height = #'(0.0 . 0.0)
    \override HorizontalBracket.staff-padding = #3.165
    \override HorizontalBracket.thickness = #-0
    \override HorizontalBracketText.font-size = #-2.75
    \override HorizontalBracketText.font-series = #BOLD
    \override TupletBracket.shorten-pair = #'(0.0 . 2.0)
  %{ \override HorizontalBracket.shorten-pair = #'(2 . 3.85) %}
  \grooveB { \gTre {
            \once \override HorizontalBracket.shorten-pair = #'(1.5 . 0)
            \once \override HorizontalBracketText.font-size = #-3.1
  \sG \gA-Doz `Q-AO-f8 D-AX16 } \gTwo { \eG @h-IH-n
  |
    _16 } \gTre { `J-IH-z8 Q-AO16 } \gTwo { @r-IH-n8 } \gTwo { `J-IH-z } \gTwo { `p-IY-C }
        %{ \once \override TupletBracket.shorten-pair = #'(0.0 . 2.0) %}
    \gTwo { p-EH-rz } } \grooveB { \gTre {
            \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
   \sG \gA-Doz `pl-EM-z s-er16 } \gTwo { \eG @Q-IH-n
  |
    _16 } \gTre { `J-IH-z8 r16 } \gTwo { `vr-EM8 } \gTwo { vr-EM } \gTwo { y-AE } \gTwo { `h-IH-r16 Q-AY } } \grooveB { \gTre {
            \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
   \sG \gA-Doz `k-EM8 Q-AY-m16 } \gTwo { \eG @Q-IH-n
  |
    _16 } \gTre { `C-IH-z8 Q-AX 16 } \gTwo { `w-EY8 } \gTwo { `fr-EM16 y-UW } \gTwo { `d-IH-r8 } \gTwo { \eG `f-IH-r } } \grooveB { \gTre {
            \once \override HorizontalBracket.shorten-pair = #'(1 . 0)
   \sG \gA-Doz `n-EN r16 } \gTwo { \eG `h-IH-p
  |
    _16 } \gTre { `h-AA-p8 Q-IH-z16 } \gTwo { `Q-IH-n Q-AH } \gTwo { `st-EY-t Q-AX-v } \gTwo { `n-AY-n8 } \gTwo { `w-EN } } \gTwo { @w-EN } \gTwo { s-OW }
  |
  }


songLyricsPlain =
  \lyricmode {
    Looks like Bat -- man brought his own Ro -- bin
    Oh God, Sad -- dam's got his own La -- den
    With his own pri -- vate plane, his own pi -- lot
    Set to blow col -- lege dorm room doors off the hin

    -- ges Or -- an -- ges, peach, pears, plums, syr -- in
    -- ges (Vrum, vrum!) Yeah, here I come, I'm in
    -- ches A -- way from you, dear, fear none Hip
    -- hop is in a state of nine one one, so...
    }
songLyricsRainbowB =
  \lyricmode
    {
      \goodColors
      \pinkRainbow

  Doc\rh AA0 is\rh IH0 (back)\rh AE0
  |
  Looks\rh UH0
    like\rh AY0
    Bat\rh AE0
    --
    man\rh Ae0
    brought\rh AO0
    his\rh IH0
    own\rh OW0
    Ro\rh AA0
    --
    bin\rh IH0
  Oh\rh OW0
    God,\rh AA0
    Sad\rh Aa0
    --
    dam's\rh EM0
    got\rh AA0
    his\rh IH0
    own\rh OW0
    La\rh AA0
    --
    den\rh EN0
  With\rh IH0
    his\rh Ih0
    own\rh OW0
    pri\rh AY0
    --
    vate\rh AX0
    plane,\rh EY0
    his\rh IH0
    own\rh OW0
    pi\rh AY0
    --
    lot\rh AX0
  Set\rh EH0
    to\rh UW0
    blow\rh OW0
    col\rh AA0
    --
    lege\rh IH0
    dorm\rh AOR0
    room\rh UW0
    doors\rh AOR0
    off\rh AO0
    the\rh AX0
    hin\rh IH0
     | }
songLyricsRainbowC =
  \lyricmode
    {
      \goodColors
      \pinkRainbow

    off\rh AO0
    the\rh AX0
    hin\rh IH0
      |
  -- ges\rh Ih0
    Or\rh AOR0
    --
    an\rh IH0
    --
    ges,\rh Ih0
    peach,\rh IY0
    pears,\rh EHR0
    plums,\rh EM0
    syr\rh ER0
    --
    in\rh IH0
  -- ges\rh Ih0
    (Vrum,\rh EM0
    vrum!)\rh Em0
    Yeah,\rh AE0
    here\rh IHR0
    I\rh AY0
    come,\rh EM0
    I'm\rh AY0
    in\rh IH0
  -- ches\rh Ih0
    A\rh AX0
    --
    way\rh EY0
    from\rh EM0
    you,\rh UW0
    dear,\rh IHR0
    fear\rh Ihr0
    none\rh EN0
    Hip\rh IH0
  -- hop\rh AA0
    is\rh IH0
    in\rh Ih0
    a\rh AX0
    state\rh EY0
    of\rh AX0
    nine\rh AY0
    one\rh AX0
    one,\rh AH0
    so...\rh OW0 |
  }
songLyricsTwoThreeBak =
  \lyricmode {
    \goodColors \pinkRainbow
  Looks like Bat -- man brought his own Ro -- bin
  Oh God, Sad -- dam's got his own La -- den
  With his own pri -- vate plane, his own pi -- lot
  Set to blow col -- lege dorm room doors off the hin

  -- ges Or -- an -- ges, peach, pears, plums, syr -- in
  -- ges (Vrum, vrum!) Yeah, here I come, I'm in
  -- ches A -- way from you, dear, fear none Hip
  -- hop is in a state of nine one one, so...
  }
songSylsTwoThreeBak =
  \lyricmode {
  r16 l-UH-ks16 `l-AY-k8 `b-AE-t m-AE-n16 @br-AO-t _16 h-IH-z `Q-OW-n8 @r-AA b-IH-n |
  Q-OW `g-AA-d s-AA16 `d-EM-z8 `g-AA-t16 _16 h-IH-z `Q-OW-n8 @l-AA d-EN |
  `w-IH-D16 h-IH-z `Q-OW-n8 `pr-AY v-AX-t16 `pl-EY-n _16 h-IH-z `Q-OW-n8 @p-AY l-AX-t |
  `s-EH-t16 t-UW `bl-OW8 `k-AA l-IH-J16 `d-AO-rm _16 r-UW-m `d-AO-rz8 `Q-AO-f D-AX16 @h-IH-n |

  _16 `J-IH-z8 Q-AO16 @r-IH-n8 `J-IH-z `p-IY-C p-EH-rz `pl-EM-z s-er16 @Q-IH-n |
  _16 `J-IH-z8 r16 `vr-EM8 vr-EM y-AE `h-IH-r16 Q-AY `k-EM8 Q-AY-m16 @Q-IH-n |
  _16 `C-IH-z8 Q-AX 16 `w-EY8 fr-EM16 y-UW `d-IH-r8 `f-IH-r `n-EN r16 `h-IH-p |
  _16 `h-AA-p8 Q-IH-z16 `Q-IH-n Q-AH `st-EY-t Q-AX-v `n-AY-n8 w-AX-n @w-AH-n s-OW |
  }

\lyricNotation \songLyricsRainbowB \songSylsGroovesB
\pageBreak
\lyricNotation \songLyricsRainbowC \songSylsGroovesC
%{ \oneLineLyricNotation \songLyrics \songSyllables %}

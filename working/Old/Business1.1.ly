\version "2.22.0"
\include "../lyric-notation.ily"


gA = "<323_2222>-0"
gA-Two = "<323_2222>-2"
gA-Doz = "<323_2222>-12"
gB-Doz = "<333232>-12"
gB-Alt = "<332_323>-15"
gC-Four = "<332_2222>-4"
gD-Doz = "<332_332>-12"

%{ businessColors = \lyricmode { %}
          \rhColor a gold
          \rhColor A gold
          \rhColor b red
          \rhColor B red
          \rhColor c lightblue
          \rhColor C lightblue
          \rhColor d #(x11-color 'DarkOrange)
          \rhColor D #(x11-color 'DarkOrange)
          \rhColor e #(x11-color 'LightSeaGreen)
          \rhColor E #(x11-color 'LightGreen)
          \rhColor EE #(x11-color 'LightGreen)
          \rhColor f #(x11-color 'YellowGreen)
          \rhColor F #(x11-color 'YellowGreen)
          \rhColor g #(x11-color 'MediumSpringGreen)
          \rhColor G #(x11-color 'MediumSpringGreen)
%{ } %}

songLyricsOh =
  \lyricmode {
    \rhColor a red
    \rhColor b lightblue
    \rhColor c yellow
    \rhColor d green
    You 'bout to wit -- ness hip -- hop\rh a0 in its most\rh b0 pur\rh a0 -- est
    Most\rh b0 raw\rh a0 -- est form,\rh b0 flow\rh b0 al -- most\rh b0 flaw\rh a0 -- less
    Most\rh b0 hard\rh a0 -- est, most\rh b0 ho\rh a0 -- nest known\rh b0 ar\rh a0 -- tist
    Chip off\rh a0 the old\rh b0 block,\rh a0 but old\rh b0 Doc\rh a0 is (back) }

songLyricsRainbow =
  \lyricmode {
  % 17 Official HTML Colors + DodgerBlue and RoyalBlue
      \goodColors
      \pinkRainbow

  You\rh ER0 'bout\rh AW0 to\rh UW0 wit\rh IH0 -- ness\rh EH0 hip\rh IH0 -- hop\rh AA0 in\rh IH0 its\rh IH0 most\rh OW0 pur\rh UH0 -- est\rh IH0
  Most\rh OW0 raw\rh AA0 -- est\rh IH0 form,\rh AA0 flow\rh OW0 al\rh AA0 -- most\rh OW0 flaw\rh AA0 -- less\rh IH0
  Most\rh OW0 hard\rh AA0 -- est,\rh IH0 most\rh OW0 ho\rh AA0 -- nest\rh IH0 known\rh OW0 ar\rh AA0 -- tist\rh IH0
  Chip\rh Ih0 off\rh AO0 the\rh IY0 old\rh OW0 block,\rh AA0 but\rh AX0 old\rh OW0
  Doc\rh AA0 is\rh IH0 (back)\rh AE0 }

songLyricsMB =
  \lyricmode {

  You\rh d0 'bout\rh a0 to\rh d0
  wit\rh c0 -- ness\rh C0 hip\rh c0
  -- hop\rh a0 in\rh c0
  its\rh C0 most\rh b0
  pur\rh d0 -- est\rh c0
  Most\rh b0 raw\rh a0
  -- est\rh c0 form,\rh a0 flow\rh b0
  al\rh a0 -- most\rh b0
  flaw\rh a0 -- less\rh c0
  Most\rh b0 hard\rh a0
  -- est,\rh c0 most\rh b0 ho\rh a0
  -- nest\rh c0 known\rh b0
  ar\rh a0 -- tist\rh c0
  Chip\rh C0 off\rh a0
  the\rh c0 old\rh b0 block,\rh a0
  but old\rh b0
  Doc\rh a0 is\rh c0 (back\rh E0)
  }

songSylsGrooveLabelsA =
  \lyricmode {
    \partial 4
      \set tupletFullLength = ##t
      \override HorizontalBracket.direction = #UP
      \override HorizontalBracket.edge-height = #'(0.0 . 0.0)
      \override HorizontalBracket.staff-padding = #3.165
      \override HorizontalBracket.thickness = #-0
      \override HorizontalBracketText.font-size = #-2.75
      %{ \override HorizontalBracketText.font-series = #BOLD %}
        %{ \override TupletBracket.padding = #0.1 %}
        %{ \override HorizontalBracket.shorten-pair = #'(2 . 3.85) %}
        %{ \override HorizontalBracketText.font-shape = #italic %}
        %{ \override HorizontalBracket.bracket-visibility = ##f %}
        %{ \override HorizontalBracket.bracket-flare = #'(0.0 . 0.0) %}
  r16 y-uw16 \gTwo { `b-aw-t  t-uw }
  |
  \grooveB { \gTre { @w-ih-t8 n-ih-s16 } \gTwo { \sG \gA `h-ih-p16
  _16 } \gTre { `h-aa-p8 Q-ih-n16 }
  \gTwo { `Q-ih-ts8 } \gTwo { \eG `m-ow-st }
  \gTwo { @py-uh-r } \gTwo { r-ih-st } }
  |
  \gTwo { `m-ow-st } \grooveB { \gTre { @r-ao
  Q-ih-st16 } \gTwo { `f-ao-rm8 } \gTre { \sG \gA-Two `fl-ow16
  _16 Q-aa-l } \gTwo { \eG `m-ow-st8 }
  \gTwo { @fl-ao } \gTwo { l-ih-s }
  |
  \gTwo { `m-ow-st } } \grooveB { \gTre { @h-aa-r
  d-ih-st16 } \gTwo { `m-ow-st8 } \gTre { \sG \gA-Two @Q-aa16
  _16 n-ih-st } \gTwo { \eG `n-ow-n8 }
  \gTwo { @Q-aa-r } \gTwo { \eG t-ih-st }
  |
  \gTwo { `C-ih-p } } \grooveB { \gTre { @Q-ao-f
  D-iy16 } \gTwo { `Q-ow-ld8 } \gTre { \sG \gB-Doz @bl-aa-k16
  _16 b-ah-t } \gTwo { \eG `Q-ow-ld8 }
  \gTre { @d-aa-k8 Q-ih-z16 } } `b-ae-k | }


songSylsMB =
  \lyricmode {
    \partial 4
  r16 y-er16 `b-aw-t t-uw |
  @w-ih-t8 n-eh-s16 `h-ih-p16 _16 h-aa-p8 Q-ih-n16 Q-ih-ts8 \sb "𝛂*" `m-ow-st @py-uh-r r-ih-st |
  \sb "𝛂" `m-ow-st @r-ao Q-ih-st16 `f-ao-rm8 `fl-ow16 _16 Q-aa \sb "𝛂" `m-ow-st8 @fl-ao l-ih-s |
  \sb "𝛂" `m-ow-st @h-aa-r d-ih-st16 \sb "𝛂" `m-ow-st8 @Q-aa16 _16 n-ih-st \sb "𝛂" `n-ow-n8 @Q-aa-r t-ih-st |
  `C-ih-p @Q-ao-f D-iy16 `Q-ow-ld8 @bl-aa-k16 _16 b-ah-t \sb "𝛂" `Q-ow-ld8 @d-aa-k Q-ih-z16 `b-ae-k
  | }
songSylsPedal =
  \lyricmode {
    \set Staff.pedalSustainStyle = #'bracket
    %{ \set PianoPedalBracket.to-barline = #t %}
    %{ \once \override PianoPedalBracket.shorten-pair = #'(0 . 2) %}
    %{ \set SustainPedalLineSpanner.to-barline = #t %}
    \partial 4
  r16 y-uw16 `b-aw-t\sustainOn t-uw\sustainOff |
  @w-ih-t8\sustainOn n-ih-s16 `h-ih-p16\sustainOff\sustainOn
  _16 h-aa-p8\sustainOff\sustainOn Q-ih-n16
  Q-ih-ts8\sustainOff\sustainOn `m-ow-st\sustainOff\sustainOn
  @py-uh-r\sustainOff\sustainOn r-ih-st\sustainOff
  |
  `m-ow-st\sustainOff\sustainOn @r-ao\sustainOff\sustainOn
  Q-ih-st16 `f-ao-rm8\sustainOff\sustainOn `fl-ow16\sustainOff\sustainOn
  _16 Q-aa-l `m-ow-st8\sustainOff\sustainOn
  @fl-ao\sustainOff\sustainOn l-ih-s\sustainOff\sustainOn
  |
  `m-ow-st\sustainOff\sustainOn @h-aa-r\sustainOff\sustainOn
  d-ih-st16 `m-ow-st8\sustainOff\sustainOn @Q-aa16\sustainOff\sustainOn
  _16 n-ih-st `n-ow-n8\sustainOff\sustainOn
  @Q-aa-r\sustainOff\sustainOn t-ih-st\sustainOff\sustainOn
  |
  `C-ih-p\sustainOff\sustainOn @Q-ao-f\sustainOff\sustainOn
  D-iy16 `Q-ow-ld8\sustainOff\sustainOn @bl-aa-k16\sustainOff\sustainOn
  _16 b-ah-t `Q-ow-ld8\sustainOff\sustainOn
  @d-aa-k\sustainOff\sustainOn Q-ih-z16 `b-ae-k\sustainOff
  | }
songSylsGrooveLabelsBak =
  \lyricmode {
    \partial 4
    \set tupletFullLength = ##t
    \override HorizontalBracket.direction = #UP
    \override HorizontalBracket.edge-height = #'(0.0 . 0.0)
    \override HorizontalBracket.staff-padding = #3.165
    %{ \override HorizontalBracket.shorten-pair = #'(2 . 3.85) %}
    \override HorizontalBracket.thickness = #-0
    \override HorizontalBracketText.font-size = #-2.5
    %{ \override HorizontalBracketText.font-series = #BOLD %}
    %{ \override HorizontalBracket.bracket-visibility = ##f %}
    %{ \override HorizontalBracket.bracket-flare = #'(0.0 . 0.0) %}
    r16 y-uw16 \gTwo { `b-aw-t  t-uw }  |
    \grooveB { \gTre { @w-ih-t8 n-ih-s16 } \gTwo { \sG \gA `h-ih-p16
    _16 } \gTre { `h-aa-p8 Q-ih-n16 }
    \gTwo { `Q-ih-ts8 } \gTwo { \eG `m-ow-st }
    \gTwo { @py-uh-r } \gTwo { r-ih-st } } |
    \gTwo { `m-ow-st } \grooveB { \gTre { @r-ao
    Q-ih-st16 } \gTwo { `f-ao-rm8 } \gTre { \sG \gA-Two `fl-ow16
    _16 Q-aa-l } \gTwo { \eG `m-ow-st8 }
    \gTwo { @fl-ao } \gTwo { l-ih-s } |
    \gTwo { `m-ow-st } } \grooveB { \gTre { @h-aa-r
    d-ih-st16 } \gTwo { `m-ow-st8 } \gTre { \sG \gA-Two @Q-aa16
    _16 n-ih-st } \gTwo { \eG `n-ow-n8 }
    \gTwo { @Q-aa-r } \gTwo { \eG t-ih-st } |
    \gTwo { `C-ih-p } } \grooveB { \gTre { @Q-ao-f
    D-iy16 } \gTwo { `Q-ow-ld8 } \gTre { \sG \gB-Doz @bl-aa-k16
    _16 b-ah-t } \gTwo { \eG `Q-ow-ld8 }
    \gTre { @d-aa-k Q-ih-z16 } }
    `b-ae-k
  | }
songLyricsPlain =
  \lyricmode {

  You 'bout to wit -- ness hip -- hop in its most pur -- est
  Most raw -- est form, flow al -- most flaw -- less
  Most hard -- est, most ho -- nest known ar -- tist
  Chip off the old block, but old Doc is (back) }
songSylsPlain =
  \lyricmode {
    \partial 4
  r16 y-uw16 `b-aw-t t-uw |
  @w-ih-t8 n-ih-s16 `h-ih-p16
  _16 h-aa-p8 Q-ih-n16
  Q-ih-ts8 `m-ow-st
  @py-uh-r r-ih-st |
  `m-ow-st @r-ao
  Q-ih-st16 `f-ao-rm8 `fl-ow16
  _16 Q-aa-l `m-ow-st8
  @fl-ao l-ih-s |
  `m-ow-st @h-aa-r
  d-ih-st16 `m-ow-st8 @Q-aa16
  _16 n-ih-st `n-ow-n8
  @Q-aa-r t-ih-st |
  `C-ih-p @Q-ao-f
  D-iy16 `Q-ow-ld8 @bl-aa-k16
  _16 b-ah-t `Q-ow-ld8
  @d-aa-k Q-ih-z16 `b-ae-k
  | }
songSylsPlainBak =
  \lyricmode {
    \partial 4
  r16 y-uw16 `b-aw-t t-uw |
  @w-ih-t8 n-ih-s16 `h-ih-p16
  _16 h-aa-p8 Q-ih-n16
  Q-ih-ts8 `m-ow-st
  @py-uh-r r-ih-st |
  `m-ow-st @r-ao
  Q-ih-st16 `f-ao-rm8 `fl-ow16
  _16 Q-aa-l `m-ow-st8
  @fl-ao l-ih-s |
  `m-ow-st @h-aa-r
  d-ih-st16 `m-ow-st8 @Q-aa16
  _16 n-ih-st `n-ow-n8
  @Q-aa-r t-ih-st |
  `C-ih-p @Q-ao-f
  D-iy16 `Q-ow-ld8 @bl-aa-k16
  _16 b-ah-t `Q-ow-ld8
  @d-aa-k Q-ih-z16 `b-ae-k
  | }

\lyricNotation \songLyricsRainbow \songSylsGrooveLabelsA
  \pageBreak
\lyricNotation \songLyricsMB \songSylsGrooveLabelsA

%{ % OUTPUT PLAIN
\lyricNotation \songLyricsRainbow \songSylsLabeled
  \pageBreak
  \oneLineLyricNotation \songLyricsRainbow \songSylsLabeled
  \pageBreak %}

% OUTPUT OHRINER

  %{ \oneLineLyricNotation \songLyricsOh \songSylsPlain %}
  %{ \pageBreak %}

% OUTPUT MB
  %{ \pageBreak %}
  %{ \oneLineLyricNotation \songLyricsMB \songSylsMB %}
  %{ \pageBreak %}

\version "2.22.0"
\include "../lyric-notation.ily"
songLyrics =
    \lyricmode {
    \goodColors
    \rgbVowelRainbow
    But now, I learned to earn 'cause I’m right -- eous
    I feel great, so may -- be, I might just
    Search for a Nine to Five, if I strive
    Then may -- be I’ll stay a -- live
    So I walk up the street, whis -- tl -- in' this
    Fee -- ling out of place 'cause, man, do I miss...
    }
songSyllables =
    \lyricmode {
        \bracketSettings
        \bracketTextBeforeBreak
        \genGrooveTupSettings
        \grooveTupletTextBeforeBreak
        \partial 8

        b-ah-t8 |
        `n-aw16 Q-ay8 `l-er-nd16 _16 t-uw `Q-er-n8 k-uh-z16 `z-ay-m8 `r-ay16 _16 C-ah-s8. |
        r16 Q-ay16 `f-iy-l8 `gr-ey-t8. s-ow16 `m-ey b-iy Q-ay8 `m-ay-t J-ah-st |
        `s-er-C f-ao-r16 Q-ah `n-ay-n8 t-uw16 `f-ay-v _8 r8 Q-ih-f16 `Q-ay8 `str-ay-v16 |
        _8. D-eh-n16 `m-ey b-iy `y-ay-l8 `st-ey16 Q-ah8 `l-ay-v16 _16 s-ow `w-ay8 |
        `w-ao-k Q-ah-p16 D-ah `str-iy-t8 r8 `w-ih16 s-el `Q-ih-n8 `D-ih-s8 r8 |
        `f-iy16 l-ih-G `Q-aw-t Q-ah-v `pl-ey-s8 k-uh-z16 `m-ae-n _16 d-uw Q-ay8 `m-ih-s8. r16 | }


songLyrics =
    \lyricmode {
    \goodColors
    \rgbVowelRainbow
    But now, I learned to earn 'cause I’m right -- eous
I feel great, so may -- be, I might just
Search for a Nine to Five, if I strive
Then may -- be I’ll stay a -- live
So I walk up the street, whis -- tl -- in' this
Fee -- ling out of place 'cause, man, do I miss...
}

songSyllables =
    \lyricmode {
        %{ \bracketSettings %}
        %{ \bracketTextBeforeBreak %}
        %{ \genGrooveTupSettings %}
        %{ \grooveTupletTextBeforeBreak %}
        \partial 8

b-ah-t8 |
`n-aw16 Q-ay8 `l-er-nd16 _16 t-uw `Q-er-n8 k-uh-z16 `z-ay-m8 `r-ay16 _16 C-ah-s8. |
r16 Q-ay16 `f-iy-l8 `gr-ey-t8. s-ow16 `m-ey b-iy Q-ay8 `m-ay-t J-ah-st |
`s-er-C f-ao-r16 Q-ah `n-ay-n8 t-uw16 `f-ay-v _8 r8 Q-ih-f16 `Q-ay8 `str-ay-v16 |
_8. D-eh-n16 `m-ey b-iy `y-ay-l8 `st-ey16 Q-ah8 `l-ay-v16 _16 s-ow `w-ay8 |
`w-ao-k Q-ah-p16 D-ah `str-iy-t8 r8 `w-ih16 s-el `Q-ih-n8 `D-ih-s8 r8 |
`f-iy16 l-ih-G `Q-aw-t Q-ah-v `pl-ey-s8 k-uh-z16 `m-ae-n _16 d-uw Q-ay8 `m-ih-s8. r16 | }
    \lyricNotation \songLyrics \songSyllables
        \pageBreak
    \oneLineLyricNotation \songLyrics \songSyllables

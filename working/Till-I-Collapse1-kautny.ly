\version "2.22.0"
\include "../lyric-notation.ily"

songLyricsPlain =
\lyricmode {
Till I col -- lapse I'm spil -- lin' these raps long as you feel 'em
Till the day that I drop you'll ne -- ver say that I'm not kil -- lin' 'em
‘Cause when I am not, then I'm -- a stop pen -- nin' 'em
And I am not hip -- hop then I'm just not E -- mi -- nem
Sub -- li -- mi -- nal thoughts, when I'm -- a stop send -- in' 'em?
Wo -- men are caught in webs, spin 'em and hock ve -- nom
Ad -- re -- na -- line shots of pe -- ni -- cil -- lin could not get the il -- lin' to stop
A -- mo -- xi -- cil -- lin's just not real e -- nough
The cri -- min -- al, cop kil -- lin', hip -- hop vil -- lain
A mi -- ni -- mal swap to cop mil -- lions of Pac list -- en -- ers
You're co -- min' with me, feel it or not
You're gon -- na fear it like I showed ya the spi -- rit of God lives in us
You hear it a lot, ly -- rics to shock
Is it a mi -- ra -- cle or am I just pro -- duct of pop fiz -- zin' up?
Fo' shiz -- zle, my wiz -- zle, this is the plot, list -- en up
You biz -- zles for -- got, Sliz -- zle does not give a fuck!
}

songLyrics =
\lyricmode {
  \rhColor a blue
  \rhColor A lightblue
  \rhColor b green
  \rhColor c yellow
  \rhColor d green
Till\rh a1 I col -- lapse I'm spil\rh a1 -- lin'\rh A2 these raps long as you feel\rh a1 'em\rh b3
Till\rh a1 the day that I drop you'll ne -- ver say that I'm not kil\rh a1 -- lin'\rh A2 'em\rh b3
‘Cause when I am not, then I'm -- a stop pen\rh a1 -- nin'\rh A2 'em\rh b3
And I am not hip -- hop then I'm just not E\rh b3 -- mi\rh A2 -- nem\rh b3
}

songSyllables =
\lyricmode {
  \partial 4
r16 t-ih-l `Q-ay k-ao |
@l-ae-ps8 Q-ay-m16 `sp-ih l-ih-n D-iy-z @r-ae-ps8 `l-ao-G16 Q-ae-z y-uw @f-iy-l Q-ah-m `t-ih-l D-ah @d-ey |
D-ae-t Q-ay @dr-aa-p8 y-uh-L16 `n-eh v-er `s-ey D-ae-t Q-ay-m @n-aa-t8 `k-ih16 l-ih-n Q-eh-m8 |
r16 k-ah-z16 `w-eh-n Q-ay Q-ae-m @n-aa-t8 D-eh-n16 `Q-ay m-ah @st-aa-p8 `p-ih16 n-ih-n Q-eh-m Q-ah-nd |
`Q-ay Q-ae-m @n-aa-t8 h-ih-p16 `h-aa-p8 D-eh-n16 `Q-ay-m J-ah-st @n-aa-t8 `Q-eh16 m-iy n-eh-m8 |

r16 s-ah16 `^bl-ih m-ih n-ah-l @T-ao-ts8 w-eh-n16 `Q-ay m-ah @st-aa-p8 `s-eh-n16 d-ih-n Q-eh-m `w-ih |
m-ah-n Q-aa-r @k-aa-t8 Q-ih-n16 `w-eh-bz8 `sp-ih-n16 Q-eh-m Q-ah-nd @x-aa-k8 `v-eh16 n-ah-m Q-ah `dr-eh |
n-ah l-ah-n @S-aa-ts8 Q-ah-v16 `p-eh n-ah `s-ih l-ah-n k-uh-d @n-aa-t8 `g-eh-t16 D-ah `Q-ih l-ih-n |
t-uw @st-aa-p8 Q-ah16 `m-aa-k8 s-ah16 `s-ih l-ih-nz J-ah-st @n-aa-t8 `r-iy-l16 Q-ih n-ah-f8 |

r16 D-ah `kr-ih m-ah n-el `k-aa-p8 `k-ih16 l-ih-n h-ih-p `h-aa-p8 `v-ih16 l-ah-n Q-ah `m-ih |
n-ah m-ah-l `sw-aa-p8 t-uw16 `k-aa-p8 `m-ih-l16 y-ah-nz Q-ah-v @p-aa-k8 `l-ih16 s-ah n-er-z8 |
r16 y-uh-r `k-ah m-ih-n w-ih-D @m-iy8 `f-iy-l16 Q-ih-t Q-ao-r @n-aa-t8 y-uh-r16 `g-aa n-ah `f-ih-r |
Q-ih-t l-ay-k @Q-ay8 `S-ow-d16 y-aa D-ah `sp-ih r-ah-t Q-ah-v @g-aa-d8 `l-ih-v16 z-ih n-ah-s8 |

r16 y-uw16 `h-iy-r Q-ih-t Q-ah `l-aa-t8 `l-ih16 r-ih-ks t-uw `S-aa-k8 `Q-ih-z16 Q-ih-t Q-ah `m-ih |
r-ah k-el `Q-ao-r Q-ae-m Q-ay `J-ah-st8 `pr-aa16 d-ah-kt Q-ah-v @p-aa-p8 `f-ih16 z-ih-n Q-ah-p8 |
r16 f-ah @S-ih z-el m-ay `wh-ih z-el `D-ih-s Q-ih-z D-ah `pl-aa-t8 `l-ih16 s-ah-n Q-ah-p8 |
r16 y-uw `b-ih z-el-z f-er @g-aa-t8 `sl-ih16 z-el d-ah-z @n-aa-t8 `g-ih-v16 Q-ah @f-ah-k8 | }

\lyricNotation \songLyrics \songSyllables

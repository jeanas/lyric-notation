\version "2.22.0"
\include "../lyric-notation.ily"

% Color DEFINITIONS
rollOutColors = {
  % Repeated sections, 1,3,5=yellow 2,4,6=pink
  \rhColor oPr #(x11-color 'Moccasin)
  \rhColor oPrr #(x11-color 'LemonChiffon)
  \rhColor ePw #(x11-color 'Pink)
  \rhColor ePww #(x11-color 'Pink)
  \rhColor Ah #(x11-color 'SkyBlue)
  \rhColor Ax #(x11-color 'PowderBlue)
  \rhColor Em #(x11-color 'PaleTurquoise)
  \rhColor En #(x11-color 'PaleTurquoise)
  \rhColor Er #(x11-color 'LightSteelBlue)

  % m.375 anticipation rhymes
  % odd=deepink
  \rhColor oPA #(x11-color 'HotPink)
  \rhColor oPa #(x11-color 'PeachPuff)
  \rhColor oPaa #(x11-color 'PeachPuff2)
  \rhColor oPLa #(x11-color 'Thistle)
  \rhColor oPLA #(x11-color 'MediumSlateBlue)

  \rhColor ePLA #(x11-color 'DarkOrange2)
  \rhColor ePLa #(x11-color 'DarkOrange1)
  \rhColor ePLAA #(x11-color 'DarkOrange3)
  \rhColor ePWA #(x11-color 'WhiteSmoke)
  \rhColor eLA #(x11-color 'WhiteSmoke)
  \rhColor eLAA #(x11-color 'WhiteSmoke)
  \rhColor ePa #(x11-color 'WhiteSmoke)

  \rhColor oPB #(x11-color 'PaleVioletRed)
  \rhColor oPLB #(x11-color 'DarkViolet)
  \rhColor oPLBB #(x11-color 'DarkViolet)
  \rhColor oPLb #(x11-color 'WhiteSmoke)
  \rhColor oLB #(x11-color 'SteelBlue)
  \rhColor oLBB #(x11-color 'WhiteSmoke)
  \rhColor oLBBB #(x11-color 'WhiteSmoke)

  \rhColor ePB #(x11-color 'Orange)
  \rhColor ePWB #(x11-color 'WhiteSmoke)
  \rhColor ePb #(x11-color 'WhiteSmoke)
  \rhColor eLb #(x11-color 'WhiteSmoke)
  \rhColor eLB #(x11-color 'WhiteSmoke)
  \rhColor eLBB #(x11-color 'WhiteSmoke)

  \rhColor oPC #(x11-color 'HotPink)
  \rhColor oPLc #(x11-color 'WhiteSmoke)
  \rhColor oPc #(x11-color 'WhiteSmoke)
  \rhColor oPLC #(x11-color 'WhiteSmoke)
  \rhColor oPLCC #(x11-color 'WhiteSmoke)
  \rhColor oLc #(x11-color 'WhiteSmoke)

  \rhColor ePLC #(x11-color 'DarkOrange)
  \rhColor ePWC #(x11-color 'WhiteSmoke)
  \rhColor ePWCC #(x11-color 'WhiteSmoke)
  \rhColor ePLC #(x11-color 'WhiteSmoke)

}

rollOutColorsLuda = {

  \rhColor La #(x11-color 'Red)
  \rhColor Laa #(x11-color 'Red)
  \rhColor LA #(x11-color 'Red)
  \rhColor LAA #(x11-color 'Red)
    \rhColor B #(x11-color 'DarkOrange2)
    \rhColor C #(x11-color 'Gold2)
    \rhColor D #(x11-color 'YellowGreen)
    \rhColor E #(x11-color 'Green)
    \rhColor F #(x11-color 'LimeGreen)
    \rhColor FB #(x11-color 'FireBrick)
    \rhColor G #(x11-color 'Cyan)
    \rhColor H #(x11-color 'Cyan3)
    \rhColor I #(x11-color 'LightSkyBlue)
    \rhColor IR #(x11-color 'IndianRed)
    \rhColor J #(x11-color 'DodgerBlue)
    \rhColor K #(x11-color 'Blue)
    \rhColor L #(x11-color 'Purple)
    \rhColor M #(x11-color 'Moccasin)
    \rhColor N #(x11-color 'Maroon)
    \rhColor O #(x11-color 'Orange)
    \rhColor P #(x11-color 'DeepPink2)
    \rhColor GY #(x11-color 'GreenYellow)
    \rhColor Y #(x11-color 'Yellow)
    \rhColor DOG #(x11-color 'DarkOliveGreen)
    \rhColor OD #(x11-color 'OliveDrab)
    %{ \rhColor OG #(x11-color 'OliveGreen) %}
    \rhColor OR #(x11-color 'OrangeRed)
    \rhColor FG #(x11-color 'ForestGreen)
    \rhColor MSG #(x11-color 'MediumSeaGreen)
    \rhColor LSG #(x11-color 'LightSeaGreen)
    \rhColor LSGLSG #(x11-color 'LightSeaGreen)
    \rhColor PT #(x11-color 'PaleTurquoise)
    \rhColor T #(x11-color 'Turquoise)
    \rhColor MT #(x11-color 'MediumTurquoise)
    \rhColor DT #(x11-color 'DarkTurquoise)
    \rhColor LS #(x11-color 'LightSalmon)
    \rhColor SAL #(x11-color 'LightSalmon)
    \rhColor SG #(x11-color 'SeaGreen)
    \rhColor SB #(x11-color 'SlateBlue)
    \rhColor MSB #(x11-color 'MediumSlateBlue)
    \rhColor CB #(x11-color 'CadetBlue)
    \rhColor CBCB #(x11-color 'CadetBlue)
    \rhColor LG #(x11-color 'LightGreen)
  \rhColor AA #(x11-color 'Red)
    \rhColor BB #(x11-color 'DarkOrange)
    \rhColor CC #(x11-color 'Gold2)
    \rhColor DD #(x11-color 'YellowGreen)
    \rhColor EE #(x11-color 'Green)
    \rhColor FF #(x11-color 'LimeGreen)
    \rhColor GG #(x11-color 'Cyan)
    \rhColor HH #(x11-color 'DarkCyan)
    \rhColor II #(x11-color 'LightSkyBlue)
    \rhColor JJ #(x11-color 'DodgerBlue)
    \rhColor KK #(x11-color 'Blue)
    \rhColor LL #(x11-color 'Purple)
    \rhColor MM #(x11-color 'Magenta)
    \rhColor NN #(x11-color 'Maroon)
    \rhColor OO #(x11-color 'DarkOrange2)
    \rhColor PP #(x11-color 'DeepPink2)
}

qOneLyrics =
  \lyricmode
  {
  \rollOutColors
  % 1
  Now where'd\rh oPr0 you\rh oPr0 |
    get\rh oPr0 that\rh oPr0 plat’\rh oPa0 -- num\rh oPa0
    chain\rh oPaa0 with\rh oPr0 them\rh oPr0
    dia\rh oPLa0 -- monds\rh oPLa0 in\rh oPA0 it?\rh oPA0
  % 2
  Where'd\rh oPr0 you\rh oPr0 |
    get\rh oPr0 that\rh oPr0 mat\rh oPa0 -- chin’\rh oPa0
    Benz\rh oPaa0 with\rh oPr0 them\rh oPr0
    win -- dows tin\rh oPA0 -- ted?\rh oPA0
  % 3
  Who\rh oPLA0 them |
    girls you\rh oPLA0 be
    wit’\rh oPaa0 when you\rh oPLA0 be
    ri\rh oPLa0 -- din’\rh oPLa0 through?\rh oPLA0
  %{ Who them | %}
    %{ girls you\rh oPr0 be\rh oPr0 %}
    %{ wit’\rh oPaa0 when you\rh oPr0 be\rh oPr0 %}
    %{ ri\rh oPLa0 -- din’\rh oPLa0 through?\rh oPLA0 %}
  % 4
  \shrinkLyrics 2
  Man\rh oPLa0 I\rh oPLa0 ain't\rh oPLa0 got\rh oPLa0 |
      no\rh oPLa0 -- thin’\rh oPLa0 to\rh Ax0 prove,\rh oPLA0 I\rh oPLa0 paid\rh oPLa0
      my\rh Ax0 dues,\rh oPLA0 brea\rh oPLa0 -- kin’\rh oPLa0 the\rh Ax0 rules,\rh oPLA0
      I\rh oPLa0 shake\rh oPLa0 fools\rh oPLA0 while\rh oPLa0 I'm\rh oPLa0
      ta\rh oPLa0 -- kin’\rh oPLa0 a\rh Ax0 cruise!\rh oPLA0 |
      \shrinkLyrics 0
    }

qTwoLyrics =
  \lyricmode
  {
  \rollOutColors
  % 1
  Tell me
    who's\rh ePa0 your\rh ePa0 weed man, and how do you smoke so good?\rh ePLA0
  % 2
  You's\rh ePa0 a\rh ePa0
    su\rh ePa0 -- per\rh ePa0 -- star, boy, why you still up in the hood?\rh ePLA0
  % 3
  What\rh ePw0 in\rh ePw0 the\rh ePw0
    world\rh ePw0 is\rh ePw0 in\rh ePw0 that\rh ePw0 bag?\rh ePWA0 What\rh ePw0 you\rh ePw0 got\rh ePw0 in\rh ePw0 that\rh ePw0 bag?\rh ePWA0
  % 4
  \shrinkLyrics 2
  A\rh Ax0 cou\rh Ah0 -- ple\rh ePLA0 of\rh Ax0 cans\rh ePWA0
    of\rh Ax0 whoop\rh ePLA0 ass,\rh ePWA0 you\rh ePLAA0 did a\rh Ax0 good\rh ePLA0 ass\rh ePWA0 job\rh ePLa0 of\rh Ax0 just\rh Ah0 eye\rh eLA0 -- in'\rh eLA0 me,\rh eLA0 spy\rh eLAA0 -- in'\rh eLAA0 me\rh eLAA0 |
    \shrinkLyrics 0
  }

qThreeLyrics =
  \lyricmode
  {
  \rollOutColors
  % 1
  Man,\rh oPLb0 that\rh oPLb0 car don't
    come out un -- til next year, where\rh oPr0 in\rh oPr0 the\rh oPr0 fuck\rh oPr0 did\rh oPr0 you\rh oPr0 get\rh oPB0 it?\rh oPB0
  % 2
  That's\rh oPLb0 eigh -- ty
    thou -- sand bucks gone, where\rh oPr0 in\rh oPr0 the\rh oPr0 fuck\rh oPr0 did\rh oPr0 you\rh oPr0 spend\rh oPB0 it?\rh oPB0
  % 3
  You must have\rh oPLb0
    eyes on your back,\rh oPLb0 ‘cause you got mo -- ney to the cei\rh oPLB0 -- lin’\rh oPLB0
    \shrinkLyrics 2
  % 4
  And\rh En0 the\rh Ax0 big\rh oLB0 -- ger\rh oLB0 the\rh Ax0 cap,\rh oPLb0
    \shrinkLyrics 4
    the\rh Ax0 big\rh oLB0 -- ger\rh oLB0 the\rh Ax0 pee\rh oPLB0 -- ling,\rh oPLB0 the\rh Ax0 bet\rh oLBB0 -- ter\rh oLBB0 I'm\rh Em0 fee\rh oPLB0 -- ling,\rh oPLB0 the\rh Ax0 more\rh oLBBB0 that\rh oPLb0I  I'm\rh Em0 chil\rh oPLB0 -- ling,\rh oPLB0 wil\rh oPLBB0 -- ling,\rh oPLBB0 dril\rh oPLB0 -- ling\rh oPLB0 and\rh En0 kil\rh oPLB0 -- ling\rh oPLB0 the\rh Ax0 fee\rh oPLB0 -- ling\rh oPLB0
    \shrinkLyrics 0
  }

qFourLyrics =
  \lyricmode
  {
  \rollOutColors
  % 1
  Now who's that
    buc -- ked na -- ked cook fi\rh ePb0 -- xin’\rh ePb0 three course meals?\rh ePB0
  % 2
  Get -- ting
      \shrinkLyrics 1
    goose -- bumps when her bo -- dy taps the six\rh ePb0 inch\rh ePb0 heels\rh ePB0
        \shrinkLyrics 0
  % 3
  What\rh ePw0 in\rh ePw0 the\rh ePw0
    world\rh ePw0 is\rh ePw0 in\rh ePw0 that\rh ePw0 room,\rh ePWB0 what\rh ePw0 you\rh ePw0 got\rh ePw0 in\rh ePw0 that\rh ePw0 room?\rh ePWB0
  % 4
  \shrinkLyrics 3
  A\rh Ax0 cou\rh eLb0 -- ple\rh eLb0 o'\rh eLb0 gats,\rh eLB0
    a\rh Ax0 cou\rh eLb0 -- ple\rh eLb0 o'\rh eLb0 knives,\rh eLBB0 a\rh Ax0 cou\rh eLb0 -- ple\rh eLb0 o'\rh eLb0 rats,\rh eLB0 a\rh Ax0 cou\rh eLb0 -- ple\rh eLb0 o'\rh eLb0 wives\rh eLBB0 now\rh eLb0 it's time\rh eLBB0 to\rh ePWB0 choose!\rh ePWB0
    \shrinkLyrics 0
  }

qFiveLyrics =
  \lyricmode
  {
  \rollOutColors
  % 1
  Are you cus\rh oPr0 -- tom\rh oPr0
    made,\rh oPLc0 cus\rh oPr0 -- tom\rh oPr0 paid,\rh oPLc0 or you just cus\rh oPr0 -- tom\rh oPr0 fit\rh oPC0 -- ted?\rh oPC0
  % 2
  Play\rh oPLc0 -- sta\rh oPLc0 -- tion
    "2" up in the ride and is\rh oPc0 that\rh oPc0 Lor\rh oPc0 -- ren -- zo kit\rh oPC0 -- ted?\rh oPC0
  % 3
  Is\rh oPc0 that\rh oPc0 your\rh oPc0
    wife, your\rh oPc0 girl -- friend or\rh oPc0 just your\rh oPc0 main\rh oPLc0 bitch?\rh oPLC0
    \shrinkLyrics 2
  % 4
  You\rh oLc0 take\rh oPLc0 a\rh Ax0 pic,\rh oPLC0
    while\rh oPLC0 I'm\rh oPLC0 rub\rh Ah0 -- bing\rh oPLC0 the\rh Ax0 hips,\rh oPLC0 touch\rh Ah0 the\rh Ax0 lips\rh oPLC0 to\rh oLc0 the\rh Ax0 top of\rh Ax0 the\rh Ax0 dick\rh oPLC0 and\rh En0 then whew!\rh oLc0
    \shrinkLyrics 0
  }

qSixLyrics =
  \lyricmode
  {
  \rollOutColors
  % 1
  And tell me
    who's your house -- kee -- per and what\rh oPr0 you\rh oPr0 keep\rh oPr0 in\rh oPr0 your\rh oPr0 house?\rh ePLC0
  % 2
  What a -- bout
    dia -- monds and gold, is that what\rh oPr0 you\rh oPr0 keep\rh oPr0 in\rh oPr0 your\rh oPr0 mouth?\rh ePLC0
  % 3
  What\rh ePw0 in\rh ePw0 the\rh ePw0
    world\rh ePw0 is\rh ePw0 in\rh ePw0 that\rh ePw0 case,\rh ePWC0 what\rh ePw0 you\rh ePw0 got\rh ePw0 in\rh ePw0 that\rh ePw0 case?\rh ePWC0
      \shrinkLyrics 2
  % 4
  Get up out\rh ePLC0 my face,\rh ePWC0
    you coul -- dn't re -- late\rh ePWC0, Wait\rh ePWCC0 to take\rh ePWC0 place\rh ePWCC0 at a si -- mi -- lar pace\rh ePWC0 So shake,\rh ePWC0 shake\rh ePWCC0 it!
    \shrinkLyrics 0
  }

%Outro:
  %{ Get out my busi -- ness, my busi -- ness
  Stay the fuck up out my busi -- ness, ah
  'Cause these nig -- gas all up in my shit and it's my busi -- ness, my busi -- ness
  Stay the fuck up out my busi -- ness, 'cause it's mine, oh mine
  My busi -- ness, my busi -- ness
  Stay the fuck up out my busi -- ness
  'Cause these nig -- gas all up in my shit
  And it's my busi -- ness, my busi -- ness
  Stay the fuck up out my busi -- ness, 'cause it's mine, oh mine } %}

qOneSyls =
  \lyricmode
  {
  \partial 4
  % 1
  r16 n-aw16 `w-ey-rd y-uw |
  <>@g-ih-t16 D-ae-t `pl-ae-t <>n-eh-m
  @C-ey-n8 `w-ih-T16 D-eh-m
  <>@d-aa8 m-EN-dz16 @Q-ih-n32 Q-ih-t '

  % 2
  r8 `w-ey-rd16 y-uw |
  <>@g-ih-t16 D-ae-t `m-ae-t C-ih-n
  <>@b-ih-nz8 w-ih-T16 D-eh-m
  @w-ih-n16. d-ow-z @t-ih-n32 <>t-ih-d '

  % 3
  r8 `h-uw16 D-eh-m |
  <>@g-el-z8 `y-uw16 b-iy
  @w-ih-t16. w-eh-n32 `y-uw16 b-iy
  <>@r-aa8 d-ih-n16 @Tr-uw16 _16 '
  % 4
  r32 m-ae-n32 `Q-ay Q-ey-nt @g-aa-t16 |
  `n-ah32 T-ih-n t-ax @pr-uw-v16 Q-ay32 `p-ey-d16
  <>m-ah32 @d-uw-z16. `br-ey32 k-ih-n D-ah @r-uw-Lz
  _32 Q-AX32 `S-ey-k16 @f-uw-Lz `w-ay-l32 Q-ay-m
  `t-ey k-ih-n Q-ah @kr-uw-z8 r32 |
  }

qTwoSyls =
  \lyricmode
  {
  \partial 8
  % 1
  `t-eh-l16 m-iy |
  `h-uw-z y-ao-r @w-iy-d8 `m-ae-n16. Q-ah-nd32 `h-aw d-uw y-uw16 @sm-ow-k16. s-ow @g-uh-d16 _16 ' r16
  % 1
  `y-uw-z16 Q-ah |
  @s-uw p-er `st-aa-r8 @b-oy w-ay16 y-uw @st-ih-l32 Q-ah-p `Q-ih-n16 D-ah @h-uh-d _16 ' r16
  % 1
  `w-ah-t32 Q-ih-n D-ah16 |
  @w-er-ld16. Q-ih-z32 `Q-ih-n16 D-ae-t @b-ae-g8 `w-ah-t16 y-uw
  @g-aa-t Q-ih-n D-ae-t @b-ae-g _16 ' r32
  % 1
  Q-ah32 `k-ah p-el Q-ah @k-ae-nz |
  _32 Q-ah @w-uh-p Q-ae-s16 `y-uw32 d-ih-d Q-ah @g-uh-d Q-ae-s16 `J-ao-b Q-ah-v32 `J-ah-st16 @Q-ay Q-ih-n32 `m-iy16 @sp-ay Q-ih-n32 `m-iy16 ' r16 r8 |
  }

qThreeSyls =
  \lyricmode
  {
    \partial 4.
  r16 @m-ae-n16 _16 D-ae-t `k-aa-r d-ow-nt |
  @k-ah-m32 Q-aw-16 Q-en32 `t-ih-l16 n-eh-kst @y-ih-r8 `w-eh-r32 Q-ih-n D-ah16 @f-ah-k16. d-ih-d32 y-uw16 @g-ih-t32 Q-ih-t ' r16

  D-ae-ts16 `Q-ey t-iy |
  @T-aw z-en-d `b-ah-ks8 @g-ao-n `w-eh-r32 Q-ih-n D-ah16 @f-ah-k16. d-ih-d32 y-uw16 @<>sp-ih-nd32 Q-ih-t ' r16.

  y-uw32 `m-ah-st16 h-ae-v |
  @Q-ay-z8 `Q-ao-n16 y-ao-r @b-ae-k16. k-ah-z32 `y-uw16 g-aa-t @m-ah32 n-iy16 `t-uw32 D-ah16 @<>s-ih32 l-ih-n ' r16

  Q-en-d32 D-ah `b-ih g-er D-ah @k-ae-p |
  _32 D-ah `b-ih g-er D-ah @<>p-ih l-ih-n D-ah `b-eh t-er Q-em @<>f-ih l-ih-n D-ah `m-ao-r D-ae-t Q-em @C-ih l-ih-n `w-ih l-ih-n `dr-ih l-ih-n Q-en-d @k-ih l-ih-n D-ah `f-ih l-ih-n16 ' r16 |
  }

qFourSyls =
  \lyricmode
  {
    \partial 4
  r16 n-aw16 `h-uw-z D-ae-t |
  @b-ah-k <>k-eh-d `n-ey k-eh-d @k-uh-k8 `f-ih-k16 s-ih-n @Tr-iy16. `k-ao-rs @m-iy-Lz16 _16 ' r16

  `g-eh t-ih-n |
  @g-uw-s `b-ah-mps8 `w-eh-n32 h-er @b-aa16 d-iy `t-ae-ps D-ah @s-ih-ks16. Q-ih-nC @h-iy-Lz16 _16 ' r16

  `w-ah-t32 Q-ih-n D-ah16 |
  @w-er-ld16. Q-ih-z32 Q-ih-n16 D-ae-t @r-uw-m8 `w-ah-t16 y-uw `g-aa-t Q-ih-n D-ae-t @r-uw-m _16 ' r32

  Q-ah32 `k-ah p-el Q-ah @g-ae-ts |
  _32 Q-ah `k-ah p-el Q-ah @n-ay-vz _32 Q-ah `k-ah p-el Q-ah @r-ae-ts _32 Q-ah `k-ah p-el Q-ah @w-ay-vz _32 n-aw16 Q-ih-ts32 `t-ay-m t-uw @C-uw-z8 ' r8 |
  }

qFiveSyls =
  \lyricmode
  {
  \partial 4
  r32 Q-aa-r32 y-uw16 `k-ah st-ah-m |
  @m-ey-d8 `k-ah16 st-ah-m @p-ey-d16. Q-ao-r32 `y-uw16 J-ah-st
  @k-ah16. st-ah-m @f-ih32 <>t-ih-d ' r16

  pl-ey16 `st-ey S-ah-n |
  @t-uw Q-ah-p `Q-ih-n D-ah32 @r-ay-d _16 Q-ah-nd32 Q-ih-z `D-ae-t16 l-ao-r
  @Q-eh-n16. z-ow @k-ih32 <>t-ih-d ' r16

  Q-ih-z16 `D-ae-t y-ao-r |
  `w-ay-f y-ao-r @g-er-l8 `fr-eh-nd16 Q-ao-r `J-ah-st y-ao-r @m-ey-n8. `b-ih-C16 ' r16

  y-uw `t-ey-k Q-ah32 @p-ih-k |
  _16 `w-ay-l32 Q-ay-m `r-ah b-ih-n D-ah @h-ih-ps _32 `t-ah-C16 D-ah32 @l-ih-ps16 t-uw32 D-ah `t-aa-p Q-ah-v D-ah @d-ih-k _32 Q-ah-nd D-eh-n @w-uw32 _16 ' r8. |
  }
qSixSyls =
  \lyricmode
  {
  \partial 4
  r16 Q-ah-nd16 `t-eh-l m-iy |
  @h-uw-z y-ao-r `h-aw-s8 @k-iy16 p-er32 Q-ah-nd `w-ah-t16 y-uw @k-iy-p Q-ih-n y-ao-r @h-aw-s _16 ' r16

  `w-ah-t32 Q-ah b-aw-t16 |
  `d-ay m-ah-ndz Q-ah-nd @g-ow-ld _16 Q-ih-z `D-ae-t w-ah-t32 y-uw @k-iy-p16 Q-ih-n y-ao-r @m-aw-T _16 ' r16

  `w-ah-t32 Q-ih-n D-ah16 |
  @w-er-ld16. Q-ih-z32 `Q-ih-n16 D-ae-t @k-ey-s8 `w-ah-t16 y-uw @g-aa-t Q-ih-n D-ae-t @k-ey-s _32 ' r32

  g-eh-t32 Q-ah-p `Q-aw-t16 m-ay32 @f-ey-s |
  _32 y-uw `k-uh d-en-t r-ih @l-ey-t16 `w-ey-t32 _32 t-ax `t-ey-k16 @pl-ey-s Q-ae-t32 Q-ah `s-ih m-ih l-er @p-ey-s _32 s-ow `S-ey-k16 @S-ey-k Q-ih-t ' r8 |
  }
%[OUTRO]

%{ verseOneLyrics = {
  \qOneLyrics
  \qTwoLyrics
} %}
%{ verseTwoLyrics = {
  \qThreeLyrics
  \qFourLyrics
} %}
%{ verseThreeLyrics = {
  \qFiveLyrics
  \qSixLyrics
} %}

%{ verseOneSyls = {
  \qOneSyls
  \qTwoSyls
} %}
%{ verseTwoSyls = {
  \qThreeSyls
  \qFourSyls
} %}
%{ verseThreeSyls = {
  \qFiveSyls
  \qSixSyls
} %}

%{ songLyrics = {
  \qOneLyrics
  \qTwoLyrics
  \qThreeLyrics
  \qFourLyrics
  \qFiveLyrics
  \qSixLyrics
  } %}

%{ songSyls = {
    \qOneSyls
    \qTwoSyls
    \qThreeSyls
    \qFourSyls
    \qFiveSyls
    \qSixSyls
  } %}

%{ \lyricNotation \songLyrics \songSyls %}
\lyricNotation \qOneLyrics \qOneSyls
\pageBreak
\lyricNotation \qThreeLyrics \qThreeSyls
\pageBreak
\lyricNotation \qFiveLyrics \qFiveSyls
\pageBreak

\lyricNotation \qTwoLyrics \qTwoSyls
\pageBreak
\lyricNotation \qFourLyrics \qFourSyls
\pageBreak
\lyricNotation \qSixLyrics \qSixSyls
\pageBreak

\lyricNotation \qOneLyrics \qOneSyls
\pageBreak
\lyricNotation \qTwoLyrics \qTwoSyls
\pageBreak
\lyricNotation \qThreeLyrics \qThreeSyls
\pageBreak
\lyricNotation \qFourLyrics \qFourSyls
\pageBreak
\lyricNotation \qFiveLyrics \qFiveSyls
\pageBreak
\lyricNotation \qSixLyrics \qSixSyls
\pageBreak
%{ \oneLineLyricNotation \qOneLyrics \qOneSyls
\pageBreak
\oneLineLyricNotation \qTwoLyrics \qTwoSyls
\pageBreak
\oneLineLyricNotation \qThreeLyrics \qThreeSyls
\pageBreak
\oneLineLyricNotation \qFourLyrics \qFourSyls
\pageBreak
\oneLineLyricNotation \qFiveLyrics \qFiveSyls
\pageBreak
\oneLineLyricNotation \qSixLyrics \qSixSyls %}

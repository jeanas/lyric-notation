\version "2.22.0"
\include "../lyric-notation.ily"

songLyricsPlain =
\lyricmode {
Till I col -- lapse I'm spil -- lin' these raps long as you feel 'em
Till the day that I drop you'll ne -- ver say that I'm not kil -- lin' 'em
‘Cause when I am not, then I'm -- a stop pen -- nin' 'em
And I am not hip -- hop then I'm just not E -- mi -- nem
Sub -- li -- mi -- nal thoughts, when I'm -- a stop send -- in' 'em?
Wo -- men are caught in webs, spin 'em and hock ve -- nom
Ad -- re -- na -- line shots of pe -- ni -- cil -- lin could not get the il -- lin' to stop
A -- mo -- xi -- cil -- lin's just not real e -- nough
The cri -- min -- al, cop kil -- lin', hip -- hop vil -- lain
A mi -- ni -- mal swap to cop mil -- lions of Pac list -- en -- ers
You're co -- min' with me, feel it or not
You're gon -- na fear it like I showed ya the spi -- rit of God lives in us
You hear it a lot, ly -- rics to shock
Is it a mi -- ra -- cle or am I just pro -- duct of pop fiz -- zin' up?
Fo' shiz -- zle, my wiz -- zle, this is the plot, list -- en up
You biz -- zles for -- got, Sliz -- zle does not give a fuck!
}

songLyrics =
\lyricmode {
  \rhColor a blue
  \rhColor A lightblue
  \rhColor AA green
  \rhColor b gold
  \rhColor c yellow
  \rhColor d red
  \rhColor e magenta

Till\rh a1 I col -- lapse\rh b0 I'm spil\rh a1 -- lin'\rh A2 these raps\rh b0 long as you feel\rh a1 'em\rh AA3
Till\rh a1 the day\rh c0 that I drop\rh d0 you'll ne -- ver say\rh c0 that I'm not\rh d0 kil\rh a1 -- lin'\rh A2 'em\rh AA3
‘Cause when\rh e0 I am not,\rh d0 then\rh e0 I'm -- a stop\rh d0 pen\rh a1 -- nin'\rh A2 'em\rh AA3
And\rh e0 I am not\rh d0 hip -- hop\rh d0 then\rh e0 I'm just not\rh d0 E\rh AA3 -- mi\rh A2 -- nem\rh AA3
}

songSyllables =
\lyricmode {
  \partial 4
r16 t-ih-l `Q-ay k-ao |
@l-ae-ps8 Q-ay-m16 `sp-ih l-ih-n D-iy-z @r-ae-ps8 `l-ao-G16 Q-ae-z y-uw @f-iy-l Q-ah-m `t-ih-l D-ah @d-ey |
D-ae-t Q-ay @dr-aa-p8 y-uh-L16 `n-eh v-er `s-ey D-ae-t Q-ay-m @n-aa-t8 `k-ih16 l-ih-n Q-eh-m8 |
r16 k-ah-z16 `w-eh-n Q-ay Q-ae-m @n-aa-t8 D-eh-n16 `Q-ay m-ah @st-aa-p8 `p-ih16 n-ih-n Q-eh-m Q-ah-nd |
`Q-ay Q-ae-m @n-aa-t8 h-ih-p16 `h-aa-p8 D-eh-n16 `Q-ay-m J-ah-st @n-aa-t8 `Q-eh16 m-iy n-eh-m8 |
}

\lyricNotation \songLyrics \songSyllables

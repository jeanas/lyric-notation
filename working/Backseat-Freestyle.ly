
\version "2.22.0"
\include "../lyric-notation.ily"

hookLyrics =
    \lyricmode {
    \goodColors
    \rgbVowelRainbow

All my life I want mo -- ney and pow -- er
Re -- spect my mind or die from lead show -- er
I pray my dick get big as the Eif -- fel Tow -- er
So I can fuck the world for se -- ven -- ty two hou -- rs
}

verseLyrics =
    \lyricmode {
    \goodColors
    \rgbVowelRainbow

God -- damn I feel a -- ma -- zin', damn, I'm in the Ma -- trix
My mind is li -- vin' on cloud nine and this nine is ne -- ver on va -- ca -- tion
Start up that Ma -- ser -- a -- ti and vroom vroom! I'm ra -- cin'
Pop -- pin' pills in the lob -- by and I pray they don't find her na -- ked
And I pray you nig -- gas is ha -- tin', shoo -- ters go af -- ter Ju -- das
Je -- sus Christ, if I live life on my knees ain't no need to do this
Park it in front of Lue -- ders, next to that Chur -- ch's Chic -- ken
All you pus -- sies is lo -- sers, all my nig -- gas is win -- ners, screa -- min'
}

hookSyls =
    \lyricmode {
        \bracketSettings
        \bracketTextBeforeBreak
        \generalGrooveSettings
        \grooveTupletTextBeforeBreak
        \partial 4

Q-ao-l m-ay l-ay-f Q-ay w-aa-nt `m-ah n-iy Q-ah-nd `p-aw Q-er r-ih `sp-eh-kt m-ay m-ay-nd Q-ao-r d-ay fr-ah-m l-eh-d `S-aw Q-er Q-ay pr-ey m-ay d-ih-k g-eh-t b-ih-g Q-ae-z D-ah `Q-ay f-ah-l `t-aw Q-er s-ow Q-ay k-ae-n f-ah-k D-ah w-er-ld f-ao-r `s-eh v-ah-n t-iy t-uw `Q-aw Q-er-z
}
verseSyls =
    \lyricmode {
        \bracketSettings
        \bracketTextBeforeBreak
        \generalGrooveSettings
        \grooveTupletTextBeforeBreak
        \partial 4
`g-aa `d-ae-m Q-ay f-iy-l Q-ah `m-ay z-ih-n d-ae-m Q-ay-m Q-ih-n D-ah `m-ey tr-ih-ks m-ay m-ay-nd Q-ih-z `l-ih v-ih-n Q-aa-n kl-aw-d n-ay-n Q-ah-nd D-ih-s n-ay-n Q-ih-z `n-eh v-er Q-aa-n v-ey `k-ey S-ah-n st-aa-rt Q-ah-p D-ae-t `m-ae s-eh `r-aa t-iy Q-ah-nd vr-uw-m vr-uw-m Q-ay-m `r-ay s-ih-n `p-aa p-ih-n p-ih-lz Q-ih-n D-ah `l-aa b-iy Q-ah-nd Q-ay pr-ey D-ey d-ow-nt f-ay-nd h-er `n-ey k-ah-d Q-ah-nd Q-ay pr-ey y-uw `n-ih g-ah-z Q-ih-z `h-ay t-ih-n `S-uw t-er-z g-ow `Q-ae-f t-er `J-uw d-ah-s `J-iy z-ah-s kr-ay-st Q-ih-f Q-ay l-ay-v l-ay-f Q-aa-n m-ay n-iy-z Q-ey-nt n-ow n-iy-d t-uw d-uw D-ih-s p-aa-rk Q-ih-t Q-ih-n fr-ah-nt Q-ah-v `l-uh d-er-z n-eh-kst t-uw D-ae-t `C-er C-ah-z `C-ih k-ah-n Q-ao-l y-uw `p-uh s-iy-z Q-ih-z `l-uw z-er-z Q-ao-l m-ay `n-ih g-ah-z Q-ih-z `w-ih n-er-z `skr-iy m-ih-n }


    \lyricNotation \songLyrics \songSyllables
        \pageBreak
    \oneLineLyricNotation \songLyrics \songSyllables

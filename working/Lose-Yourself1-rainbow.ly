\version "2.22.0"
\include "../lyric-notation.ily"
\justP
#(set-global-staff-size 14)

songLyricsRainbow =
  \lyricmode {
    \rgbRainbowGradients


  % Pickup
  His\rh ih0 palms\rh aa0
  % Bar 1
  are\rh aar0 swea\rh eh0 -- ty,\rh iy0
  knees\rh IY0 weak,\rh iy0
  arms\rh aar0 are\rh AAR0 hea\rh eh0 --
  vy\rh iy0 There's\rh ehr0 vo\rh aa0 -- mit\rh ih0
  % Bar 2
  on\rh aa0 his\rh ih0 swea\rh eh0 -- ter\rh er0
  al\rh aa0 -- rea\rh eh0 -- dy,\rh iy0
  mom's\rh ao0 spa\rh ah0 -- ghe\rh eh0 --
  tti\rh iy0 He's\rh IY0 ner\rh er0 -- vous\rh ax0
  % Bar 3
  but\rh AX0 on\rh aa0 the\rh ax0
  sur\rh er0 -- face\rh ax0 he\rh iy0 looks\rh uh0
  calm\rh aa0 and\rh ae0 rea\rh eh0 --
  dy\rh iy0 To\rh uw0
  drop\rh aa0
  % Bar 4
  bombs\rh AA0
  but\rh ax0 he\rh iy0 keeps\rh IY0
  on\rh aa0 for\rh aor0 -- ge\rh eh0 --
  tting\rh ih0 What\rh ah0 he\rh iy0 wrote\rh ow0
  % Bar 5
  dowon,\rh aw0 the\rh ax0
  whole\rh ow0 crowd\rh aw0
  goes\rh ow0 so\rh OW0 loud\rh aw0
  He\rh ih0 o\rh ow0 --
  % Bar 6
  pens\rh ih0 his\rh IH0 mouth,\rh aw0
  but\rh ah0 the\rh ax0 words\rh er0
  won't\rh ow0 come\rh ax0 out\rh aw0
  He's\rh iy0 cho\rh ow0 --
  % Bar 7
  king,\rh ih0 how?\rh aw0
  Eve\rh eh0 -- ry\rh iy0 -- bo\rh ah0 -- dy's\rh iy0
  jo\rh ow0 -- king\rh ih0 now\rh aw0
  The\rh ax0 clock's\rh ow0
  % Bar 8
  run\rh ax0 out,\rh aw0
  time's\rh ay0 up,\rh ah0
  o\rh ow0 -- ver,\rh er0 blaow!\rh aw0
  Snap\rh ae0
  % Bar 9
  back\rh AE0 to\rh uw0 re\rh iy0 --
  a\rh ae0 -- li\rh ih0 -- ty,\rh iy0 oh\rh ow0
  there\rh ehr0 goes\rh ow0
  gra\rh ae0 -- vi\rh ih0 -- ty,\rh iy0 oh\rh ow0
  % Bar 10
  There\rh ehr0 goes\rh ow0
  Ra\rh ae0 -- bbit,\rh ih0 he\rh iy0 choked,\rh ow0
  he's\rh iy0 so\rh ow0
  mad\rh ae0 but\rh ax0 he\rh iy0 won't\rh ow0
  % Bar 11
  Give\rh ih0 up\rh ow0
  that\rh ae0 ea\rh iy0 -- sy,\rh IY0 no,\rh ow0
  he\rh iy0 won't\rh ow0
  have\rh ae0 it,\rh ih0 he\rh iy0 knows\rh ow0
  % Bar 12
  His\rh ih0 whole\rh ow0
  back's\rh ae0 to\rh uw0 these\rh iy0 ropes,\rh ow0
  it\rh ih0 don't\rh ow0
  ma\rh ae0 -- tter,\rh er0 he's\rh iy0 dope\rh ow0
  % Bar 13
  He\rh iy0 knows\rh ow0
  that\rh ae0 but\rh ax0 he's\rh iy0 broke,\rh ow0
  he's\rh iy0 so\rh ow0
  stag\rh ae0 -- nant,\rh AE0 he\rh iy0 knows\rh ow0
  % Bar 14
  When\rh eh0 he\rh iy0 goes\rh ow0
  back\rh ae0 to\rh uw0 this\rh ih0 mo\rh ow0 --
  bile\rh el0 home,\rh ow0
  that's\rh ae0 when\rh eh0 it's\rh ih0
  % Bar 15
  Back\rh ae0 to\rh uw0 the\rh ax0
  lab\rh ae0 a\rh ax0 -- gain\rh ih0 yo,\rh ow0
  this\rh ih0 whole\rh ow0
  rhap\rh ae0 -- so\rh ow0 -- dy,\rh iy0 Bet\rh eh0 --
  % Bar 16
  ter\rh er0 go\rh ow0 cap\rh ae0 -- ture\rh er0
  this\rh ih0 mo\rh ow0 -- ment\rh eh0 and\rh ax0
  hope\rh ow0 it\rh ih0 don't\rh ow0
  pass\rh ae0 him\rh ih0
  }

songSylsPlain =
  \lyricmode {
  \partial 4
  % Pickup
  r16 h-ih-z16 @p-aa-lmz8 |
  % Bar 1
  Q-aa-r16 `sw-eh -- t-iy8
  @n-iy-z `w-iy-k
  @Q-aa-rm z-aa-r16 `h-eh --
  v-iy D-eh-rz `v-aa -- m-ih-t |
  % Bar 2
  `Q-aa-n h-ih-z `sw-eh -- t-er
  Q-aa-l -- `r-eh -- d-iy8
  @m-aa-mz sp-ah16 -- `g-eh --
  t-iy16 h-iy-z `n-er -- v-ah-s |
  % Bar 3
  r16. b-ah-t32 `Q-aa-n16 D-ah
  `s-er -- f-ah-s `h-iy l-uh-ks
  @k-aa-lm8 Q-ah-nd16 `r-eh --
  d-iy t-uw `dr-aa-p8 |
  % Bar 4
  @b-aa-mz8 r8
  `b-ah-t16 h-iy `k-iy-ps8
  @Q-aa-n f-er16 -- `g-eh --
  t-ih-G16 `w-ah-t h-iy `r-ow-t |
  % Bar 5
  _16 @d-aw-n8 D-ah16
  `h-ow-l8 @kr-aw-d
  `g-ow-z s-ow16 `l-aw-d
  _16  h-iy `y-ow8 -- |
  % Bar 6
  p-ih-nz16 h-ih-z `m-aw-T8
  `b-ah-t16 D-ah `w-er-dz8
  `w-ow-nt k-ah16 `m-aw-t
  r16 h-iy-z `C-ow8 -- |
  % Bar 7
  k-ih-G16 @h-aw8 r16
  `Q-eh-v16 -- r-iy -- `b-ah -- d-iy-z
  `J-ow8 -- k-ih-G16 @n-aw
  r16 D-ah `kl-aa-ks8 |
  % Bar 8
  r-ah-n16 @Q-aw-t8 r16
  `t-ay-mz8 Q-ah-p
  `Q-ow -- v-er16 @bl-aw16
  _16 r16 `sn-ae-p8 |
  % Bar 9
  @b-ae-k8 t-uw16 r-iy --
  `y-ae l-ih -- t-iy @Q-ow
  r16 D-eh-r16 `g-ow-z8
  `gr-ae16 -- v-ih -- t-iy @Q-ow |
  % Bar 10
  r16 D-eh-r16 `g-ow-z8
  `r-ae16 -- b-ih-t h-iy @C-ow-kt
  r16 h-iy-z16 `s-ow8
  `m-ae-d16 b-ah-t h-iy @w-ow-nt |
  % Bar 11
  _16   g-ih16 `<>v-uh-p8
  `D-ae-t16 Q-iy -- z-iy @n-ow
  r16 h-iy `w-ow-nt8
  `h-ae16 v-ih-t h-iy @n-ow-z |
  % Bar 12
  _16 h-ih-z `h-ow-l8
  `b-ae-ks16 t-ah D-iy-z @r-ow-ps
  r16 Q-ih-t `d-ow-nt8
  `m-ae16 -- t-er h-iy-z @d-ow-p |
  % Bar 13
  r16 h-iy `n-ow-z8
  `D-ae-t16 b-ah-t h-iy-z @br-ow-k
  r16 h-iy-z `s-ow8
  `st-ae-g16 -- n-ih-nt h-iy @n-ow-z |
  % Bar 14
  _16 w-eh-n32 h-iy `g-ow-z8
  \tuplet 3/2 8 { `b-ae-k8 t-ah16 } D-ih-s16 @m-ow --
  _16 b-el `h-ow-m8
  \tuplet 3/2 8 { `D-ae-ts8 w-eh-n16 } Q-ih-ts16 r16 |
  % Bar 15
  @b-ae-k8 t-uw16 D-ah
  `l-ae b-ah -- g-ih-n @y-ow
  r16 D-ih-s `h-ow-l8
  `r-ae-p16 -- s-ih -- d-iy `b-eh -- |
  % Bar 16
  t-er g-ow `k-ae-p -- C-er
  D-ih-s `m-ow -- m-eh-nt Q-ax-nd
  `h-ow p-ih-t `d-ow-nt8
  @p-ae-s16 h-ih-m r8 |
  }

songSylsBrackets =
  \lyricmode {
    \partial 4
     %{ \set Score.tupletFullLength = ##t %}

    \oneLineBracketSettings
    %{ \bSet %}
    \bracketTextBeforeBreak
    %{ \override Score.AnalysisNumber.outside-staff-priority = ##f %}
    \override Score.AnalysisNumber.direction = #DOWN
  % Pickup
    \phrasingSlurDashed \slurDotted
                r16
                \domTupLabelOL "[a1]"
                \oneDomTup {
                %\multiSet
                \innerNestedBracket "[[ab1]]"
    % his palms
    h-ih-z16(
    @p-aa-lmz8) } |
      % Bar 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    \domTupLabelOL "[b1]"
                    \oneDomTup {
    % are
    %{ \once \override Fingering.direction = #down %}
    Q-aa-r16(
    %{ \finger \markup { \teeny \smaller \smaller \smaller  "1"} %}
    % swea-ty
    `sw-eh -- \eB t-iy8)}
                    \domTupLabelOL "[c1]"
                    \oneDomTup {
    % knees
    @n-iy-z( }
                    \domTupLabelOL "[c2]"
                    \oneDomTup {
    % weak
    `w-iy-k) }
                    \domTupLabelOL "[A2]"
                    \oneDomTup {
                    %\multiSet
                    \innerNestedBracket "[[AB2]]"
    % arms
    @Q-aa-rm }
                    \domTupLabelOL "[b2]"
                    \oneDomTup {
    % are
    z-aa-r16(
    % hea-
    `h-eh --
                    \eB
    % -vy
    v-iy) }
                    \domTupLabelOL "[a⟺3]" \oneDomTup {
                    %\multiSet
                    \onceBracketTextAfterBreak
                    %{ \once \override Score.HorizontalBracketText.direction = #1 %}
                    \outerNestedBracket "[[aabb3]]"
    % theres
    D-eh-rz(

                    \innerNestedBracket "[[aa1]]"
    % vo-mit
    `v-aa -- m-ih-t)
     }
    | % Bar 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    \domTupLabelOL "[a⟺4]" \oneDomTup {
    % on his
    `Q-aa-n( \eb h-ih-z)
     }
                    \domTupLabelOL "[b⟺3]"
                    \oneDomTup {
    % swea-ter
    `sw-eh( -- t-er) }
                    \domTupLabelOL "[b4]" \oneDomTup {
    % all
    Q-aa-l( --
    % rea-dy
    `r-eh -- \eB d-iy8) }
                    \domTupLabelOL "[A5]" \oneDomTup {
                    %\multiSet
                    \innerNestedBracket "[[AB4]]"
    % mom's spa-
    @m-aa-mz }
                    \domTupLabelOL "[b5]" \oneDomTup {
    % spa-
    sp-ah16( --
    % -ghe
    `g-eh --
                    \eB
    % -ti
    t-iy16) }
                    \domTupLabelOL "[d1]" \oneDomTup {
    % he's
    h-iy-z(

    % ner-vous
    `n-er -- v-ah-s) } |
    % Bar 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        r16.
                    \domTupLabelOL "[a6]" \oneDomTup {
    % but
    b-ah-t32(
    % on
    `Q-aa-n16 }
                    \domTupLabelOL "[d2]" \oneDomTup
    % the
    { D-ah
    % sur-face
    `s-er -- f-ah-s) }
                    %{ \domTupLabelOL "c3" \oneDomTup { %}
                    \domTupLabelOL "[A7]" \oneDomTup {
    % he
    h-iy(

                    %{ \domTupLabelOL "A7" \oneDomTup { %}
    % looks
    l-uh-ks
                    %\multiSet
                    \innerNestedBracket "[[AB5]]"
    % calm and
    @k-aa-lm8) }
                    \domTupLabelOL "[B6]" \oneDomTup {
    % and
    Q-ah-nd16(
    % rea-
    `r-eh --
                    \eB
    % -dy
    d-iy) }
    % to
                    \domTupLabelOL "[a8]" \oneDomTup {
                    %\multiSet
                    \onceBracketTextAfterBreak
                    \innerNestedBracket "[[ab6]]"
    t-uw(
                    \once \override Score.HorizontalBracket.shorten-pair = #'(-15 . -1.5)
                    \innerNestedBracket "[[aa2]]"
    % drop
    `dr-aa-p8) }
    | % Bar 4 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    \domTupLabelOL "[a9]" \oneDomTup {
                        \eB
    % bombs
    @b-aa-mz8 } r8
    % but he
                    \domTupLabelOL "[b7]" \oneDomTup {
    `b-ah-t16( h-iy
                        \eB
    % keeps
    `k-iy-ps8) }
                    \domTupLabelOL "[A10]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[AB7]]"
    % on for-
    @Q-aa-n }
                    \domTupLabelOL "[B8]" \oneDomTup {
    %  for-
    f-er16( --
    % -get
    `g-eh --
                         \eB
    % -ting}
    t-ih-G16) }
                    \domTupLabelOL "[E1]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EF1]]"
    % what he
    `w-ah-t(
    h-iy)

    % wrote
    `r-ow-t } |

    % Bar 5 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        _16
                      \domTupLabelOL "[F1]" \oneDomTup {
                      \eB
    % down
    @d-aw-n8 }
                      \domTupLabelOL "[E2]" \oneDomTup {
                      %\multiSet
                    \innerNestedBracket "[[EF*2]]"
    % the
    D-ah16(

    % whole
    `h-ow-l8) }
                    \domTupLabelOL "[F2]" \oneDomTup {
                        %{ \eB %}
    % crowd
    @kr-aw-d }
                    \domTupLabelOL "[E3]" \oneDomTup {
                        %\multiSet
                    %{ \innerNestedBracket "[[EF3]]" %}
    % goes so
    `g-ow-z }
    % goes so
                    \domTupLabelOL "[F3]" \oneDomTup {
    s-ow16(
                        \eB
    % loud
    `l-aw-d) }
                        _16
                    \domTupLabelOL "[E4]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EF3]]"
    % he
    h-iy(

    % o-
    `y-ow8) -- } |

    % Bar 6 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    \domTupLabelOL "[F4]" \oneDomTup {
    % -pens his
    p-ih-nz16(
     h-ih-z

                        \eB
    % mouth
    `m-aw-T8) }
                    \domTupLabelOL "[ʌəɚ]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EF*4]]"
    % but the
    `b-ah-t16(
    D-ah
                    %{ } \domTupLabelOL "XX" \oneDomTup { %}
    % words
    `w-er-dz8) %\finger \markup { \teeny \smaller \smaller \smaller "ɝ" }
                    } \domTupLabelOL "[E5]" \oneDomTup {
    % wont
    `w-ow-nt }
                    \domTupLabelOL "[F5]" \oneDomTup {
    %  come
    k-ah16(
                        \eB
    % out
    `m-aw-t) }
                        r16
                    \domTupLabelOL "[E6]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EF5]]"
    % he's
    h-iy-z(

    % cho-
    `C-ow8)  -- } |

    % Bar 7 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    \domTupLabelOL "[F6]" \oneDomTup {
    % -king
    k-ih-G16(

                        \eB
    % how
    @h-aw8) } r16

                    \domTupLabelOL "[XX]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EF*6]]"
    % eve-ry
    `Q-eh-v16( --
    r-iy) --
                    } \domTupLabelOL "[XX]" \oneDomTup {
    % bo-dys
    `b-ah( --
    d-iy-z)
                    } \domTupLabelOL "[E7]" \oneDomTup {
    % jo-
    `J-ow8 --
                    } \domTupLabelOL "[F7]" \oneDomTup {
    % -king
    k-ih-G16(
                        \eB
    % now
    @n-aw) }
                        r16
                    \domTupLabelOL "[E8]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EF7]]"
    % the
    D-ah(
    % clocks
    `kl-aa-ks8) }
    | % Bar 8 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    \domTupLabelOL "[F8]" \oneDomTup {
    % run
    r-ah-n16(
                        \eB
    % out
    @Q-aw-t8) } r16

                    \domTupLabelOL "[E9]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EF*8]]"
    % times up
    `t-ay-mz8(
                    %{ } \domTupLabelOL "XX" \oneDomTup { %}
    %  up
    Q-ah-p)
                    %{ } \domTupLabelOL "XX" \oneDomTup { %}
    % o-
    `Q-ow --
                    } \domTupLabelOL "[F9]" \oneDomTup {
    % ver blaow
    v-er16(

                        \eB
    % ver blaow
    @bl-aw16)
                      } _16 r16
                    \domTupLabelOL "[g1]" \oneDomTup {
    % snap
    `sn-ae-p8 }
    | % Bar 9 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    \domTupLabelOL "[G*2]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[G*GE1]"
    % back to re-
    @b-ae-k8( t-uw16 r-iy) -- }
                    \domTupLabelOL "[G3]" \oneDomTup {
    % -a-li-ty
    `y-ae( l-ih -- t-iy) }
                    \domTupLabelOL "[E*10]" \oneDomTup {
                        \eB
    % oh
    @Q-ow }
                        r16
                    \domTupLabelOL "[E11]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EGE2]]"
    % there
    D-eh-r16(

    % goes
    `g-ow-z8) }
                    \domTupLabelOL "[G4]" \oneDomTup {
    % gra-vi-ty
    `gr-ae16( -- v-ih -- t-iy) }
                    \domTupLabelOL "[E*12]" \oneDomTup {
                        \eB
    % oh
    @Q-ow }
    | % Bar 10 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        r16
                    \domTupLabelOL "[E13]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EGE3]]"
    % there
    D-eh-r16(
    % goes
    `g-ow-z8) }
                    \domTupLabelOL "[G5]" \oneDomTup {
    % rab-bit he
    `r-ae16( -- b-ih-t h-iy) }
                    \domTupLabelOL "[[E*14]]" \oneDomTup {
                        \eB
    % choked
    @C-ow-kt }
                        r16
                    \domTupLabelOL "[E15]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EGE4]]"
    % hes
    h-iy-z16(

    % so
    `s-ow8) }
                    \domTupLabelOL "[G*6]" \oneDomTup {
    % mad but he
    `m-ae-d16( b-ah-t h-iy) }
                    \domTupLabelOL "[E*16]" \oneDomTup {
                        \eB
    % wont
    @w-ow-nt }
    | % Bar 11 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        _16
                    \domTupLabelOL "[E17]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EGE5]]"
    % give
    g-ih16(

    % up
    `<>v-uh-p8) }
                    \domTupLabelOL "[G7]" \oneDomTup {
    % that ea-sy
    `D-ae-t16( Q-iy -- z-iy) }
                    \domTupLabelOL "[E*18]" \oneDomTup {
                        \eB
    % no
    @n-ow }
                        r16
                    \domTupLabelOL "[E19]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EGE6]]"
    % he
    h-iy(

    % wont
    `w-ow-nt8) }
                    \domTupLabelOL "[G8]" \oneDomTup {
    % have it he
    `h-ae16( v-ih-t h-iy) }
                    \domTupLabelOL "[E*20]" \oneDomTup {
                        \eB
    % knows
    @n-ow-z }
    | % Bar 12 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        _16
                    \domTupLabelOL "[E21]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EGE7]]"
    % hes
    h-ih-z(

    % whole
    `h-ow-l8) }
                    \domTupLabelOL "[G*9]" \oneDomTup {
    % backs to these
    `b-ae-ks16( t-ah D-iy-z) }
                    \domTupLabelOL "[E*22]" \oneDomTup {
                        \eB
    % ropes
    @r-ow-ps }
                        r16
                    \domTupLabelOL "[E23]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EGE8]]"
    % it
    Q-ih-t(

    % dont
    `d-ow-nt8) }
                    \domTupLabelOL "[G*10]" \oneDomTup {
    % mat-ter hes
    `m-ae16( -- t-er h-iy-z) }
                    \domTupLabelOL "[E*24]" \oneDomTup {
                        \eB
    % dope
    @d-ow-p }
    | % Bar 13 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        r16
                    \domTupLabelOL "[E25]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EGE9]]"
    % he
    h-iy(

    % knows
    `n-ow-z8) }
                    \domTupLabelOL "[G*11]" \oneDomTup {
    % that but hes
    `D-ae-t16( b-ah-t h-iy-z) }
                    \domTupLabelOL "[E*26]" \oneDomTup {
                        \eB
    % broke
    @br-ow-k }
                        r16
                    \domTupLabelOL "[E27]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EGE10]]"
    % hes
    h-iy-z(

    % so
    `s-ow8) }
                    \domTupLabelOL "[G12]" \oneDomTup {
    % stag-nant he
    `st-ae-g16( -- n-ih-nt h-iy) }
                    \domTupLabelOL "[E*28]" \oneDomTup {
                        \eB
    % knows
    @n-ow-z }
    | % Bar 14 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    _16
                    \domTupLabelOL "[E29]" \oneDomTup {
                    %\multiSet
                    \innerNestedBracket "[[EGE11]]"
    % when he
    w-eh-n32(
    h-iy

    % goes
    `g-ow-z8) }

                    \domTupLabelOL "[G*13]" \oneDomTup {
                        \tuplet 3/2 8 {
    % back to
    `b-ae-k8( t-ah16 }
    % this -
    D-ih-s16) }
                    \domTupLabelOL "[E*30]" \oneDomTup {
                        \eB
    %  mo-
    @m-ow --
        _16 }
                    \domTupLabelOL "[E31]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EGE12]]"
    % -bile
    b-el(
    % home
    `h-ow-m8) }

                    \domTupLabelOL "[G14]" \oneDomTup {
                        \tuplet 3/2 8 {
    % thats when
    `D-ae-ts8( w-eh-n16 }
                        \eB
    % its
    Q-ih-ts16) } r16
    | % Bar 15 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    \domTupLabelOL "[G*15]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[G*EG13]]"
    % back to the
    @b-ae-k8( t-uw16 D-ah) }
                    \domTupLabelOL "[G*16]" \oneDomTup {
    % lab a-gain
    `l-ae( b-ah -- g-ih-n) }
                    \domTupLabelOL "[E*32]" \oneDomTup {
                        \eB
    % yo
    @y-ow }
                        r16
                    \domTupLabelOL "[E33]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[EGE14]]"
    % This
    D-ih-s(

    % whole
    `h-ow-l8)
                  }  \domTupLabelOL "[G17]" \oneDomTup {
    % rhap-so-
    `r-ae-p16( -- s-ih --
                        \eB
    % -dy
    d-iy) }
                    \domTupLabelOL "[e34]" \oneDomTup {
                        %\multiSet
                    \innerNestedBracket "[[ege15]]"
    % bet-
    `b-eh( --
    | % Bar 16 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % -ter goe
    t-er
    g-ow) }
                    \domTupLabelOL "[g*18]" \oneDomTup {
    % cap-ture
    `k-ae-p( -- C-er
    % This
    D-ih-s) }
                    \domTupLabelOL "[e35]" \oneDomTup {
                        %{ \eB %}
    % mo-
    `m-ow( --
                    \eb    %\multiSet
    % - ment
    m-eh-nt)
                    } \domTupLabelOL "[e36]" \oneDomTup {
                    \innerNestedBracket "[[E*EG16]]"
    % and
    Q-en(
    % hope
    `h-ow) }
                    \domTupLabelOL "[e37]" \oneDomTup {
    %  it
    p-ih-t(

    % dont
    `d-ow-nt8) }
                    \domTupLabelOL "[G*19]" \oneDomTup {
    % pass
    @p-ae-s16(
                        \eB
    % him
    h-ih-m) r8 } |
      }

\oneLineLyricNotation \songLyricsRainbow \songSylsBrackets

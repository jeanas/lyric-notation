\version "2.22.0"


%{ \include "highlight.ily" %}

loseYourselfColors = \lyricmode {
      \rhColor a #(x11-color 'LightSeaGreen)
    \rhColor A #(x11-color 'LightSeaGreen)
    \rhColor b #(x11-color 'LightGreen)
    \rhColor B #(x11-color 'LightGreen)
    \rhColor c #(x11-color 'LawnGreen)
    \rhColor C #(x11-color 'LawnGreen)
    \rhColor i #(x11-color 'GreenYellow)
    \rhColor iii #(x11-color 'MediumSeaGreen)
    \rhColor d #(x11-color 'Gold)
    %{ \rhColor ax #(x11-color 'Khaki) %}
    \rhColor ii #(x11-color 'DarkKhaki)
    \rhColor f #(x11-color 'Tomato)
    \rhColor g #(x11-color 'IndianRed)
    \rhColor h #(x11-color 'Magenta)
    \rhColor H #(x11-color 'Orchid)
    \rhColor HH #(x11-color 'Orchid)
    \rhColor ah #(x11-color 'Khaki)
    \rhColor ax #(x11-color 'PaleGoldenrod)
    \rhColor er #(x11-color 'BurlyWood2)
    \rhColor ER #(x11-color 'BurlyWood1)
    \rhColor eh #(x11-color 'Green)
    \rhColor ehr #(x11-color 'Green3)

}
% Good quality basic color palette
goodColors = \lyricmode {
    \rhColor A #(x11-color 'Red)
      \rhColor B #(x11-color 'DarkOrange)
      \rhColor C #(x11-color 'Gold2)
      \rhColor D #(x11-color 'YellowGreen)
      \rhColor E #(x11-color 'Green)
      \rhColor F #(x11-color 'LimeGreen)
      \rhColor G #(x11-color 'Cyan)
      \rhColor H #(x11-color 'DarkCyan)
      \rhColor I #(x11-color 'LightSkyBlue)
      \rhColor J #(x11-color 'DodgerBlue)
      \rhColor K #(x11-color 'Blue)
      \rhColor L #(x11-color 'MediumPurple)
      \rhColor M #(x11-color 'Magenta)
      \rhColor N #(x11-color 'Maroon)
      \rhColor O #(x11-color 'DarkOrange2)
      \rhColor P #(x11-color 'DeepPink2)
        \rhColor a #(x11-color 'Red)
          \rhColor b #(x11-color 'DarkOrange)
          \rhColor c #(x11-color 'Gold2)
          \rhColor d #(x11-color 'YellowGreen)
          \rhColor e #(x11-color 'Green)
          \rhColor f #(x11-color 'LimeGreen)
          \rhColor g #(x11-color 'Cyan)
          \rhColor h #(x11-color 'DarkCyan)
          \rhColor i #(x11-color 'LightSkyBlue)
          \rhColor j #(x11-color 'DodgerBlue)
          \rhColor k #(x11-color 'Blue)
          \rhColor l #(x11-color 'MediumPurple)
          \rhColor m #(x11-color 'Magenta)
          \rhColor n #(x11-color 'Maroon)
          \rhColor o #(x11-color 'DarkOrange2)
          \rhColor p #(x11-color 'DeepPink2)
    \rhColor AA #(x11-color 'Red)
      \rhColor BB #(x11-color 'DarkOrange)
      \rhColor CC #(x11-color 'Gold2)
      \rhColor DD #(x11-color 'YellowGreen)
      \rhColor EE #(x11-color 'Green)
      \rhColor FF #(x11-color 'LimeGreen)
      \rhColor GG #(x11-color 'Cyan)
      \rhColor HH #(x11-color 'DarkCyan)
      \rhColor II #(x11-color 'LightSkyBlue)
      \rhColor JJ #(x11-color 'DodgerBlue)
      \rhColor KK #(x11-color 'Blue)
      \rhColor LL #(x11-color 'Purple)
      \rhColor MM #(x11-color 'Magenta)
      \rhColor NN #(x11-color 'Maroon)
      \rhColor OO #(x11-color 'DarkOrange2)
      \rhColor PP #(x11-color 'DeepPink2)
  }


%% RGB Vowel Rainbows
% "D" ending = Dark 60% shade
% "mD" ending = medium Dark 30% shade
% "mL" ending = medium Light 30% tint
% "D" ending = Light 60% tint


rgbVowelRainbow = {
  \rhColor IY "#00ff00"
  \rhColor IH "#33cc00"
  \rhColor EY "#669900"
  \rhColor EH "#996600"
  \rhColor AE "#cc3300"
  \rhColor AY "#ff0000"
  \rhColor AH "#ff0000"
  \rhColor AX "#ff0000"
  \rhColor EN "#ff0000"
  \rhColor EM "#ff0000"
  \rhColor ER "#ff0000"
  \rhColor AW "#ff0000"
  \rhColor AA "#cc0033"
  \rhColor AO "#990066"
  \rhColor OW "#660099"
  \rhColor UH "#3300cc"
  \rhColor UW "#0000ff"
  \rhColor BBG "#0033cc"
  \rhColor BGG "#006699"
  \rhColor GBB "#009966"
  \rhColor GGB "#00cc33"
  \rhColor iy "#00ff00"
  \rhColor ih "#33cc00"
  \rhColor ey "#669900"
  \rhColor eh "#996600"
  \rhColor ae "#cc3300"
  \rhColor ay "#ff0000"
  \rhColor ah "#ff0000"
  \rhColor ax "#ff0000"
  \rhColor en "#ff0000"
  \rhColor em "#ff0000"
  \rhColor er "#ff0000"
  \rhColor aw "#ff0000"
  \rhColor aa "#cc0033"
  \rhColor ao "#990066"
  \rhColor ow "#660099"
  \rhColor uh "#3300cc"
  \rhColor uw "#0000ff"
  \rhColor bbg "#0033cc"
  \rhColor bgg "#006699"
  \rhColor gbb "#009966"
  \rhColor ggb "#00cc33"
}
% sDark 60% shade
rgbVowelRainbowDark = {
    \rhColor IYD "#006600"
    \rhColor IHD "#145200"
    \rhColor EYD "#293d00"
    \rhColor EHD "#3d2900"
    \rhColor AED "#521400"
    \rhColor AHD "#660000"
    \rhColor AAD "#520014"
    \rhColor AOD "#3d0029"
    \rhColor OWD "#29003d"
    \rhColor UHD "#140052"
    \rhColor UWD "#000066"
    \rhColor BBGD "#001452"
    \rhColor BGGD "#00293d"
    \rhColor GBBD "#003d29"
    \rhColor GGBD "#005214"
  }
rgbVowelRainbowMedDark = {
% medium Dark 30% shade
    \rhColor IYmD "#00b300"
    \rhColor IHmD "#248f00"
    \rhColor EYmD "#476b00"
    \rhColor EHmD "#6b4700"
    \rhColor AEmD "#8f2400"
    \rhColor AHmD "#b30000"
    \rhColor AAmD "#8f0024"
    \rhColor AOmD "#6b0047"
    \rhColor OWmD "#47006b"
    \rhColor UHmD "#24008f"
    \rhColor UWmD "#0000b3"
    \rhColor BBGmD "#00248f"
    \rhColor BGGmD "#00476b"
    \rhColor GBBmD "#006b47"
    \rhColor GGBmD "#008f24"
}
rgbRainbowGradients = \lyricmode {
  \rhColor iy "#4dff4d"
  \rhColor ih "#70db4d"
  \rhColorGradient ihr "#70db4d" "#ff0000"
  \rhColorGradient ey "#94b84d" "#70db4d"
  \rhColor eh "#b8944d"
  \rhColorGradient ehr "#b8944d" "#ff0000"
  \rhColor ae "#db704d"
  \rhColorGradient ay "#ff3333" "#33cc00"
% AX is 30% tint, AH is 20% tint, ER is pure red
  \rhColor ax "#ff4d4d"
  \rhColor ah "#ff3333"
  \rhColor en "#ff4d4d"
  \rhColor em "#ff4d4d"
  \rhColor er "#ff0000"
  \rhColorGradient aw "#ff3333" "#4d4dff"
  \rhColor aa "#db4d70"
  \rhColorGradient aar "#db4d70" "#ff0000"
  \rhColor ao "#b84d94"
  \rhColorGradient aor "#b84d94" "#ff0000"
  \rhColorGradient ow "#944db8" "#4d4dff"
  \rhColor uh "#704ddb"
  \rhColorGradient uhr "#704ddb" "#ff0000"
  \rhColor el "#704ddb"
  \rhColor uw "#4d4dff"
  \rhColor bbg "#4d70db"
  \rhColor bgg "#4d94b8"
  \rhColor gbb "#4db894"
  \rhColor ggb "#4ddb70"
  \rhColor IY "#4dff4d"
  \rhColor IH "#70db4d"
  \rhColorGradient IHR "#70db4d" "#ff0000"
  \rhColorGradient EY "#94b84d" "#70db4d"
  \rhColor EH "#b8944d"
  \rhColorGradient EHR "#b8944d" "#ff0000"
  \rhColor AE "#db704d"
  \rhColorGradient AY "#ff3333" "#33cc00"
% AX is 30% tint, AH is 20% tint, ER is pure red
  \rhColor AX "#ff4d4d"
  \rhColor AH "#ff3333"
  \rhColor EN "#ff4d4d"
  \rhColor EM "#ff4d4d"
  \rhColor ER "#ff0000"
  \rhColorGradient AW "#ff3333" "#4d4dff"
  \rhColor AA "#db4d70"
  \rhColorGradient AAR "#db4d70" "#ff0000"
  \rhColor AO "#b84d94"
  \rhColorGradient AOR "#b84d94" "#ff0000"
  \rhColorGradient OW "#944db8" "#4d4dff"
  \rhColor UH "#704ddb"
  \rhColorGradient UHR "#704ddb" "#ff0000"
  \rhColor EL "#704ddb"
  \rhColor UW "#4d4dff"
  \rhColor BBG "#4d70db"
  \rhColor BGG "#4d94b8"
  \rhColor GBB "#4db894"
  \rhColor GGB "#4ddb70"
  }
rgbVowelRainbowMedLight = {
% medium Light 30% tint
    \rhColor IYmL "#4dff4d"
    \rhColor IHmL "#70db4d"
    \rhColor EYmL "#94b84d"
    \rhColor EHmL "#b8944d"
    \rhColor AEmL "#db704d"
    \rhColor AymL "#ff4d4d"
    \rhColor AHmL "#ff4d4d"
    \rhColor AXmL "#ff4d4d"
    \rhColor ERmL "#ff4d4d"
    \rhColor AWmL "#ff4d4d"
    \rhColor AAmL "#db4d70"
    \rhColor AOmL "#b84d94"
    \rhColor OWmL "#944db8"
    \rhColor UHmL "#704ddb"
    \rhColor UWmL "#4d4dff"
    \rhColor BBGmL "#4d70db"
    \rhColor BGGmL "#4d94b8"
    \rhColor GBBmL "#4db894"
    \rhColor GGBmL "#4ddb70"
}
rgbVowelRainbowLight = {
%  Light 60% tint
    \rhColor IYL "#99ff99"
    \rhColor IHL "#adeb99"
    \rhColor EYL "#c2d699"
    \rhColor EHL "#d6c299"
    \rhColor AEL "#ebad99"
    \rhColor AHL "#ff9999"
    \rhColor AAL "#eb99ad"
    \rhColor AOL "#d699c2"
    \rhColor OWL "#c299d6"
    \rhColor UHL "#ad99eb"
    \rhColor UWL "#9999ff"
    \rhColor BBGL "#99adeb"
    \rhColor BGGL "#99c2d6"
    \rhColor GBBL "#99d6c2"
    \rhColor GGBL "#99ebad"
}


rgbVowelRainbowAll = {
  \rhColor IY "#00ff00"
  \rhColor IH "#33cc00"
  \rhColor EY "#669900"
  \rhColor EH "#996600"
  \rhColor AE "#cc3300"
  \rhColor AY "#ff0000"
  \rhColor AH "#ff0000"
  \rhColor AX "#ff0000"
  \rhColor EN "#ff0000"
  \rhColor EM "#ff0000"
  \rhColor ER "#ff0000"
  \rhColor AW "#ff0000"
  \rhColor AA "#cc0033"
  \rhColor AO "#990066"
  \rhColor OW "#660099"
  \rhColor UH "#3300cc"
  \rhColor UW "#0000ff"
  \rhColor BBG "#0033cc"
  \rhColor BGG "#006699"
  \rhColor GBB "#009966"
  \rhColor GGB "#00cc33"
% Dark 60% shade
    \rhColor IYD "#006600"
    \rhColor IHD "#145200"
    \rhColor EYD "#293d00"
    \rhColor EHD "#3d2900"
    \rhColor AED "#521400"
    \rhColor AHD "#660000"
    \rhColor AAD "#520014"
    \rhColor AOD "#3d0029"
    \rhColor OWD "#29003d"
    \rhColor UHD "#140052"
    \rhColor UWD "#000066"
    \rhColor BBGD "#001452"
    \rhColor BGGD "#00293d"
    \rhColor GBBD "#003d29"
    \rhColor GGBD "#005214"
% medium Dark 30% shade
    \rhColor IYmD "#00b300"
    \rhColor IHmD "#248f00"
    \rhColor EYmD "#476b00"
    \rhColor EHmD "#6b4700"
    \rhColor AEmD "#8f2400"
    \rhColor AHmD "#b30000"
    \rhColor AAmD "#8f0024"
    \rhColor AOmD "#6b0047"
    \rhColor OWmD "#47006b"
    \rhColor UHmD "#24008f"
    \rhColor UWmD "#0000b3"
    \rhColor BBGmD "#00248f"
    \rhColor BGGmD "#00476b"
    \rhColor GBBmD "#006b47"
    \rhColor GGBmD "#008f24"
% medium Light 30% tint
    \rhColor IYmL "#4dff4d"
    \rhColor IHmL "#70db4d"
    \rhColor EYmL "#94b84d"
    \rhColor EHmL "#b8944d"
    \rhColor AEmL "#db704d"
    \rhColor AHmL "#ff4d4d"
    \rhColor AAmL "#db4d70"
    \rhColor AOmL "#b84d94"
    \rhColor OWmL "#944db8"
    \rhColor UHmL "#704ddb"
    \rhColor UWmL "#4d4dff"
    \rhColor BBGmL "#4d70db"
    \rhColor BGGmL "#4d94b8"
    \rhColor GBBmL "#4db894"
    \rhColor GGBmL "#4ddb70"
%  Light 60% tint
    \rhColor IYL "#99ff99"
    \rhColor IHL "#adeb99"
    \rhColor EYL "#c2d699"
    \rhColor EHL "#d6c299"
    \rhColor AEL "#ebad99"
    \rhColor AHL "#ff9999"
    \rhColor AAL "#eb99ad"
    \rhColor AOL "#d699c2"
    \rhColor OWL "#c299d6"
    \rhColor UHL "#ad99eb"
    \rhColor UWL "#9999ff"
    \rhColor BBGL "#99adeb"
    \rhColor BGGL "#99c2d6"
    \rhColor GBBL "#99d6c2"
    \rhColor GGBL "#99ebad"
}

rgbShadeRainbow = {
  \rhColor GGG "#00ff00"
  \rhColor GGR "#33cc00"
  \rhColor GRR "#669900"
  \rhColor RGG "#996600"
  \rhColor RRG "#cc3300"
  \rhColor RRR "#ff0000"
  \rhColor RRB "#cc0033"
  \rhColor RBB "#990066"
  \rhColor BRR "#660099"
  \rhColor BBR "#3300cc"
  \rhColor BBB "#0000ff"
  \rhColor BBG "#0033cc"
  \rhColor BGG "#006699"
  \rhColor GBB "#009966"
  \rhColor GGB "#00cc33"

    \rhColor GGGD "#006600"
    \rhColor GGRD "#145200"
    \rhColor GRRD "#293d00"
    \rhColor RGGD "#3d2900"
    \rhColor RRGD "#521400"
    \rhColor RRRD "#660000"
    \rhColor RRBD "#520014"
    \rhColor RBBD "#3d0029"
    \rhColor BRRD "#29003d"
    \rhColor BBRD "#140052"
    \rhColor BBBD "#000066"
    \rhColor BBGD "#001452"
    \rhColor BGGD "#00293d"
    \rhColor GBBD "#003d29"
    \rhColor GGBD "#005214"

    \rhColor GGGmD "#00b300"
    \rhColor GGRmD "#248f00"
    \rhColor GRRmD "#476b00"
    \rhColor RGGmD "#6b4700"
    \rhColor RRGmD "#8f2400"
    \rhColor RRRmD "#b30000"
    \rhColor RRBmD "#8f0024"
    \rhColor RBBmD "#6b0047"
    \rhColor BRRmD "#47006b"
    \rhColor BBRmD "#24008f"
    \rhColor BBBmD "#0000b3"
    \rhColor BBGmD "#00248f"
    \rhColor BGGmD "#00476b"
    \rhColor GBBmD "#006b47"
    \rhColor GGBmD "#008f24"

    \rhColor GGGmL "#4dff4d"
    \rhColor GGRmL "#70db4d"
    \rhColor GRRmL "#94b84d"
    \rhColor RGGmL "#b8944d"
    \rhColor RRGmL "#db704d"
    \rhColor RRRmL "#ff4d4d"
    \rhColor RRBmL "#db4d70"
    \rhColor RBBmL "#b84d94"
    \rhColor BRRmL "#944db8"
    \rhColor BBRmL "#704ddb"
    \rhColor BBBmL "#4d4dff"
    \rhColor BBGmL "#4d70db"
    \rhColor BGGmL "#4d94b8"
    \rhColor GBBmL "#4db894"
    \rhColor GGBmL "#4ddb70"

    \rhColor GGGL "#99ff99"
    \rhColor GGRL "#adeb99"
    \rhColor GRRL "#c2d699"
    \rhColor RGGL "#d6c299"
    \rhColor RRGL "#ebad99"
    \rhColor RRRL "#ff9999"
    \rhColor RRBL "#eb99ad"
    \rhColor RBBL "#d699c2"
    \rhColor BRRL "#c299d6"
    \rhColor BBRL "#ad99eb"
    \rhColor BBBL "#9999ff"
    \rhColor BBGL "#99adeb"
    \rhColor BGGL "#99c2d6"
    \rhColor GBBL "#99d6c2"
    \rhColor GGBL "#99ebad"
}

% Dessa Rainbow
        %{ \goodColors %}
        %{ \pinkRainbow %}
            %{ \rhColor Iy "#4dff4d"
            \rhColor Ih "#70db4d"
            \rhColor Ey "#94b84d"
            \rhColor Eh "#b8944d"
            \rhColor Ae "#db704d"
            \rhColor Ay "#ff4d4d"
            \rhColor Ah "#ff4d4d"
            \rhColor Ax "#ff4d4d"
            \rhColor En "#ff4d4d"
            \rhColor Em "#ff4d4d"
            \rhColor Er "#ff4d4d"
            \rhColor Aw "#ff4d4d"
            \rhColor Aa "#db4d70"
            \rhColor Ao "#b84d94"
            \rhColor Ow "#944db8"
            \rhColor Uh "#704ddb"
            \rhColor El "#704ddb"
            \rhColor Uw "#4d4dff"
            \rhColor IY "#4dff4d"
            \rhColor IH "#70db4d"
            \rhColor EY "#94b84d"
            \rhColor EH "#b8944d"
            \rhColor AE "#db704d"
            \rhColor AY "#ff4d4d"
            \rhColor AH "#ff4d4d"
            \rhColor AX "#ff4d4d"
            \rhColor EN "#ff4d4d"
            \rhColor EM "#ff4d4d"
            \rhColor ER "#ff4d4d"
            \rhColor AW "#ff4d4d"
            \rhColor AA "#db4d70"
            \rhColor AO "#b84d94"
            \rhColor OW "#944db8"
            \rhColor UH "#704ddb"
            \rhColor EL "#704ddb"
            \rhColor UW "#4d4dff" %}

%{
rollOutColors = {
  \rhColor Ax #(x11-color 'Red)
  \rhColor En #(x11-color 'Red)
  \rhColor Er #(x11-color 'Red)
  \rhColor Ah #(x11-color 'Red)
  \rhColor A #(x11-color 'Red)
    \rhColor B #(x11-color 'DarkOrange2)
    \rhColor C #(x11-color 'Gold2)
    \rhColor D #(x11-color 'YellowGreen)
    \rhColor E #(x11-color 'Green)
    \rhColor F #(x11-color 'LimeGreen)
    \rhColor FB #(x11-color 'FireBrick)
    \rhColor G #(x11-color 'Cyan)
    \rhColor H #(x11-color 'Cyan3)
    \rhColor I #(x11-color 'LightSkyBlue)
    \rhColor IR #(x11-color 'IndianRed)
    \rhColor J #(x11-color 'DodgerBlue)
    \rhColor K #(x11-color 'Blue)
    \rhColor L #(x11-color 'Purple)
    \rhColor M #(x11-color 'Moccasin)
    \rhColor N #(x11-color 'Maroon)
    \rhColor O #(x11-color 'Orange)
    \rhColor P #(x11-color 'DeepPink2)
    \rhColor GY #(x11-color 'GreenYellow)
    \rhColor Y #(x11-color 'Yellow)
    \rhColor DOG #(x11-color 'DarkOliveGreen)
    \rhColor OD #(x11-color 'OliveDrab)
    \rhColor OG #(x11-color 'OliveGreen)
    \rhColor OR #(x11-color 'OrangeRed)
    \rhColor FG #(x11-color 'ForestGreen)
    \rhColor MSG #(x11-color 'MediumSeaGreen)
    \rhColor LSG #(x11-color 'LightSeaGreen)
    \rhColor LSGLSG #(x11-color 'LightSeaGreen)
    \rhColor PT #(x11-color 'PaleTurquoise)
    \rhColor T #(x11-color 'Turquoise)
    \rhColor MT #(x11-color 'MediumTurquoise)
    \rhColor DT #(x11-color 'DarkTurquoise)
    \rhColor LS #(x11-color 'LightSalmon)
    \rhColor SAL #(x11-color 'LightSalmon)
    \rhColor SG #(x11-color 'SeaGreen)
    \rhColor SB #(x11-color 'SlateBlue)
    \rhColor MSB #(x11-color 'MediumSlateBlue)
    \rhColor CB #(x11-color 'CadetBlue)
    \rhColor CBCB #(x11-color 'CadetBlue)
    \rhColor LG #(x11-color 'LightGreen)
  \rhColor AA #(x11-color 'Red)
    \rhColor BB #(x11-color 'DarkOrange)
    \rhColor CC #(x11-color 'Gold2)
    \rhColor DD #(x11-color 'YellowGreen)
    \rhColor EE #(x11-color 'Green)
    \rhColor FF #(x11-color 'LimeGreen)
    \rhColor GG #(x11-color 'Cyan)
    \rhColor HH #(x11-color 'DarkCyan)
    \rhColor II #(x11-color 'LightSkyBlue)
    \rhColor JJ #(x11-color 'DodgerBlue)
    \rhColor KK #(x11-color 'Blue)
    \rhColor LL #(x11-color 'Purple)
    \rhColor MM #(x11-color 'Magenta)
    \rhColor NN #(x11-color 'Maroon)
    \rhColor OO #(x11-color 'DarkOrange2)
    \rhColor PP #(x11-color 'DeepPink2)
}
 %}

pinkRainbow = { % NEW Vowel Rainbow Pink
  %{ \rhColor Uw #(x11-color 'DeepPink2) %}
    %{ \rhColor Uh #(x11-color 'Red)
    \rhColor El #(x11-color 'Red)
    \rhColor Ow #(x11-color 'DarkOrange2)
    \rhColor Ao #(x11-color 'Gold2)
    \rhColor Oy #(x11-color 'LightSeaGreen)
    \rhColor Aa #(x11-color 'Yellow)
    \rhColor Aw #(x11-color 'RosyBrown)
    \rhColor Ah #(x11-color 'Khaki)
    \rhColor Ax #(x11-color 'PaleGoldenrod)
    \rhColor Er #(x11-color 'BurlyWood2)
    \rhColor Ay #(x11-color 'OliveDrab)
    \rhColor Ae #(x11-color 'YellowGreen)
    \rhColor Eh #(x11-color 'Green)
    \rhColor Ey #(x11-color 'MediumSpringGreen)
    \rhColor Ih #(x11-color 'Turquoise)
    \rhColor Iy #(x11-color 'DeepSkyBlue) %}

  \rhColor Uw #(x11-color 'DeepPink1)
    \rhColor Uwr #(x11-color 'Red3)
    \rhColor Uh #(x11-color 'Red)
    \rhColor Uhr #(x11-color 'Red3)
    \rhColor El #(x11-color 'Red)
    \rhColor Ow #(x11-color 'DarkOrange2)
    \rhColor Ao #(x11-color 'Gold2)
    \rhColor Aor #(x11-color 'Gold4)
    \rhColor Oy #(x11-color 'LightSeaGreen)
    \rhColor Aa #(x11-color 'Yellow1)
    \rhColor Aar #(x11-color 'Yellow3)
    \rhColor Aw #(x11-color 'RosyBrown)
    \rhColor Ah #(x11-color 'Khaki)
    \rhColor Ax #(x11-color 'PaleGoldenrod)
    \rhColor Er #(x11-color 'BurlyWood2)
    \rhColor Ay #(x11-color 'MediumSeaGreen)
    \rhColor Ae #(x11-color 'YellowGreen)
    \rhColor Eh #(x11-color 'Green)
    \rhColor Ehr #(x11-color 'Green3)
    \rhColor Ey #(x11-color 'MediumSpringGreen)
    \rhColor Eyr #(x11-color 'Green3)
    \rhColor Ih #(x11-color 'Turquoise)
    \rhColor Ihr #(x11-color 'Turquoise3)
    \rhColor Iy #(x11-color 'DeepSkyBlue)
    \rhColor Iyl #(x11-color 'DeepSkyBlue3)

  \rhColor UW #(x11-color 'DeepPink1)
    \rhColor UWL #(x11-color 'Red3)
    \rhColor UH #(x11-color 'Red)
    \rhColor UHR #(x11-color 'Red3)
    \rhColor EL #(x11-color 'Red2)
    \rhColor OW #(x11-color 'DarkOrange2)
    \rhColor OWL #(x11-color 'DarkOrange3)
    \rhColor AO #(x11-color 'Gold2)
    \rhColor AOR #(x11-color 'Gold4)
    \rhColor OY #(x11-color 'LightSeaGreen)
    \rhColor AA #(x11-color 'Yellow1)
    \rhColor AAR #(x11-color 'Yellow3)
    \rhColor AW #(x11-color 'RosyBrown)
    \rhColor AH #(x11-color 'Khaki)
    \rhColor AX #(x11-color 'PaleGoldenrod)
    \rhColor ER #(x11-color 'BurlyWood2)
    \rhColor AY #(x11-color 'MediumSeaGreen)
    \rhColor AE #(x11-color 'YellowGreen)
    \rhColor EH #(x11-color 'Green)
    \rhColor EHR #(x11-color 'Green3)
    \rhColor EY #(x11-color 'MediumSpringGreen)
    \rhColor EYR #(x11-color 'Green3)
    \rhColor IH #(x11-color 'Turquoise)
    \rhColor IHR #(x11-color 'Turquoise3)
    \rhColor IY #(x11-color 'DeepSkyBlue1)
    \rhColor IYL #(x11-color 'DeepSkyBlue3)
    \rhColor X #(x11-color 'WhiteSmoke)

    \rhColor Em #(x11-color 'GreenYellow)
    \rhColor En #(x11-color 'GreenYellow)
    \rhColor Eg #(x11-color 'GreenYellow)
    \rhColor EM #(x11-color 'GreenYellow)
    \rhColor EN #(x11-color 'GreenYellow)
    \rhColor EG #(x11-color 'GreenYellow)
  }

\version "2.22.0"

% We must create breathing signs as separate grobs, because
% otherwise they cut beams...

\include "define-grob.ily"
\include "switch-one-line.ily"

#(set-object-property! 'breathing-sign-offset 'backend-type? number?)

#(define (calc-breathing-sign-y-offset grob)
   (let* ((staff-symbol (ly:grob-object grob 'staff-symbol))
          (refpoint (ly:grob-common-refpoint grob staff-symbol Y))
          (staff-symbol-extent (ly:grob-extent staff-symbol refpoint Y))
          (additional-offset (ly:grob-property grob 'breathing-sign-offset)))
     (+ (interval-end staff-symbol-extent)
        additional-offset)))


#(define-grob!
   'CustomizedBreathingSign
   `((break-align-symbol . breathing-sign)
     (break-visibility . ,begin-of-line-invisible)
     (breathing-sign-offset . ,(switch-based-on-one-line 4 6))
     (font-size . 2)
     (direction . ,UP)
     (extra-spacing-height . (-inf.0 . +inf.0))
     (non-musical . #t)
     (space-alist . ((ambitus . (extra-space . 2.0))
                     (custos . (minimum-space . 1.0))
                     (key-signature . (minimum-space . 1.5))
                     (time-signature . (minimum-space . 1.5))
                     (staff-bar . (minimum-space . 1.5))
                     (clef . (minimum-space . 2.0))
                     (cue-clef . (minimum-space . 2.0))
                     (cue-end-clef . (minimum-space . 2.0))
                     (first-note . (fixed-space . 1.0)) ;huh?
                     (right-edge . (extra-space . 0.1))))
     (stencil . ,ly:text-interface::print)
     (text . ,(make-musicglyph-markup "scripts.rcomma"))
     (Y-offset . ,calc-breathing-sign-y-offset)
     (Y-extent . ,grob::always-Y-extent-from-stencil)
     (meta . ((class . Item)
              (interfaces . (break-aligned-interface
                             font-interface
                             outside-staff-interface
                             text-interface))))))
   
#(define (Customized_breathing_sign_engraver context)
   (let ((breathing-event #f))
     (make-engraver
       (listeners
         ((breathing-event engraver event)
            (set! breathing-event event)))
       ((process-music engraver)
          (if breathing-event
              (ly:engraver-make-grob engraver 'CustomizedBreathingSign breathing-event)))
       ((stop-translation-timestep engraver)
          (set! breathing-event #f)))))


\layout {
  \context {
    \Global
    \grobdescriptions #all-grob-descriptions
  }
  \context {
    \Voice
    \remove Breathing_sign_engraver
    \consists #Customized_breathing_sign_engraver
  }
}
\version "2.22.0"

#(define (Handle_phantom_note_head_engraver context)
   (make-engraver
     (acknowledgers
       ((note-head-interface engraver grob source-engraver)
          (let* ((cause (event-cause grob))
                 ; The meaning of these two properties is: if is-suppressed
                 ; is true, hide the note head; if is-phantom is true, move
                 ; it to extremal position. It is assumed that is-phantom
                 ; implies is-suppressed.
                 (is-suppressed (ly:event-property cause 'is-suppressed))
                 (is-phantom (ly:event-property cause 'is-phantom)))

            (if is-suppressed
                (ly:grob-set-property! grob 'stencil point-stencil))
            (if is-phantom
                (ly:grob-set-property!
                  grob
                  'staff-position
                  (ly:grob-property
                    (ly:grob-object grob 'staff-symbol)
                    'line-count))))))))

\layout {
  \context {
    \Staff
    \consists #Handle_phantom_note_head_engraver
  }
}
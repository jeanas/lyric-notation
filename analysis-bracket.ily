\version "2.22.0"

tb =
  \tweak TupletNumber.font-size #-3.75
  \tweak TupletNumber.font-series #'bold
  \tweak TupletNumber.layer #4
  %{ \tweak TupletNumber.whiteout-style #'outline %}
  %{ \tweak TupletNumber.whiteout ##t %}
  \tweak TupletNumber.direction #1
  \tweak TupletBracket.staff-padding #3
  \tweak TupletBracket.layer #3
  %{ \tweak TupletBracket.shorten-pair #'(0 . 0) %}
  %{ \tweak TupletBracket.outside-staff-priority #1101 %}
  %{ \tweak TupletBracket.edge-height #'(0.5 . 0.5) %}
  \tweak TupletBracket.bracket-visibility ##t
  \tweak TupletBracket.direction #UP
  %{ \tweak TupletNumber.stencil ##f %}
  \tweak TupletBracket.stencil ##f
  \tuplet 1/1 \etc

tB =
  %{ \tweak TupletNumber.font-size #-3.75 %}
  %{ \tweak TupletNumber.font-series #'bold %}
  \tweak TupletNumber.layer #4
  %{ \tweak TupletNumber.staff-padding #4 %}
  %{ \tweak TupletNumber.whiteout-style #'outline %}
  %{ \tweak TupletNumber.whiteout ##t %}
  \tweak TupletNumber.direction #1
  \tweak TupletBracket.staff-padding #3
  \tweak TupletBracket.layer #3
  %{ \tweak TupletBracket.shorten-pair #'(0 . 0) %}
  %{ \tweak TupletBracket.outside-staff-priority #1101 %}
  \tweak TupletBracket.connect-to-neighbor #'(#t . #t)
  %{ \tweak TupletBracket.tuplet-slur ##t %}
  \tweak TupletBracket.bracket-visibility ##t
  \tweak TupletBracket.direction #UP
  %{ \tweak TupletNumber.stencil ##f %}
  \tweak TupletBracket.stencil ##f
  \tuplet 1/1 \etc

domTup =
#(define-music-function
     (note)
     (ly:music?)
   #{
       \tweak TupletNumber.font-size #-3.75

       \tweak TupletNumber.font-series #'bold

       \tweak TupletNumber.layer #4
       %{ \tweak TupletNumber.whiteout-style #'outline %}
       %{ \tweak TupletNumber.whiteout ##t %}
       \tweak TupletNumber.direction #1
       \tweak TupletBracket.staff-padding #3
       \tweak TupletBracket.layer #3
       %{ \tweak TupletBracket.shorten-pair #'(0 . 0) %}
       %{ \tweak TupletBracket.outside-staff-priority #1101 %}
       %{ \tweak TupletBracket.edge-height #'(0.5 . 0.5) %}
       \tweak TupletBracket.bracket-visibility ##t
       \tweak TupletBracket.direction #UP
       %{ \tweak TupletNumber.stencil ##f %}
       \tuplet 1/1
        #note
   #})


%%% Bracket Functions
dB =
  #(define-music-function
   (string tuplet)
   (string? ly:music?)
  #{
    %{ #note %}
    %{ \tweak TupletBracket.shorten-pair #'(-0.5 . -0.5) %}
    %{ \tweak TupletNumber.font-size #-3.75 %}
    %{ \tweak TupletNumber.font-series #'bold %}
    %{ \tweak TupletNumber.layer #4 %}
    %{ \tweak TupletNumber.whiteout-style #'outline %}
    %{ \tweak TupletNumber.whiteout ##t %}
    %{ \tweak TupletNumber.direction #-1 %}
    %{ \tweak TupletNumber.padding #2 %}
    %{ \tweak TupletNumber.padding #'() %}
    %{ \tweak TupletNumber.staff-padding #2 %}
    %{ \tweak TupletNumber.staff-padding #'() %}
    %{ \tweak TupletNumber.outside-staff-padding #2 %}
    %{ \tweak TupletNumber.outside-staff-padding #'() %}
    %{ \tweak TupletBracket.direction #DOWN %}
    %{ \tweak TupletBracket.stencil ##f %}
    %{ \tweak TupletBracket.tuplet-slur ##t %}
    %{ \tweak TupletBracket.outside-staff-priority #1100 %}
    \tweak TupletBracket.padding #3
    %{ \tweak TupletBracket.staff-padding #5 %}
    %{ \tweak TupletBracket.outside-staff-padding #5 %}
    %{ \tweak TupletBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ \tweak TupletBracket.connect-to-neighbor #'(#f . #f) %}
    %{ \tweak TupletBracket.edge-height #'(0.6 . 0.6) %}
    \tweak TupletNumber.text #string
    %{ \tb %}
    #tuplet
    #})
% domain bracket
db =
  #(define-music-function
   (string tuplet)
   (string? ly:music?)
  #{
    %{ #note %}
    %{ -\tweak HorizontalBracket.shorten-pair #'(-1 . -1) %}
    %{ -\tweak HorizontalBracketText.font-size #-3.75 %}
    %{ -\tweak HorizontalBracketText.font-series #'bold %}
    %{ -\tweak HorizontalBracketText.layer #4 %}
    %{ -\tweak HorizontalBracketText.whiteout-style #'outline %}
    %{ -\tweak HorizontalBracketText.whiteout ##t %}
    %{ -\tweak HorizontalBracketText.direction #-1 %}
    %{ -\tweak HorizontalBracketText.padding #0.2 %}
    %{ -\tweak HorizontalBracketText.padding #'() %}
    %{ -\tweak HorizontalBracketText.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.staff-padding #'() %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #'() %}
    %{ -\tweak HorizontalBracket.direction #DOWN %}
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #2 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ -\tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.6 . 0.6) %}
    \tweak TupletNumber.text #string
    %{ \tb %}
    #tuplet
    #})
bSet =
  {
    %{ \override Score.HorizontalBracket.direction = #UP %}
    \override Score.HorizontalBracket.outside-staff-priority = #1150
    %{ \override Score.HorizontalBracket.padding = #0.2 %}
    \override Score.HorizontalBracket.padding = #0.5
    %{ \override Score.HorizontalBracket.staff-padding = #0.2 %}
    \override Score.HorizontalBracket.staff-padding = #15
    \override Score.HorizontalBracket.outside-staff-padding = #15
    %{ \override Score.HorizontalBracket.outside-staff-padding = #'() %}

    %{ \override Score.HorizontalBracket.shorten-pair = #'(0 . 0) %}
    \override Score.HorizontalBracket.bracket-flare = #'(0.5 . 0.5)
    %{ \override Score.HorizontalBracket.connect-to-neighbor = #'(#f . #f) %}
    \override Score.HorizontalBracket.edge-height = #'(0.6 . 0.6)
    \override Score.HorizontalBracket.layer = #5

    \override Score.HorizontalBracketText.font-size = #-3
    \override Score.HorizontalBracketText.font-series = #'bold
    \override Score.HorizontalBracketText.layer = #6
    \override Score.HorizontalBracketText.whiteout-style = #'box
    \override Score.HorizontalBracketText.whiteout = ##t
    \override Score.HorizontalBracketText.direction = #0

    %{ \override Score.HorizontalBracketText.padding = #0.2 %}
    \override Score.HorizontalBracketText.padding = #'()
    %{ \override Score.HorizontalBracketText.staff-padding = #0.2 %}
    \override Score.HorizontalBracketText.staff-padding = #'()
    %{ \override Score.HorizontalBracketText.outside-staff-padding = #0.2 %}
    \override Score.HorizontalBracketText.outside-staff-padding = #'()
 }
lb =
  #(define-music-function
   (string note)
   (string? ly:music?)
  #{
    #note
    -\tweak HorizontalBracketText.Y-offset #-0.75
    -\tweak HorizontalBracket.shorten-pair #'(-2 . -0.5)
    -\tweak HorizontalBracket.shorten-pair #'(-5 . -1.5)
    %{ -\tweak HorizontalBracketText.font-size #-3.75 %}
    %{ -\tweak HorizontalBracketText.font-series #'bold %}
    %{ -\tweak HorizontalBracketText.layer #4 %}
    -\tweak HorizontalBracketText.whiteout-style #'outline
    %{ -\tweak HorizontalBracketText.whiteout ##t %}
    %{ -\tweak HorizontalBracketText.direction #-1 %}
    %{ -\tweak HorizontalBracketText.padding #0.2 %}
    %{ -\tweak HorizontalBracketText.padding #'() %}
    %{ -\tweak HorizontalBracketText.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.staff-padding #'() %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #'() %}
    %{ -\tweak HorizontalBracket.direction #DOWN %}
    %{ -\tweak HorizontalBracketText.direction #CENTER %}
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #2 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ -\tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.6 . 0.6) %}
    -\tweak HorizontalBracketText.text #string
    \startGroup
    #})
lB =
  #(define-music-function
   (string note)
   (string? ly:music?)
  #{
    #note
    -\tweak HorizontalBracketText.Y-offset #-0.75
    -\tweak HorizontalBracket.shorten-pair #'(-2 . -0.5)
    %{ -\tweak HorizontalBracketText.font-size #-3.75 %}
    %{ -\tweak HorizontalBracketText.font-series #'bold %}
    %{ -\tweak HorizontalBracketText.layer #4 %}
    %{ -\tweak HorizontalBracketText.whiteout-style #'outline %}
    %{ -\tweak HorizontalBracketText.whiteout ##t %}
    %{ -\tweak HorizontalBracketText.direction #-1 %}
    %{ -\tweak HorizontalBracketText.padding #'() %}
    %{ -\tweak HorizontalBracketText.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.staff-padding #'() %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #'() %}
    -\tweak HorizontalBracket.direction #DOWN
    -\tweak HorizontalBracketText.direction #0
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #2 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ -\tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.6 . 0.6) %}
    -\tweak HorizontalBracketText.text #string
    \startGroup
    #})
lBB =
  #(define-music-function
   (string note)
   (string? ly:music?)
  #{
    %{ \once \override Score.HorizontalBracket.Y-offset #'() %} %}
    #note
    -\tweak HorizontalBracketText.Y-offset #0.2
    %{ -\tweak HorizontalBracket.Y-offset #'() %}
    -\tweak HorizontalBracket.shorten-pair #'(-2 . -0.5)
    %{ -\tweak HorizontalBracketText.font-size #-3.75 %}
    %{ -\tweak HorizontalBracketText.font-series #'bold %}
    %{ -\tweak HorizontalBracketText.layer #4 %}
    %{ -\tweak HorizontalBracketText.whiteout-style #'outline %}
    %{ -\tweak HorizontalBracketText.whiteout ##t %}
    %{ -\tweak HorizontalBracketText.direction #-1 %}
    %{ -\tweak HorizontalBracketText.padding #'() %}
    %{ -\tweak HorizontalBracketText.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.staff-padding #'() %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #'() %}
    -\tweak HorizontalBracket.direction #DOWN
    -\tweak HorizontalBracketText.direction #0
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #2 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ -\tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.6 . 0.6) %}
    -\tweak HorizontalBracketText.text #string
    \startGroup
    #})

%%% SETTINGS DEFINITIONS
% Make Slurs break horizontal
#(define (horizontalise-broken-slurs grob)
    (let*
     ((orig (ly:grob-original grob))
      (siblings (if (ly:grob? orig)
                    (ly:spanner-broken-into orig)
                    '()))
      (control-points (ly:grob-property grob 'control-points)))

     (if (>= (length siblings) 2)
         (let ((new-control-points
                (cond
                 ((eq? (first siblings) grob)
                  (list (first control-points)
                        (second control-points)
                        (third control-points)
                        (third control-points)))
                 ((eq? (last siblings) grob)
                  (list (second control-points)
                        (second control-points)
                        (third control-points)
                        (fourth control-points)))
                 (else control-points))))
           (ly:grob-set-property! grob 'control-points
           new-control-points)))))

%% HorizontalBracket SETTINGS
% Places bracket labels before line breaks
bracketSettings = {
    \override Score.HorizontalBracket.direction = #DOWN
    \override Score.HorizontalBracket.layer = #6
    \override Score.HorizontalBracket.outside-staff-priority = #1100
    %{ \override Score.HorizontalBracket.padding = #0.2 %}
    \override Score.HorizontalBracket.staff-padding = #'()
    \override Score.HorizontalBracket.outside-staff-padding = #'()
    \override Score.HorizontalBracket.shorten-pair = #'(-0.5 . -0.5)
    \override Score.HorizontalBracket.bracket-flare = #'(0.5 . 0.5)
    %{ \override Score.HorizontalBracket.connect-to-neighbor = #'(#f . #f) %}
    \override Score.HorizontalBracket.edge-height = #'(0.5 . 0.5)
    \override Score.HorizontalBracket.avoid-scripts = ##f

    \override Score.HorizontalBracketText.font-size = #-3
    \override Score.HorizontalBracketText.font-series = #'bold
    \override Score.HorizontalBracketText.layer = #6
    \override Score.HorizontalBracketText.whiteout-style = #'box
    \override Score.HorizontalBracketText.whiteout = ##t
    %{ \override Score.HorizontalBracketText.direction = #-1 %}
    %{ \override Score.HorizontalBracketText.padding = #0.2 %}
    \override Score.HorizontalBracketText.padding = #'()
    %{ \override Score.HorizontalBracketText.staff-padding = #0.2 %}
    \override Score.HorizontalBracketText.staff-padding = #'()
    %{ \override Score.HorizontalBracketText.outside-staff-padding = #'() %}
    %{ \override Score.HorizontalBracketText.outside-staff-padding = #0.2 %}
 }

oneLineBracketSettings = {
    \override Score.HorizontalBracket.direction = #DOWN
    \override Score.HorizontalBracket.layer = #4
    \override Score.HorizontalBracket.outside-staff-priority = #1100
    %{ \override Score.HorizontalBracket.padding = #0.2 %}
    \override Score.HorizontalBracket.staff-padding = #2.5
    \override Score.HorizontalBracket.outside-staff-padding = #0.5
    \override Score.HorizontalBracket.shorten-pair = #'(-1.5 . -1.5)
    \override Score.HorizontalBracket.bracket-flare = #'(0.9 . 0.9)
    %{ \override Score.HorizontalBracket.connect-to-neighbor = #'(#f . #f) %}
    \override Score.HorizontalBracket.edge-height = #'(0.4 . 0.4)
    \override Score.HorizontalBracket.avoid-scripts = ##t

    \override Score.HorizontalBracketText.font-size = #-8
    %{ \override Score.HorizontalBracketText.stencil = ##f %}
    \override Score.HorizontalBracketText.font-series = #'bold
    \override Score.HorizontalBracketText.layer = #4
    \override Score.HorizontalBracketText.whiteout-style = #'outline
    \override Score.HorizontalBracketText.whiteout = ##t
    %{ \override Score.HorizontalBracketText.direction = #-1 %}
    %{ \override Score.HorizontalBracketText.padding = #0.2 %}
    \override Score.HorizontalBracketText.padding = #'()
    %{ \override Score.HorizontalBracketText.staff-padding = #0.2 %}
    \override Score.HorizontalBracketText.staff-padding = #'()
    \override Score.HorizontalBracketText.outside-staff-padding = #0.2
    %{ \override Score.HorizontalBracketText.outside-staff-padding = #0.2 %}
 }


onceBracketTextBeforeBreak = {
    \once \override Score.HorizontalBracketText.after-line-breaking =
    #(lambda (grob)
       (if (not-first-broken-spanner? grob)
           (ly:grob-set-property! grob 'stencil '())))
         }
% Places bracket labels after line breaks
onceBracketTextAfterBreak = {
    \once \override Score.HorizontalBracketText.after-line-breaking =
    #(lambda (grob)
       (if (first-broken-spanner? grob)
           (ly:grob-set-property! grob 'stencil '())))
         }
bracketTextBeforeBreak = {
    \override Score.HorizontalBracketText.after-line-breaking =
    #(lambda (grob)
       (if (not-first-broken-spanner? grob)
           (ly:grob-set-property! grob 'stencil '())))
         }
% Places bracket labels after line breaks
bracketTextAfterBreak = {
    \override Score.HorizontalBracketText.after-line-breaking =
    #(lambda (grob)
       (if (first-broken-spanner? grob)
           (ly:grob-set-property! grob 'stencil '())))
         }

% Groove Tuplets Settings
grooveUnitSettings =
  %{ \tweak TupletBracket.outside-staff-priority #1100 %}
  \tweak TupletNumber.font-series #'bold
  \tweak TupletNumber.font-shape #'upright
  %{ \tweak TupletNumber.font-size #-4 %}
  %{ \tweak TupletNumber.whiteout ##f %}
  %{ \tweak TupletNumber.whiteout-style #'outline %}
  %{ \tweak TupletNumber.layer #1 %}
  %{ \tweak TupletBracket.layer #1 %}
  %{ \tweak TupletBracket.direction #up %}
  %{ \tweak TupletBracket.edge-height #'(0.4 . 0.4) %}
  %{ \tweak TupletBracket.thickness #1 %}
  %{ \tweak TupletBracket.padding #0.15 %}
  %{ \tweak TupletBracket.staff-padding #3.5 %}
  %{ \tweak TupletBracket.outside-staff-padding #0.15 %}
  \tweak TupletBracket.shorten-pair #'(0 . 1)
  \tweak TupletBracket.avoid-scripts ##t
  %{ \tweak TupletBracket.full-length-to-extent ##t %}
  \etc
% Groove brackets under the staff
grooveUnitSettingsDown =
  %{ \tweak TupletBracket.outside-staff-priority #1100 %}
  \tweak TupletNumber.font-series #'bold
  \tweak TupletNumber.font-shape #'upright
  %{ \tweak TupletNumber.font-size #-4 %}
  %{ \tweak TupletNumber.whiteout ##f %}
  %{ \tweak TupletNumber.whiteout-style #'outline %}
  %{ \tweak TupletNumber.layer #1 %}
  %{ \tweak TupletBracket.layer #1 %}
  \tweak TupletBracket.direction #DOWN
  %{ \tweak TupletBracket.edge-height #'(0.4 . 0.4) %}
  %{ \tweak TupletBracket.thickness #1 %}
  %{ \tweak TupletBracket.padding #0.15 %}
  %{ \tweak TupletBracket.staff-padding #3.5 %}
  %{ \tweak TupletBracket.outside-staff-padding #0.15 %}
  \tweak TupletBracket.shorten-pair #'(0 . 1)
  \tweak TupletBracket.avoid-scripts ##t
  \etc

% Groove Settings for inside lyricmode expression
generalGrooveSettings = {
 \set Score.tupletFullLength = ##t
 \override Score.TupletBracket.outside-staff-priority = #1100
  \override Score.TupletNumber.font-series = #'bold
  \override Score.TupletNumber.font-shape = #'upright
  \override Score.TupletNumber.font-size = #-4
  \override Score.TupletNumber.whiteout = ##t
  \override Score.TupletNumber.whiteout-style = #'outline
  \override Score.TupletNumber.layer = #1
  \override Score.TupletBracket.layer = #1
  \override Score.TupletBracket.direction = #up
  \override Score.TupletBracket.edge-height = #'(0.5 . 0.5)
  \override Score.TupletBracket.thickness = #1
  \override Score.TupletBracket.padding = #0.15
  \override Score.TupletBracket.staff-padding = #3.5
  \override Score.TupletBracket.outside-staff-padding = #0.15
  \override Score.TupletBracket.avoid-scripts = ##t
}
% alternante settings
oneLineGrooveSettings = {
 \set Score.tupletFullLength = ##t
 \override Score.TupletBracket.outside-staff-priority = #1100
  \override Score.TupletNumber.font-series = #'bold
  \override Score.TupletNumber.font-shape = #'upright
  \override Score.TupletNumber.font-size = #-4
  \override Score.TupletNumber.whiteout = ##t
  \override Score.TupletNumber.whiteout-style = #'outline
  \override Score.TupletNumber.layer = #1
  \override Score.TupletBracket.layer = #1
  \override Score.TupletBracket.direction = #up
  \override Score.TupletBracket.edge-height = #'(0.5 . 0.5)
  \override Score.TupletBracket.thickness = #1
  \override Score.TupletBracket.padding = #0.15
  \override Score.TupletBracket.staff-padding = #3.5
  \override Score.TupletBracket.outside-staff-padding = #0.15
  \override Score.TupletBracket.avoid-scripts = ##t
}
grooveTupletTextBeforeBreak = {
 \override TupletNumber.after-line-breaking =
 #(lambda (grob)
    (if (not-first-broken-spanner? grob)
        (ly:grob-set-property! grob 'stencil '())))
  }

grooveTupletTextAfterBreak = {
 \override TupletNumber.after-line-breaking =
 #(lambda (grob)
    (if (first-broken-spanner? grob)
        (ly:grob-set-property! grob 'stencil '())))
  }

% Labeled Bracket
elb =
  #(define-music-function
   (string note)
   (string? ly:music?)
  #{
    #note
    %{ -\tweak HorizontalBracket.shorten-pair #'(-1 . -1) %}
    %{ -\tweak HorizontalBracketText.font-size #-3.75 %}
    %{ -\tweak HorizontalBracketText.font-series #'bold %}
    %{ -\tweak HorizontalBracketText.layer #4 %}
    -\tweak HorizontalBracketText.whiteout-style #'outline
    %{ -\tweak HorizontalBracketText.whiteout ##t %}
    %{ -\tweak HorizontalBracketText.direction #-1 %}
    %{ -\tweak HorizontalBracketText.padding #0.2 %}
    %{ -\tweak HorizontalBracketText.padding #'() %}
    %{ -\tweak HorizontalBracketText.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.staff-padding #'() %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #'() %}
    %{ -\tweak HorizontalBracket.direction #DOWN %}
    %{ -\tweak HorizontalBracketText.direction #CENTER %}
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #2 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ -\tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.6 . 0.6) %}
    -\tweak HorizontalBracketText.text #string
    \startGroup
    #})
eLB =
  #(define-music-function
   (string note)
   (string? ly:music?)
  #{
    #note
    %{ -\tweak HorizontalBracket.shorten-pair #'(-1 . -1) %}
    %{ -\tweak HorizontalBracketText.font-size #-3.75 %}
    %{ -\tweak HorizontalBracketText.font-series #'bold %}
    %{ -\tweak HorizontalBracketText.layer #4 %}
    -\tweak HorizontalBracketText.whiteout-style #'box
    %{ -\tweak HorizontalBracketText.whiteout ##t %}
    %{ -\tweak HorizontalBracketText.direction #-1 %}
    %{ -\tweak HorizontalBracketText.padding #0.2 %}
    %{ -\tweak HorizontalBracketText.padding #'() %}
    %{ -\tweak HorizontalBracketText.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.staff-padding #'() %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #0.2 %}
    %{ -\tweak HorizontalBracketText.outside-staff-padding #'() %}
    -\tweak HorizontalBracket.direction #DOWN
    -\tweak HorizontalBracketText.direction #DOWN
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #0.2 %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #2 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ -\tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.6 . 0.6) %}
    -\tweak HorizontalBracketText.text #string
    \startGroup
    #})


% Basic, unlabeled Bracket
bB =
  #(define-music-function
   (note)
   (ly:music?)
  #{
    #note
    -\tweak HorizontalBracket.shorten-pair #'(-1.5 . -1.5)
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.direction #DOWN %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    -\tweak HorizontalBracket.staff-padding #1.5
    %{ -\tweak HorizontalBracket.outside-staff-padding #0 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ -\tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.5 . 0.5) %}
    \startGroup
    #})
% overlapping brackets
oB =
  #(define-music-function
   (note)
   (ly:music?)
  #{
    #note
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.direction #DOWN %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #'() %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #0 %}
    -\tweak HorizontalBracket.shorten-pair #'(-8.5 . -1.5)
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ \tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.5 . 0.5) %}
    \startGroup
    #})
% Asymmetrical overlapping bracket pair
oBa =
  #(define-music-function
   (note)
   (ly:music?)
  #{
    #note
    -\tweak HorizontalBracket.shorten-pair #'(-1.5 . -3.75)
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.direction #DOWN %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #2 %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #0 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ \tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.5 . 0.5) %}
    \startGroup
    #})
oBb =
  #(define-music-function
   (note)
   (ly:music?)
  #{
    #note
    -\tweak HorizontalBracket.shorten-pair #'(-0.5 . -1)
    %{ -\tweak HorizontalBracket.outside-staff-priority #1100 %}
    %{ -\tweak HorizontalBracket.direction #DOWN %}
    %{ -\tweak HorizontalBracket.padding #0.2 %}
    %{ -\tweak HorizontalBracket.staff-padding #2 %}
    %{ -\tweak HorizontalBracket.outside-staff-padding #0 %}
    %{ -\tweak HorizontalBracket.bracket-flare #'(0.2 . 0.2) %}
    %{ \tweak HorizontalBracket.connect-to-neighbor #'(#f . #f) %}
    %{ -\tweak HorizontalBracket.edge-height #'(0.5 . 0.5) %}
    \startGroup
    #})


% end bracket
eB =
  #(define-music-function
   (note)
   (ly:music?)
  #{
    #note
    \stopGroup
    #})
eb =
  #(define-music-function
   (note)
   (ly:music?)
  #{
    #note
    \stopGroup
    #})

%%% Groove tuplet Functions
gTwo =

  \grooveUnitSettings
  \tweak TupletNumber.text #(tuplet-number::non-default-tuplet-denominator-text 2)
  \tuplet 1/1 \etc
gTre =
  \grooveUnitSettings
  \tweak TupletNumber.text #(tuplet-number::non-default-tuplet-denominator-text 3)
  \tuplet 1/1 \etc

gClass =
  #(define-music-function
   (string note)
   (string? ly:music?)
  #{
      %{ \tweak TupletNumber.font-series #'bold %}
      %{ \tweak TupletNumber.font-shape #'upright %}
      %{ \tweak TupletNumber.stencil ##t %}
      %{ \tweak TupletNumber.font-size  #-4 %}
      \tweak TupletNumber.whiteout  ##f
      \tweak TupletNumber.whiteout-style  #'outline
      \tweak TupletBracket.layer #2
      \tweak TupletBracket.outside-staff-priority #1200
      \tweak TupletBracket.direction #up
      \tweak TupletBracket.edge-height #'(0.5 . 0.5)
      %{ \tweak TupletBracket.to-barline ##t %}
      \tweak TupletBracket.shorten-pair #'(0.0 . 1)
      \tweak TupletBracket.thickness #1.6
      \tweak TupletBracket.padding #0
      %{ \tweak TupletBracket.staff-padding #-2 %}
      \tweak TupletBracket.outside-staff-padding #0.2
    \tweak TupletNumber.text #string
    \tuplet 1/1
    #note
    #})


gTwoD =
  %{ \tweak TupletBracket.outside-staff-priority #1100
  \tweak TupletNumber.font-series #'bold
  \tweak TupletNumber.font-shape #'upright
  \tweak TupletNumber.font-size #-4
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.layer #1
  \tweak TupletBracket.layer #1
  \tweak TupletBracket.direction #up
  \tweak TupletBracket.edge-height #'(0.4 . 0.4)
  \tweak TupletBracket.thickness #1
  \tweak TupletBracket.padding #0
  \tweak TupletBracket.staff-padding #1
  \tweak TupletBracket.shorten-pair #'(0 . 1) %}
  \grooveUnitSettingsDown
  \tweak TupletNumber.text #(tuplet-number::non-default-tuplet-denominator-text 2)
  \tuplet 1/1 \etc
gTreD =
  %{ \tweak TupletBracket.outside-staff-priority #1100
  \tweak TupletNumber.font-series #'BOLD
  \tweak TupletNumber.font-shape #'upright
  \tweak TupletNumber.font-size #-4
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.layer #1
  \tweak TupletBracket.layer #1
  \tweak TupletBracket.direction #up
  \tweak TupletBracket.edge-height #'(0.4 . 0.4)
  \tweak TupletBracket.thickness #1
  \tweak TupletBracket.padding #0.6
  \tweak TupletBracket.shorten-pair #'(0 . 1) %}
  \grooveUnitSettingsDown
  \tweak TupletNumber.text #(tuplet-number::non-default-tuplet-denominator-text 3)
  \tuplet 1/1 \etc

gClassD =
  #(define-music-function
   (string note)
   (string? ly:music?)
  #{
      \tweak TupletNumber.font-series #'bold
      \tweak TupletNumber.font-shape #'upright
      %{ \tweak TupletNumber.stencil ##t %}
      %{ \tweak TupletNumber.font-size  #-4 %}
      \tweak TupletNumber.whiteout  ##t
      \tweak TupletNumber.whiteout-style  #'outline
      \tweak TupletBracket.layer #2
      \tweak TupletBracket.outside-staff-priority #1200
      \tweak TupletBracket.direction #DOWN
      \tweak TupletBracket.edge-height #'(0.5 . 0.5)
      %{ \tweak TupletBracket.to-barline ##t %}
      \tweak TupletBracket.shorten-pair #'(0.0 . 1)
      \tweak TupletBracket.thickness #1.6
      \tweak TupletBracket.padding #0
      %{ \tweak TupletBracket.staff-padding #-2 %}
      \tweak TupletBracket.outside-staff-padding #0.2
    \tweak TupletNumber.text #string
    \tuplet 1/1
    #note
    #})

% Smaller 32nd note units, instead of 16ths
microGrooveUnitSettings =
  \tweak TupletBracket.outside-staff-priority #1050
  %{ \tweak TupletNumber.font-series #'bold %}
  %{ \tweak TupletNumber.font-shape #'upright %}
  \tweak TupletNumber.font-size #-5
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.layer #1
  \tweak TupletBracket.layer #1
  \tweak TupletBracket.direction #up
  \tweak TupletBracket.edge-height #'(0.3 . 0.3)
  \tweak TupletBracket.thickness #.75
  \tweak TupletBracket.padding #0.1
  \tweak TupletBracket.staff-padding #3
  \tweak TupletBracket.outside-staff-padding #0.1
  \tweak TupletBracket.shorten-pair #'(0 . 1)
  \tweak TupletBracket.avoid-scripts ##t
  \etc
microGrooveUnitSettingsDown =
  \tweak TupletBracket.outside-staff-priority #1050
  %{ \tweak TupletNumber.font-series #'bold %}
  %{ \tweak TupletNumber.font-shape #'upright %}
  \tweak TupletNumber.font-size #-5
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.layer #1
  \tweak TupletBracket.layer #1
  \tweak TupletBracket.direction #down
  \tweak TupletBracket.edge-height #'(0.3 . 0.3)
  \tweak TupletBracket.thickness #.75
  \tweak TupletBracket.padding #0.1
  \tweak TupletBracket.staff-padding #3
  \tweak TupletBracket.outside-staff-padding #0.1
  \tweak TupletBracket.shorten-pair #'(0 . 1)
  \tweak TupletBracket.avoid-scripts ##t
  \etc


mGTwo =
  %{ \tweak TupletBracket.outside-staff-priority #1100
  \tweak TupletNumber.font-series #'bold
  \tweak TupletNumber.font-shape #'upright
  \tweak TupletNumber.font-size #-4
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.layer #1
  \tweak TupletBracket.layer #1
  \tweak TupletBracket.direction #up
  \tweak TupletBracket.edge-height #'(0.4 . 0.4)
  \tweak TupletBracket.thickness #1
  \tweak TupletBracket.padding #0
  \tweak TupletBracket.staff-padding #1
  \tweak TupletBracket.shorten-pair #'(0 . 1) %}
  \grooveUnitSettings
  \tweak TupletNumber.text #(tuplet-number::non-default-tuplet-denominator-text 2)
  \tuplet 1/1 \etc
mGTre =
  %{ \tweak TupletBracket.outside-staff-priority #1100
  \tweak TupletNumber.font-series #'BOLD
  \tweak TupletNumber.font-shape #'upright
  \tweak TupletNumber.font-size #-4
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.layer #1
  \tweak TupletBracket.layer #1
  \tweak TupletBracket.direction #up
  \tweak TupletBracket.edge-height #'(0.4 . 0.4)
  \tweak TupletBracket.thickness #1
  \tweak TupletBracket.padding #0.6
  \tweak TupletBracket.shorten-pair #'(0 . 1) %}
  \grooveUnitSettings
  \tweak TupletNumber.text #(tuplet-number::non-default-tuplet-denominator-text 3)
  \tuplet 1/1 \etc
mGTwoD =
  %{ \tweak TupletBracket.outside-staff-priority #1100
  \tweak TupletNumber.font-series #'bold
  \tweak TupletNumber.font-shape #'upright
  \tweak TupletNumber.font-size #-4
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.layer #1
  \tweak TupletBracket.layer #1
  \tweak TupletBracket.direction #up
  \tweak TupletBracket.edge-height #'(0.4 . 0.4)
  \tweak TupletBracket.thickness #1
  \tweak TupletBracket.padding #0
  \tweak TupletBracket.staff-padding #1
  \tweak TupletBracket.shorten-pair #'(0 . 1) %}
  \grooveUnitSettingsDown
  \tweak TupletNumber.text #(tuplet-number::non-default-tuplet-denominator-text 2)
  \tuplet 1/1 \etc
mGTreD =
  %{ \tweak TupletBracket.outside-staff-priority #1100
  \tweak TupletNumber.font-series #'BOLD
  \tweak TupletNumber.font-shape #'upright
  \tweak TupletNumber.font-size #-4
  \tweak TupletNumber.whiteout ##t
  \tweak TupletNumber.whiteout-style #'outline
  \tweak TupletNumber.layer #1
  \tweak TupletBracket.layer #1
  \tweak TupletBracket.direction #up
  \tweak TupletBracket.edge-height #'(0.4 . 0.4)
  \tweak TupletBracket.thickness #1
  \tweak TupletBracket.padding #0.6
  \tweak TupletBracket.shorten-pair #'(0 . 1) %}
  \grooveUnitSettingsDown
  \tweak TupletNumber.text #(tuplet-number::non-default-tuplet-denominator-text 3)
  \tuplet 1/1 \etc

\version "2.22.0"

% No (parenthesized) bar number for the pickup.
#(define (custom-bar-number-visibility barnum measure-position)
   (<= 0 (ly:moment-main measure-position)))

\layout {
  \context {
    \Score
    \remove Bar_number_engraver
  }
  \context {
    \Staff
    % FIXME: why the hell such an inconsistent space
    % at start of line when the engraver is in Score?
    \consists Bar_number_engraver
    barNumberVisibility = #custom-bar-number-visibility
    \override BarNumber.font-shape = #'italic
    \override BarNumber.X-offset = -1.4
    % The support staff ends early enough that bar numbers
    % collide with instrument names without this.  This same
    % hack is used for default BarNumber settings (but there
    % is less horizon-padding, so it doesn't suffice).
    \override BarNumber.horizon-padding = 10000
    \override BarNumber.padding = 0.5
  }
}
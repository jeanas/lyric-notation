\version "2.22.0"

% This is to prevent a2 in
% \rh a2
% from being interpreted as an A note.
% We never want direct pitch input
% anyway.

#(set!
   language-pitch-names
   (acons
     'nopitches
     ; The test in define-note-names.scm is pair?,
     ; so empty languages are not appreciated.
     `((DUMMY-PITCH . ,(ly:make-pitch -1 0 NATURAL)))
     language-pitch-names))

\language nopitches
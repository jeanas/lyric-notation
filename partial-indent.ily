\version "2.22.0"

#(define (Partial_indent_engraver context)
   ; Apparently PartialSet events aren't broadcast
   ; anyhow but treated by an iterator instead of
   ; an engraver. Anyway, we can recognize a partial
   ; through a negative measure position at the start
   ; of the piece.
   ;
   ; Also, there isn't yet an interface to get the
   ; output definition from a context... We have to
   ; take it from a random grob. This uses the
   ; currentMusicalColumn.
   ;
   ; This whole business relies on uniform stretching
   ; and proportional notation.
   ;
   ; TODO: what if there are non-musical columns taking
   ; space?
   (let ((processing-done #f)
         (layout #f)
         (new-indent-fraction #f))
     (make-engraver
       ; Must do this processing in stop-translation-timestep,
       ; because start-translation-timestep may not be run
       ; in start of piece in recent versions, and initialize
       ; may happen before the measure position has had a chance
       ; to be set.
       ((stop-translation-timestep engraver)
          (if (not processing-done)
              (begin
                (set! processing-done #t)
                (let* ((layout (ly:grob-layout
                                 (ly:context-property context 'currentMusicalColumn)))
                       (line-width (ly:output-def-lookup layout 'line-width))
                       (measure-position (ly:moment-main
                                           (ly:context-property context 'measurePosition)))
                       (measure-length (ly:moment-main
                                         (ly:context-property context 'measureLength)))
                       (music-fraction (/ (- measure-position)
                                          measure-length))
                       (empty-fraction (- 1 music-fraction))
                       (new-indent (* empty-fraction line-width)))
                  (if (negative? measure-position)
                      (ly:output-def-set-variable! layout 'indent new-indent)))))))))

\layout {
  \context {
    \Score
    \consists #Partial_indent_engraver
  }
}
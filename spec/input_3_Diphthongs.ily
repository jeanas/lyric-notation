\version "2.22.0"

\include "headphones.ily"

#(use-modules (srfi srfi-26)
              (ice-9 regex))

%{
  It would be a nightmare to make these configurable,
  since output definitions are not accessible at music
  processing stage, and there does not seem to be a
  Scheme interface for turning a music event into a
  stream event (so we can't just ly:broadcast things
  in an engraver).
%}

stress-event = ^\markup \fontsize #7 "´"
accent-event = ^>

% NEW VOWELS definitions with
% only 3 diphthongs, not EY or OW
 #(define VOWELS
    ; Shape of an element is
    ;   (ARPABET-representation pitch [pitch2])
    ; (this actually makes an associative list).
    ; Note: lowercase is supported, too.
    ; Vowels are listed according to their vertical
    ; position on the staff from top to bottom
    ; Diphthongs are listed according to initial position
    ; blank lines separate the vowels of each staff space
    ; ER and AXR are rhotic variants of AH and AX.
    ; They are printed on the same line and have
    ; a hook printed on the notehead
    '(
      ; Space 1
      ("IY" (2 2))
      ; Space 2
      ("IH" (2 0))
      ("IHR" (2 0) (0 6))
      ; Space 3
      ("EY" (1 5))
      ; Space 4
      ("EH" (1 3))
      ("EHR" (1 3) (0 6))
      ; Space 5
      ("AE" (1 1))
      ; Space 6
      ("AH" (0 6))
      ("AX" (0 6))
      ("ER" (0 6))
      ("AXR" (0 6))
      ; Space 7
      ("AA" (0 4))
      ("AY" (0 4) (2 0))
      ("AW" (0 4) (-1 5))
      ("AAR" (0 4) (0 6))
      ; Space 8
      ("AO" (0 2))
      ("OY" (0 2) (2 0))
      ("AOR" (0 2) (0 6))
      ; Space 9
      ("OW" (0 0))
      ; Space 10
      ("UH" (-1 5))
      ("UHR" (-1 5) (0 6))
      ; Space 11
      ("UW" (-1 3))
      ))


#(define CONSONANTS
      ; Maps lists of single-letter ARPABET codes to symbols
      ; representing their groups: '(category voiced?).
   '(("Q" glottal-stop ())
     ("p" plosive #f)
     ("t" plosive #f)
     ("k" plosive #f)
     ("b" plosive #t)
     ("d" plosive #t)
     ("g" plosive #t)
     ("C" affricate #f)
     ("J" affricate #t)
     ("f" fricative #f)
     ("T" fricative #f)
     ("s" fricative #f)
     ("S" fricative #f)
     ("h" fricative #f)
     ("x" fricative #f)
     ("v" fricative #t)
     ("D" fricative #t)
     ("z" fricative #t)
     ("Z" fricative #t)
     ("m" nasal ())
     ("n" nasal ())
     ("G" nasal ())
     ("l" liquid ())
     ("r" liquid ())
     ("y" glide ())
     ("w" glide ())))


#(define lyric-regexp (make-regexp "((<>|`|@)*)([a-zA-Z]+)-([a-zA-Z]+)(-([a-zA-Z]+))?"))

#(define (get-vowel-info vowel source)
   (or (assoc-ref VOWELS vowel)
       (begin
         (ly:music-warning source
                          "Cannot find vowel '~a'"
                         vowel)
         '((0 0)))))

#(define (get-consonant-classes str source)

   "Return the consonant classes of all characters in `str`, as list.

The second parameter is a music event to warn about if a consonant
is not found."
   (map
     (lambda (consonant-char)
       (let ((consonant (string consonant-char)))
         (or
           (assoc-ref CONSONANTS consonant)
           (begin
             (ly:music-warning source
                               "Cannot find consonant info for '~a'"
                               consonant)
             '(glottal-stop #f)))))
   (string->list str)))

#(define (prepare-articulations is-stressed is-accented)
   ; Not sure how to code this properly...
   (append
     (if is-stressed
         (list stress-event)
         '())
     (if is-accented
         (list accent-event)
         '())))

#(define (chord-from-lyric-text one-line lyric-event)
   "Return music for the text of the given lyric event,
for example: <>f-UW .

The previous event is passed (#f if we are processing
the first event). It is mutated in case we need to tie
the preceding note to this one.

In this helper routine, we don't cater for _, r and R."
   (let* ((text (ly:music-property lyric-event 'text))
          (match (regexp-exec lyric-regexp text))
          (modifiers (match:substring match 1))
          (onset (match:substring match 3))
          (vowel (match:substring match 4))
          ; Support lowercase input. Keys in the alist
          ; above are uppercase.
          (vowel (string-upcase! vowel))
          (coda (match:substring match 6))
          (rhotacized-through-vowel (equal? vowel "ER"))
          (rhotacized-through-coda (and coda
                                        (equal? (substring coda 0 1)
                                                "r")))
          (is-rhotacized (or rhotacized-through-vowel
                             rhotacized-through-coda))
          (vowel (if rhotacized-through-coda
                     (string-append vowel "R")
                     vowel))
          ; match:substring returns #f in case there is
          ; no onset, but we want the empty string
          ; (although #f is convenient for is-rhotacized
          ; above).
          (coda (or coda ""))
          (coda (if rhotacized-through-vowel
                    (string-append "r" coda)
                    coda))
          (is-stressed (string-contains modifiers "`"))
          (is-accented (string-contains modifiers "@"))
          (is-alt (string-contains modifiers "<>"))
          (pitches (get-vowel-info vowel lyric-event))
          (pitches (map (cute apply ly:make-pitch <>) pitches))
          (onset-classes (get-consonant-classes onset lyric-event))
          (coda-classes (get-consonant-classes coda lyric-event))
          (duration (ly:music-property lyric-event 'duration))
          (articulations (ly:music-property lyric-event 'articulations))
          (phantom-note (make-music 'NoteEvent
                                     lyric-event
                                     'duration duration
                                     ; Dummy pitch; the staff position is set
                                     ; later on based on is-phantom.
                                     'pitch (ly:make-pitch 0 0)
                                     'is-stressed #f
                                     'is-accented #f
                                     'is-centered #f
                                     'is-phantom #t))
          (chord
            (case (length pitches)
              ((1)
               (make-music 'EventChord
                           'elements
                           (list
                             (make-music 'NoteEvent
                                         lyric-event
                                         'pitch (first pitches)
                                         'duration duration
                                         'is-alt is-alt
                                         'is-rhotacized is-rhotacized
                                         'onset-classes onset-classes
                                         'coda-classes coda-classes
                                         'is-stressed is-stressed
                                         'is-accented is-accented
                                         'is-centered #f
                                         'is-phantom #f))
                           'articulations articulations))
              ((2)
               (make-music 'EventChord
                           'elements
                           (list
                             (make-music 'NoteEvent
                                         lyric-event
                                         'pitch (first pitches)
                                         'duration duration
                                         'is-alt is-alt
                                         'is-rhotacized #f
                                         'onset-classes onset-classes
                                         'coda-classes '()
                                         'is-stressed is-stressed
                                         'is-accented is-accented
                                         'is-centered #f
                                         'is-phantom #f)
                             (make-music 'NoteEvent
                                         lyric-event
                                         'pitch (second pitches)
                                         'duration duration
                                         'is-alt #f ; TODO: check
                                         'is-rhotacized is-rhotacized
                                         'onset-classes '()
                                         'coda-classes coda-classes
                                         'is-stressed is-stressed
                                         'is-accented is-accented
                                         'is-centered #t
                                         'is-phantom #f))
                           'articulations articulations)))))
     (ly:music-set-property!
       chord
       'elements
       (append (if one-line
                   '()
                   (list phantom-note))
               (ly:music-property chord 'elements)
               (prepare-articulations is-stressed is-accented)))
     chord))


#(define (music-from-lyric-text one-line lyric-event previous-chord)
   (let ((text (ly:music-property lyric-event 'text))
         (duration (ly:music-property lyric-event 'duration)))
     (cond
       ((equal? text " ") ; '_' in the input
        (if previous-chord
            (let ((clone (ly:music-deep-copy previous-chord)))
              (ly:music-set-property!
                previous-chord
                'articulations
                (cons
                  (make-music 'TieEvent)
                  (ly:music-property
                    previous-chord
                    'articulations)))
              clone)
            (begin
              (ly:music-warning lyric-event
                                "No previous note to tie with")
              (make-music 'RestEvent
                          'duration duration))))
       ((equal? text "r")
        (make-music 'RestEvent
                    'duration duration))
       ((equal? text "R")
        (make-music 'MultiMeasureRestMusic
                    'duration duration))
       (else
         (chord-from-lyric-text one-line lyric-event)))))

#(define (music-from-lyrics one-line event-list)
   (let loop ((remaining-events event-list)
              (last-chord #f)
              (acc '()))
     (if (null? remaining-events)
         (reverse acc)
         (let ((next-event (car remaining-events))
               (rest (cdr remaining-events)))
           (if (music-is-of-type? next-event 'lyric-event)
             ; Got a lyric event. Process it and use the
             ; associated chord as a last chord event in
             ; the future, in case the next chord needs
             ; a tie.
             (let ((new-chord
                     (music-from-lyric-text one-line
                                            next-event
                                            last-chord)))
               (loop rest
                     new-chord
                     (cons new-chord acc)))
             ; Got something else -- maybe a bar check or
             ; a \partial. We want to keep those, so put
             ; it in `acc`, but keep the current last chord.
             (loop rest
                   last-chord
                   (cons next-event acc)))))))

#(define (lyric-notation-input one-line input)
   ; Assume SequentialMusic.
   (make-music
     'SequentialMusic
     'elements
     (music-from-lyrics
         one-line
         (ly:music-property input 'elements))))

% Testing
%{
\void \displayMusic #(lyric-notation-input #{ \lyricmode { r16 n-aw16 `w-eyr-rd <>@y-uw } #})
%}

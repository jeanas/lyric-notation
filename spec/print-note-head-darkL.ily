\version "2.22.0"

\include "headphones.ily"

% Draw note heads with headphones and optional rhotacization.

#(use-modules (srfi srfi-26))

#(define DEFAULT-HEADPHONE-COLORS
   '((glottal-stop (102 51 153))
     (plosive (255 51 51))
     (affricate (255 51 51) (255 255 0)) ; makes up two headphones
     (fricative (255 255 0))
     (nasal (0 255 0))
     (liquid (0 153 255))
     (glide (0 0 255))))

\layout {
  headphone-colors = #DEFAULT-HEADPHONE-COLORS
}

#(define (make-color lst)
   (apply rgb-color
          (map (cute / <> 255)
            lst)))


#(define (get-class-colors grob class)
   (or
     (assq-ref
       (ly:output-def-lookup
         (ly:grob-layout grob)
         'headphone-colors)
       class)
     (begin
       (ly:warning "No colors for category ~s"
                   class)
       '((0 0 0)))))

#(define (is-pointy-headphone-class? extended-class)
   (let ((class (first extended-class))
         (voiced (second extended-class)))
     (and (not voiced)
          (memq class
                '(affricate fricative plosive)))))

#(define (is-squared-headphone-class? extended-class)
   (let ((class (first extended-class)))
     (eq? class 'glottal-stop)))


#(define (make-headphone grob extended-class color)
   ((cond
      ((is-pointy-headphone-class? extended-class)
        pointy-headphone)
      ((is-squared-headphone-class? extended-class)
       glottal-headphone)
      (else
       regular-headphone))
    grob
    (make-color color)))

#(define (prepare-headphones grob extended-classes)
   ; colors-list-list here is a list of lists of colors:
   ; when an element of colors is a list of more than one
   ; element, several headphones are created with the
   ; same category (taken from the extended class) and
   ; different colors.
   (let* ((classes (map first extended-classes))
          (color-list-list (map (cute get-class-colors grob <>)
                                classes)))
     (map make-stencil-markup
          (apply append
                 (map
                   (lambda (extended-class color-list)
                     (make-headphone grob extended-class (first color-list))
                     (map (cute make-headphone grob extended-class <>)
                          color-list))
                   extended-classes
                   color-list-list)))))

#(define (calc-note-head-stencil grob)
   (let* ((cause (event-cause grob))
          (onset-extended-classes (ly:event-property cause 'onset-classes))
          (coda-extended-classes (ly:event-property cause 'coda-classes))
          (left-headphones (prepare-headphones grob onset-extended-classes))
          (right-headphones (prepare-headphones grob coda-extended-classes))
          (is-rhotacized (ly:event-property cause 'is-rhotacized))
          (is-velarized (ly:event-property cause 'is-velarized))
          (is-alt (ly:event-property cause 'is-alt))
          (note-head
            (if is-alt
                (diamond-note-head grob)
                (note-head-expanded grob)))
          (liquids
            (cond
              (is-rhotacized
                (ly:grob-property grob 'hook-markup))
              (is-velarized
                (ly:grob-property grob 'darkL-markup))
              (else empty-markup)))
          (staff-symbol (ly:grob-object grob 'staff-symbol))
          (factor (- (ly:staff-symbol-staff-space staff-symbol)
                     (* 2 (ly:staff-symbol-line-thickness staff-symbol)))))
     (ly:stencil-scale
       (stack-headphones grob
         (make-combine-markup (make-stencil-markup note-head)
                              liquids)
         left-headphones
         right-headphones)
       factor
       factor)))


#(set-object-property! 'hook-markup 'backend-type? markup?)
#(set-object-property! 'darkL-markup 'backend-type? markup?)


\layout {
  \context {
    \Voice
    \override NoteHead.stencil = #calc-note-head-stencil
    \override Stem.layer = -1
    \override NoteHead.hook-markup =
      \markup
        \center-align
          \vcenter
            %%% \with-color #green \box
            \with-color #white
              % DEPRICATED: hook glyph in CharisSIL is not centered.
              % Toggle \with-color #green \box above to adjust extents.
              %%% \with-dimensions #'(-0.07 . 0.15) #'(0.78 . 0.81)
              %%% "˞"
              \fontsize #-5
              "ɹ"
    \override NoteHead.darkL-markup =
      \markup
        \center-align
          \vcenter
            %%% \with-color #green \box
            \with-color #white
              % DEPRICATED: hook glyph in CharisSIL is not centered.
              % Toggle \with-color #green \box above to adjust extents.
              %%% \with-dimensions #'(-0.07 . 0.15) #'(0.78 . 0.81)
              %%% "˞"
              \fontsize #-5
              "ɫ"
  }
  \context {
    \Score
    \override Stem.layer = -1
    \override StaffSymbol.layer = -2
  }
}

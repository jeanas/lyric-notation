\version "2.22.0"

\include "define-grob.ily"
\include "define-event.ily"
\include "prepend.ily"

#(use-modules (srfi srfi-26))

#(set-object-property! 'vertical-tip-length 'backend-type? number?)
#(set-object-property! 'horizontal-tip-length 'backend-type? number?)
#(set-object-property! 'ignore-in-column-extent 'backend-type? boolean?)
#(set-object-property! 'with-analysis-number-x-padding 'backend-type? number?)
#(set-object-property! 'without-analysis-number-x-padding 'backend-type? number?)


#(define-event-class 'angle-bracket-event 'music-event)

#(define-event!
   'AngleBracketEvent
   '((description . "Draw an angle bracket on this note.")
     (types . (post-event angle-bracket-event event))))

#(define (angle-bracket::text grob)
   (let* ((vertical-tip-length (ly:grob-property grob 'vertical-tip-length))
          (horizontal-tip-length (ly:grob-property grob 'horizontal-tip-length))
          (vertical-tip
            (make-draw-line-markup
              (cons 0
                    (- vertical-tip-length))))
          (horizontal-tip
             (make-draw-line-markup
               (cons (- horizontal-tip-length)
                     0))))
     ; The extent of AngleBracket is only that of its
     ; horizontal part, to allow the side-position-interface
     ; to let the vertical part hang on the right of the beam.
     (make-with-outline-markup
       horizontal-tip
       (make-combine-markup
         horizontal-tip
         vertical-tip))))

#(define (is-visible-analysis-number? grob)
   (and (grob::has-interface grob 'analysis-number-interface)
        ; Cater for the analysis number 0 which is not printed.
        (not (ly:stencil-empty? (ly:grob-property grob 'stencil)))))

#(define (angle-bracket::x-offset grob)
   (let* ((highlight (ly:grob-object grob 'highlight))
          (highlight-extra-right
            (if (null? highlight)
                0
                (interval-end (ly:grob-property highlight 'box-y-extra))))
          (parent (ly:grob-parent grob X))
          (parent-extent (ly:grob-extent parent parent X))
          (parent-end (interval-end parent-extent))
          (side-support
            (ly:grob-array->list (ly:grob-object grob 'side-support-elements)))
          (has-number (any is-visible-analysis-number? side-support))
          (x-padding (ly:grob-property grob (if has-number
                                                'with-analysis-number-x-padding
                                                'without-analysis-number-x-padding))))
     (+ parent-end
        (if (not has-number)
            highlight-extra-right
            0)
        x-padding)))

#(define-grob!
   'AngleBracket
   `((direction . ,UP)
     (horizontal-tip-length . 1.5)
     (ignore-in-column-extent . #t)
     (outside-highlight . #t)
     (padding . 0.4)
     (self-alignment-X . ,LEFT)
     (side-axis . ,Y)
     (stencil . ,ly:text-interface::print)
     (text . ,angle-bracket::text)
     (thickness . 5)
     (vertical-tip-length . 1.5)
     (with-analysis-number-x-padding . 0.9)
     (without-analysis-number-x-padding . 0.75)
     (X-offset . ,angle-bracket::x-offset)
     (Y-offset . ,side-position-interface::y-aligned-side)
     (Y-extent . ,grob::always-Y-extent-from-stencil)
     (meta . ((class . Item)
              (interfaces . (outside-staff-interface
                             text-interface
                             text-script-interface))))))


#(define (add-y-support! bracket grob)
   (ly:pointer-group-interface::add-grob bracket 'side-support-elements grob))


#(define (Angle_bracket_engraver context)
   (let ((angle-bracket-event #f)
         (angle-bracket #f)
         (last-angle-bracket #f)
         (ending-highlight #f)
         (text-scripts '()))
     
     (make-engraver
       (listeners
         ((angle-bracket-event engraver event)
            (set! angle-bracket-event event)))
       
       ((process-music engraver)
          (if angle-bracket-event
              (set! angle-bracket
                    (ly:engraver-make-grob engraver 'AngleBracket angle-bracket-event))))
       
       (acknowledgers
         ((text-script-interface engraver grob source-engraver)
            (prepend! grob text-scripts)))
       
       (end-acknowledgers
         ((highlight-interface engraver grob source-engraver)
            (set! ending-highlight grob)))
       
       ((stop-translation-timestep engraver)
          (if (and last-angle-bracket ending-highlight)
              (begin
                (ly:grob-set-object! last-angle-bracket 'highlight ending-highlight)
                (add-y-support! last-angle-bracket ending-highlight)))
          (if angle-bracket
              (for-each (cute add-y-support! angle-bracket <>)
                        text-scripts))
          (set! angle-bracket-event #f)
          (set! last-angle-bracket angle-bracket)
          (set! angle-bracket #f)
          (set! text-scripts '())
          (set! ending-highlight #f)))))



#(define (account-for-me? grob)
   (null? (ly:grob-property grob 'ignore-in-column-extent)))

#(define (paper-column-selective-X-extent grob)
   (let* ((elements-array (ly:grob-object grob 'elements))
          (elements-list (ly:grob-array->list elements-array)))
     (for-each
       (lambda (elt)
         (if (not (and (grob::has-interface elt 'stem-interface)
                       (ly:grob-property elt 'cross-staff)))
             (ly:grob-property grob 'vertical-skylines)))
       elements-list)
     (let* ((common (ly:grob-common-refpoint-of-array grob elements-array X))
            (my-coord (ly:grob-relative-coordinate grob common X))
            (filtered-grobs (filter account-for-me? elements-list))
            (extents (map (cute ly:grob-extent <> common X)
                          filtered-grobs))
            (non-empty-extents (filter (lambda (interval)
                                         (not (interval-empty? interval)))
                                       extents))
            (r (reduce interval-union empty-interval non-empty-extents)))
       (cons (- (car r)
                my-coord)
             (- (cdr r)
                my-coord)))))

\layout {
  \context {
    \Global
    \grobdescriptions #all-grob-descriptions
  }
  \context {
    \Score
    \consists #Angle_bracket_engraver
    \override PaperColumn.X-extent = #paper-column-selective-X-extent
  }
}
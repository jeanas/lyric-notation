\version "2.22.0"

#(define (note-head-x-offset grob)
   (let* ((cause (event-cause grob))
          (is-centered (ly:event-property cause 'is-centered))
          (layout (ly:grob-layout grob))
          (one-line (ly:output-def-lookup layout 'one-line-staff))
          (extent (ly:grob-extent grob grob X))
          (stem (ly:grob-object grob 'stem))
          (staff-symbol (ly:grob-object grob 'staff-symbol))
          (line-thickness (ly:staff-symbol-line-thickness staff-symbol))
          (stem-thickness (* line-thickness
                             (ly:grob-property stem 'thickness))))
     ; TODO: use stem-thickness to correct.
     (cond
       ((and one-line is-centered)
        ; Diphthong and one-line setting. Put note head
        ; on the right of the stem, to avoid collision
        ; since the two note heads are on the same vertical
        ; position.
        (- (car extent)))
       (is-centered
        ; Centered and normal setting.
        0)
       (else
        ; Put on left of stem.
        (- (cdr extent))))))

\layout {
  \context {
    \Voice
    \stemUp
    % Don't distribute note heads around the stem
    % when they are close -- we're doing the alignment
    % ourselves.
    \override Stem.positioning-done = ##t
    % Prevent stem from translating itself according to
    % note heads.
    \override Stem.X-offset = 0
    \override NoteHead.X-offset = #note-head-x-offset
  }
}
\version "2.22.0"

% Horizontal brackets require note columns, so they can only be created
% in Voice or upper contexts, not Lyrics.  We want them below the lyrics,
% so we just add these as support to the horizontal brackets for side
% positioning.

\include "prepend.ily"

#(define (Horizontal_bracket_lyric_support_engraver context)
   (let ((brackets '())
         (lyric-text #f))
     ; Uh-oh. The Horizontal_bracket_engraver does not announce the end
     ; of its grobs. Anyway, it's not the only quadratic behavior in
     ; this code...
     (make-engraver
       (acknowledgers
         ((horizontal-bracket-interface engraver grob source-engraver)
            (prepend! grob brackets))
         ((lyric-syllable-interface engraver grob source-engraver)
            (set! lyric-text grob)))
       ((stop-translation-timestep engraver)
          (if lyric-text
              (for-each
                (cute ly:pointer-group-interface::add-grob
                      <>
                      'side-support-elements
                      lyric-text)
                brackets))
          (set! lyric-text #f)))))

\layout {
  \context {
    \Score
    \consists Horizontal_bracket_engraver
    \consists #Horizontal_bracket_lyric_support_engraver
    \override HorizontalBracketText.stencil =
      #(lambda (grob)
         (let ((text (ly:grob-property grob 'text)))
           (if (markup? text)
               (grob-interpret-markup grob text)
               (ly:grob-suicide! grob))))}
  }
